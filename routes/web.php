<?php

/**
 * Global Routes
 * Routes that are used between both frontend and backend.
 */

// Switch between the included languages

Route::group(['middleware' => ['backend-view']], function () {

    Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\controllers\UploadController@upload')->name('unisharp.lfm.upload');
    Route::group(['prefix' => 'laravel-filemanager','as'=>'filemanager'], function () {

        \UniSharp\LaravelFilemanager\Lfm::routes();

    });

});


Route::get('/{lang}', function ($lang){
    App\Langs\Lang::set($lang);
    return redirect()->back();
})->where(['lang'=>'ru|en']);

Route::get('lang/{lang}', 'LanguageController');



/*
 * Backend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['backend-view']], function () {
    /*
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     * These routes can not be hit if the password is expired
     */
    include_route_files(__DIR__.'/backend/');
});

/*
 * Frontend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
    include_route_files(__DIR__ . '/frontend/');
});
