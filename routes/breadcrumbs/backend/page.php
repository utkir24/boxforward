<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */


    Breadcrumbs::for('admin.page.index', function ($trail) {
        $trail->parent('admin.dashboard');
        $trail->push(__('page.backend.label.management'), route('admin.page.index'));
    });

    Breadcrumbs::for('admin.page.create', function ($trail) {
        $trail->parent('admin.page.index');
        $trail->push(__('page.backend.menu.create'), route('admin.page.create'));
    });

    Breadcrumbs::for('admin.page.show', function ($trail, $id) {
        $trail->parent('admin.page.index');
        $trail->push(__('page.backend.menu.view'), route('admin.page.show', $id));
    });

    Breadcrumbs::for('admin.page.edit', function ($trail, $id) {
        $trail->parent('admin.page.index');
        $trail->push(__('page.backend.menu.edit'), route('admin.page.edit', $id));
    });
    Breadcrumbs::for('admin.page.type', function ($trail, $type) {
        $trail->parent('admin.page.index');
        $trail->push(__('page.backend.menu.type'), route('admin.page.type', $type));
    });


