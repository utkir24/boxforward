<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */


    Breadcrumbs::for('admin.faq.index', function ($trail) {
        $trail->parent('admin.dashboard');
        $trail->push(__('faq.backend.label.management'), route('admin.faq.index'));
    });

    Breadcrumbs::for('admin.faq.create', function ($trail) {
        $trail->parent('admin.faq.index');
        $trail->push(__('faq.backend.menu.create'), route('admin.faq.create'));
    });

    Breadcrumbs::for('admin.faq.show', function ($trail, $id) {
        $trail->parent('admin.faq.index');
        $trail->push(__('faq.backend.menu.view'), route('admin.faq.show', $id));
    });

    Breadcrumbs::for('admin.faq.edit', function ($trail, $id) {
        $trail->parent('admin.faq.index');
        $trail->push(__('faq.backend.menu.edit'), route('admin.faq.edit', $id));
    });


