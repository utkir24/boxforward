<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */


Breadcrumbs::for('admin.shops.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('shops.backend.label.management'), route('admin.shops.index'));
});

Breadcrumbs::for('admin.shops.create', function ($trail) {
    $trail->parent('admin.shops.index');
    $trail->push(__('shops.backend.menu.create'), route('admin.shops.create'));
});

Breadcrumbs::for('admin.shops.show', function ($trail, $id) {
    $trail->parent('admin.shops.index');
    $trail->push(__('shops.backend.menu.view'), route('admin.shops.show', $id));
});

Breadcrumbs::for('admin.shops.edit', function ($trail, $id) {
    $trail->parent('admin.shops.index');
    $trail->push(__('shops.backend.menu.edit'), route('admin.shops.edit', $id));
});


