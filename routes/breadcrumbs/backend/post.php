<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */


    Breadcrumbs::for('admin.post.index', function ($trail) {
        $trail->parent('admin.dashboard');
        $trail->push(__('post.backend.label.management'), route('admin.post.index'));
    });

    Breadcrumbs::for('admin.post.create', function ($trail) {
        $trail->parent('admin.post.index');
        $trail->push(__('post.backend.menu.create'), route('admin.post.create'));
    });

    Breadcrumbs::for('admin.post.show', function ($trail, $id) {
        $trail->parent('admin.post.index');
        $trail->push(__('post.backend.menu.view'), route('admin.post.show', $id));
    });

    Breadcrumbs::for('admin.post.edit', function ($trail, $id) {
        $trail->parent('admin.post.index');
        $trail->push(__('post.backend.menu.edit'), route('admin.post.edit', $id));
    });
    Breadcrumbs::for('admin.post.type', function ($trail, $type) {
        $trail->parent('admin.post.index');
        $trail->push(__('post.backend.menu.type'), route('admin.post.type', $type));
    });


