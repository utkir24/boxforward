<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
require __DIR__.'/faq.php';
require __DIR__.'/page.php';
require __DIR__.'/post.php';
require __DIR__.'/category.php';
require __DIR__.'/settings.php';
require __DIR__.'/menu.php';
