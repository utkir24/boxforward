<?php
Breadcrumbs::for('admin.settings.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('settings.backend.label.management'), route('admin.settings.index'));
});

Breadcrumbs::for('admin.settings.create', function ($trail) {
    $trail->parent('admin.settings.index');
    $trail->push(__('settings.backend.menu.create'), route('admin.settings.create'));
});

Breadcrumbs::for('admin.settings.show', function ($trail, $id) {
    $trail->parent('admin.settings.index');
    $trail->push(__('settings.backend.menu.view'), route('admin.settings.show', $id));
});

Breadcrumbs::for('admin.settings.edit', function ($trail, $id) {
    $trail->parent('admin.settings.index');
    $trail->push(__('settings.backend.menu.edit'), route('admin.settings.edit', $id));
});
