<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */


Breadcrumbs::for('admin.menu.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('menu.backend.label.management'), route('admin.menu.index'));
});

Breadcrumbs::for('admin.menu.create', function ($trail) {
    $trail->parent('admin.menu.index');
    $trail->push(__('menu.backend.menu.create'), route('admin.menu.create'));
});

Breadcrumbs::for('admin.menu.show', function ($trail, $id) {
    $trail->parent('admin.menu.index');
    $trail->push(__('menu.backend.menu.view'), route('admin.menu.show', $id));
});

Breadcrumbs::for('admin.menu.edit', function ($trail, $id) {
    $trail->parent('admin.menu.index');
    $trail->push(__('menu.backend.menu.edit'), route('admin.menu.edit', $id));
});


