<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */


    Breadcrumbs::for('admin.category.index', function ($trail) {
        $trail->parent('admin.dashboard');
        $trail->push(__('category.backend.label.management'), route('admin.category.index'));
    });

    Breadcrumbs::for('admin.category.create', function ($trail) {
        $trail->parent('admin.category.index');
        $trail->push(__('category.backend.menu.create'), route('admin.category.create'));
    });

    Breadcrumbs::for('admin.category.show', function ($trail, $id) {
        $trail->parent('admin.category.index');
        $trail->push(__('category.backend.menu.view'), route('admin.category.show', $id));
    });

    Breadcrumbs::for('admin.category.edit', function ($trail, $id) {
        $trail->parent('admin.category.index');
        $trail->push(__('category.backend.menu.edit'), route('admin.category.edit', $id));
    });

 Breadcrumbs::for('admin.category.type', function ($trail, $type) {
    $trail->parent('admin.category.index');
    $trail->push(__('category.backend.menu.type'), route('admin.category.type', $type));
});


