<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

/**
 * All route names are prefixed with 'admin.category'.
 */

Route::resource('menu', 'Menu\MenuController');
Route::post("menu/ajax",'Menu\MenuController@ajax')->name('menu.ajax');
Route::get("menu/type/{type}",['uses'=>'Menu\MenuController@type'])->name('menu.type');
Route::get("menu/{id}/destroy",['uses'=>'Menu\MenuController@destroy'])->name('menu.destroy');

