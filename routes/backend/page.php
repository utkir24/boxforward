<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

/**
 * All route names are prefixed with 'admin.page'.
 */

Route::resource('page', 'Page\PageController');
Route::get("page/type/{type}",['uses'=>'Page\PageController@type'])->name('page.type');
Route::resource('settings', 'Settings\SettingsController');
Route::get('admin/settings' . '/download/{setting}', 'Settings\SettingsController@fileDownload')->name('settings.download');

Route::resource('post', 'Post\PostController');
Route::get("post/type/{type}",['uses'=>'Post\PostController@type'])->name('post.type');