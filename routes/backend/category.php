<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

/**
 * All route names are prefixed with 'admin.category'.
 */

Route::resource('category', 'Category\CategoryController');
Route::post("category/ajax",'Category\CategoryController@ajax')->name('category.ajax');
Route::get("category/type/{type}",['uses'=>'Category\CategoryController@type'])->name('category.type');
Route::get("category/{id}/destroy",['uses'=>'Category\CategoryController@destroy'])->name('category.destroy');

