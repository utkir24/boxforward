<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', 'DashboardController@index')->name('dashboard');
Route::get('lang/{lang}', 'LanguageController')->name('lang');
Route::resource('widget', 'Widget\WidgetController');
Route::get('/widget/notifications/{id}', function ($id){

    $widget = App\Models\Widget\Widget::where('widget_id',$id)->first();

    if($widget) {

        $notifications = $widget->notifications->map(function ($item, $key) {

            $images = $item->images->map(function ($img, $imgsKey) {
                return $img->image;
            })->reject(function ($img) {
                return !empty($img->image);
            })->all();

            $children = $item->children->map(function ($child, $childKey) {
                $images = $child->images->map(function ($img, $imgsKey) {
                    return $img->image;
                })->reject(function ($img) {
                    return !empty($img->image);
                })->all();
                return [
                    'id'=>$child->notification_id,
                    'title' => $child->title ?? '',
                    'description' => $child->description ??'',
                    'content' => $child->content??'',
                    'image_list'=>$images,
                    'images' =>$images ? implode(',',$images) : '',

                ];
            })->all();

            $extraFields = $item->fieldsExtra->map(function ($child, $childKey){
                $fields = [];
                if(is_array($child->data)){
                    foreach ($child->data as $name =>$value){
                        array_push($fields,['name'=>$name, 'value'=>$value]);
                    }
                }
                return $fields;
            })->all();
            return [
                'id'=>$item->notification_id,
                'title' => $item->title??'',
                'description' => $item->description??'',
                'content' => $item->content??'',
                'children' => $children ? $children : [],
                'image_list'=>$images,
                'images' =>$images ? implode(',',$images) : '',
                'extraFields'=>count($extraFields) ? $extraFields[0] : []
            ];

        })->all();

        return $notifications;
    }

    return [];

})->name('widget.notifications');

Route::get('/widget/categories/{id}', function ($id){

    $widget = App\Models\Widget\Widget::where('widget_id',$id)->first();



    if($widget) {
        $catWidgets = $widget->widgetCategories->map(function ($item, $key) {

            return [
                'category_id' => $item->category_id,
                'count' => $item->count,
                'selected' => $item->category_id,
            ];

        })->all();

        return [
          'items' => $catWidgets,
          'list' =>App\Models\Category\Category::getDropdownList(),
        ];

    }

    return [
        'items' =>[],
        'list' =>App\Models\Category\Category::getDropdownList(),
    ];

})->name('widget.categories');