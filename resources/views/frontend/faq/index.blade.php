@extends('frontend.layouts.box')

@section('title', app_name() . ' | '.__('navs.general.home'))
@section('content')

    {{$content = ''}}
    {{$list = ''}}
    @php(

      $faqIds = collect($faqs->items())->map(function ($faq) {
            return $faq->id;
        })->all()


    )
    @foreach($categories as $index => $category)
        @php(
           $category_class = $index == 0 ? 'active' : ''
        )
        @php(
          $list .="<div class='faq-tab-item $category_class'>
                                <h3 class='__title'>
                                    <a class='faq-tab-item-link js-faq-link' href='#block$category->id'>$category->title</a>
                                </h3>
                                <ul class='__list'>"
        )
        @foreach($category->faqs as $index => $faq)
            @if(in_array($faq->id,$faqIds))
                @php(
                     $faq_class = $index == 0 ? 'active' : ''
                )
                @php( $list.="<li class='$faq_class'>
                                   <a class='js-faq-sublink' href='#block$faq->id'>
                                       $faq->title
                                   </a>
                               </li>"
                  )
            @endif
        @endforeach

        @php(
        $list.="</ul></div>".PHP_EOL
        )

        @foreach ($category->faqs as $index => $faq)
            @php(
                   $content_class = $index == 0 ? 'active' : ''
              )
            @php(
            $content .="<div class='faq-page-main_tabs_position_content $content_class' id='block$category->id'>
                                    <div class='faq-page-main_tabs_position_content_title' id='block$faq->id'>
                                        <h2 class='__title'>
                                            $faq->title
                                        </h2>

                                    </div>
                                    <div class='faq-page-main_tabs_position_content_main'>
                                       $faq->content
                                    </div>
                                </div>"

            )
        @endforeach
    @endforeach



    <section class="faq-page-main">
        <div class="container">
            <div class="faq-page-main_tabs">
                <div class="faq-page-main_tabs_position">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                            <div class="faq-page-main_tabs_positions_tabs">
                                {!! $content !!}
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <div class="faq-page-main_tabs_position_tabs">
                                {!! $list !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

