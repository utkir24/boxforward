@extends('frontend.layouts.box')

@section('title', app_name() . ' | '.__('navs.general.home'))
@section('content')
<section class="faq-page-main">
    <div class="container">
        <div class="faq-page-main_tabs">
            <div class="faq-page-main_tabs_position">
                <div class="row">

                    <div class="col-md-8 col-sm-8">
                        <div class="faq-page-main_tabs_positions_tabs">
                            @foreach($categories as $indexCategory => $category)
                                <div class='faq-page-main_tabs_position_content {{ $indexCategory == 0 ? 'active' : '' }}' id='block{{$category['id']}}'>
                                        <div class='faq-page-main_tabs_position_content_title' id='block{{$category['id']}}'>
                                            <h3 class="__title-main title-faq">
                                                {{$category['title']}}
                                            </h3>
                                        </div>
                                        <div class='faq-page-main_tabs_position_content_main'>
                                            @if(array_key_exists('children',$category))
                                                @foreach($category['children'] as $indexChild => $child)
                                                    @foreach(App\Models\Category\Category::find($child['id'])->faqs as $faq)
                                                        <h4 class="__title-sm title-faq-items" id='block{{$child['id']}}'>
                                                            {{$faq->title}}
                                                        </h4>
                                                        <p class="__content">
                                                            {!! $faq->content !!}
                                                        </p>
                                                    @endforeach
                                                @endforeach
                                            @endif
                                        </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="faq-page-main_tabs_position_tabs">
                            @foreach($categories as $index => $category)
                                <div class='faq-tab-item {{$index == 0 ? 'active' : ''}}'>
                                    <h3 class='__title'>
                                        <a class='faq-tab-item-link js-faq-link' href='#block{{$category['id']}}'>{{$category['title']}}</a>
                                    </h3>
                                @if(array_key_exists('children',$category))
                                            <ul class='__list'>
                                                @foreach($category['children'] as $index => $cat)
                                                    <li class='{{$index == 0 ? 'active' : ''}}'>
                                                        <a class='js-faq-sublink' href='#block{{$cat['id']}}'>
                                                            {{$cat['title']}}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

