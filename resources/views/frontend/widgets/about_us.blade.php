@if($widget)
<section class="home-about-us">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="home-about-us_content">
                    <h2 class="__title">
                        {{$widget->title}}
                    </h2>
                    <p class="__content">
                        {!! $widget->description !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
@endif