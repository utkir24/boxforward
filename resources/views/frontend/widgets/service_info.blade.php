@if($widget->notifications)
    <section class="home-service-info">
        <div class="container">
            <div class="row">
                @foreach($widget->notifications as $item => $value)
                    <div class="col-sm-4">
                        <div class="home-service-info_position">
                            <h3 class="__title">{{$value->title}}</h3>
                            <div class="__describe">
                                {!! $value->content !!}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endif