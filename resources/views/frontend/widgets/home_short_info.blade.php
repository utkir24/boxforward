@if($widget->notifications)
<div class="home-short-info_describe_steps">
    <div class="home-short-info_describe_steps_container">
        @foreach($widget->notifications as $item => $value)
         <div class="home-short-info_describe_steps_container_position">
            <div class="home-short-info_describe_steps_container_position_img">
                @foreach($value->images as $indexImage => $image)
                   <img src="{{$image->image}}" alt="{{$value->title}}" class="img-responsive">
                @endforeach
            </div>
            <div class="home-short-info_describe_steps_container_position_content">
                <h3 class="__title">
                    {{$value->title}}
                </h3>
                <div class="__content">
                    {!! $value->content !!}
                </div>
                <button type="button"  data-toggle-open="{{__('widget.frontend.widget.text.read more')}}" data-toggle-close="{{__('widget.frontend.widget.text.close')}}" class="__link toggle-button-home">{{__('widget.frontend.widget.text.read more')}}...</button>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endif
@section('homeShortjs')
<script type="text/javascript">
 $(function () {
     $('button.toggle-button-home').on('click',function (e) {
         if($(this).closest('.home-short-info_describe_steps_container_position_content').find('.__content').hasClass('show')){
             $(this).html($(this).data('toggle-close'))
         }else{
             $(this).html($(this).data('toggle-open')+'...')
         }
     });
 });

</script>
@stop
