@if($widget->notifications)
<section class="home-payments-method">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="home-payments-method_title">
                    <h2 class="__title">
                        {{$widget->title}}
                    </h2>
                </div>
            </div>
            <div class="col-md-12">
                <div class="home-payments-method_container">
                    <div class="row">
                        @foreach($widget->notifications as $item => $value)
                          <div class="col-sm-4">
                            <div class="home-payments-method_position">
                                <div class="home-payments-method_position_img">
                                    @foreach($value->images as $indexImage => $image)
                                        <img src="{{$image->image}}" srcset="img/qiwi@2x.png 2x, img/qiwi@3x.png 3x" class="img-responsive">
                                    @endforeach
                                </div>
                                <div class="home-payments-method_position_describe">
                                    <div class="__content">
                                        {!! $value->content !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
{{--<div class="col-sm-4">--}}
    {{--<div class="home-payments-method_position">--}}
        {{--<div class="home-payments-method_position_img">--}}
            {{--<img src="img/visa.png" srcset="img/visa@2x.png 2x, img/visa@3x.png 3x" class="img-responsive __visa">--}}
            {{--<img src="img/mc.png" srcset="img/mc@2x.png 2x, img/mc@3x.png 3x" class="img-responsive __mc">--}}
        {{--</div>--}}
        {{--<div class="home-payments-method_position_describe">--}}
            {{--<p class="__content">--}}
                {{--We accept credit card payments using latest security standards, such as 3D Secure--}}
            {{--</p>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}