<section class="home-start-shopping">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="home-start-shopping_title">
                    <h2 class="__title">
                        {{$widget->title}}
                    </h2>
                    <a href="/signup" class="__btn">
                       {{$widget->description}}
                    </a>
                </div>
                @if($widget->notifications)
                    @foreach($widget->notifications as $notification)
                        <div class="home-start-shopping_calculate">
                            <h3 class="__title">
                               {{$notification->title}}
                            </h3>
                            <span class="__btn">
                                    {!! $notification->content !!}
                            </span>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</section>
