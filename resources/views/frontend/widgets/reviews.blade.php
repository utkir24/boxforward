@if($widget->notifications)
<section class="home-reviews">
    <div class="container">
        <div class="row">
            @foreach($widget->notifications as $item =>$value)
                <div class="col-md-4">
                    <div class="home-reviews_position">
                        <div class="__like">
                            @foreach($value->images as $indexImage => $image)
                                 <img src="{{$image->image}}" srcset="img/icon@2x.png 2x, img/icon@3x.png 3x" >
                            @endforeach
                        </div>
                        <div class="__content">
                            <h4 class="__title">
                                {{$value->title}}
                            </h4>
                            <div class="__describe">
                                {!! $value->content !!}
                            </div>
                            <p class="__info">
                                <span class="__date">{{$value->created_at}}</span>
                                <span class="__package-number">{{$value->description}}</span>
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
@endif