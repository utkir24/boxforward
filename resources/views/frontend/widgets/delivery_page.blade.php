@if($widget->notifications)
    <section class="delivery-page_content"->
    <div class="delivery-page_content_container">
        <div class="delivery-page_content_list">
            <ul class="__list">
                @foreach($widget->notifications as $item => $notification)
                    @if($item === 0)
                        <li class="active">
                            {{$notification->title}}
                        </li>
                    @else
                        <li>
                            {{$notification->title}}
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
        <div class="delivery-page_content_tabs">
            @foreach($widget->notifications as $item => $notification)
                @if($item === 0)
                    @php($class = 'active')
                @else
                    @php($class = '')
                @endif

            @php(
             $extraField=[
               '0'=>'',
               '1'=>'',
               '2'=>'',
               '3'=>''
             ]
            )

                    @php($countExtra = 0)
                @foreach($notification->fieldsExtra as $extraIndex => $extra )
                    @foreach($extra->data as $extraDataIndex => $extraData)
                         @if($countExtra > 3) @break @endif
                         @php( $extraField[$countExtra] = $extraData)
                        @php($countExtra++)
                    @endforeach
                    @break
                @endforeach
                    <div class="delivery-page_content_tabs_position {{$class}}">
                        <div class="delivery-page_content_tabs_position_info">
                            <p class="__content">
                                {{ $extraField[0]  }}
                            </p>
                            <p class="__deadlines">
                                {{ $extraField[1]  }}
                            </p>
                        </div>
                        @foreach($notification->children->chunk(3) as $child_item =>$child)
                            <div class="delivery-page_content_tabs_position_row">
                                @if($child_item === 0)
                                    <div class="delivery-page_content_tabs_position_row_col logo md">
                                        @foreach($notification->images as $noteImage)
                                         <img src="{{$noteImage->image}}" alt="logo" class="img-responsive">
                                        @endforeach
                                        <div class="__content">
                                            {{$notification->description}}
                                        </div>
                                    </div>
                                @endif
                                @foreach($child as $chil_item =>$chi)
                                     <div class="delivery-page_content_tabs_position_row_col {{$chi->description}}">
                                        <h4 class="__title">
                                            {{$chi->title}}
                                        </h4>
                                         <div class="__content">
                                             {!!$chi->content!!}
                                         </div>
                                    </div>
                               @endforeach
                            </div>
                        @endforeach

                        <div class="delivery-page_content_tabs_position_p-s">
                            <p class="__content">
                                {{ $extraField[2]  }}
                            </p>
                            <a href="/calculator" class="__link">
                                {{ $extraField[3]  }}
                            </a>
                        </div>
                    </div>
            @endforeach
        </div>
    </div>
</section>
@endif
