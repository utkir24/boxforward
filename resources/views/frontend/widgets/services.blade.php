@if($widget->notifications)
<section class="home-services">
    <div class="container">
        <div class="row">
                <div class="col-md-12">
                    <div class="home-services_title">
                        <h2 class="__title">{{$widget->title}}</h2>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="home-services_container">
                        @foreach($widget->notifications->chunk(3) as $items)
                            <div class="row">
                                @foreach($items as $item)
                                    <div class="col-sm-4">
                                        <div class="home-services_position">
                                            <h4 class="__title">
                                                {{$item->title}}
                                            </h4>
                                            <div class="__content">
                                                {!! $item->content !!}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="home-services_calculate-button">
                        <button onclick="location.href = '{{ route('frontend.calculator.index') }}';" class="home-services_calculate-button_btn">
                            {!! $widget->description !!}
                        </button>
                    </div>
                </div>
        </div>
    </div>
</section>
@endif
