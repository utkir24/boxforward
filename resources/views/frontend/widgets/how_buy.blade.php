@if($widget->notifications)
    <section class="how-buy-page_content">
        <div class="how-buy-page_content_container">
            @php(
               $countKey=1
            )
            @foreach($widget->notifications->chunk(2) as $item => $value)
                    <div class="how-buy-page_content_row">
                        @foreach($value as $key => $val)
                        <div class="how-buy-page_content_position {{\App\Widgets\Page\PageWidgets::getClass($key)}}">
                            <div class="__number">
                                {{$countKey++}}
                            </div>
                            <h3 class="__title">
                                <span class="__text">{{$val->title}}</span>
                            </h3>
                            <p class="__content __static __content-sm">
                                {{$val->description}}
                            </p>
                            <button class="__more" type="button">
                                {{__('widget.frontend.widget.text.read more')}}...
                            </button>
                            <div class="__content __content-sm">
                                {!!$val->content!!}
                            </div>
                            <button class="__close" type="button">
                                {{__('widget.frontend.widget.close')}}
                            </button>
                        </div>
                       @endforeach
                    </div>
            @endforeach
        </div>
        @if($widget->image)
            <div class="how-buy-page_content_bgi">
                <img src="{{$widget->image}}" alt="loading" class="img-responsive">
            </div>
        @endif
    </section>
@endif