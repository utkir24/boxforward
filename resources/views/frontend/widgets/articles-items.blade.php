@php($count = 3)
<section class="home-articles">
    <div class="container">
        <div class="row">
            @foreach($categories as $category)
                <div class="col-sm-4">
                    <div class="home-articles_position">
                        <div class="home-articles_position_title">
                            <h3 class="__title">
                               {{$category->title}}
                            </h3>
                        </div>
                        @if($category->pages)
                            <div class="home-articles_position_list">
                                <ul class="__list">
                                    @foreach($category->pages()->sortupdated()->active()->lang()->get() as $item => $article)
                                        <li>
                                            <a href="{{route('frontend.category.post', ['slug'=>$article->slug])}}">
                                                <div class="__container-img">
                                                    <img src="{{$article->image}}" srcset="{{$article->image}} 2x, {{$article->image}} 3x"
                                                         alt="loading">
                                                </div>
                                                <div class="__container-content">
                                                    <h5 class="__title">
                                                        {{$article->title}}
                                                    </h5>
                                                    <p class="__content">
                                                       {{$article->description}}
                                                    </p>
                                                </div>
                                            </a>
                                        </li>
                                        @if($count === $item + 1)
                                            @break
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>