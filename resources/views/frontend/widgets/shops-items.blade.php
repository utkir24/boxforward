@php($count = 6)
<section class="market-page_content" data-pjax-container id="data-pjax-container">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-md-push-9">
                <div class="market-page_content_sidebar">
                    <button class="__btn">
                        <span class="__line"></span>
                        <span class="__line"></span>
                        <span class="__line"></span>
                    </button>
                    <ul class="market-page_content_sidebar_list">
                        @if($slug === false)
                            <li class="active"  >
                                <a class="sidebar-list-item-link" data-pjax href="{{Request::fullUrlWithQuery(['category'=>null,'page'=>1])}}">{{__('shops.frontend.text.catalog')}}</a>
                            </li>
                        @else
                            <li>
                                <a class="sidebar-list-item-link" data-pjax href="{{Request::fullUrlWithQuery(['category'=>null,'page'=>1])}}">{{__('shops.frontend.text.catalog')}}</a>
                            </li>
                        @endif
                        @foreach($categories as $category)
                            @if($slug['category'] === $category->slug)
                                <li class="active">
                                    <a class="sidebar-list-item-link" data-pjax href="{{Request::fullUrlWithQuery(['category'=>$category->slug,'page'=>1])}}">{{$category->title}}</a>
                                </li>
                            @else
                                <li>
                                    <a class="sidebar-list-item-link" data-pjax href="{{Request::fullUrlWithQuery(['category'=>$category->slug,'page'=>1])}}">{{$category->title}}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="market-page_content_tabs">
                <div class="market-page_content_tabs_position active">
                    @foreach($dataProvider->chunk(12) as $key =>$item)
                            <div class="col-md-9 col-md-pull-3">
                                <div class="market-page_content_top">
                                    @foreach($item->chunk(3) as $value)
                                        <div class="row">
                                            @foreach($value as $val)
                                                <div class="col-md-4 col-sm-4">
                                                    <div class="market-page_content_position">
                                                        <div class="market-page_content_position_short">
                                                            <h4 class="__title">
                                                               {{$val->title}}
                                                            </h4>
                                                            <img src="{{$val->image}}"
                                                                 srcset="img/img-market@2x.jpg 2x, img/img-market@3x.jpg 3x"
                                                                 alt="" class="img-responsive">
                                                            <p class="__content">
                                                                {{$val->description}}

                                                            </p>
                                                        </div>
                                                        <div class="marketpage_content_position_full">
                                                            <div class="__content">
                                                                {!!  $val->content!!}
                                                            </div>
                                                            <p class="__site">
                                                                <span>{{__('shops.frontend.text.shop_site')}}</span>
                                                                <a href="{{$val->link_site}}" class="__link" target="_blank">{{$val->shop_site}}</a>
                                                            </p>
                                                            <p class="__payments">
                                                                <span>{{__('shops.frontend.text.payment_system')}}</span>
                                                                {{$val->payment_system}}
                                                            </p>
                                                            <p class="__price">
                                                                <span>{{__('shops.frontend.text.price')}}</span>
                                                                {{$val->price}}
                                                            </p>
                                                            <p class="__info">
                                                                <span>{{__('shops.frontend.text.worth_knowing')}}</span>
                                                                {{$val->worth_knowing}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                    @endforeach
                    <div class="col-md-12">
                        <div class="market-page_pagination">
                            <div class="market-page_pagination_container">
                                <ul class="market-page_pagination_list">
                                    {{$dataProvider->links()}}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .sidebar-list-item-link{
        color: #0d0d0d;
    }
    .sidebar-list-item-link:hover{
        text-decoration: none;!important;
    }
    .sidebar-list-item-link:focus{
        text-decoration: none;!important;
    }
</style>
