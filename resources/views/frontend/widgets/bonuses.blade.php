@if($widget->notifications)

<section class="bonuses-page_content">
    <div class="bonuses-page_content_container">
        @foreach($widget->notifications as $value)
        <div class="bonuses-page_content_position">
            <div class="bonuses-page_content_position_top">
                <h5 class="__title">
                    {{$value->title}}
                </h5>
                <div class="__describe">
                    {!!$value->content!!}
                </div>
            </div>
            @foreach($value->children as $item)
                 <div class="bonuses-page_content_position_list">
                <h6 class="__title">
                    {{$item->title}}
                </h6>
                <ul class="__list">
                   {!!$item->content!!}
                </ul>
            </div>
            @endforeach
            <div class="bonuses-page_content_position_bottom">
                <p class="__content">
                   {{$value->description}}
                </p>
            </div>
        </div>
        @endforeach
    </div>
    <div class="bonuses-page_content_info">
        <p class="__content">
            {!! $widget->description !!}
        </p>
    </div>
    <div class="bonuses-page_bgi">
        <img src="{{$widget->image}}" alt="loading">
    </div>
</section>
@endif