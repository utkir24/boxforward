@if($widget->notifications)
<section class="prices-bonuse">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="prices-bonuse_container">
                    <div class="prices-bonuse_container_title">
                        <h3 class="__title">
                            {{$widget->title}}
                        </h3>
                    </div>
                    <div class="row">
                        @php(
                            $count = 1
                            )
                        @foreach($widget->notifications as $value)
                            @if($count%2 === 1)
                                @php($class ='left')
                            @else
                                @php($class ='right')
                            @endif
                        @php($count++)
                            <div class="col-md-6">
                                <div class="prices-bonuse_position {{$class}}">
                                    <div class="prices-bonuse_position_top">
                                        <h4 class="__title">
                                            {{$value->title}}
                                        </h4>
                                        <div class="__describe">
                                            {{$value->description}}

                                        </div>
                                    </div>
                                    @foreach($value->children as $item)
                                        <div class="prices-bonuse_position-square">
                                            <h5 class="__title">
                                                {{$item->title}}
                                            </h5>
                                            <ul class="__list">
                                               {!! $item->content !!}
                                            </ul>
                                        </div>
                                    @endforeach
                                    <div class="prices-bonuse_position_bottom">
                                        <p class="__describe">
                                            {!! $value->content !!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="col-md-12">
                            <div class="prices-bonuse_position_info">
                                <p class="__content">
                                    {!! $widget->description !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif