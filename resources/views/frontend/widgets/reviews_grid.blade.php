@if($widget->notifications)
    <section class="why-buy_content">
        <div class="why-buy_content_container">
            @foreach($widget->notifications as $item =>$value)
              <div class="why-buy_content_position">
                <div class="why-buy_content_position_content">
                    <div class="__title">
                       {{$value->title}}
                    </div>
                    <div class="__describe">
                        {!! $value->content !!}
                    </div>
                </div>
                <div class="why-buy_content_position_img">
                    @foreach($value->images as $indexImage => $image)
                        <img src="{{$image->image}}" srcset="img/img@2x.png 2x, img/img@3x.png 3x" alt="" class="img-responsive">
                    @endforeach
                    <p class="__info">
                        {{$value->description}}
                    </p>
                </div>
            </div>
            @endforeach
        </div>
    </section>
@endif