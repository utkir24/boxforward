@if($widget->notifications)
    @php($array = $widget->notifications)
    <section class="prices-main">
        <div class="prices-main_container">
            @foreach($widget->notifications->chunk(2) as $key => $items)
                @if(count($array)===3)
                    @break
                @endif
                <div class="prices-main_positions-left-right">
                    @php(
                    $class = 'left'
                    )
                    @foreach($items as $key =>$value)
                        <div class="prices-main_positions-left-right_position {{$class}}">
                            <h4 class="__title">
                                {{$value->title}}
                            </h4>
                            <h5 class="__title-describe">
                                {{$value->description}}
                            </h5>
                            <div class="__content">
                                {!! $value->content !!}
                            </div>
                        </div>
                        @php(
                        $class ='right'
                        )
                        @if(count($array)=== 3)
                            @break
                        @endif
                        @unset($array[$key])
                    @endforeach
                </div>
            @endforeach
            @if(!empty($array))
                <div class="prices-main_positions-full">
                  @foreach($array as $value)
                    <div class="prices-main_positions-left-right_position">
                        <h4 class="__title">
                            {{$value->title}}
                        </h4>
                        <h5 class="__title-describe">
                            {{$value->description}}
                        </h5>
                        <div class="__content">
                            {!!$value->content !!}
                        </div>
                    </div>
                 @endforeach
               </div>
            @endif
            <div class="prices-main_btn-link">
                <a href="/calculator" class="__link">
                    {{$widget->description}}
                </a>
            </div>
        </div>
    </section>
@endif