@if($widget->notifications)
@php(
$count = 1
)
<section class="home-reasons">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="home-reasons_title">
                    <h2 class="__title">{{$widget->title}}</h2>
                </div>
            </div>
            <div class="col-md-12">
                <div class="home-reasons_reason-container">
                    <h4 class="__title">{{$widget->description}} </h4>
                    @foreach($widget->notifications->chunk(3) as $items)
                        <div class="row">
                            @foreach($items as $item => $value)
                                <div class="col-sm-4">
                                    <div class="home-reasons_reason_position">
                                        <span class="__number">{{$count++}}</span>
                                        <h5 class="__title">
                                            {{$value->title}}
                                        </h5>
                                        <div class="__content">
                                          {!! $value->content !!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endif
