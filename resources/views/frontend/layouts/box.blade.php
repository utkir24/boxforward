<!DOCTYPE html>
@php($helper = new \App\Helpers\Settings\SettingsHelper())
@php($loginStatus = new \App\Helpers\Auth\LoginStatus())
@langrtl
<html lang="{{ app()->getLocale() }}" dir="rtl">
@else
    <html lang="{{ app()->getLocale() }}">
@endlangrtl
<head>
    <meta charset="UTF-8">
    <title>@yield('title', app_name())</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="@yield('description')">
    @yield('meta_tags')
    @stack('before-styles')
    <link rel="shortcut icon" href="{{ asset('/box/img/favicon.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('/box/libs/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/box/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('/box/css/sidebar-faq-style.css') }}">
    @stack('after-styles')
    <link href="https://fonts.googleapis.com/css?family=Comfortaa:400,700" rel="stylesheet">

</head>
<body>
<!-- SLIDE MENUS STARTS -->
<div class="home-short-info_menu_list_container">
    <div class="__container">
        <div class="__btn-close">
            <button class="__btn">
                &#10006;
            </button>
        </div>
        <div class="__logo">
            <a href="/">
                <img src="{{ asset('/box/img/logo.png') }}" srcset="{{ asset('/box/img/logo@2x.png') }} 2x, {{ asset('/box/img/logo@3x.png') }} 3x" alt="BoxForward" class="img-responsive">
            </a>
        </div>
        @php($menuSidebar = \App\Models\Menu\Menu::getMenu(\App\Models\Menu\Menu::TYPE_SIDEBAR))
        @if(!empty($menuSidebar))
            <nav class="__nav">
                <ul class="__list">
                    @foreach($menuSidebar as $item =>$value)
                        <li>
                            <a href="{{$value->link}}">
                                {{$value->title}}
                            </a>
                        </li>
                    @endforeach

                </ul>
            </nav>
        @endif

    </div>
</div>
<!-- SLIDE MENU ENDS -->
<!-- FEEDBACK FROM STARTS -->
<div class="home-about-us_feedback-form_btn">
    <button class="__btn" type="submit">
        <span class="convert-connect"></span>
    </button>
</div>
<div class="home-about-us_feedback-form">
    {{ html()->form('POST', route('frontend.contact.send'))->open() }}
        <p>
            {{ html()->label(__('validation.attributes.frontend.name'))->for('name') }}
            {{ html()->text('name')
                ->attribute('maxlength', 191)
                ->required()
                ->autofocus() }}
        </p>
        <p>
            {{ html()->label(__('validation.attributes.frontend.email'))->for('email') }}

            {{ html()->text('email')
                ->attribute('maxlength', 191)
                ->attribute('type','email')
                ->required() }}
        </p>
        <p>
            {{ html()->label(__('validation.attributes.frontend.message'))->for('message') }}

            {{ html()->textarea('message')
                ->required()
                ->attribute('rows', 3)
                 }}
        </p>
            <p>
          {!! NoCaptcha::renderJs() !!}
          {!! NoCaptcha::display() !!}
             </p>
        <p>
            {{ form_submit(__('labels.frontend.contact.button')) }}
        </p>
    {{ html()->form()->close() }}

</div>

@if (auth()->user() && session()->has("admin_user_id") && session()->has("temp_user_id"))
    <div class="alert alert-warning logged-in-as mb-0">
        {{__('header.frontend.text.You are currently logged in as')}} {{ auth()->user()->name }}. <a href="{{ route("frontend.auth.logout-as") }}">{{__('frontend.header.text.Re-Login as')}} {{ session()->get("admin_user_name") }}</a>.
    </div><!--alert alert-warning logged-in-as-->
@endif

<!-- FEEDBACK FROM ENDS -->
<div class="__shadow"></div>
<!-- WRAPPER CONTAINER FOR ALL CONTENT -->
<div class="wrapper">
    <header>
        <div class="header-container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4 col-xs-6 col-md-2">
                        <div class="header_logo">
                            <a href="/">
                                <img src="{{asset('box/img/logo.png')}}" srcset="{{asset('box/img/logo@2x.png')}} 2x, {{asset('box/img/logo@3x.png')}} 3x" alt="BoxForward" class="img-responsive">
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-8 col-xs-6 col-md-10">
                        <div class="header_info">
                            <div class="home-short-info-main">
                                <div class="home-short-info_menu">
                                    <div class="home-short-info_menu_btn all">
                                        <button type="button" class="home-short-info_menu_btn-button">
                                            <span class="__menu-btn"></span>
                                            <span class="__menu-btn"></span>
                                            <span class="__menu-btn"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="header_info_calculator">
                                <a class="header_info_calculator_btn" href="/calculator">
                                    <svg width="18px" height="24px" viewBox="0 0 18 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="00-Landing" transform="translate(-1116.000000, -27.000000)" fill="#0080FF">
                                                <g id="calculator" transform="translate(1115.884545, 27.589370)">
                                                    <path d="M16.7837833,0.372490966 L1.29974816,0.372490966 C0.731410327,0.372490966 0.268767626,0.835133667 0.268767626,1.40588739 L0.268767626,22.049657 C0.268767626,22.6204107 0.731410327,23.0830534 1.29974816,23.0830534 L16.7837833,23.0830534 C17.354537,23.0830534 17.8171797,22.6204107 17.8171797,22.049657 L17.8171797,1.40588739 C17.8171797,0.835133667 17.354537,0.372490966 16.7837833,0.372490966 Z M2.33314458,5.53222542 L15.7528028,5.53222542 L15.7528028,2.43686793 L2.33314458,2.43686793 L2.33314458,5.53222542 Z M2.33314458,21.0186764 L2.33314458,7.59901827 L15.7528028,7.59901827 L15.7528028,21.0162606 L2.33314458,21.0162606 L2.33314458,21.0186764 Z" id="Fill-1"></path>
                                                    <path d="M6.02522661,8.51222422 L3.88052135,8.51222422 C3.59544647,8.51222422 3.36654101,8.74354557 3.36654101,9.02620456 L3.36654101,11.1709098 C3.36654101,11.4565887 3.59786236,11.68791 3.88052135,11.68791 L6.02522661,11.68791 C6.30848956,11.68791 6.53981092,11.4565887 6.53981092,11.1709098 L6.53981092,9.02620456 C6.5422268,8.74112968 6.31090545,8.51222422 6.02522661,8.51222422 Z" id="Fill-2"></path>
                                                    <path d="M10.1141184,8.51222422 L7.97182899,8.51222422 C7.68615014,8.51222422 7.45482879,8.74354557 7.45482879,9.02620456 L7.45482879,11.1709098 C7.45482879,11.4565887 7.68615014,11.68791 7.97182899,11.68791 L10.1141184,11.68791 C10.3991932,11.68791 10.6305146,11.4565887 10.6305146,11.1709098 L10.6305146,9.02620456 C10.6305146,8.74112968 10.3991932,8.51222422 10.1141184,8.51222422 Z" id="Fill-3"></path>
                                                    <path d="M14.204822,8.51222422 L12.0601168,8.51222422 C11.7774578,8.51222422 11.5461364,8.74354557 11.5461364,9.02620456 L11.5461364,11.1709098 C11.5461364,11.4565887 11.7774578,11.68791 12.0601168,11.68791 L14.204822,11.68791 C14.488085,11.68791 14.7194063,11.4565887 14.7194063,11.1709098 L14.7194063,9.02620456 C14.7194063,8.74112968 14.488085,8.51222422 14.204822,8.51222422 Z" id="Fill-4"></path>
                                                    <path d="M6.02522661,12.7188906 L3.88052135,12.7188906 C3.59544647,12.7188906 3.36654101,12.9496079 3.36654101,13.2352868 L3.36654101,15.379992 C3.36654101,15.6656709 3.59786236,15.8963883 3.88052135,15.8963883 L6.02522661,15.8963883 C6.30848956,15.8963883 6.53981092,15.6656709 6.53981092,15.379992 L6.53981092,13.2352868 C6.5422268,12.9496079 6.31090545,12.7188906 6.02522661,12.7188906 Z" id="Fill-5"></path>
                                                    <path d="M10.1141184,12.7188906 L7.97182899,12.7188906 C7.68615014,12.7188906 7.45482879,12.9496079 7.45482879,13.2352868 L7.45482879,15.379992 C7.45482879,15.6656709 7.68615014,15.8963883 7.97182899,15.8963883 L10.1141184,15.8963883 C10.3991932,15.8963883 10.6305146,15.6656709 10.6305146,15.379992 L10.6305146,13.2352868 C10.6305146,12.9496079 10.3991932,12.7188906 10.1141184,12.7188906 Z" id="Fill-6"></path>
                                                    <path d="M6.02522661,16.9273688 L3.88052135,16.9273688 C3.59544647,16.9273688 3.36654101,17.1586902 3.36654101,17.444369 L3.36654101,19.5890743 C3.36654101,19.8741491 3.59786236,20.1054705 3.88052135,20.1054705 L6.02522661,20.1054705 C6.30848956,20.1054705 6.53981092,19.8741491 6.53981092,19.5890743 L6.53981092,17.444369 C6.5422268,17.1586902 6.31090545,16.9273688 6.02522661,16.9273688 Z" id="Fill-7"></path>
                                                    <path d="M10.1141184,16.9273688 L7.97182899,16.9273688 C7.68615014,16.9273688 7.45482879,17.1586902 7.45482879,17.444369 L7.45482879,19.5890743 C7.45482879,19.8741491 7.68615014,20.1054705 7.97182899,20.1054705 L10.1141184,20.1054705 C10.3991932,20.1054705 10.6305146,19.8741491 10.6305146,19.5890743 L10.6305146,17.444369 C10.6305146,17.1586902 10.3991932,16.9273688 10.1141184,16.9273688 Z" id="Fill-8"></path>
                                                    <path d="M14.204822,12.7188906 L12.0601168,12.7188906 C11.7774578,12.7188906 11.5461364,12.9496079 11.5461364,13.2352868 L11.5461364,19.5890743 C11.5461364,19.8741491 11.7774578,20.1054705 12.0601168,20.1054705 L14.204822,20.1054705 C14.488085,20.1054705 14.7194063,19.8741491 14.7194063,19.5890743 L14.7194063,13.2352868 C14.7194063,12.9496079 14.488085,12.7188906 14.204822,12.7188906 Z" id="Fill-9"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                    <span class="__content">
                                        {{__('header.frontend.text.Calculate the cost')}}
										</span>
                                </a>
                            </div>
                            <div class="header_info_time">
                                <div class="header_info_time_img">
                                    <svg width="27px" height="27px" viewBox="0 0 27 27" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="00-Landing" transform="translate(-1153.000000, -26.000000)">
                                                <g id="Group-2" transform="translate(1153.111949, 22.496141)">
                                                    <g id="clock" transform="translate(0.000000, 3.421594)">
                                                        <circle id="Oval" fill="#D8EBFE" cx="15.1434173" cy="15.1434173" r="11.0133944"></circle>
                                                        <path d="M13.072298,0.639096357 C6.3231218,0.639096357 0.862702302,6.10562371 0.862702302,12.8547999 C0.862702302,19.6039762 6.3231218,25.0705035 13.072298,25.0705035 C19.8214743,25.0705035 25.2941095,19.6039762 25.2941095,12.8547999 C25.2941095,6.10562371 19.8214743,0.639096357 13.072298,0.639096357 Z M13.0784059,22.6273628 C7.6790649,22.6273628 3.30584302,18.2541409 3.30584302,12.8547999 C3.30584302,7.45545896 7.6790649,3.08223707 13.0784059,3.08223707 C18.4777469,3.08223707 22.8509687,7.45545896 22.8509687,12.8547999 C22.8509687,18.2541409 18.4777469,22.6273628 13.0784059,22.6273628 Z M13.6891911,6.74694815 L11.8568355,6.74694815 L11.8568355,14.0763703 L18.2639721,17.9243169 L19.1862577,16.4217854 L13.6891911,13.1601925 L13.6891911,6.74694815 Z" id="Fill-1" fill="#0080FF"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                                <div class="header_info_time_cont">
                                    <p class="header_info_time_cities">
                                        <span class="__city">{{__('header.frontend.text.new york')}}</span>
                                        <span class="__city">{{__('header.frontend.text.moscow')}}</span>
                                    </p>
                                    <p class="header_info_time_times">
                                        <span class="__time">{{\App\Helpers\Date\DateHelper::getCreatedNewYork()}}</span>
                                        <span class="__time">{{\App\Helpers\Date\DateHelper::getCreatedMoskow()}}</span>
                                    </p>
                                </div>
                            </div>
                            <!-- JUST ADD CLASS "LOGIN" TO REMOVE SIGNING BUTTON AND SHOW USER INFO -->
                            <div class="header_info_login login">
                                    @if ($loginStatus->isLoginApi())
                                    <div class="__balance">
                                        <span>{{__('header.frontend.text.your balance')}}</span>
                                        <span>{{ $loginStatus->balance }}$</span>
                                        <span></span>
                                    </div>
                                    <div class="__icon-user">
                                        <img src="{{$loginStatus->thumb_photo}}" alt="user" class="img-responsive">
                                        <button class="__btn" type="button">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="292.362px" height="292.362px" viewBox="0 0 292.362 292.362" style="enable-background:new 0 0 292.362 292.362;" xml:space="preserve">
												<g>
                                                    <path d="M286.935,69.377c-3.614-3.617-7.898-5.424-12.848-5.424H18.274c-4.952,0-9.233,1.807-12.85,5.424   C1.807,72.998,0,77.279,0,82.228c0,4.948,1.807,9.229,5.424,12.847l127.907,127.907c3.621,3.617,7.902,5.428,12.85,5.428   s9.233-1.811,12.847-5.428L286.935,95.074c3.613-3.617,5.427-7.898,5.427-12.847C292.362,77.279,290.548,72.998,286.935,69.377z"/>
                                                </g>
											</svg>
                                        </button>
                                        <div class="__dropdown-menu">
                                            @foreach($loginStatus->address as $key => $addresCart)
                                                <p class="__title">
                                                    {{$key}}
                                                </p>
                                                <p class="__title-sm">
                                                    {{__('header.frontend.text.street')}}:
                                                </p>
                                                <p class="__describe">
                                                    {{$addresCart['street']}}
                                                </p>
                                                <p class="__city">
                                                    {{__('header.frontend.text.city')}}:
                                                    <span>
                                                    {{$addresCart['city']}}
                                                    </span>
                                                </p>
                                                <p class="__info">
                                                    {{__('header.frontend.text.region')}}:
                                                    <span>
                                                        {{$addresCart['state']}}
                                                    </span>
                                                </p>
                                                <p class="__info">
                                                    {{__('header.frontend.text.code')}}:
                                                    <span>
                                                        {{$addresCart['zip']}}
                                                    </span>
                                                </p>
                                                <p class="__info">
                                                    {{__('header.frontend.text.phone number')}}:
                                                    <span>
                                                        <a href="tel:">
                                                            +{{$addresCart['phone']}}
                                                        </a>
                                                    </span>
                                                </p>

                                            @endforeach
                                        </div>
                                    </div>
                                    @else
                                    <div class="header_info_login">
                                        <a class="header_info_login_sign-up" href="https://account.boxfwd.com/ru/users/sign_up">{{__('header.frontend.text.sign-up')}}</a>
                                        <a class="header_info_login_sign-in" href="https://account.boxfwd.com/ru/users/sign_in">{{__('header.frontend.text.sign-in')}}</a>
                                    </div>

                                @endif

                            </div>
                            <div class="header_info_lang">
                                @foreach(\App\Langs\Lang::langs() as $code => $title)
                                @if(!($code == app()->getLocale()))
                                    <a href="{{ '/lang/'.$code }}" class="header_info_lang_active">
                                        {{$code}}
                                    </a>
                                @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="header_info_menu-mobile visible-xs">
                            <div class="home-short-info_menu_btn_mobile">
                                <button type="button" class="home-short-info_menu_btn-button" data-toggle="dropdown" data-target="#js_dropdown_menu">
                                    <span class="__menu-btn"></span>
                                    <span class="__menu-btn"></span>
                                    <span class="__menu-btn"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="header_info_menu_main">

                            <nav>
                                <ul class="header_info_menu_main_list">
                                    @php($headerMenu = \App\Models\Menu\Menu::getMenu(\App\Models\Menu\Menu::TYPE_HEADER))
                                    @if(!empty($headerMenu))
                                        @foreach($headerMenu as $item => $value)
                                            <li>
                                                <a href="{{$value->link}}">
                                                    {{$value->title}}
                                                </a>
                                            </li>
                                        @endforeach
                                    @endif
                                    @if (auth()->user() && session()->has("admin_user_id") && session()->has("temp_user_id"))
                                        <li class="__pers_area">
                                            <a href="#">
                                                Личный кабинет
                                            </a>
                                        </li>
                                    @else
                                        <li class="__pers_area">
                                            <a href="#">
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    @yield('content')
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="footer_content_position">
                        <h4 class="__title">
                            {{__('footer.frontend.text.Additional Information')}}
                        </h4>
                        @php($footerMenu = \App\Models\Menu\Menu::getMenu(\App\Models\Menu\Menu::TYPE_FOOTER))
                        @if(!empty($footerMenu))
                            <ul class="__list-info">
                            @foreach($footerMenu as $item =>$value)
                                    <li>
                                        <a href="{{$value->link}}">
                                            {{$value->title}}
                                        </a>
                                    </li>
                            @endforeach
                            </ul>
                        @endif

                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="footer_content_position">
                        <h4 class="__title">
                            {{__('footer.frontend.text.payment options')}}
                        </h4>
                        <div class="footer_content_position_imgs">
                            <img src="{{ asset('/box/img/visa-sm.png') }}" srcset="{{ asset('/box/img/visa-sm@2x.png') }} 2x, {{ asset('/box/img/visa-sm@3x.png') }} 3x" class="__visa">
                            <img src="{{ asset('/box/img/mc-sm.png') }}" srcset="{{ asset('/box/img/mc-sm@2x.png') }} 2x, {{ asset('/box/img/mc-sm@3x.png') }} 3x" class="__mc">
                            <img src="{{ asset('/box/img/qiwi-sm.png') }}" srcset="{{ asset('/box/img/qiwi-sm@2x.png') }} 2x, {{ asset('/box/img/qiwi-sm@3x.png') }} 3x" class="__qiwi">
                            <img src="{{ asset('/box/img/pay-pal-sm.png') }}" srcset="{{ asset('/box/img/pay-pal-sm@2x.png') }} 2x, {{ asset('/box/img/pay-pal-sm@3x.png') }} 3x" class="__pay-pal">

                        </div>
                    </div>
                </div>

                @php( $facebook_url = $vkontakte_url = $twitter_url = '#')
                @if($helper->has('facebook_'.app()->getLocale()))
                    @php( $facebook_url = $helper->get('facebook_'.app()->getLocale()))
                @endif
                @if($helper->has('vkontakte_'.app()->getLocale()))
                    @php( $vkontakte_url = $helper->get('vkontakte_'.app()->getLocale()))
                @endif
                @if($helper->has('twitter_'.app()->getLocale()))
                    @php( $twitter_url = $helper->get('twitter_'.app()->getLocale()))
                @endif
                <div class="col-sm-3">
                    <div class="footer_content_position">
                        <h4 class="__title">
                            {{__('footer.frontend.text.Follow Us')}}

                        </h4>
                        <ul class="__list-soc">
                            <li>

                                <span class="__tooltip">Vkontakte</span>
                                <a href="{{ $vkontakte_url}}" target="_blank" >
                                    <svg width="34px" height="34px" viewBox="0 0 34 34" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="00-Landing" transform="translate(-994.000000, -5509.000000)">
                                                <g id="Group-5" transform="translate(994.000000, 5509.186261)">
                                                    <g id="vk" transform="translate(0.000000, 0.267013)">
                                                        <circle id="XMLID_11_" fill="#4D76A1" fill-rule="nonzero" cx="16.512604" cy="16.512604" r="16.512604"></circle>
                                                        <path d="M15.8888704,23.7548605 L17.1849058,23.7548605 C17.1849058,23.7548605 17.576395,23.711885 17.7762606,23.4964188 C17.9602311,23.2986137 17.9543441,22.9271404 17.9543441,22.9271404 C17.9543441,22.9271404 17.9290297,21.188104 18.7361451,20.9320171 C19.5317808,20.6797567 20.5534794,22.6127716 21.6364034,23.3560125 C22.4552929,23.9185207 23.0775547,23.7951869 23.0775547,23.7951869 L25.9731033,23.7548605 C25.9731033,23.7548605 27.4878429,23.6615507 26.769622,22.4705992 C26.7107515,22.3731684 26.3513467,21.5896013 24.6167256,19.9794914 C22.8011574,18.2943215 23.0442929,18.5668922 25.2313342,15.6519164 C26.5632807,13.8766746 27.0957649,12.7928675 26.9294556,12.3286731 C26.7707994,11.8865552 25.7911933,12.0034133 25.7911933,12.0034133 L22.5309416,12.0237236 C22.5309416,12.0237236 22.2892779,11.9907561 22.110017,12.0979005 C21.9348771,12.2029845 21.82214,12.447886 21.82214,12.447886 C21.82214,12.447886 21.3061395,13.8216306 20.6179426,14.9899169 C19.1661946,17.4551217 18.5857308,17.5852256 18.3484825,17.4321622 C17.7965709,17.0754066 17.9343281,15.9989583 17.9343281,15.2342297 C17.9343281,12.8452623 18.2966764,11.8491724 17.2287644,11.5913193 C16.8743636,11.5056627 16.613567,11.4491469 15.7072547,11.440022 C14.5439724,11.4279535 13.5593622,11.4435542 13.001858,11.7167136 C12.6309735,11.8983293 12.3448625,12.3030644 12.5191194,12.3263183 C12.7345857,12.3551649 13.2226226,12.457894 13.4813587,12.8102343 C13.8154492,13.2647151 13.8036751,14.2855306 13.8036751,14.2855306 C13.8036751,14.2855306 13.9955931,17.0977774 13.3553757,17.4471742 C12.915907,17.6867773 12.3130724,17.197563 11.0188031,14.9610703 C10.3556262,13.8154492 9.85493211,12.5491434 9.85493211,12.5491434 C9.85493211,12.5491434 9.75838439,12.3124837 9.58618799,12.185912 C9.3771975,12.0325542 9.08519951,11.9836916 9.08519951,11.9836916 L5.98713617,12.004002 C5.98713617,12.004002 5.52205873,12.0169535 5.3513341,12.2191739 C5.19944805,12.3990235 5.33926564,12.7710854 5.33926564,12.7710854 C5.33926564,12.7710854 7.7647328,18.4456189 10.5110445,21.3052564 C13.0292328,23.9270569 15.8888704,23.7548605 15.8888704,23.7548605 Z" id="Shape" fill="#FFFFFF"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <span class="__tooltip">FaceBook</span>
                                <a href="{{ $facebook_url }}" target="_blank" >
                                    <svg width="34px" height="34px" viewBox="0 0 34 34" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="00-Landing" transform="translate(-1036.000000, -5509.000000)" fill-rule="nonzero">
                                                <g id="Group-5" transform="translate(994.000000, 5509.186261)">
                                                    <g id="facebook" transform="translate(42.550473, 0.267013)">
                                                        <circle id="Oval" fill="#3B5998" cx="16.6036647" cy="16.6036647" r="16.6036647"></circle>
                                                        <path d="M20.777815,17.2536281 L17.8150947,17.2536281 L17.8150947,28.1076619 L13.3263218,28.1076619 L13.3263218,17.2536281 L11.1914466,17.2536281 L11.1914466,13.4390887 L13.3263218,13.4390887 L13.3263218,10.9706484 C13.3263218,9.20544723 14.164822,6.44132686 17.8550514,6.44132686 L21.1800464,6.45523774 L21.1800464,10.1578982 L18.7675456,10.1578982 C18.3718256,10.1578982 17.8153906,10.3556102 17.8153906,11.1976621 L17.8153906,13.4426404 L21.1699832,13.4426404 L20.777815,17.2536281 Z" id="Shape" fill="#FFFFFF"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <span class="__tooltip">Twitter</span>
                                <a href="{{ $twitter_url }}" target="_blank" >
                                    <svg width="34px" height="34px" viewBox="328 355 335 276" xmlns="http://www.w3.org/2000/svg">
                                        <path d="
                                                M 630, 425
                                                A 195, 195 0 0 1 331, 600
                                                A 142, 142 0 0 0 428, 570
                                                A  70,  70 0 0 1 370, 523
                                                A  70,  70 0 0 0 401, 521
                                                A  70,  70 0 0 1 344, 455
                                                A  70,  70 0 0 0 372, 460
                                                A  70,  70 0 0 1 354, 370
                                                A 195, 195 0 0 0 495, 442
                                                A  67,  67 0 0 1 611, 380
                                                A 117, 117 0 0 0 654, 363
                                                A  65,  65 0 0 1 623, 401
                                                A 117, 117 0 0 0 662, 390
                                                A  65,  65 0 0 1 630, 425
                                                Z"
                                              style="fill:#3BA9EE;"/>
                                    </svg>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                        @if($helper->has('contactus_'.app()->getLocale()))
                            {!! $helper->get('contactus_'.app()->getLocale()) !!}
                        @endif
                    {{--<div class="footer_content_position">--}}
                        {{--<h4 class="__title">--}}
                            {{--Contact Us--}}
                        {{--</h4>--}}
                        {{--<p class="__tel-title">--}}
                            {{--Tel. in USA:--}}
                        {{--</p>--}}
                        {{--<p class="__tel">--}}
                            {{--<a href="tel:+1 (732) 893-7447" class="__link">--}}
                                {{--+1 (732) 893-7447--}}
                            {{--</a>--}}
                        {{--</p>--}}
                        {{--<p class="__address-title">--}}
                            {{--Warehouse address--}}
                        {{--</p>--}}
                        {{--<address class="__address">--}}
                            {{--310 Vraen Ave, Wyckoff, NJ 07481--}}
                        {{--</address>--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="footer-foot">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="footer-foot_coptyright">
                            <p>
                                &#169; 2018 BOXFORWARD. All rights reserved
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="footer-foot_logo">
                            <a href="/">
                                <img src="{{ asset('/box/img/logo.png') }}" srcset="{{ asset('/box/img/logo@2x.png') }} 2x, {{ asset('/box/img/logo@3x.png') }} 3x" alt="BoxForward" class="img-responsive">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<!-- /WRAPPER -->
@stack('before-scripts')
<script src="{{asset('box/libs/jquery/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('box/libs/jquery/jquery-ui.min.js')}}"></script>
<script src="{{asset('box/libs/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('box/libs/waypoint/waypoint.min.js')}}"></script>
<script src="{{asset('box/js/common.js')}}"></script>
<script src="{{asset('box/js/sidebar-faq-scripts.js')}}"></script>
@yield('calculatorJs')
@yield('homeShortjs')
@stack('after-scripts')
</body>
</html>
