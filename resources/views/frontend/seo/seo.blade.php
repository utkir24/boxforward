@section('meta_tags')
    @if(isset($seos) && !empty($seos))
        @foreach($seos as $name => $seo)
            <meta {{$seo['type']."="}}<?='"'.$name.'"'?> content="{{$seo['value']}}">
        @endforeach
    @endif
@endsection