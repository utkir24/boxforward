@extends('frontend.layouts.box')

@section('title', app_name() . ' | '.__('navs.general.home'))
@section('content')

    <section class="why-buy-title">
        <div class="why-buy-title_container">
            <h2 class="__title">
                Зачем покупать в США?
            </h2>
            <p class="__describe">
                Поздравляем, вы сделали правильный выбор! Шоппинг в США это море удовольствия! Всего 4 простых шага и заветная покупка окажется у Вас!
            </p>
        </div>
    </section>
    <section class="why-buy_content">
        <div class="why-buy_content_container">
            <div class="why-buy_content_position">
                <div class="why-buy_content_position_content">
                    <div class="__title">
                        Прекрасное качество и дешевые цены!
                    </div>
                    <div class="__describe">
                        “Соотношение цены, качества и внешнего вида вещей в наших магазинах оставляет желает лучшего. Особенно это касается детских товаров. Либо покупаешь качественные и красивые вещи за космические суммы,  либо что-то недолговечное, и очень часто не особо стильное и красивое, за тоже, в общем-то не малые деньги. По совету знакомой попробовала купить вещи в магазине Carter`s в США. Получив вещи, просто была в шоке – качественные, стильные вещи и стоимость очень дешевая. Теперь я регулярно делаю покупки в США. Children`s Place, Gymboree, Amazon, а потом еще и сайты закрытых распродаж Zulily, Ideeli  - стали моими любимыми магазинами. И самое важное – в США постоянно какие-нибудь распродажи!”
                    </div>
                </div>
                <div class="why-buy_content_position_img">
                    <img src="{{ asset('/box/img/img.jpg') }}" srcset="{{ asset('/box/img/img@2x.png') }} 2x, {{ asset('/box/img/img@3x.png') }} 3x" alt="" class="img-responsive">
                    <p class="__info">
                        Мария, 34 года, мама 2-х детей, Москва
                    </p>
                </div>
            </div>
            <div class="why-buy_content_position">
                <div class="why-buy_content_position_content">
                    <div class="__title">
                        Прекрасное качество и дешевые цены!
                    </div>
                    <div class="__describe">
                        “Соотношение цены, качества и внешнего вида вещей в наших магазинах оставляет желает лучшего. Особенно это касается детских товаров. Либо покупаешь качественные и красивые вещи за космические суммы,  либо что-то недолговечное, и очень часто не особо стильное и красивое, за тоже, в общем-то не малые деньги. По совету знакомой попробовала купить вещи в магазине Carter`s в США. Получив вещи, просто была в шоке – качественные, стильные вещи и стоимость очень дешевая. Теперь я регулярно делаю покупки в США. Children`s Place, Gymboree, Amazon, а потом еще и сайты закрытых распродаж Zulily, Ideeli  - стали моими любимыми магазинами. И самое важное – в США постоянно какие-нибудь распродажи!”
                    </div>
                </div>
                <div class="why-buy_content_position_img">
                    <img src="{{ asset('/box/img/img.jpg') }}" srcset="{{ asset('/box/img/img@2x.png') }} 2x, {{ asset('/box/img/img@3x.png') }} 3x" alt="" class="img-responsive">

                    <p class="__info">
                        Мария, 34 года, мама 2-х детей, Москва
                    </p>
                </div>
            </div>
            <div class="why-buy_content_position">
                <div class="why-buy_content_position_content">
                    <div class="__title">
                        Прекрасное качество и дешевые цены!
                    </div>
                    <div class="__describe">
                        “Соотношение цены, качества и внешнего вида вещей в наших магазинах оставляет желает лучшего. Особенно это касается детских товаров. Либо покупаешь качественные и красивые вещи за космические суммы,  либо что-то недолговечное, и очень часто не особо стильное и красивое, за тоже, в общем-то не малые деньги. По совету знакомой попробовала купить вещи в магазине Carter`s в США. Получив вещи, просто была в шоке – качественные, стильные вещи и стоимость очень дешевая. Теперь я регулярно делаю покупки в США. Children`s Place, Gymboree, Amazon, а потом еще и сайты закрытых распродаж Zulily, Ideeli  - стали моими любимыми магазинами. И самое важное – в США постоянно какие-нибудь распродажи!”
                    </div>
                </div>
                <div class="why-buy_content_position_img">
                    <img src="{{ asset('/box/img/img.jpg') }}" srcset="{{ asset('/box/img/img@2x.png') }} 2x, {{ asset('/box/img/img@3x.png') }} 3x" alt="" class="img-responsive">

                    <p class="__info">
                        Мария, 34 года, мама 2-х детей, Москва
                    </p>
                </div>
            </div>
        </div>
    </section>

@endsection
