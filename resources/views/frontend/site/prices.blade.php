@extends('frontend.layouts.box')

@section('title', app_name() . ' | '.__('navs.general.home'))
@section('content')
    <section class="prices-title">
        <div class="prices-title_container">
            <h2 class="__title">
                Стоимость услуг
            </h2>
            <p class="__describe">
                Сервис BOXFORWARD предоставляет своим клиентам самый обширный спектр услуг по доступным ценам.
            </p>
        </div>
    </section>
    <section class="prices-main">
        <div class="prices-main_container">
            <div class="prices-main_positions-left-right">
                <div class="prices-main_positions-left-right_position left">
                    <h4 class="__title">
                        Прием заказов
                    </h4>
                    <h5 class="__title-describe">
                        бесплатно
                    </h5>
                    <p class="__content">
                        $0 за принятый заказ
                    </p>
                </div>
                <div class="prices-main_positions-left-right_position right">
                    <h4 class="__title">
                        Хранение заказов
                    </h4>
                    <h5 class="__title-describe">
                        бесплатно
                    </h5>
                    <p class="__content">
                        $0 за хранение 90 дней
                    </p>
                </div>
            </div>
            <div class="prices-main_positions-left-right">
                <div class="prices-main_positions-left-right_position left">
                    <h4 class="__title">
                        платные услуги
                    </h4>
                    <h5 class="__title-describe">
                        10% за услугу
                    </h5>
                    <p class="__content">
                        Звонки в магазин проверка товаров и много другое
                    </p>
                </div>
                <div class="prices-main_positions-left-right_position right">
                    <h4 class="__title">
                        страхование вложения
                    </h4>
                    <h5 class="__title-describe">
                        1.5% от стоимости
                    </h5>
                    <p class="__content">
                        Дополнительная страховка отправлений
                    </p>
                </div>
            </div>
            <div class="prices-main_positions-left-right">
                <div class="prices-main_positions-left-right_position left">
                    <h4 class="__title">
                        Помощь при покупке
                    </h4>
                    <h5 class="__title-describe">
                        5% от суммы заказа
                    </h5>
                    <p class="__content">
                        Цслуги посредника по покупке товара
                    </p>
                </div>
                <div class="prices-main_positions-left-right_position right">
                    <h4 class="__title">
                        Абонемент на услуги
                    </h4>
                    <h5 class="__title-describe">
                        от 50$ в месяц
                    </h5>
                    <p class="__content">
                        Абонементы на 30, 90, 180 дней
                    </p>
                </div>
            </div>
            <div class="prices-main_positions-full">
                <div class="prices-main_positions-left-right_position">
                    <h4 class="__title">
                        Фото заказа
                    </h4>
                    <h5 class="__title-describe">
                        бесплатно
                    </h5>
                    <p class="__content">
                        $0 за принятый на склад заказ
                    </p>
                </div>
                <div class="prices-main_positions-left-right_position">
                    <h4 class="__title">
                        большие объемы
                    </h4>
                    <h5 class="__title-describe">
                        особые условия
                    </h5>
                    <p class="__content">
                        скидки для отправляющих более 100кг в месяц
                    </p>
                </div>
                <div class="prices-main_positions-left-right_position">
                    <h4 class="__title">
                        консолидация заказов
                    </h4>
                    <h5 class="__title-describe">
                        бесплатно
                    </h5>
                    <p class="__content">
                        Сбор нескольких заказв в одну посылку
                    </p>
                </div>
            </div>
            <div class="prices-main_btn-link">
                <a href="#" class="__link">
                    расчитать стоимость
                </a>
            </div>
        </div>
    </section>
    <section class="prices-bonuse">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="prices-bonuse_container">
                        <div class="prices-bonuse_container_title">
                            <h3 class="__title">
                                Бонусные программы
                            </h3>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="prices-bonuse_position left">
                                    <div class="prices-bonuse_position_top">
                                        <h4 class="__title">
                                            100 рублей
                                        </h4>
                                        <p class="__describe">
                                            Энтузиастов зарубежного шопинга ожидает бонусная программа, позволяющая получить назад часть средств затраченных на услуги склада
                                        </p>
                                    </div>
                                    <div class="prices-bonuse_position-square">
                                        <h5 class="__title">
                                            Ежеквартальная выплата бонусов
                                        </h5>
                                        <ul class="__list">
                                            <li>
                                                От 5 до 10 отправленных за прошедший квартал посылок -<span> $10 на баланс</span>
                                            </li>
                                            <li>
                                                От 10 до 20 отправленных за прошедший квартал посылок - дополнительные<span>$15 на баланс</span>
                                            </li>
                                            <li>
                                                20 и более отправленных за прошедший квартал посылок - дополнительные<span>$25 на баланс</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="prices-bonuse_position-square">
                                        <h5 class="__title">
                                            Дополнительный, годовой бонус
                                        </h5>
                                        <ul class="__list">
                                            <li>
                                                От 50 до 99 посылок за календарный год -<span> $50</span>
                                            </li>
                                            <li>
                                                100 и более посылок за календарный год - дополнительные<span> $100</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="prices-bonuse_position_bottom">
                                        <p class="__describe">
                                            Бонусы начисляются автоматически в конце каждого квартала
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="prices-bonuse_position right">
                                    <div class="prices-bonuse_position_top">
                                        <h4 class="__title">
                                            100 друзей
                                        </h4>
                                        <p class="__describe">
                                            Для тех, кто готов поделится опытом и привлечь друзей к покупкам в США, создана специальная партнерская программа
                                        </p>
                                    </div>
                                    <div class="prices-bonuse_position-square">
                                        <h5 class="__title">
                                            Выплата бонуса за регистрацию пользователя
                                        </h5>
                                        <p class="__content">
                                            <span>1$</span> за каждого прошедшего процесс проверки документов пользователя
                                        </p>
                                    </div>
                                    <div class="prices-bonuse_position-square">
                                        <h5 class="__title">
                                            Выплата бонуса за отправленную пользователем посылку
                                        </h5>
                                        <p class="__content">
                                            <span>$2</span> за первую отправленную пользователем посылку
                                        </p>
                                    </div>
                                    <div class="prices-bonuse_position_bottom">
                                        <p class="__describe">
                                            Поделитесь с другом реферальной ссылкой на страничку регистрации (находится в закладке "Настройки") и бонус автоматически зачислится после исполнения условий указанных выше
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="prices-bonuse_position_info">
                                    <p class="__content">
                                        * Администрация BOXFWD оставляет за собой право изменять условия бонусных программ, предварительно уведомив об этом пользователей
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="home-payments-method">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="home-payments-method_title">
                        <h2 class="__title">
                            BoxForward is providing maximum flexibility, safety and convenience when it comes to payment options
                        </h2>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="home-payments-method_container">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="home-payments-method_position">
                                    <div class="home-payments-method_position_img">
                                        <img src="{{ asset('/box/img/visa.png') }}" srcset="{{ asset('/box/img/visa@2x.png') }} 2x, {{ asset('/box/img/visa@3x.png') }} 3x" class="img-responsive __visa">
                                        <img src="{{ asset('/box/img/mc.png') }}" srcset="{{ asset('/box/img/mc@2x.png') }} 2x, {{ asset('/box/img/mc@3x.png') }} 3x" class="img-responsive __mc">
                                    </div>
                                    <div class="home-payments-method_position_describe">
                                        <p class="__content">
                                            We accept credit card payments using latest security standards, such as 3D Secure
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="home-payments-method_position">
                                    <div class="home-payments-method_position_img">
                                        <img src="{{ asset('/box/img/qiwi.png') }}" srcset="{{ asset('/box/img/qiwi@2x.png') }} 2x,{{ asset('/box/img/qiwi@3x.png') }} 3x" class="img-responsive">
                                    </div>
                                    <div class="home-payments-method_position_describe">
                                        <p class="__content">
                                            Qiwi is an electronic wallet which offers instantaneous payments and convenient account refill options through physical terminals and phone applications
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="home-payments-method_position">
                                    <div class="home-payments-method_position_img">
                                        <img src="{{ asset('/box/img/pay-pal.png') }}" srcset="{{ asset('/box/img/pay-pal@2x.png') }} 2x, {{ asset('/box/img/pay-pal@3x.') }}png 3x" class="img-responsive">
                                    </div>
                                    <div class="home-payments-method_position_describe">
                                        <p class="__content">
                                            PayPal is a gold standard of electronic transactions, offering an unmatched simplicity and security using only your e-mail address and password
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection