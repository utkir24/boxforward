@extends('frontend.layouts.box')

@section('title', app_name() . ' | '.__('navs.general.home'))
@section('content')

<section class="bonuses-page-title">
    <div class="bonuses-page-title_container">
        <h2 class="__title">
            Бонусные программы
        </h2>
        <p class="__describe">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis dignissimos sit quos id reprehenderit at doloremque enim obcaecati repudiandae rem.
        </p>
    </div>
</section>
<section class="bonuses-page_content">
    <div class="bonuses-page_content_container">
        <div class="bonuses-page_content_position">
            <div class="bonuses-page_content_position_top">
                <h5 class="__title">
                    100 рублей
                </h5>
                <p class="__content">
                    Энтузиастов зарубежного шопиинга, ожидает бонусная программа, позволяющая получить назад часть средств затраченных на услуги склада
                </p>
            </div>
            <div class="bonuses-page_content_position_list">
                <h6 class="__title">
                    Ежеквартальная выплата бонусов
                </h6>
                <ul class="__list">
                    <li>
                        От 5 до 10 отправленных за прошедший квартал посылок -<span> $10 на баланс</span>
                    </li>
                    <li>
                        От 10 до 20 отправленных за прошедший квартал посылок - дополнительные<span>$15 на баланс</span>
                    </li>
                    <li>
                        20 и более отправленных за прошедший квартал посылок - дополнительные<span>$25 на баланс</span>
                    </li>
                </ul>
            </div>
            <div class="bonuses-page_content_position_list">
                <h6 class="__title">
                    Дополнительный, годовой бонус
                </h6>
                <ul class="__list">
                    <li>
                        От 50 до 99 посылок за календарный год -<span> $50</span>
                    </li>
                    <li>
                        100 и более посылок за календарный год - дополнительные<span> $100</span>
                    </li>
                </ul>
            </div>
            <div class="bonuses-page_content_position_bottom">
                <p class="__content">
                    Бонусы начисляются автоматически в конце каждого квартала
                </p>
            </div>
        </div>
        <div class="bonuses-page_content_position">
            <div class="bonuses-page_content_position_top">
                <h4 class="__title">
                    100 друзей
                </h4>
                <p class="__content">
                    Для тех, кто готов поделится опытом и привлечь друзей к покупкам в США, создана специальная партнерская программа
                </p>
            </div>
            <div class="bonuses-page_content_position_list">
                <h5 class="__title">
                    Выплата бонуса за регистрацию пользователя
                </h5>
                <p class="__content">
                    <span>1$</span> за каждого прошедшего процесс проверки документов пользователя
                </p>
            </div>
            <div class="bonuses-page_content_position_list">
                <h5 class="__title">
                    Выплата бонуса за отправленную пользователем посылку
                </h5>
                <p class="__content">
                    <span>$2</span> за первую отправленную пользователем посылку
                </p>
            </div>
            <div class="bonuses-page_content_position_bottom">
                <p class="__content">
                    Поделитесь с другом реферальной ссылкой на страничку регистрации (находится в закладке "Настройки") и бонус автоматически зачислится после исполнения условий указанных выше
                </p>
            </div>
        </div>
    </div>
    <div class="bonuses-page_content_info">
        <p class="__content">
            * Администрация BOXFWD оставляет за собой право изменять условия бонусных программ, предварительно уведомив об этом пользователей
        </p>
    </div>
    <div class="bonuses-page_bgi">
        <img src="{{ asset('/box/img/girl-04.svg') }}" alt="loading">
    </div>
</section>
@endsection