@extends('frontend.layouts.box')

@section('title', app_name() . ' | '.__('navs.general.home'))
@section('content')
    <section class="delivery-page-title">
        <div class="delivery-page-title_container">
            <h2 class="__title">
                Доставка в Россию
            </h2>
            <p class="__describe">
                BOXFORWARD предоставляет наиболее выгодные способы отправки в Росиию.
            </p>
            <p class="__describe">С нами вы экономите на каждом этапе пересылки товаров, от магазина до двери вашего дома
            </p>
        </div>
    </section>
    <section class="delivery-page_content">
        <div class="delivery-page_content_container">
            <div class="delivery-page_content_list">
                <ul class="__list">
                    <li>
                        USPS Priority
                    </li>
                    <li class="active">
                        USPS Express
                    </li>
                    <li>
                        Boxberry
                    </li>
                </ul>
            </div>
            <div class="delivery-page_content_tabs">
                <div class="delivery-page_content_tabs_position">
                    <div class="delivery-page_content_tabs_position_info">
                        <p class="__content">
                            Армения, Азербайджан, Белорусь, Грузия, Казахстан, Кыргызстан, Латвия, Литва, Молдова, Узбекистан, Эстония, Украина, Россия
                        </p>
                        <p class="__deadlines">
                            Сроки от 7 до 14 дней
                        </p>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col logo md">
                            <img src="{{ asset('/box/img/logo-1.svg') }}" alt="logo" class="img-responsive">
                            <p class="__content">
                                Экспресс сервис, предоставляемый USPS в сотрудничестве с почтовыми и курьерскими сервисами других стран, позволяет получать почтовые отправления из США до порога вашего дома.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col sm">
                            <h4 class="__title">
                                Тарифы
                            </h4>
                            <ul class="__list">
                                <li>
                                    1 кг = $69.75
                                </li>
                                <li>
                                    5 кг = $113
                                </li>
                                <li>
                                    10 кг = $165.25
                                </li>
                                <li>
                                    20 кг = $269.75
                                </li>
                                <li>
                                    30 кг = $374.25
                                </li>
                            </ul>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col sm">
                            <h4 class="__title">
                                Вложения
                            </h4>
                            <p class="__content">
                                Одежда и обувь, игрушки, косметика, пищевые добавки, спортивный инвентарь
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Габариты и вес
                            </h4>
                            <p class="__content">
                                Максимальная длина одной посылки не должны превышать 152см, длина плюс охват не должны превышать 274см. Максимально разрешенный вес - 32кг.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Превышение лимитов
                            </h4>
                            <p class="__content">
                                Если сумма вложения или вес превышают разрешенные лимиты, взимается таможенная пошлина в размере до 30% от стоимости превышения, но не менее $6 за килограмм физического веса.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Страхование вложения
                            </h4>
                            <p class="__content">
                                Вложение автоматически бесплатно стразуется USPS от утери на сумму в $100. Предоставляется также возможность приобрести дополнительную страховку BOXFORWARD из расчета $1.6 на каждые $100 стоимости вложения.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Отслеживание посылки
                            </h4>
                            <p class="__content">
                                Доступно на сайте www.usps.com или почтовом сайте страны получаетеля по номеру отслживания, присвоенному поссылке.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col lg">
                            <h4 class="__title">
                                Запрещены к отправке
                            </h4>
                            <p class="__content">
                                Легковоспламеняющиеся и взрывоопасные предметы, химикаты и ртуть, огнестрельно оружие и боеприпасы, наркотичиские вещества, лекарственные препараты и прочие предеты запрещенные к вывозу из США, ввозу страну адресата.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col lg">
                            <h4 class="__title">
                                Таможенное оформление
                            </h4>
                            <p class="__content">
                                Товары оформляются на основе таможенной лекарации, заполняемой получателем в личном кабинете. Таможенные службы оставляют за собой право досмотра вложения и проверки соответствия товара ценам и артикулам, указанным в декларации. Если сумма вложения или вес превышают разрегенные лимиты, взимается таможенная пошлина в соотв. с таможенным кодексом страны адресата.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_p-s">
                        <p class="__content">
                            * Страховка распостроняется только на полную и подтвержденную почтовой службой утрату посылки.
                        </p>
                        <a href="calculator.html" class="__link">
                            Расчитать стоимость
                        </a>
                    </div>
                </div>
                <div class="delivery-page_content_tabs_position active">
                    <div class="delivery-page_content_tabs_position_info">
                        <p class="__content">
                            Армения, Азербайджан, Белорусь, Грузия, Казахстан, Кыргызстан, Латвия, Литва, Молдова, Узбекистан, Эстония, Украина, Россия
                        </p>
                        <p class="__deadlines">
                            Сроки от 7 до 14 дней
                        </p>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col logo md">
                            <img src="{{ asset('/box/img/logo-1.svg') }}" alt="logo" class="img-responsive">
                            <p class="__content">
                                Экспресс сервис, предоставляемый USPS в сотрудничестве с почтовыми и курьерскими сервисами других стран, позволяет получать почтовые отправления из США до порога вашего дома.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col sm">
                            <h4 class="__title">
                                Тарифы
                            </h4>
                            <ul class="__list">
                                <li>
                                    1 кг = $69.75
                                </li>
                                <li>
                                    5 кг = $113
                                </li>
                                <li>
                                    10 кг = $165.25
                                </li>
                                <li>
                                    20 кг = $269.75
                                </li>
                                <li>
                                    30 кг = $374.25
                                </li>
                            </ul>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col sm">
                            <h4 class="__title">
                                Вложения
                            </h4>
                            <p class="__content">
                                Одежда и обувь, игрушки, косметика, пищевые добавки, спортивный инвентарь
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Габариты и вес
                            </h4>
                            <p class="__content">
                                Максимальная длина одной посылки не должны превышать 152см, длина плюс охват не должны превышать 274см. Максимально разрешенный вес - 32кг.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Превышение лимитов
                            </h4>
                            <p class="__content">
                                Если сумма вложения или вес превышают разрешенные лимиты, взимается таможенная пошлина в размере до 30% от стоимости превышения, но не менее $6 за килограмм физического веса.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Страхование вложения
                            </h4>
                            <p class="__content">
                                Вложение автоматически бесплатно стразуется USPS от утери на сумму в $100. Предоставляется также возможность приобрести дополнительную страховку BOXFORWARD из расчета $1.6 на каждые $100 стоимости вложения.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Отслеживание посылки
                            </h4>
                            <p class="__content">
                                Доступно на сайте www.usps.com или почтовом сайте страны получаетеля по номеру отслживания, присвоенному поссылке.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col lg">
                            <h4 class="__title">
                                Запрещены к отправке
                            </h4>
                            <p class="__content">
                                Легковоспламеняющиеся и взрывоопасные предметы, химикаты и ртуть, огнестрельно оружие и боеприпасы, наркотичиские вещества, лекарственные препараты и прочие предеты запрещенные к вывозу из США, ввозу страну адресата.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col lg">
                            <h4 class="__title">
                                Таможенное оформление
                            </h4>
                            <p class="__content">
                                Товары оформляются на основе таможенной лекарации, заполняемой получателем в личном кабинете. Таможенные службы оставляют за собой право досмотра вложения и проверки соответствия товара ценам и артикулам, указанным в декларации. Если сумма вложения или вес превышают разрегенные лимиты, взимается таможенная пошлина в соотв. с таможенным кодексом страны адресата.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_p-s">
                        <p class="__content">
                            * Страховка распостроняется только на полную и подтвержденную почтовой службой утрату посылки.
                        </p>
                        <a href="calculator.html" class="__link">
                            Расчитать стоимость
                        </a>
                    </div>
                </div>
                <div class="delivery-page_content_tabs_position">
                    <div class="delivery-page_content_tabs_position_info">
                        <p class="__content">
                            Армения, Азербайджан, Белорусь, Грузия, Казахстан, Кыргызстан, Латвия, Литва, Молдова, Узбекистан, Эстония, Украина, Россия
                        </p>
                        <p class="__deadlines">
                            Сроки от 7 до 14 дней
                        </p>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col logo md">
                            <img src="{{ asset('/box/img/logo-1.svg') }}" alt="logo" class="img-responsive">
                            <p class="__content">
                                Экспресс сервис, предоставляемый USPS в сотрудничестве с почтовыми и курьерскими сервисами других стран, позволяет получать почтовые отправления из США до порога вашего дома.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col sm">
                            <h4 class="__title">
                                Тарифы
                            </h4>
                            <ul class="__list">
                                <li>
                                    1 кг = $69.75
                                </li>
                                <li>
                                    5 кг = $113
                                </li>
                                <li>
                                    10 кг = $165.25
                                </li>
                                <li>
                                    20 кг = $269.75
                                </li>
                                <li>
                                    30 кг = $374.25
                                </li>
                            </ul>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col sm">
                            <h4 class="__title">
                                Вложения
                            </h4>
                            <p class="__content">
                                Одежда и обувь, игрушки, косметика, пищевые добавки, спортивный инвентарь
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Габариты и вес
                            </h4>
                            <p class="__content">
                                Максимальная длина одной посылки не должны превышать 152см, длина плюс охват не должны превышать 274см. Максимально разрешенный вес - 32кг.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Превышение лимитов
                            </h4>
                            <p class="__content">
                                Если сумма вложения или вес превышают разрешенные лимиты, взимается таможенная пошлина в размере до 30% от стоимости превышения, но не менее $6 за килограмм физического веса.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Страхование вложения
                            </h4>
                            <p class="__content">
                                Вложение автоматически бесплатно стразуется USPS от утери на сумму в $100. Предоставляется также возможность приобрести дополнительную страховку BOXFORWARD из расчета $1.6 на каждые $100 стоимости вложения.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Отслеживание посылки
                            </h4>
                            <p class="__content">
                                Доступно на сайте www.usps.com или почтовом сайте страны получаетеля по номеру отслживания, присвоенному поссылке.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col lg">
                            <h4 class="__title">
                                Запрещены к отправке
                            </h4>
                            <p class="__content">
                                Легковоспламеняющиеся и взрывоопасные предметы, химикаты и ртуть, огнестрельно оружие и боеприпасы, наркотичиские вещества, лекарственные препараты и прочие предеты запрещенные к вывозу из США, ввозу страну адресата.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col lg">
                            <h4 class="__title">
                                Таможенное оформление
                            </h4>
                            <p class="__content">
                                Товары оформляются на основе таможенной лекарации, заполняемой получателем в личном кабинете. Таможенные службы оставляют за собой право досмотра вложения и проверки соответствия товара ценам и артикулам, указанным в декларации. Если сумма вложения или вес превышают разрегенные лимиты, взимается таможенная пошлина в соотв. с таможенным кодексом страны адресата.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_p-s">
                        <p class="__content">
                            * Страховка распостроняется только на полную и подтвержденную почтовой службой утрату посылки.
                        </p>
                        <a href="calculator.html" class="__link">
                            Расчитать стоимость
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="delivery-page_content">
        <div class="delivery-page_content_container">
            <div class="delivery-page_content_list">
                <ul class="__list">
                    <li>
                        USPS Priority
                    </li>
                    <li class="active">
                        USPS Express
                    </li>
                    <li>
                        Boxberry
                    </li>
                </ul>
            </div>
            <div class="delivery-page_content_tabs">
                <div class="delivery-page_content_tabs_position">
                    <div class="delivery-page_content_tabs_position_info">
                        <p class="__content">
                            Армения, Азербайджан, Белорусь, Грузия, Казахстан, Кыргызстан, Латвия, Литва, Молдова, Узбекистан, Эстония, Украина, Россия
                        </p>
                        <p class="__deadlines">
                            Сроки от 7 до 14 дней
                        </p>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col logo md">
                            <img src="{{ asset('/box/img/logo-1.svg') }}" alt="logo" class="img-responsive">
                            <p class="__content">
                                Экспресс сервис, предоставляемый USPS в сотрудничестве с почтовыми и курьерскими сервисами других стран, позволяет получать почтовые отправления из США до порога вашего дома.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col sm">
                            <h4 class="__title">
                                Тарифы
                            </h4>
                            <ul class="__list">
                                <li>
                                    1 кг = $69.75
                                </li>
                                <li>
                                    5 кг = $113
                                </li>
                                <li>
                                    10 кг = $165.25
                                </li>
                                <li>
                                    20 кг = $269.75
                                </li>
                                <li>
                                    30 кг = $374.25
                                </li>
                            </ul>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col sm">
                            <h4 class="__title">
                                Вложения
                            </h4>
                            <p class="__content">
                                Одежда и обувь, игрушки, косметика, пищевые добавки, спортивный инвентарь
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Габариты и вес
                            </h4>
                            <p class="__content">
                                Максимальная длина одной посылки не должны превышать 152см, длина плюс охват не должны превышать 274см. Максимально разрешенный вес - 32кг.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Превышение лимитов
                            </h4>
                            <p class="__content">
                                Если сумма вложения или вес превышают разрешенные лимиты, взимается таможенная пошлина в размере до 30% от стоимости превышения, но не менее $6 за килограмм физического веса.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Страхование вложения
                            </h4>
                            <p class="__content">
                                Вложение автоматически бесплатно стразуется USPS от утери на сумму в $100. Предоставляется также возможность приобрести дополнительную страховку BOXFORWARD из расчета $1.6 на каждые $100 стоимости вложения.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Отслеживание посылки
                            </h4>
                            <p class="__content">
                                Доступно на сайте www.usps.com или почтовом сайте страны получаетеля по номеру отслживания, присвоенному поссылке.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col lg">
                            <h4 class="__title">
                                Запрещены к отправке
                            </h4>
                            <p class="__content">
                                Легковоспламеняющиеся и взрывоопасные предметы, химикаты и ртуть, огнестрельно оружие и боеприпасы, наркотичиские вещества, лекарственные препараты и прочие предеты запрещенные к вывозу из США, ввозу страну адресата.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col lg">
                            <h4 class="__title">
                                Таможенное оформление
                            </h4>
                            <p class="__content">
                                Товары оформляются на основе таможенной лекарации, заполняемой получателем в личном кабинете. Таможенные службы оставляют за собой право досмотра вложения и проверки соответствия товара ценам и артикулам, указанным в декларации. Если сумма вложения или вес превышают разрегенные лимиты, взимается таможенная пошлина в соотв. с таможенным кодексом страны адресата.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_p-s">
                        <p class="__content">
                            * Страховка распостроняется только на полную и подтвержденную почтовой службой утрату посылки.
                        </p>
                        <a href="calculator.html" class="__link">
                            Расчитать стоимость
                        </a>
                    </div>
                </div>
                <div class="delivery-page_content_tabs_position active">
                    <div class="delivery-page_content_tabs_position_info">
                        <p class="__content">
                            Армения, Азербайджан, Белорусь, Грузия, Казахстан, Кыргызстан, Латвия, Литва, Молдова, Узбекистан, Эстония, Украина, Россия
                        </p>
                        <p class="__deadlines">
                            Сроки от 7 до 14 дней
                        </p>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col logo md">
                            <img src="{{ asset('/box/img/logo-1.svg') }}" alt="logo" class="img-responsive">
                            <p class="__content">
                                Экспресс сервис, предоставляемый USPS в сотрудничестве с почтовыми и курьерскими сервисами других стран, позволяет получать почтовые отправления из США до порога вашего дома.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col sm">
                            <h4 class="__title">
                                Тарифы
                            </h4>
                            <ul class="__list">
                                <li>
                                    1 кг = $69.75
                                </li>
                                <li>
                                    5 кг = $113
                                </li>
                                <li>
                                    10 кг = $165.25
                                </li>
                                <li>
                                    20 кг = $269.75
                                </li>
                                <li>
                                    30 кг = $374.25
                                </li>
                            </ul>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col sm">
                            <h4 class="__title">
                                Вложения
                            </h4>
                            <p class="__content">
                                Одежда и обувь, игрушки, косметика, пищевые добавки, спортивный инвентарь
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Габариты и вес
                            </h4>
                            <p class="__content">
                                Максимальная длина одной посылки не должны превышать 152см, длина плюс охват не должны превышать 274см. Максимально разрешенный вес - 32кг.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Превышение лимитов
                            </h4>
                            <p class="__content">
                                Если сумма вложения или вес превышают разрешенные лимиты, взимается таможенная пошлина в размере до 30% от стоимости превышения, но не менее $6 за килограмм физического веса.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Страхование вложения
                            </h4>
                            <p class="__content">
                                Вложение автоматически бесплатно стразуется USPS от утери на сумму в $100. Предоставляется также возможность приобрести дополнительную страховку BOXFORWARD из расчета $1.6 на каждые $100 стоимости вложения.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Отслеживание посылки
                            </h4>
                            <p class="__content">
                                Доступно на сайте www.usps.com или почтовом сайте страны получаетеля по номеру отслживания, присвоенному поссылке.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col lg">
                            <h4 class="__title">
                                Запрещены к отправке
                            </h4>
                            <p class="__content">
                                Легковоспламеняющиеся и взрывоопасные предметы, химикаты и ртуть, огнестрельно оружие и боеприпасы, наркотичиские вещества, лекарственные препараты и прочие предеты запрещенные к вывозу из США, ввозу страну адресата.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col lg">
                            <h4 class="__title">
                                Таможенное оформление
                            </h4>
                            <p class="__content">
                                Товары оформляются на основе таможенной лекарации, заполняемой получателем в личном кабинете. Таможенные службы оставляют за собой право досмотра вложения и проверки соответствия товара ценам и артикулам, указанным в декларации. Если сумма вложения или вес превышают разрегенные лимиты, взимается таможенная пошлина в соотв. с таможенным кодексом страны адресата.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_p-s">
                        <p class="__content">
                            * Страховка распостроняется только на полную и подтвержденную почтовой службой утрату посылки.
                        </p>
                        <a href="calculator.html" class="__link">
                            Расчитать стоимость
                        </a>
                    </div>
                </div>
                <div class="delivery-page_content_tabs_position">
                    <div class="delivery-page_content_tabs_position_info">
                        <p class="__content">
                            Армения, Азербайджан, Белорусь, Грузия, Казахстан, Кыргызстан, Латвия, Литва, Молдова, Узбекистан, Эстония, Украина, Россия
                        </p>
                        <p class="__deadlines">
                            Сроки от 7 до 14 дней
                        </p>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col logo md">
                            <img src="{{ asset('/box/img/logo-1.svg') }}" alt="logo" class="img-responsive">
                            <p class="__content">
                                Экспресс сервис, предоставляемый USPS в сотрудничестве с почтовыми и курьерскими сервисами других стран, позволяет получать почтовые отправления из США до порога вашего дома.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col sm">
                            <h4 class="__title">
                                Тарифы
                            </h4>
                            <ul class="__list">
                                <li>
                                    1 кг = $69.75
                                </li>
                                <li>
                                    5 кг = $113
                                </li>
                                <li>
                                    10 кг = $165.25
                                </li>
                                <li>
                                    20 кг = $269.75
                                </li>
                                <li>
                                    30 кг = $374.25
                                </li>
                            </ul>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col sm">
                            <h4 class="__title">
                                Вложения
                            </h4>
                            <p class="__content">
                                Одежда и обувь, игрушки, косметика, пищевые добавки, спортивный инвентарь
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Габариты и вес
                            </h4>
                            <p class="__content">
                                Максимальная длина одной посылки не должны превышать 152см, длина плюс охват не должны превышать 274см. Максимально разрешенный вес - 32кг.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Превышение лимитов
                            </h4>
                            <p class="__content">
                                Если сумма вложения или вес превышают разрешенные лимиты, взимается таможенная пошлина в размере до 30% от стоимости превышения, но не менее $6 за килограмм физического веса.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Страхование вложения
                            </h4>
                            <p class="__content">
                                Вложение автоматически бесплатно стразуется USPS от утери на сумму в $100. Предоставляется также возможность приобрести дополнительную страховку BOXFORWARD из расчета $1.6 на каждые $100 стоимости вложения.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col md">
                            <h4 class="__title">
                                Отслеживание посылки
                            </h4>
                            <p class="__content">
                                Доступно на сайте www.usps.com или почтовом сайте страны получаетеля по номеру отслживания, присвоенному поссылке.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_row">
                        <div class="delivery-page_content_tabs_position_row_col lg">
                            <h4 class="__title">
                                Запрещены к отправке
                            </h4>
                            <p class="__content">
                                Легковоспламеняющиеся и взрывоопасные предметы, химикаты и ртуть, огнестрельно оружие и боеприпасы, наркотичиские вещества, лекарственные препараты и прочие предеты запрещенные к вывозу из США, ввозу страну адресата.
                            </p>
                        </div>
                        <div class="delivery-page_content_tabs_position_row_col lg">
                            <h4 class="__title">
                                Таможенное оформление
                            </h4>
                            <p class="__content">
                                Товары оформляются на основе таможенной лекарации, заполняемой получателем в личном кабинете. Таможенные службы оставляют за собой право досмотра вложения и проверки соответствия товара ценам и артикулам, указанным в декларации. Если сумма вложения или вес превышают разрегенные лимиты, взимается таможенная пошлина в соотв. с таможенным кодексом страны адресата.
                            </p>
                        </div>
                    </div>
                    <div class="delivery-page_content_tabs_position_p-s">
                        <p class="__content">
                            * Страховка распостроняется только на полную и подтвержденную почтовой службой утрату посылки.
                        </p>
                        <a href="calculator.html" class="__link">
                            Расчитать стоимость
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection