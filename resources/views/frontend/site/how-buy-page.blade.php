@extends('frontend.layouts.box')

@section('title', app_name() . ' | '.__('navs.general.home'))
@section('content')
    <section class="how-buy-page-title">
        <div class="how-buy-page-title_container">
            <h2 class="__title">
                Как покупать в магазинах США
            </h2>
            <p class="__describe">
                Поздравляем, вы сделали правильный выбор! Шоппинг в США - это море удовольствия.
            </p>
            <p class="__describe">
                Всего 4 простых шага и заветная покупка окажется у Вас!
            </p>
        </div>
    </section>
    <section class="how-buy-page_content">
        <div class="how-buy-page_content_container">
            <div class="how-buy-page_content_row">
                <div class="how-buy-page_content_position first">
                    <div class="__number">
                        1
                    </div>
                    <h3 class="__title">
                        <span class="__text">Регистрация</span>
                    </h3>
                    <p class="__content __static __content-sm">
                        Для покупок в США вам понадобится местный адрес, на который американские магазины будут высылать товар.
                    </p>
                    <button class="__more" type="button">
                        read more...
                    </button>
                    <p class="__content __content-sm">
                        Желательно, что бы этот адрес был в штате, в котором не взымается налог с продаж. Компания BXFWD предоставляет вам такой адрес СОВЕРШЕННО БЕСПЛАТНО, в двух штатах - Делавер (полное отсутствие налогов) и Нью-джерси (отсутствие налогов на одежду и обувь).
                    </p>
                    <p class="__content __content-md">
                        После регистрации мы предоставляем Вам почтовый адрес в США и доступ к личному кабинету на нашем сайте, в котором вы сможете отслеживать Ваши заказы, консолидировать посылки и оплачивать услуги.
                    </p>
                    <button class="__close" type="button">
                        close
                    </button>
                </div>
                <div class="how-buy-page_content_position second">
                    <div class="__number">
                        2
                    </div>
                    <h3 class="__title">
                        <span class="__text">Покупка товаров</span>
                    </h3>
                    <p class="__content __static">
                        Приступаем к покупкам! Для начала выберите интересующий Вас товар или магазин. В этом Вам поможет наш каталог товаров и список магазинов.
                    </p>
                    <button class="__more" type="button">
                        read more...
                    </button>
                    <p class="__content">
                        Для наиболее популярных магазинов, мы составили подробные инструкции, которые шаг за шагом помогут Вам совершать покупку - от регистрации на сайте магазина до отправки товара на наш склад.
                    </p>
                    <p class="__content">
                        Для покупок Вам также необходима банковская карта (кредитная или дебетовая) или счет в электронном кошельке PayPal. Неоторые магазины США не принимают к оплате карты, не выпущенные в США. В подобном случае, Вы всегда можете обратиться за помощью к нашему партнеру-посреднику, который поможет выкупить понравившийся Вам товар.
                    </p>
                    <button class="__close" type="button">
                        close
                    </button>
                </div>
            </div>
            <div class="how-buy-page_content_row">
                <div class="how-buy-page_content_position third">
                    <div class="__number">
                        3
                    </div>
                    <h3 class="__title">
                        <span class="__text">Хранение и консолидация</span>
                    </h3>
                    <p class="__content __static">
                        После оплаты заказа, магазин высылает товар на указанный Вами адрес склада. Мы совершенно бесплатно примем товар на хранение, сфотографируем его и с удовольствием подождем остальных Ваших заказов из других магазинов. Ведь консолидируя товары из разных магазинов в одну поссылку, Вы сможете существенно сэкономить на стоимости пересылки товара. Проверьте это на нашем калькуляторе доставки.
                    </p>
                    <button class="__more" type="button">
                        read more...
                    </button>
                    <p class="__content">
                        Как только Вы будете готовы отправить закаы, Вы сможете отправить посылку на консолидацию, используя личный кабинет для выбора одного из множества возможных способов доставки и указания Ваших пожеланий по упаковке.
                    </p>
                    <p class="__content">
                        Все сопроводительные документы Вы тоже сможете оформить в личном кабинете. Мы гарнтируем быстрое выполнение Вашей просьбы, качественную упаковку и подготовку посылки к дальнему путешествию. В случае необходимости, Вы всегда можете обратится в службу поддержки с вопросом о правильности оформления сопроводительных документов или за помощью в выборе лучшего способа отправки для Вашей посылки
                    </p>
                    <button class="__close" type="button">
                        close
                    </button>
                </div>
                <div class="how-buy-page_content_position fourth">
                    <div class="__number">
                        4
                    </div>
                    <h3 class="__title">
                        <span class="__text">Отправка и доставка</span>
                    </h3>
                    <p class="__content __static">
                        Приступаем к покупкам! Для начала выберите интересующий Вас товар или магазин. В этом Вам поможет наш каталог товаров и список магазинов.
                    </p>
                    <button class="__more" type="button">
                        read more...
                    </button>
                    <p class="__content">
                        Для наиболее популярных магазинов, мы составили подробные инструкции, которые шаг за шагом помогут Вам совершить покупку - от регистрации на сайте магазина до отправки товара на наш склад.
                    </p>
                    <p class="__content">
                        Для покупок Вам также необходима банковская карта (кредитная или дебетовая) или счет в электронном кошельке PayPal. Неоторые магазины США не принимают к оплате карты, не выпущенные в США. В подобном случае, Вы всегда можете обратиться за помощью к нашему партнеру-посреднику, который поможет выкупить понравившийся Вам товар.
                    </p>
                    <button class="__close" type="button">
                        close
                    </button>
                </div>
            </div>
        </div>
        <div class="how-buy-page_content_bgi">
            <img src="{{ asset('/box/img/girl-01.svg') }}" alt="loading" class="img-responsive">
        </div>
    </section>
@endsection