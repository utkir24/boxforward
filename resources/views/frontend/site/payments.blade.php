@extends('frontend.layouts.box')

@section('title', app_name() . ' | '.__('navs.general.home'))
@section('content')
    <section class="payments-title">
        <div class="payments-title_container">
            <h2 class="__title">
                Способы оплаты
            </h2>
            <p class="__describe">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam officia impedit, voluptas aliquid nisi eaque!
            </p>
        </div>
    </section>
    <section class="home-payments-method">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="home-payments-method_container">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="home-payments-method_position">
                                    <div class="home-payments-method_position_img">
                                        <img src="{{ asset('/box/img/visa.png') }}" srcset="{{ asset('/box/img/visa@2x.png') }} 2x, {{ asset('/box/img/visa@3x.png') }} 3x" class="img-responsive __visa">
                                        <img src="{{ asset('/box/img/mc.png') }}" srcset="{{ asset('/box/img/mc@2x.png') }} 2x, {{ asset('/box/img/mc@3x.png') }} 3x" class="img-responsive __mc">
                                    </div>
                                    <div class="home-payments-method_position_describe">
                                        <p class="__content">
                                            We accept credit card payments using latest security standards, such as 3D Secure
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="home-payments-method_position">
                                    <div class="home-payments-method_position_img">
                                        <img src="{{ asset('/box/img/qiwi.png') }}" srcset="{{ asset('/box/img/qiwi@2x.png') }} 2x, {{ asset('/box/img/qiwi@3x.png') }} 3x" class="img-responsive">
                                    </div>
                                    <div class="home-payments-method_position_describe">
                                        <p class="__content">
                                            Qiwi is an electronic wallet which offers instantaneous payments and convenient account refill options through physical terminals and phone applications
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="home-payments-method_position">
                                    <div class="home-payments-method_position_img">
                                        <img src="{{ asset('/box/img/pay-pal.png') }}" srcset="{{ asset('/box/img/pay-pal@2x.png') }} 2x, {{ asset('/box/img/pay-pal@3x.png') }} 3x" class="img-responsive">
                                    </div>
                                    <div class="home-payments-method_position_describe">
                                        <p class="__content">
                                            PayPal is a gold standard of electronic transactions, offering an unmatched simplicity and security using only your e-mail address and password
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection