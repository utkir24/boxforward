@extends('frontend.layouts.box')

@section('title', app_name() . ' | '.$page->title)
@section('content')
    {!! $page->content !!}
@endsection
