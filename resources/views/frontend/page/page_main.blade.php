@extends('frontend.layouts.box')
@section('title', app_name() . ' | '.$page->title)
@include('frontend.seo.seo',['seos'=>$page->seo])
@section('description',$page->description)

@php(app('widget'))
@section('content')
    <section class="home-short-info">
        <div class="home-short-info_container">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="home-short-info_describe">
                            <div class="home-short-info_describe_head-title">
                                <h4 class="__title">
                                   {{$page->description}}
                                </h4>
                            </div>
                            <div class="home-short-info_describe_title">
                                <h1 class="__title">{{$page->title}}</h1>
                            </div>
                            <div class="home-short-info_describe_step-title">
                                <h3 class="__title">
                                    {{$page->small_title}}
                                </h3>
                                <p class="__describe">
                                    {!! $page->content !!}
                                </p>
                                <img src="img/top-lines.svg" alt="" class="img-responsive">
                            </div>
                            @if(!empty($page->widgets))
                                @foreach($page->widgets as $widget)
                                    @if($widget->type === \App\Models\Widget\Widget::TYPE_HOME_SHORT_INFO)
                                         @include('frontend.widgets.home_short_info',['widget'=>$widget])
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="home-short-info_img">
                            <img src="{{$page->image}}" srcset="img/girl-01@2x.png 2x, img/girl-01@3x.png 3x" class="img-responsive __girl-img">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="home-short-info_bgi-imgs">
            <div class="home-short-info_bgi-imgs_cloud">
                <svg  viewBox="0 0 1483 1118" preserveAspectRatio="xMinYMin" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="00-Landing" fill="#fff">
                            <path d="M700.560004,1117.46986 L700.387912,1117.98387 L-276.248178,1117.98387 L-276.248178,-141.041793 L1319.77251,-141.275958 C1323.92065,-141.620744 1328.11676,-141.796586 1332.35395,-141.796586 C1336.57601,-141.796586 1340.75729,-141.621997 1344.89097,-141.279644 L1364.81599,-141.282567 L1363.68787,-138.515926 C1431.41552,-124.109157 1482.22118,-63.9524039 1482.22118,8.0706474 C1482.22118,90.840035 1415.12333,157.937881 1332.35395,157.937881 C1318.01,157.937881 1304.13673,155.922737 1291.00177,152.160107 C1308.90879,180.402864 1319.27727,213.897266 1319.27727,249.813358 C1319.27727,350.669325 1237.51736,432.42924 1136.66139,432.42924 C1101.99386,432.42924 1069.58257,422.769107 1041.97248,405.993801 C1042.75849,413.029126 1043.16213,420.179984 1043.16213,427.425117 C1043.16213,496.568349 1006.40046,557.125435 951.35614,590.61737 C1027.51245,658.225129 1075.50132,756.862705 1075.50132,866.708682 C1075.50132,963.52743 1038.22002,1051.63866 977.229932,1117.46986 C835.131463,1117.46986 787.490672,1117.46986 700.559928,1117.46986 Z" id="cloud_top_white"></path>
                        </g>
                    </g>
                </svg>
            </div>
            <div class="home-short-info_bgi-imgs_line">
                <svg viewBox="0 0 1492 1125" version="1.1"  preserveAspectRatio="xMinYMin" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="00-Landing" stroke="#0080FF" stroke-width="2">
                            <path d="M708.587002,1123.28257 L708.414911,1123.79659 L-268.22118,1123.79659 L-268.22118,-135.229081 L1327.79951,-135.463247 C1331.94765,-135.808032 1336.14376,-135.983874 1340.38094,-135.983874 C1344.603,-135.983874 1348.78429,-135.809285 1352.91796,-135.466932 L1372.84298,-135.469855 L1371.71487,-132.703214 C1439.44251,-118.296445 1490.24818,-58.139692 1490.24818,13.8833593 C1490.24818,96.6527469 1423.15033,163.750593 1340.38094,163.750593 C1326.037,163.750593 1312.16372,161.735449 1299.02877,157.972819 C1316.93578,186.215576 1327.30427,219.709978 1327.30427,255.626069 C1327.30427,356.482037 1245.54435,438.241952 1144.68839,438.241952 C1110.02085,438.241952 1077.60957,428.581819 1049.99948,411.806513 C1050.78549,418.841838 1051.18913,425.992696 1051.18913,433.237829 C1051.18913,502.381061 1014.42746,562.938146 959.383138,596.430081 C1035.53945,664.03784 1083.52832,762.675417 1083.52832,872.521394 C1083.52832,969.340142 1046.24701,1057.45137 985.25693,1123.28257 C843.158461,1123.28257 795.51767,1123.28257 708.586927,1123.28257 Z" id="cloud_top"></path>
                        </g>
                    </g>
                </svg>
            </div>
            <div class="home-short-info_bgi-imgs_city">
                <svg width="1915px" height="1080px" viewBox="0 0 1915 1080" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <g id="00-Landing" transform="translate(-5.000000, -28.000000)" fill-rule="nonzero">
                            <g id="city1" transform="translate(5.000000, 28.000000)">
                                <rect id="Rectangle-path" fill="#E2EFDF" x="0" y="0" width="1920" height="1080"></rect>
                                <path d="M1920,643.52 L1886.45,643.52 L1831.13,547.11 L1775.82,643.52 L1719.45,643.52 L1719.45,943.88 L1693.7,943.88 L1693.7,643.52 L1613.87,643.52 L1613.87,943.88 L1597.57,943.88 L1597.57,456.08 L1580,456.08 C1572.79488,437.564563 1555.41908,424.980604 1535.58,423.91 L1532.29,286.29 L1529,424 C1509.66147,425.548949 1492.91112,438.018485 1485.88,456.1 L1463.67,456.1 L1463.67,748 L1432.77,748 L1432.77,689.3 L1364.1,689.3 L1364.1,724.71 L1299.72,724.71 L1299.72,775.71 L1261.1,775.71 L1261.1,943.91 L1244.79,943.91 L1244.79,569.24 L1160.67,569.24 L1160.67,617.67 L1154.67,617.67 L1125.19,566.27 L1095.7,617.67 L1036.18,617.67 L1036.18,535.55 L988.16,535.55 L985.1,407.82 L982,535.55 L945.92,535.55 L945.92,943.88 L892,943.88 L892,709.17 L796.56,709.17 L796.56,943.88 L728.26,943.88 L728.26,645.25 L644.64,645.25 L644.64,943.88 L535.81,943.88 L535.81,544.19 C535.886722,522.007925 518.677219,503.602039 496.54,502.19 L494,396 L491.45,502.21 C469.312781,503.622039 452.103278,522.027925 452.18,544.21 L452.18,943.88 L434.52,943.88 L434.52,612.18 L350.9,612.18 L350.9,943.88 L238.62,943.88 L238.62,734.22 L168.94,734.22 L168.94,943.88 L156.22,943.88 L156.22,643.52 L112.16,643.52 L108.58,494.37 L105,643.52 L60.41,643.52 L60.41,712.62 L0,712.62 L0,1080 L1920,1080 L1920,643.52 Z" id="Shape" fill="#C3DBE9"></path>
                                <polygon id="Shape" fill="#AED7B5" points="1920 802.27 1791.55 802.27 1791.55 1047.38 1710.23 1047.38 1710.23 815.11 1569.67 815.11 1569.67 1047.38 1539.55 1047.38 1539.55 777.59 1399 777.59 1399 1047.38 1362.86 1047.38 1362.86 677.87 1273.11 677.87 1273.11 759.82 1234.96 759.82 1234.96 1047.38 1187.16 1047.38 1187.16 842.76 1097.42 842.76 1097.42 1047.38 967.28 1047.38 967.28 863.49 845.8 863.49 845.8 804.25 695.83 804.25 695.83 850.65 560.29 850.65 560.29 1047.38 462.29 1047.38 462.29 921.74 437.19 921.74 437.19 869.41 358.49 869.41 358.49 994.8 277.17 994.81 277.17 790.43 235.07 790.43 235.07 676.41 103.71 676.41 103.71 1047.38 74.92 1047.38 74.92 777.59 0 777.59 0 1080 1920 1080"></polygon>
                                <polygon id="Shape" fill="#A7C5DE" points="30.23 925.87 18.44 925.87 18.44 913.34 0 913.34 0 1080 30.23 1080"></polygon>
                                <rect id="Rectangle-path" fill="#A7C5DE" x="43.51" y="889.32" width="61.67" height="190.68"></rect>
                                <rect id="Rectangle-path" fill="#A7C5DE" x="114.26" y="980.17" width="76.27" height="99.83"></rect>
                                <rect id="Rectangle-path" fill="#A7C5DE" x="197.83" y="809.97" width="79.37" height="270.03"></rect>
                                <polygon id="Shape" fill="#A7C5DE" points="1857.71 937.36 1805.86 937.36 1805.86 966.59 1783.99 966.59 1783.99 832.41 1704.61 880.45 1704.61 1080 1857.71 1080"></polygon>
                                <rect id="Rectangle-path" fill="#A7C5DE" x="1603.36" y="809.97" width="79.38" height="270.03"></rect>
                                <rect id="Rectangle-path" fill="#A7C5DE" x="1519.81" y="980.17" width="79.38" height="99.83"></rect>
                                <polygon id="Shape" fill="#A7C5DE" points="1240.4 830.85 1156.85 830.85 1156.85 947.79 1123.43 947.79 1123.43 1080 1240.4 1080"></polygon>
                                <rect id="Rectangle-path" fill="#A7C5DE" x="971.13" y="889.32" width="79.35" height="190.68"></rect>
                                <polygon id="Shape" fill="#A7C5DE" points="947.65 980.17 921.11 980.17 921.11 901.85 838.42 901.85 838.42 941.53 811.95 941.53 811.95 774.46 729.25 774.46 729.25 1080 947.65 1080"></polygon>
                                <rect id="Rectangle-path" fill="#A7C5DE" x="557.23" y="856.44" width="82.68" height="223.56"></rect>
                                <polygon id="Shape" fill="#A7C5DE" points="1435.77 925.87 1423.97 925.87 1423.97 913.34 1403.09 913.34 1403.09 906.03 1385.4 906.03 1385.4 889.32 1343.62 889.32 1343.62 947.79 1277.76 947.79 1277.76 1080 1435.77 1080"></polygon>
                                <rect id="Rectangle-path" fill="#A7C5DE" x="1449.04" y="889.32" width="61.68" height="190.68"></rect>
                                <polygon id="Shape" fill="#A7C5DE" points="452.17 937.36 400.32 937.36 400.32 966.59 378.45 966.59 378.45 832.41 299.07 880.45 299.08 1080 452.17 1080"></polygon>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>
        </div>
    </section>
    @if(!empty($page->widgets))
        @foreach($page->widgets as $widget)
            @widget('page',['widget'=>$widget])
        @endforeach
    @endif
@endsection