@extends('frontend.layouts.box')
@php(app('widget'))
@section('title', app_name() . ' | '.__('navs.general.home'))
@section('description',$page->description)

@include('frontend.seo.seo',['seos'=>$page->seo])
@section('content')
    <section class="faq-page-main">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="faq-page-main_tabs">
                        <div class="faq-page-main_tabs_position">
                            <div class="row">
                                <div class="col-md-8 col-sm-9">
                                    <div class="faq-page-main_tabs_positions_tabs">
                                        <div class="faq-page-main_tabs_position_content active">
                                            <div class="faq-page-main_tabs_position_content_title">
                                                <h2 class="__title-main title-faq">
                                                    {{$page->title}}
                                                </h2>
                                                <p class="__content">
                                                    {{$page->description}}
                                                </p>
                                            </div>
                                            <div class="faq-page-main_tabs_position_content_main">
                                                {!! $page->content !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if(!empty($page->widgets))
        @foreach($page->widgets as $widget)
            @widget('page',['widget'=>$widget])
        @endforeach
    @endif
@endsection

