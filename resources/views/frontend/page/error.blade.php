@extends('frontend.layouts.box')

@section('title', app_name() . ' | '.__('navs.general.home'))
@section('content')
    <section class="faq-page-main">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="faq-page-main_tabs">
                        <div class="faq-page-main_tabs_position">
                            <div class="row">
                                <div class="col-md-8 col-sm-9">
                                    <div class="faq-page-main_tabs_positions_tabs">
                                        <div class="faq-page-main_tabs_position_content active">
                                            <div class="faq-page-main_tabs_position_content_title">
                                                <h2 class="__title-main title-faq">
                                                    {{__('page.frontend.text.error.title')}}
                                                </h2>
                                                <p class="__content">
                                                    {{__('page.frontend.text.error.description')}}
                                                </p>
                                            </div>
                                            <div class="faq-page-main_tabs_position_content_main">
                                                {{__('page.frontend.text.error.content')}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

