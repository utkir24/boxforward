@extends('frontend.layouts.box')
@include('frontend.seo.seo',['seos'=>$page->seo])
@section('title', app_name() . ' | '.$page->title)
@section('description',$page->description)
@php(app('widget'))
@section('content')
    <section class="cashback-title">
        <div class="cashback-title_container">
            <h2 class="__title">
                {{$page->title}}
            </h2>
            <p class="__describe">
                {{$page->description}}
            </p>
        </div>
    </section>
    <section class="cashback-main">
        <div class="container">
            <div class="row">
                <div class="main_content" style="background-image:url({{$page->image}});!important;">
                    {!!$page->content!!}
                </div>
            </div>
        </div>
    </section>
    @if(!empty($page->widgets))
        @foreach($page->widgets as $widget)
            @widget('page',['widget'=>$widget])
        @endforeach
    @endif
@endsection
