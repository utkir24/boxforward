@extends('frontend.layouts.box')
@php(app('widget'))
@section('title', app_name() . ' | '.__('navs.general.home'))
@section('description',$page->description)

@include('frontend.seo.seo',['seos'=>$page->seo])

@section('content')
<section class="prices-title">
    <div class="prices-title_container">
        <h2 class="__title">
            {{$page->title}}
        </h2>
        <p class="__describe">
            {!! $page->content !!}
        </p>
    </div>
</section>
    @if(!empty($page->widgets))
        @foreach($page->widgets as $widget)
            @widget('page',['widget'=>$widget])
        @endforeach
    @endif
@endsection

