<div class="home-articles_position">
    @foreach($categories as $cat)
        <div class="home-articles_position_list">
            <ul class="__list news">
                <h3 class="__title">
                    <a href="{{ $cat->single_link }}">
                        {{ $cat->title  }}
                    </a>
                </h3>
                @foreach($cat->pages()->sortupdated()->active()->lang()->take(4)->get() as $item)
                    <li>
                        <a href="{{ $item->singleLinkForCategory()}}">
                            <div class="__container-img">
                                <img src="{{ $item->image_main}}" srcset="{{ $item->image_main}} 2x, {{ $item->image_main}} 3x" alt="{{  $item->title }}">
                            </div>
                            <div class="__container-content">
                                <h5 class="__title">
                                    {{ $item->title }}
                                </h5>
                                <div class="__content">
                                    {{$item->created_at}}
                                </div>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    @endforeach
</div>