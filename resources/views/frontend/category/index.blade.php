
@extends('frontend.layouts.box')

@section('title', app_name() . ' | '.__('navs.general.home'))
@section('content')
    <section class="faq-page-main single">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <h1>{{ $category->title }}</h1>
                    <p class="__describe">{{$category->description}}</p>
                    @foreach($posts as $post)
                        <div class="col-sm-12 cat">
                            <div class="home-service-info_position">
                                <h3 class="__title">{{ $post->title  }}</h3>
                                <p class="__describe">
                                    {{ $post->description }}
                                </p>
                                <a href="{{$post->singleLinkForCategory()}}" class="btn">
                                    {{ __('Подробнее') }}
                                </a>
                            </div>
                        </div>
                    @endforeach
                    {{ $posts->links() }}
                </div>
                <div class="col-md-4 col-sm-4">
                    @include('frontend.category._sidebar',['categories'=>$categories])
                </div>
            </div>
        </div>
    </section>
@endsection

