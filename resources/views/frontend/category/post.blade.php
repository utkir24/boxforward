
@extends('frontend.layouts.box')

@section('title', app_name() . ' | '.__('navs.general.home'))
@section('content')
    <section class="faq-page-main single">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <h1>{{ $post->title }}</h1>
                   {!! $post->content !!}
                </div>
                <div class="col-md-4 col-sm-4">
                    @include('frontend.category._sidebar',['categories'=>$categories])
                </div>
            </div>
        </div>
    </section>

@endsection

