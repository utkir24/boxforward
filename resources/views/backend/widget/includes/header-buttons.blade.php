<div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
    <a href="#myModal" class="btn-widget-modal btn btn-success ml-1" data-toggle="modal" title="{{ __('widget.backend.menu.create') }}">
        <i class="fas fa-plus-circle"></i>
    </a>
</div><!--btn-toolbar-->


<!-- Modal HTML -->

<div id="myModal" class="modal fade bs-example-modal-lg">

    <div class="modal-dialog modal-lg">

        <div class="modal-content" style="height: 600px;overflow: auto;">

            <div class="modal-header">


                <h4 class="modal-title">Widgets template</h4>

            </div>

            <div class="modal-body">

                <div class="row">
                    @foreach(App\Models\Widget\Widget::getTypes() as $type => $value)
                    <div class="col-md-4 col-sm-4 col-xs-4">
                       <a href="{{ route('admin.widget.create',['type'=>$type]) }}">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">{{$value}}</h3>
                            </div>
                            <div class="panel-body"><img src="{{\App\Models\Widget\Widget::getTypeImages($type)}}" class="img-responsive" alt="{{$value}}"></div>
                        </div>
                       </a>
                    </div>
                   @endforeach
                </div>
            </div>
        </div>

    </div>

</div>

@section('js')


    <script type="text/javascript">

        $(document).ready(function(){

            $(".btn-widget-modal").click(function(){

                $("#myModal").modal('show');

            });

        });

    </script>


@endsection
<style>
    .panel-primary > .panel-heading {
        color: #fff;
        background-color: #29363d;
        border-color: #337ab7;
    }
</style>

