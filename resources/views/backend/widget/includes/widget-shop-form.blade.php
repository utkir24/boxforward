<div class="form-group">
    <div id="tree">
        {{__('page.backend.validation.attributes.Category')}}
        <ul id="treeData">
            {{html()->hidden('categories')->class('cat-fa')}}
            @if(!isset($widget))
                {{\App\Models\Category\Category::renderFancyPage(App\Models\Category\Category::TYPE_SHOPS)}}
            @else
                {{\App\Models\Category\Category::renderFancy(App\Models\Category\Category::TYPE_SHOPS,array_pluck($widget->categories,'id'))}}
            @endif
        </ul>
    </div>
</div>