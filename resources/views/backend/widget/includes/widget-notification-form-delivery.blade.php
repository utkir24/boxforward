<div class="container root-col" id="accordions-bonuses" data-widget-id="{{ isset($widget) ? $widget->widget_id : 0 }}">
    <div class="row" >
        <div class="col-sm-12" v-for="(widget, index) in widgets">
            <div class="card">
                <div class="card-header" :id="getParentHeadingName(index)">
                    <h5 class="mb-0">
                        <button @click="clikedNotification" class="btn btn-link"
                                type="button" data-toggle="collapse"
                                :data-target="getParentCollapseName('#',index)" aria-expanded="false"
                                :aria-controls="getParentCollapseName('',index)">
                            <span class="label">Notification Block @{{ index +1 }}</span>
                        </button>
                        <button type="button" class="btn btn-danger btn-sm" @click="deleteParent(index)">Delete</button>
                    </h5>
                </div>
                <div :id="getParentCollapseName('',index)" class="collapse show" :aria-labelledby="getParentHeadingName(index)" data-parent="#accordions-bonuses">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header" :id="getChildHeadingName(index + 'p',index)">
                                        <h5 class="mb-0">
                                            <button type="button" @click="clikedNotification" class="btn btn-link collapsed" data-toggle="collapse" :data-target="getChildCollapseName('#',index + 'p',index)" aria-expanded="false" :aria-controls="getChildCollapseName('',index + 'p',index)">
                                                <span class="label">Notification Parent  :  @{{ widget.title}}</span>
                                            </button>
                                            <button type="button" class="btn btn-danger btn-sm" @click="deleteParent(index)">Delete</button>
                                        </h5>
                                    </div>
                                    <div :id="getChildCollapseName('',index + 'p',index)" class="collapse" :aria-labelledby="getChildHeadingName(index + 'p',index)" :data-parent="getParentCollapseName('#',index)">
                                        <div class="card-body">
                                            <div class="container">
                                           <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                @if(isset($widget))
                                                    <div style="display:none;" class="form-group" v-html="getParentHiddenInput(index,'id',widget.id)" ></div>
                                                @endif
                                                <div class="form-group" v-html="getParentInput('Title',index,'title',widget.title,true)" ></div>
                                                <div class="form-group" v-html="getParentInput('Description',index,'description',widget.description,true)"></div>
                                                    <div class="form-group" v-html="getParentImageInput(index,'images',widget)"></div>
                                                <div class="form-group" v-html="getParentInput('Content',index,'content',widget.content,false)"></div>

                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <label class="label">Extra Fileds</label>
                                                <div  v-for="(extraField, extraFieldIndex) in widget.extraFields">
                                                    <div class="form-group input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">@{{ extraFieldIndex + 1 }}</span>
                                                        </div>
                                                        <input type="text" class="form-control" placeholder="Name" v-model="extraField.name" :name="getParentInputExtraName(index,extraFieldIndex,'name')">
                                                        <input type="text" class="form-control" placeholder="Value" v-model="extraField.value" :name="getParentInputExtraName(index,extraFieldIndex,'value')">
                                                        <button type="button" @click="deleteExtrafield(index,extraFieldIndex)" class="btn btn-danger">Delete</button>
                                                    </div>
                                                </div>
                                                <button type="button" @click="addExtraFiled(index)" class="is-small button is-primary">Add Filed</button>
                                            </div>
                                           </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6" v-for="(child, childIndex) in widget.children">
                                <div class="card">
                                    <div class="card-header" :id="getChildHeadingName(childIndex,index)">
                                        <h5 class="mb-0">
                                            <button type="button" @click="clikedNotification" class="btn btn-link collapsed" data-toggle="collapse" :data-target="getChildCollapseName('#',childIndex,index)" aria-expanded="false" :aria-controls="getChildCollapseName('',childIndex,index)">
                                                <span class="label">Notification Child  :  @{{ child.title}}</span>
                                            </button>
                                            <button type="button" class="btn btn-danger btn-sm" @click="deleteChild(index,childIndex)">Delete</button>
                                        </h5>
                                    </div>
                                    <div :id="getChildCollapseName('',childIndex,index)" class="collapse" :aria-labelledby="getChildHeadingName(childIndex,index)" :data-parent="getParentCollapseName('#',index)">
                                        <div class="card-body">
                                            @if(isset($widget))
                                                <div style="display:none;" class="form-group" v-html="getChildrenHiddenInput(index,childIndex,'id',child.id)" ></div>
                                            @endif
                                            <div v-html="getChildrenInput(index,'Title',childIndex,'title',child.title,true)" class="form-group"></div>
                                                <div  class="form-group">
                                                    <label class="label">@{{ child.title }}</label>
                                                    <div class="control">
                                                        <select class="form-control form-control-sm"
                                                                v-model="child.description"
                                                                :name="getChildrenInputName(index, childIndex, 'description')"
                                                                v-on:change="onChangeChildrenInputSelect(index,$event)"
                                                                style="height: 30px;">
                                                            <option  v-for="option in ClassList" v-bind:value="option.id" >@{{ option.title }}(@{{ option.id }})</option>
                                                        </select>

                                                    </div>
                                                </div>
                                            <div v-html="getChildrenInput(index,'Content',childIndex,'content',child.content,false)" class="form-group"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" @click="addWidgetChildren(index)" class=" is-small button is-primary">Add child</button>
                    </div>
                </div>
            </div>
        </div>
        <button type="button" @click="addWidget" class="is-small button is-primary">Add Notification Block</button>
    </div>
</div>

