<div class="container root-col" id="widget-categories" data-widget-id="{{ isset($widget) ? $widget->widget_id : 0 }}">
    <div class="row" >
        <div class="col-sm-6" v-for="(itemWidget, index) in widgetCat">
            <div class="card">
                <div class="card-header" :id="getParentHeadingName(index + 'p',index)">
                    <h5 class="mb-0">
                        <button type="button" @click="clickedWidgetCat" class="btn btn-link collapsed" data-toggle="collapse" :data-target="getParentCollapseName('#',index + 'p',index)" aria-expanded="false" :aria-controls="getParentCollapseName('',index + 'p',index)">
                           <span class="label">Category Block @{{ index +1 }}</span>
                        </button>
                        <button type="button" class="btn btn-danger btn-sm" @click="deleteWidgetCat(index)">Delete</button>
                    </h5>
                </div>
                <div :id="getParentCollapseName('',index + 'p',index)" class="collapse" :aria-labelledby="getParentHeadingName(index + 'p',index)" data-parent="#widget-categories">
                    <div class="card-body">
                        <div class="form-group row">
                            <label  class="col-md-12 form-control-label">Category</label>
                            <div class="col-md-12">

                                <select class="form-control form-control-sm" v-model="itemWidget.category_id" v-bind:name="getCatInputName(index,'category_id')" v-on:change="onChangeWidgetCat(index,$event)"  style="height: 30px;">
                                    <option  v-for="option in list" v-bind:value="option.id" >@{{ option.title }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-md-12 form-control-label">Count</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" v-model="itemWidget.count" v-bind:name="getCatInputName(index,'count')">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
        <button type="button" @click="addWidgetCat" class="is-small button is-primary">Add Category Block</button>
        </div>
    </div>
</div>

