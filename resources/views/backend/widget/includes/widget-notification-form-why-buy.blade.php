<div class="container root-col" id="accordions-bonuses" data-widget-id="{{ isset($widget) ? $widget->widget_id : 0 }}">
    <div class="row" >
        <div class="col-sm-6" v-for="(widget, index) in widgets">
            <div class="card">
                <div class="card-header" :id="getParentHeadingName(index + 'p',index)">
                    <h5 class="mb-0">
                        <button @click="clikedNotification" type="button" class="btn btn-link collapsed" data-toggle="collapse" :data-target="getParentCollapseName('#',index + 'p',index)" aria-expanded="false" :aria-controls="getParentCollapseName('',index + 'p',index)">
                            <span class="label"> Nottification : @{{ widget.title }}</span>
                        </button>
                        <button type="button" class="btn btn-danger btn-sm" @click="deleteParent(index)">Delete</button>
                    </h5>
                </div>
                <div :id="getParentCollapseName('',index + 'p',index)" class="collapse" :aria-labelledby="getParentHeadingName(index + 'p',index)" data-parent="accordions-bonuses">
                    <div class="card-body">
                        @if(isset($widget))
                            <div style="display:none;" class="form-group" v-html="getParentHiddenInput(index,'id',widget.id)" ></div>
                        @endif
                        <div class="form-group" v-html="getParentInput('Title',index,'title',widget.title,true)" ></div>
                        <div class="form-group" v-html="getParentInput('Description',index,'description',widget.description,true)"></div>
                        <div class="form-group" v-html="getParentInput('Content',index,'content',widget.content,false)"></div>
                        <div class="form-group" v-html="getParentImageInput(index,'image',widget.images)"></div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <button type="button" @click="addWidget" class="is-small button is-primary">Add Notification</button>
        </div>
    </div>
</div>

