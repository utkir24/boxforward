<div class="col">
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>{{ __('widget.backend.label.tabs.content.overview.id') }}</th>
                <td>{{ $widget->widget_id }}</td>
            </tr>

            <tr>
                <th>{{ __('widget.backend.label.tabs.content.overview.title') }}</th>
                <td>{{ $widget->title }}</td>
            </tr>
            <tr>
                <th>{{ __('widget.backend.label.tabs.content.overview.description') }}</th>
                <td>{{ $widget->description }}</td>
            </tr>
            <tr>
                <th>{{ __('widget.backend.label.tabs.content.overview.created_at') }}</th>
                <td>{{ $widget->created_at->diffForHumans() }}</td>
            </tr>
            <tr>
                <th>{{ __('widget.backend.label.tabs.content.overview.updated_at') }}</th>
                <td>{{ $widget->updated_at->diffForHumans() }}</td>
            </tr>
        </table>
    </div>
</div><!--table-responsive-->