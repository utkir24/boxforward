@extends ('backend.layouts.bot')

@section ('title', __('widget.backend.label.management') . ' | ' . __('widget.backend.label.create'))

@section('breadcrumb-links')
    @include('backend.widget.includes.breadcrumb-links')
@endsection

@section('content')

    {!!  \App\Langs\LangTabWidget::widget(\App\Models\Widget\Widget::class,'widget',null,['type'=>$type])  !!}
    {{ html()->form('POST', route('admin.widget.store'))->class('form-horizontal')->open() }}
    {{ csrf_field() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('widget.backend.label.management') }}
                        <small class="text-muted">{{ __('widget.backend.label.create') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->
            <hr />
            @include('backend.widget._form',['type'=>$type])
            <hr />
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Widget Additional Information
                    </h4>
                </div><!--col-->
            </div>
            <hr />
            @switch($type)

                @case (App\Models\Widget\Widget::TYPE_HOW_BUY )
                @case (App\Models\Widget\Widget::TYPE_PRICES_MAIN)
                @case(App\Models\Widget\Widget::TYPE_REASON)
                @case(App\Models\Widget\Widget::TYPE_SERVICES)
                @case(App\Models\Widget\Widget::TYPE_SERVICES_INFO)
                @case(App\Models\Widget\Widget::TYPE_ABOUT_US)
                @case(App\Models\Widget\Widget::TYPE_START_SHOPPING)
                    @include('backend.widget.includes.widget-notification-form')
                    @break

                @case (App\Models\Widget\Widget::TYPE_BONUSES)

                @include('backend.widget.includes.widget-notification-form-bonuses')
                    @break

                @case (App\Models\Widget\Widget::TYPE_PAYMENTS_METHOD)
                @case(App\Models\Widget\Widget::TYPE_HOME_SHORT_INFO)
                @case (App\Models\Widget\Widget::TYPE_REVIEWS_GRID)
                @case (App\Models\Widget\Widget::TYPE_REVIEWS)
                    @include('backend.widget.includes.widget-notification-form-payment')
                    @break

                @case (App\Models\Widget\Widget::TYPE_ARTICLES)
                    @include('backend.widget.includes.widget-category-form')
                    @break
                @case (App\Models\Widget\Widget::TYPE_DELIVERY_PAGE)
                    @include('backend.widget.includes.widget-notification-form-delivery')
                    @break

                @case (App\Models\Widget\Widget::TYPE_SHOPS)
                @include('backend.widget.includes.widget-shop-form')
                @break

            @endswitch

        </div><!--card-body-->
        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.widget.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
