@extends ('backend.layouts.bot')

@section ('title', __('widget.backend.label.management') . ' | ' . __('widget.backend.label.edit'))

@section('breadcrumb-links')
    @include('backend.widget.includes.breadcrumb-links')
@endsection

@section('content')
    {!!  \App\Langs\LangTabWidget::widget(\App\Models\Widget\Widget::class,'widget',$widget,['type'=>$widget->type])  !!}
    {{ html()->modelForm($widget, 'PATCH', route('admin.widget.update', $widget->widget_id))->class('form-horizontal')->open() }}
    {{ csrf_field() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('widget.backend.label.management') }}
                        <small class="text-muted">{{ __('widget.backend.label.edit') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />
            @include('backend.widget._form',['widget'=>$widget,'type'=>$widget->type])
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Widget Additional Information
                    </h4>
                </div><!--col-->
            </div>
            <hr />
            @switch($widget->type)

             @case (App\Models\Widget\Widget::TYPE_HOW_BUY )
             @case (App\Models\Widget\Widget::TYPE_PRICES_MAIN)
             @case(App\Models\Widget\Widget::TYPE_REASON)
             @case(App\Models\Widget\Widget::TYPE_SERVICES)
             @case(App\Models\Widget\Widget::TYPE_SERVICES_INFO)
             @case(App\Models\Widget\Widget::TYPE_ABOUT_US)
            @case(App\Models\Widget\Widget::TYPE_START_SHOPPING)
            @include('backend.widget.includes.widget-notification-form',['widget'=>$widget])
                 @break

             @case (App\Models\Widget\Widget::TYPE_BONUSES)
                @include('backend.widget.includes.widget-notification-form-bonuses',['widget'=>$widget])
                 @break


                @case (App\Models\Widget\Widget::TYPE_PAYMENTS_METHOD)
                @case(App\Models\Widget\Widget::TYPE_HOME_SHORT_INFO)
                @case (App\Models\Widget\Widget::TYPE_REVIEWS_GRID)
                @case (App\Models\Widget\Widget::TYPE_REVIEWS)
                 @include('backend.widget.includes.widget-notification-form-payment',['widget'=>$widget])
                 @break

                @case (App\Models\Widget\Widget::TYPE_ARTICLES)
                    @include('backend.widget.includes.widget-category-form',['widget'=>$widget])
                    @break
                @case (App\Models\Widget\Widget::TYPE_DELIVERY_PAGE)
                    @include('backend.widget.includes.widget-notification-form-delivery')
                    @break

                @case (App\Models\Widget\Widget::TYPE_SHOPS)
                @include('backend.widget.includes.widget-shop-form',['widget'=>$widget])
                @break


            @endswitch
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.auth.user.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->closeModelForm() }}
@endsection

