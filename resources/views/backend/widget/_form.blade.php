<div class="container">
    <div class="row">
        <div class="col-md-4">
            {{ html()->label('Widget Type')->class('form-control-label') }}
            <img  src="{{\App\Models\Widget\Widget::getTypeImages($type)}}" class="is-square img-responsive" alt="">
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-10 form-group row">
                    {{ html()->label(__('widget.backend.validation.attributes.title'))->class('col-md-3 form-control-label')->for('title') }}

                    <div class="col-md-9">
                        {{ html()->text('title')
                            ->class('form-control')
                            ->placeholder(__('widget.backend.validation.attributes.title'))
                            ->autofocus() }}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="col-md-10 form-group row">
                    {{ html()->label(__('widget.backend.validation.attributes.description'))->class('col-md-3 form-control-label')->for('description') }}

                    <div class="col-md-9">
                        {{ html()->text('description')
                            ->class('form-control')
                            ->placeholder(__('widget.backend.validation.attributes.description'))
                         }}
                    </div><!--col-->
                </div><!--form-group-->
                <div class="col-md-10 form-group row">
                    <div class="input-group">
                                       <span class="input-group-btn">
                                         <a id="lfm-image" data-input="thumbnail-widget" data-preview="holder-image" class="btn btn-primary">
                                           <i class="fa fa-picture-o"></i> Image
                                         </a>
                                       </span>
                        <input id="thumbnail-widget" class="form-control" value="{{ isset($widget) ? $widget->image : ''  }}" type="text" name="image">
                    </div>
                        <img id="holder-image"  class="img-thumbnail"  style="margin-top:15px;max-height:200px;">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-10">
        {{ html()->hidden('type')
            ->class('form-control')
            ->value(isset($type) ? $type : 0)
            ->placeholder(__('widget.backend.validation.attributes.type'))
            ->required() }}
    </div><!--col-->
</div><!--form-group-->

{!!  \App\Langs\LangTabWidget::langForm() !!}
@section('js')

    <script type="text/javascript">

        $(document).ready(function() {
            var image_input = $('#thumbnail-widget').val().split(',');
            if (image_input.length > 0) {
                var image_object_single = $('#holder-image');
                image_object_single.html('');
                for (indexImage in image_input) {
                    if(image_input[indexImage].length > 0) {
                        image_object_single.append(
                            $('<img>').css('height', '5rem').attr('src', image_input[indexImage])
                        );
                    }
                }
                image_object_single.trigger('change');
            }
            $('div.input-group span.input-group-btn a[id^="widget-image-btn"]').filemanager('image');
        });


    </script>

@endsection

@section('category-tree-js')
    <script src="{{ asset('box/js/category-tree.js') }}"></script>
@stop
