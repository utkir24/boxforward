@extends ('backend.layouts.app')

@section('breadcrumb-links')
    @include('backend.widget.includes.breadcrumb-links')
@endsection
@section('content')
    @php($search = [
           'title' => '',
           'description' => '',
           'content' => '',
       ])
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-4">
                    <h4 class="card-title mb-0">
                        {{ __('widget.backend.label.management') }} <small class="text-muted">{{ __('widget.backend.label.active') }}</small>
                    </h4>
                </div><!--col-->
                <div class="col-lg-4">
                    <form method="get">
                        <input class="form-control" name="search[title]" placeholder="{{__('page.backend.search.find')}}"
                               value="{{ $search['title'] }}"/>
                    </form>
                </div>
                <div class="col-sm-4">
                    @include('backend.widget.includes.header-buttons')
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>{{ __('widget.backend.label.table.id') }}</th>
                                <th>{{ __('widget.backend.label.table.title') }}</th>
                                <th>{{ __('widget.backend.label.table.created_at') }}</th>
                                <th>{{ __('widget.backend.label.table.updated_at') }}</th>
                                <th>{{ __('widget.backend.label.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($widgets as $widget)
                                <tr>
                                    <td>{{ $widget->id }}</td>
                                    <td>{{ $widget->title}}</td>
                                    <td>{{ $widget->created_at->diffForHumans() }}</td>
                                    <td>{{ $widget->updated_at->diffForHumans() }}</td>
                                    <td>{!! $widget->action_buttons !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
            <div class="row">
                <div class="col-7">
                    <div class="float-left">
                        {{$widgets->links()}}
                    </div>
                </div><!--col-->

                <div class="col-5">
                    <div class="float-right">
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection
