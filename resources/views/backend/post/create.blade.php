
@extends ('backend.layouts.app')

@section ('title', __('post.backend.label.management') . ' | ' . __('post.backend.label.create'))

@section('breadcrumb-links')
    @include('backend.post.includes.breadcrumb-links')
@endsection

@section('content')
    {!!  \App\Langs\LangTabWidget::widget(\App\Models\Page\Page::class,'post')  !!}

    {{ html()->form('POST', route('admin.post.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('post.backend.label.management') }}
                        <small class="text-muted">{{ __('post.backend.label.create') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->
            <hr />
            @include('backend.post._form',['page'=>null])
            {!!  \App\Langs\LangTabWidget::langForm() !!}

        </div><!--card-body-->
        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.post.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection


