@extends ('backend.layouts.app')

@section ('title', __('post.backend.label.management') . ' | ' . __('post.backend.label.edit'))

@section('breadcrumb-links')
    @include('backend.post.includes.breadcrumb-links')
@endsection

@section('content')
    <div class="btn-group" role="group" aria-label="page Actions">
        {!!  \App\Langs\LangTabWidget::widget(\App\Models\Page\Page::class,'post',$page)  !!}
    </div>
    {{ html()->modelForm($page, 'PATCH', route('admin.post.update', $page->id))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('post.backend.label.management') }}
                        <small class="text-muted">{{ __('post.backend.label.edit') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />
            {{ html()->hidden('id')
                       ->value($page->id)
                       ->attribute('maxlength', 191)
                        ->required()
                                  }}
            @include('backend.post._form',['page'=>$page])
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.auth.user.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->closeModelForm() }}
@endsection
