
<div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">{{__('post.backend.text.crud.Information Edit')}}</strong>
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div id="pay-invoice">
                    <div class="card-body">
                            <div class="form-group">

                                <div class="col-sm-8 col-md-8">
                                    {{ html()->label(__('post.backend.validation.attributes.title'))->class('control-label mb-1')->for('title') }}
                                    {{ html()->text('title')
                                     ->class('form-control')
                                     ->placeholder(__('post.backend.validation.attributes.title'))
                                     ->required()
                                     ->autofocus()
                                      }}
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="col-sm-8 col-md-8">
                                    {{ html()->label(__('post.backend.validation.attributes.description'))->class('control-label mb-1')->for('description') }}
                                    {{ html()->text('description')
                                       ->class('form-control')
                                       ->placeholder(__('post.backend.validation.attributes.description'))


                                    }}
                                </div>
                                <div class="col-sm-8 col-md-8">
                                    {{ html()->label(__('post.backend.validation.attributes.image'))->class('control-label mb-1')->for('image') }}
                                    <div class="input-group">
                                       <span class="input-group-btn">
                                         <a id="lfm" data-input="thumbnail" data-preview="holder"
                                            class="btn btn-primary">
                                           <i class="fa fa-picture-o"></i> {{__('post.backend.validation.attributes.Choose')}}
                                         </a>
                                       </span>
                                        @php($img = isset($page->image) ? $page->image : '')
                                        <input id="thumbnail" class="form-control" value="{{$img}}" type="text" name="image">

                                    </div>
                                    <img id="holder"  style="margin-top:15px;max-height:100px;">

                                </div>
                                <div class="col-sm-12 col-md-12">
                                    {{ html()->label(__('post.backend.validation.attributes.content'))->class('control-label mb-1')->for('content') }}
                                    {{ html()->textarea('content')
                                        ->class('form-control textarea-page')
                                        ->placeholder(__('post.backend.validation.attributes.content'))
                                        ->required()
                                    }}
                                </div>

                            </div>
                    </div>
                </div>

            </div>
        </div> <!-- .card -->

    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">{{__('post.backend.text.crud.Additional info')}}</strong>
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div id="pay-invoice">
                    <div class="card-body">
                            <div class="form-group">
                                {{ html()->label(__('post.backend.validation.attributes.status'))->class('control-label mb-1')->for('status') }}
                                <label class="switch switch-3d switch-primary">
                                    {{ html()->checkbox('status')->class('switch-input') }}
                                    <span class="switch-label"></span>
                                    <span class="switch-handle"></span>
                                </label>
                            </div>
                            <div class="form-group">
                                {{ html()->hidden('type')
                                        ->value(App\Models\Page\Page::TYPE_POST)
                                      ->class('form-control')
                                      ->attribute('style','height:35px;')

                                  }}
                                <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                            </div>
                        @if(!empty($page))
                            <div class="form-group">
                                    {{ html()->label(__('post.backend.validation.attributes.slug'))->class('control-label mb-1')->for('slug') }}
                                    {{ html()->text('slug')
                                       ->class('form-control')
                                       ->placeholder(__('post.backend.validation.attributes.slug'))
                                       ->attribute('maxlength', 255)
                                       ->required()}}
                                    <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                                </div>
                        @endif

                        <div class="form-group">
                            {{ html()->label(__('post.backend.validation.attributes.sort'))->class('control-label mb-1')->for('sort') }}
                                {{ html()->text('sort')
                                   ->value(!isset($page)? \App\Models\Page\Page::where('status',App\Models\Page\Page::STATUS_ACTIVE)->where('type',\App\Models\Page\Page::TYPE_POST)->lang()->max('sort') + 1:$page->sort)
                                   ->class('form-control')
                                   ->placeholder(__('post.backend.validation.attributes.sort'))

                                }}
                                <span class="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true"></span>
                            </div>
                        <div class="form-group">
                        <div id="tree">
                                {{__('post.backend.validation.attributes.Category')}}
                                <ul id="treeData">
                                    {{html()->hidden('categories')->class('cat-fa')}}
                                    @if(!isset($page))
                                        {{\App\Models\Category\Category::renderFancyPage(0,[],[App\Models\Category\Category::TYPE_POST])}}
                                    @else
                                        {{\App\Models\Category\Category::renderFancy(App\Models\Category\Category::TYPE_POST,array_pluck($page->categories,'id'))}}
                                    @endif
                                </ul>
                            </div>
                        </div>

                        @section('js')
                            <script type="text/javascript">
                                $(document).ready(function(){
                                        $('#holder').html('').append(
                                            $('<img>').css('height', '10rem').attr('src','{{$img}}')
                                        ).trigger('change');
                                });
                                if ($("#values-table").length > 0) {
                                    $(document).on('click', '#add-value', function () {
                                        var index = $('#values-table tr:last').data('index');
                                        if (isNaN(index)) {
                                            index = 0;
                                        } else {
                                            index++;
                                        }
                                        $('#values-table tr:last').after('<tr id="tr_' + index + '" data-index="' + index + '"><td>' +
                                            '<input name="Seo[' + index + '][type]" type="text"' +
                                            'value="" class="form-control"/></td><td>' +
                                            '<input name="Seo[' + index + '][key]" type="text"' +
                                            'value="" class="form-control"/></td><td>' +
                                            '<input name="Seo[' + index + '][value]" type="text"' +
                                            'value="" class="form-control"/></td>' +
                                            '<td><button type="button" class="btn btn-danger remove-value" data-index="' + index + '">'
                                            + '<i class="fa fa-trash"></i></button></td>' +
                                            '</tr>');
                                    });

                                    $(document).on('click', '.remove-value', function () {
                                        var index = $(this).data('index');
                                        $("#tr_" + index).remove();
                                    });
                                }


                            </script>
                        @endsection
                    </div>
                </div>

            </div>
        </div> <!-- .card -->
    </div>
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">{{__('seo.backend.label.title')}}</strong>
                <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                    <div class="">
                        <button type="button" class="btn btn-success" id="add-value"><i
                                    class="fa fa-plus"></i>{{' '.__('buttons.general.crud.add') }}

                        </button>
                    </div>
                </div><!--btn-toolbar-->
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div class="bs-example">
                    @include('backend.page.includes.seo',['page'=>$page])
                </div>
            </div>
        </div> <!-- .card -->
    </div>
</div>

@section('category-tree-js')
 <script src="{{ asset('box/js/category-tree.js') }}"></script>
@stop



