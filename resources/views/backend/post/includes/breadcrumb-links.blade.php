<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ __('post.backend.menu.main') }}
            </a>
            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.post.index') }}">{{ __('post.backend.menu.all') }}</a>
                <a class="dropdown-item" href="{{ route('admin.post.create') }}">{{ __('post.backend.menu.create') }}</a>
            </div>
        </div><!--dropdown-->
    </div><!--btn-group-->
</li>