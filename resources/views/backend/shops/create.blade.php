@extends ('backend.layouts.app')

@section ('title', __('shops.backend.label.management') . ' | ' . __('shops.backend.label.create'))

@section('breadcrumb-links')
    @include('backend.shops.includes.breadcrumb-links')
@endsection

@section('content')
    {!!  \App\Langs\LangTabWidget::widget(\App\Models\Shops\Shops::class,'shops')  !!}
    {{ html()->form('POST', route('admin.shops.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('shops.backend.label.management') }}
                        <small class="text-muted">{{ __('shops.backend.label.create') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->
            <hr />
            @include('backend.shops._form')
            {!!  \App\Langs\LangTabWidget::langForm() !!}

        </div><!--card-body-->
        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.shops.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection