@php($img = isset($page->image) ? $page->image : '')
<div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">{{__('page.backend.text.crud.Information Edit')}}</strong>
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div id="pay-invoice">
                    <div class="card-body">
                        <div class="form-group">
                            <div class="col-sm-8 col-md-8">
                                {{ html()->label(__('shops.backend.validation.attributes.title'))->class('control-label mb-1')->for('title') }}
                                {{ html()->text('title')
                                 ->class('form-control')
                                 ->placeholder(__('shops.backend.validation.attributes.title'))
                                 ->attribute('maxlength', 191)
                                 ->required()
                                 ->autofocus()
                                  }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </div>
                            <div class="col-sm-8 col-md-8">
                                {{ html()->label(__('shops.backend.validation.attributes.description'))->class('control-label mb-1')->for('description') }}
                                {{ html()->text('description')
                                   ->class('form-control')
                                   ->placeholder(__('shops.backend.validation.attributes.description'))
                                   ->attribute('maxlength', 191)
                                   ->required()
                                }}
                            </div>
                            <div class="col-sm-8 col-md-8">
                                {{ html()->label(__('page.backend.validation.attributes.image'))->class('control-label mb-1')->for('image') }}
                                <div class="input-group">
                                       <span class="input-group-btn">
                                         <a id="lfm" data-input="thumbnail" data-preview="holder"
                                            class="btn btn-primary">
                                           <i class="fa fa-picture-o"></i> {{__('page.backend.validation.attributes.Choose')}}
                                         </a>
                                       </span>

                                    <input id="thumbnail" class="form-control" value="{{$img}}" type="text" name="image">

                                </div>
                                <img id="holder"  style="margin-top:15px;max-height:100px;">
                            </div>
                            <div class="col-sm-12 col-md-12">
                                {{ html()->label(__('shops.backend.validation.attributes.content'))->class('control-label mb-1')->for('content') }}
                                {{ html()->textarea('content')
                                    ->class('form-control')
                                    ->placeholder(__('shops.backend.validation.attributes.content'))
                                    ->attribute('maxlength', 191)
                                    ->required()
                                }}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div> <!-- .card -->

    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">{{__('page.backend.text.crud.Additional info')}}</strong>
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div id="pay-invoice">
                    <div class="card-body">
                        <div class="form-group">
                            {{ html()->label(__('shops.backend.validation.attributes.status'))->class('control-label mb-1')->for('status') }}
                            <label class="switch switch-3d switch-primary">
                                {{ html()->checkbox('status')->class('switch-input') }}
                                <span class="switch-label"></span>
                                <span class="switch-handle"></span>
                            </label>
                        </div>
                        <div class="form-group">
                            {{ html()->label(__('shops.backend.validation.attributes.price'))->class('control-label mb-1')->for('price') }}
                            {{ html()->text('price')
                               ->class('form-control')
                               ->placeholder(__('shops.backend.validation.attributes.price'))
                               ->attribute('maxlength', 191)
                               ->required()}}
                            <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                        </div>
                        <div class="form-group">
                            {{ html()->label(__('shops.backend.validation.attributes.payment_system'))->class('control-label mb-1')->for('payment_system') }}
                            {{ html()->text('payment_system')
                               ->class('form-control')
                               ->placeholder(__('shops.backend.validation.attributes.payment_system'))
                               ->attribute('maxlength', 191)
                               ->required()}}
                            <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                        </div>
                        <div class="form-group">
                            {{ html()->label(__('shops.backend.validation.attributes.shop_site'))->class('control-label mb-1')->for('shop_site') }}
                            {{ html()->text('shop_site')
                               ->class('form-control')
                               ->placeholder(__('shops.backend.validation.attributes.shop_site'))
                               ->attribute('maxlength', 191)
                               ->required()}}
                            <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                        </div>
                        <div class="form-group">
                            {{ html()->label(__('shops.backend.validation.attributes.worth_knowing'))->class('control-label mb-1')->for('worth_knowing') }}
                            {{ html()->text('worth_knowing')
                               ->class('form-control')
                               ->placeholder(__('shops.backend.validation.attributes.worth_knowing'))
                               ->attribute('maxlength', 191)
                               ->required()}}
                            <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                        </div>
                        <div class="form-group">
                            {{ html()->label(__('shops.backend.validation.attributes.link_site'))->class('control-label mb-1')->for('link_site') }}
                            {{ html()->text('link_site')
                               ->class('form-control')
                               ->placeholder(__('shops.backend.validation.attributes.link_site'))
                               ->attribute('maxlength', 191)
                               ->required()}}
                            <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                        </div>
                        <div class="form-group">
                            {{ html()->label(__('page.backend.validation.attributes.sort'))->class('control-label mb-1')->for('sort') }}
                            {{ html()->text('sort')
                               ->value(!isset($shops)? \App\Models\Shops\Shops::where('status',App\Models\Shops\Shops::STATUS_ACTIVE)->lang()->max('sort') + 1:$shops->sort)
                               ->class('form-control')
                               ->placeholder(__('page.backend.validation.attributes.sort'))
                               ->attribute('maxlength', 191)
                               ->required()
                            }}
                            <span class="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true"></span>
                        </div>
                        <div class="form-group">
                            <div id="tree">
                                {{__('page.backend.validation.attributes.Category')}}
                                <ul id="treeData">
                                    {{html()->hidden('categories')->class('cat-fa')}}
                                    @if(empty($shops))
                                        {{\App\Models\Category\Category::renderFancyPage(0,[],[App\Models\Category\Category::TYPE_SHOPS,App\Models\Category\Category::TYPE_SHOPS])}}
                                    @else
                                        {{\App\Models\Category\Category::renderFancy(App\Models\Category\Category::TYPE_SHOPS,array_pluck($shops->categories,'id'))}}
                                    @endif
                                </ul>
                            </div>
                        </div>
                        @section('js')
                            <script>
                                $(document).ready(function(){
                                    $('#holder').html('').append(
                                        $('<img>').css('height', '10rem').attr('src','{{$img}}')
                                    ).trigger('change');

                                });
                            </script>
                        @endsection
                    </div>
                </div>

            </div>
        </div> <!-- .card -->
    </div>
</div>
@section('category-tree-js')
    <script src="{{ asset('box/js/category-tree.js') }}"></script>
@stop

