@extends ('backend.layouts.app')

@section('breadcrumb-links')
    @include('backend.shops.includes.breadcrumb-links')
@endsection
@section('content')
    @php($search = [
           'title' => '',
           'description' => '',
           'content' => '',
       ])
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-4">
                    <h4 class="card-title mb-0">
                        {{ __('shops.backend.label.management') }} <small class="text-muted">{{ __('shops.backend.label.active') }}</small>
                    </h4>
                </div><!--col-->
                <div class="col-lg-4">
                    <form method="get">
                        <input class="form-control" name="search[title]" placeholder="{{__('page.backend.search.find')}}"
                               value="{{ $search['title'] }}"/>
                    </form>
                </div>
                <div class="col-sm-4">
                    @include('backend.shops.includes.header-buttons')
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>{{ __('shops.backend.label.table.id') }}</th>
                                <th>{{ __('shops.backend.label.table.title') }}</th>
                                <th>{{ __('shops.backend.label.table.status') }}</th>
                                <th>{{ __('shops.backend.label.table.sort') }}</th>
                                <th>{{ __('shops.backend.label.table.created_at') }}</th>
                                <th>{{ __('shops.backend.label.table.updated_at') }}</th>
                                <th>{{ __('shops.backend.label.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($shops as $shop)
                                <tr>
                                    <td>{{ $shop->id }}</td>
                                    <td>{{ $shop->title}}</td>
                                    <td>{!! $shop->status_label !!}</td>
                                    <td>{{ $shop->sort }}</td>
                                    <td>{{ $shop->created_at->diffForHumans() }}</td>
                                    <td>{{ $shop->updated_at->diffForHumans() }}</td>
                                    <td>{!! $shop->action_buttons !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
            <div class="row">
                <div class="col-7">
                    <div class="float-left">
                        {{$shops->links()}}
                    </div>
                </div><!--col-->

                <div class="col-5">
                    <div class="float-right">
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection
