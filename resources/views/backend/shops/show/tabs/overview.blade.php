<div class="col">
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>{{ __('faq.backend.label.tabs.content.overview.id') }}</th>
                <td>{{ $faq->id }}</td>
            </tr>
            <tr>
                <th>{{ __('faq.backend.label.tabs.content.overview.status') }}</th>
                <td>{!! $faq->status_label !!}</td>
            </tr>
            <tr>
                <th>{{ __('faq.backend.label.tabs.content.overview.title') }}</th>
                <td>{{ $faq->title }}</td>
            </tr>
            <tr>
                <th>{{ __('faq.backend.label.tabs.content.overview.description') }}</th>
                <td>{{ $faq->description }}</td>
            </tr>
            <tr>
                <th>{{ __('faq.backend.label.tabs.content.overview.created_at') }}</th>
                <td>{{ $faq->created_at->diffForHumans() }}</td>
            </tr>
            <tr>
                <th>{{ __('faq.backend.label.tabs.content.overview.updated_at') }}</th>
                <td>{{ $faq->updated_at->diffForHumans() }}</td>
            </tr>
        </table>
    </div>
</div><!--table-responsive-->