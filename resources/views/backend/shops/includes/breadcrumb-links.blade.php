<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ __('faq.backend.menu.main') }}
            </a>
            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.shops.index') }}">{{ __('faq.backend.menu.all') }}</a>
                <a class="dropdown-item" href="{{ route('admin.shops.create') }}">{{ __('faq.backend.menu.create') }}</a>
            </div>
        </div><!--dropdown-->
    </div><!--btn-group-->
</li>