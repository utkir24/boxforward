@extends ('backend.layouts.app')

@section ('title', __('faq.backend.label.management') . ' | ' . __('faq.backend.label.edit'))

@section('breadcrumb-links')
    @include('backend.faq.includes.breadcrumb-links')
@endsection

@section('content')
    {!!  \App\Langs\LangTabWidget::widget(\App\Models\Faq\Faq::class,'faq',$faq)  !!}
    {{ html()->modelForm($faq, 'PATCH', route('admin.faq.update', $faq->id))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('faq.backend.label.management') }}
                        <small class="text-muted">{{ __('faq.backend.label.edit') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />
            @include('backend.faq._form',['faq'=>$faq])
            <div id="tree">
                Category
                <ul id="treeData">
                    {{html()->hidden('categories')->class('cat-fa')}}
                    {{\App\Models\Category\Category::renderFancy(App\Models\Category\Category::TYPE_FAQ,array_pluck($faq->categories,'id'))}}
                </ul>
            </div>
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.auth.user.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->closeModelForm() }}
@endsection
@section('category-tree-js')
    <script src="{{ asset('box/js/category-tree.js') }}"></script>
@stop
