@extends ('backend.layouts.app')

@section ('title', __('faq.backend.label.management') . ' | ' . __('faq.backend.label.create'))

@section('breadcrumb-links')
    @include('backend.faq.includes.breadcrumb-links')
@endsection

@section('content')
    {!!  \App\Langs\LangTabWidget::widget(\App\Models\Faq\Faq::class,'faq')  !!}
    {{ html()->form('POST', route('admin.faq.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('faq.backend.label.management') }}
                        <small class="text-muted">{{ __('faq.backend.label.create') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->
            <hr />
            @include('backend.faq._form')
            {!!  \App\Langs\LangTabWidget::langForm() !!}
            <div id="tree">
                Category
                <ul id="treeData">
                    {{\App\Models\Category\Category::renderFancy(App\Models\Category\Category::TYPE_FAQ)}}
                    {{html()->hidden('categories')->class('cat-fa')}}
                </ul>
            </div>

        </div><!--card-body-->
        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.faq.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
@section('category-tree-js')
    <script src="{{ asset('box/js/category-tree.js') }}"></script>
@stop