@extends ('backend.layouts.app')

@section('breadcrumb-links')
    @include('backend.faq.includes.breadcrumb-links')
@endsection
@section('content')
    @php($search = [
           'title' => '',
           'description' => '',
           'content' => '',
       ])
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-4">
                    <h4 class="card-title mb-0">
                        {{ __('faq.backend.label.management') }} <small class="text-muted">{{ __('faq.backend.label.active') }}</small>
                    </h4>
                </div><!--col-->
                <div class="col-lg-4">
                    <form method="get">
                        <input class="form-control" name="search[title]" placeholder="{{__('page.backend.search.find')}}"
                               value="{{ $search['title'] }}"/>
                    </form>
                </div>
                <div class="col-sm-4">
                    @include('backend.faq.includes.header-buttons')
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>{{ __('faq.backend.label.table.id') }}</th>
                                <th>{{ __('faq.backend.label.table.title') }}</th>
                                <th>{{ __('faq.backend.label.table.status') }}</th>
                                <th>{{ __('faq.backend.label.table.sort') }}</th>
                                <th>{{ __('faq.backend.label.table.created_at') }}</th>
                                <th>{{ __('faq.backend.label.table.updated_at') }}</th>
                                <th>{{ __('faq.backend.label.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($faqs as $faq)
                                <tr>
                                    <td>{{ $faq->id }}</td>
                                    <td>{{ $faq->title}}</td>
                                    <td>{!! $faq->status_label !!}</td>
                                    <td>{{ $faq->sort }}</td>
                                    <td>{{ $faq->created_at ? $faq->created_at->diffForHumans():$faq->created_at }}</td>
                                    <td>{{ $faq->updated_at ? $faq->updated_at->diffForHumans():$faq->updated_at}}</td>
                                    <td>{!! $faq->action_buttons !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
            <div class="row">
                <div class="col-7">
                    <div class="float-left">
                        {{$faqs->links()}}
                    </div>
                </div><!--col-->

                <div class="col-5">
                    <div class="float-right">
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection
