<div class="row mt-4 mb-4">
    <div class="col">
        <div class="form-group row">
            {{ html()->label(__('faq.backend.validation.attributes.title'))->class('col-md-2 form-control-label')->for('title') }}

            <div class="col-md-10">
                {{ html()->text('title')
                    ->class('form-control')
                    ->placeholder(__('faq.backend.validation.attributes.title'))
                    ->required()
                    ->autofocus() }}
            </div><!--col-->
        </div><!--form-group-->
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group row">
            {{ html()->label(__('faq.backend.validation.attributes.description'))->class('col-md-2 form-control-label')->for('description') }}

            <div class="col-md-10">
                {{ html()->text('description')
                    ->class('form-control')
                    ->placeholder(__('faq.backend.validation.attributes.description'))
                     }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label(__('faq.backend.validation.attributes.content'))->class('col-md-2 form-control-label')->for('content') }}

            <div class="col-md-10">
                {{ html()->textarea('content')
                    ->class('form-control')
                    ->placeholder(__('faq.backend.validation.attributes.content'))
                    }}
            </div><!--col-->
        </div><!--form-group-->
        <div class="form-group row">
            {{ html()->label(__('faq.backend.validation.attributes.status'))->class('col-md-2 form-control-label')->for('status') }}
            <div class="col-md-10">
                <label class="switch switch-3d switch-primary">
                    {{ html()->checkbox('status')->class('switch-input') }}
                    <span class="switch-label"></span>
                    <span class="switch-handle"></span>
                </label>
            </div><!--col-->
        </div><!--form-group-->
        <div class="form-group row">
            {{ html()->label(__('faq.backend.validation.attributes.sort'))->class('col-md-2 form-control-label')->for('sort') }}
            <div class="col-md-10">
                {{ html()->text('sort')
                    ->value(!isset($faq)? \App\Models\Faq\Faq::where('status',\App\Models\Faq\Faq::STATUS_ACTIVE)->lang()->max('sort') + 1:$faq->sort)
                    ->class('form-control')
                    ->placeholder(__('faq.backend.validation.attributes.sort'))
                    ->attribute('maxlength', 11)
                    }}
            </div><!--col-->
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

