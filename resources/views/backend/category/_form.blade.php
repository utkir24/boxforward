<div class="row mt-12 mb-12">
    <div class="col">
        <div class="form-group row">
            <div class="col-md-12">
                {{ html()->text('title')
                    ->class('form-control')
                    ->placeholder(__('category.backend.validation.attributes.title'))
                    ->attribute('maxlength', 191)
                    ->required()
                    ->autofocus() }}
            </div><!--col-->
        </div><!--form-group-->
        <div class="form-group row">
            <div class="col-md-12">
                {{ html()->text('description')
                    ->class('form-control')
                    ->placeholder(__('category.backend.validation.attributes.description'))
                    ->attribute('maxlength', 191)
                    ->required() }}

            </div><!--col-->
        </div><!--form-group-->
        <div class="form-group row">
            <div class="col-md-12">
                {{ html()->text('slug')
                    ->class('form-control')
                    ->placeholder(__('category.backend.validation.attributes.slug'))
                    ->attribute('maxlength', 191)
                    ->required()
                    }}
            </div><!--col-->
        </div><!--form-group-->
        <div class="form-group row">
            {{ html()->label(__('category.backend.validation.attributes.status'))->class('col-md-12 form-control-label')->for('status') }}
            <div class="col-md-12">
                <label class="switch switch-3d switch-primary">
                    {{ html()->checkbox('status', true, '1')->class('switch-input') }}
                    <span class="switch-label"></span>
                    <span class="switch-handle"></span>
                </label>
            </div><!--col-->
        </div><!--form-group-->

        @if(((@$category instanceof \App\Models\Category\Category) && (intval(@$category->parent_id) == 0)) || @$category == null)
            <div class="form-group row">
                {{ html()->label(__('category.backend.validation.attributes.type'))->class('col-md-12 form-control-label')->for('type') }}
                <div class="col-md-12">
                    {{html()->select('type',\App\Models\Category\Category::types())
                    ->class("form-control form-select2")
                     ->style('height:30px')}}
                </div><!--col-->
            </div><!--form-group-->
        @endif
    </div><!--col-->
</div><!--row-->

