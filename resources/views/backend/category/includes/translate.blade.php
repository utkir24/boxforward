<h3>Translate</h3>
<div class="form-group row">
    <div class="col-md-12">
        <?php
        $langs = \App\Langs\Lang::langs();
        unset($langs[\App\Langs\Lang::current()]);
        ?>
            @foreach($langs as $key_lang=>$lang)
                {{$lang}}
                {{html()
                ->select('translate['.$key_lang.']',options_array(\App\Models\Category\Category::withoutGlobalScope(\App\Langs\LangScope::class)->lang($key_lang)->get(),'id','title'),$category->translation($key_lang)->id)
                ->class("form-control form-select2")
                ->style('height:30px')}}
            @endforeach
    </div><!--col-->
</div><!--form-group-->
