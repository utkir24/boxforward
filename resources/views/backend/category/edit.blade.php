@extends ('backend.category.index')

@section ('title', __('category.backend.label.management') . ' | ' . __('category.backend.label.edit'))

@section('breadcrumb-links')
    @include('backend.category.includes.breadcrumb-links')
@endsection
@section('content')
    <div class="btn-group" role="group" aria-label="category Actions">
    </div>
    {{ html()->modelForm($category, 'PATCH', route('admin.category.update', $category->id))->class('form-horizontal')->open() }}

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('category.backend.label.management') }}
                        <small class="text-muted">{{ __('category.backend.label.edit') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />
            @include('backend.category._form')
            @include('backend.category.includes.translate')
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.category.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->closeModelForm() }}

    @parent
@endsection
