<div class="col">
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>{{ __('page.backend.label.tabs.content.overview.id') }}</th>
                <td>{{ $page->id }}</td>
            </tr>
            <tr>
                <th>{{ __('page.backend.label.tabs.content.overview.status') }}</th>
                <td>{!! $page->status_label !!}</td>
            </tr>
            <tr>
                <th>{{ __('page.backend.label.tabs.content.overview.title') }}</th>
                <td>{{ $page->title }}</td>
            </tr>
            <tr>
                <th>{{ __('page.backend.label.tabs.content.overview.description') }}</th>
                <td>{{ $page->description }}</td>
            </tr>
            <tr>
                <th>{{ __('page.backend.label.tabs.content.overview.created_at') }}</th>
                <td>{{ $page->created_at->diffForHumans() }}</td>
            </tr>
            <tr>
                <th>{{ __('page.backend.label.tabs.content.overview.updated_at') }}</th>
                <td>{{ $page->updated_at->diffForHumans() }}</td>
            </tr>
        </table>
    </div>
</div><!--table-responsive-->