
@extends ('backend.layouts.app')

@section ('title', __('page.backend.label.management') . ' | ' . __('page.backend.label.create'))

@section('breadcrumb-links')
    @include('backend.page.includes.breadcrumb-links')
@endsection

@section('content')
    @php(
$request = Request::get('template')
)

    @if(empty($request))
        @php($request = isset($page->template_id) ? $page->template_id : App\Models\Page\Page::TEMPLATE_PAGE_DEFAULT )
    @endif
    {!!  \App\Langs\LangTabWidget::widget(\App\Models\Page\Page::class,'page',null,['template_id'=>$request])  !!}

    {{ html()->form('POST', route('admin.page.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('page.backend.label.management') }}
                        <small class="text-muted">{{ __('page.backend.label.create') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->
            <hr />
            @include('backend.page._form',['page'=>null])
            {!!  \App\Langs\LangTabWidget::langForm() !!}

        </div><!--card-body-->
        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.page.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection


