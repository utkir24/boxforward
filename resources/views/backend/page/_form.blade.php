@php(
$request = Request::get('template')
)
@php($widgets = '')
@if(!empty($page->widgets))
    @php($widgets = implode(',',options_array($page->widgets,'widget_id','widget_id')))
@endif
@php($img = '')
@if(empty($request))
    @php($img = isset($page->image) ? $page->image : '')
    @php($request = isset($page->template_id) ? $page->template_id : App\Models\Page\Page::TEMPLATE_PAGE_DEFAULT )
@endif
<div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">{{__('page.backend.text.crud.Information Edit')}}</strong>
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div id="pay-invoice">
                    <div class="card-body">
                            <div class="form-group">
                                <div class="col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <img src="{{\App\Models\Page\Page::getTemplateImages($request)}}" class="img-responsive" alt="">
                                        <div class="caption">
                                            {{ html()->label('Template Type')->class('form-control-label') }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8 col-md-8">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    {{ html()->label(__('page.backend.validation.attributes.title'))->class('control-label mb-1')->for('title') }}
                                    {{ html()->text('title')
                                     ->class('form-control')
                                     ->placeholder(__('page.backend.validation.attributes.title'))
                                     ->required()
                                     ->autofocus()
                                      }}
                                    {{ html()->text('template_id')
                                     ->attribute('hidden',true)
                                     ->attribute('value',$request)
                                     ->autofocus()
                                      }}
                                </div>
                                @if($request == \App\Models\Page\Page::TEMPLATE_MAIN)
                                    <div class="col-sm-8 col-md-8">
                                        {{ html()->label(__('page.backend.validation.attributes.small_title'))->class('control-label mb-1')->for('small_title') }}
                                        {{ html()->text('small_title')
                                        ->class('form-control')
                                        ->placeholder(__('page.backend.validation.attributes.small_title'))

                                        ->autofocus()
                                        }}
                                    </div>
                                @endif
                                <input hidden name="widgets" id="widgets-ids" value="{{$widgets}}">
                            @if($request == \App\Models\Page\Page::TEMPLATE_MAIN || $request == \App\Models\Page\Page::TEMPLATE_PAGE_IMAGES)
                                <div class="col-sm-8 col-md-8">
                                    {{ html()->label(__('page.backend.validation.attributes.image'))->class('control-label mb-1')->for('image') }}
                                    <div class="input-group">
                                       <span class="input-group-btn">
                                         <a id="lfm" data-input="thumbnail" data-preview="holder"
                                            class="btn btn-primary">
                                           <i class="fa fa-picture-o"></i> {{__('page.backend.validation.attributes.Choose')}}
                                         </a>
                                       </span>

                                        <input id="thumbnail" class="form-control" value="{{$img}}" type="text" name="image">

                                    </div>
                                    <img id="holder"  style="margin-top:15px;max-height:100px;">
                                </div>
                                @endif

                                <div class="col-sm-8 col-md-8">
                                    {{ html()->label(__('page.backend.validation.attributes.description'))->class('control-label mb-1')->for('description') }}
                                    {{ html()->text('description')
                                       ->class('form-control')
                                       ->placeholder(__('page.backend.validation.attributes.description'))

                                    }}
                                </div>
                                <div class="col-sm-12 col-md-12">
                                    {{ html()->label(__('page.backend.validation.attributes.content'))->class('control-label mb-1')->for('content') }}
                                    {{ html()->textarea('content')
                                        ->class('form-control')
                                        ->placeholder(__('page.backend.validation.attributes.content'))

                                    }}
                                </div>
                                @if($request == \App\Models\Page\Page::TEMPLATE_PAGE_IMAGES)
                                    <div class="col-sm-12 col-md-12">
                                        {{ html()->label(__('page.backend.validation.attributes.content_two'))->class('control-label mb-1')->for('content_two') }}
                                        {{ html()->textarea('content_two')
                                            ->class('form-control')
                                            ->placeholder(__('page.backend.validation.attributes.content_two'))

                                        }}
                                    </div>
                                @endif

                            </div>
                    </div>
                </div>

            </div>
        </div> <!-- .card -->

    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">{{__('page.backend.text.crud.Additional info')}}</strong>
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div id="pay-invoice">
                    <div class="card-body">
                            <div class="form-group">
                                {{ html()->label(__('page.backend.validation.attributes.status'))->class('control-label mb-1')->for('status') }}
                                <label class="switch switch-3d switch-primary">
                                    {{ html()->checkbox('status')->class('switch-input') }}
                                    <span class="switch-label"></span>
                                    <span class="switch-handle"></span>
                                </label>
                                {{ html()->text('type')
                                   ->attribute('hidden',true)
                                   ->attribute('value',\App\Models\Page\Page::TYPE_PAGE)
                                }}
                            </div>

                        @if(!empty($page))
                            <div class="form-group">
                                    {{ html()->label(__('page.backend.validation.attributes.slug'))->class('control-label mb-1')->for('slug') }}
                                    {{ html()->text('slug')
                                       ->class('form-control')
                                       ->placeholder(__('page.backend.validation.attributes.slug'))
                                       ->attribute('maxlength', 255)
                                       ->required()}}
                                    <span class="help-block field-validation-valid" data-valmsg-for="cc-name" data-valmsg-replace="true"></span>
                                </div>
                        @endif

                        <div class="form-group">
                            {{ html()->label(__('page.backend.validation.attributes.sort'))->class('control-label mb-1')->for('sort') }}
                                {{ html()->text('sort')
                                   ->value(!isset($page)? \App\Models\Page\Page::where('status',App\Models\Page\Page::STATUS_ACTIVE)->lang()->max('sort') + 1:$page->sort)
                                   ->class('form-control')
                                   ->placeholder(__('page.backend.validation.attributes.sort'))

                                }}
                                <span class="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true"></span>
                            </div>
                        <div class="form-group">
                        <div id="tree">
                                {{__('page.backend.validation.attributes.Category')}}
                                <ul id="treeData">
                                    {{html()->hidden('categories')->class('cat-fa')}}
                                    @if(empty($page))
                                        {{\App\Models\Category\Category::renderFancyPage(0,[],[App\Models\Category\Category::TYPE_PAGE])}}
                                    @else
                                        {{\App\Models\Category\Category::renderFancy(App\Models\Category\Category::TYPE_PAGE,array_pluck($page->categories,'id'))}}
                                    @endif
                                </ul>
                            </div>
                        </div>
                        @section('js')
                            <script>

                                $(document).ready(function(){

                                    $('#holder').html('').append(
                                        $('<img>').css('height', '10rem').attr('src','{{$img}}')
                                    ).trigger('change');
                                });
                                $(document).ready(function(){

                                    $(".button-add").click(function(){

                                        $("#myModal").modal('show');

                                    });
                                    $('.widget-add').click(function () {
                                        console.log($(this));
                                        var parent =  $(this).parents('.col-sm-6.col-md-3');
                                        var $clone = parent.clone(true);
                                        var widget = $("#widgets");
                                        widget.append($clone);
                                        var value =null;
                                        widget.children().each(function(){
                                            var _this = $(this);
                                            var a = _this.find('.widget-add');
                                            a.after('<a class="btn btn-danger delete-widget"><i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="delete">\n' +
                                                '                      </i></a>');
                                            a.removeClass('btn-success widget-add').addClass('btn-primary').attr('href','/admin/widget/'+_this.data('widget_id')+'/edit').html('<i class="fas fa-edit text-dark"></i>');
                                            if(value === null){
                                                value = _this.data('widget_id');
                                            }else{
                                                value = value+','+_this.data('widget_id');
                                            }
                                        });

                                        $('.delete-widget').click(function(){
                                            $(this).parents('.col-sm-6.col-md-3').remove();
                                        });
                                        $("#widgets-ids").val(value);
                                    });

                                    $('.delete-widget').click(function(){
                                        $(this).parents('.col-sm-6.col-md-3').remove();
                                        var widget = $("#widgets");
                                        var value =null;
                                        widget.children().each(function(){
                                            var _this = $(this);
                                            if(value === null){
                                                value = _this.data('widget_id');
                                            }else{
                                                value = value+','+_this.data('widget_id');
                                            }
                                        });
                                        $("#widgets-ids").val(value);
                                    });

                                });

                                if ($("#values-table").length > 0) {
                                    $(document).on('click', '#add-value', function () {
                                        var index = $('#values-table tr:last').data('index');
                                        if (isNaN(index)) {
                                            index = 0;
                                        } else {
                                            index++;
                                        }
                                        $('#values-table tr:last').after('<tr id="tr_' + index + '" data-index="' + index + '"><td>' +
                                            '<input name="Seo[' + index + '][type]" type="text"' +
                                            'value="" class="form-control"/></td><td>' +
                                            '<input name="Seo[' + index + '][key]" type="text"' +
                                            'value="" class="form-control"/></td><td>' +
                                            '<input name="Seo[' + index + '][value]" type="text"' +
                                            'value="" class="form-control"/></td>' +
                                            '<td><button type="button" class="btn btn-danger remove-value" data-index="' + index + '">'
                                            + '<i class="fa fa-trash"></i></button></td>' +
                                            '</tr>');
                                    });

                                    $(document).on('click', '.remove-value', function () {
                                        var index = $(this).data('index');
                                        $("#tr_" + index).remove();
                                    });
                                }

                            </script>
                        @endsection
                    </div>
                </div>

            </div>
        </div> <!-- .card -->
    </div>
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">{{__('seo.backend.label.title')}}</strong>
                <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                    <div class="">
                        <button type="button" class="btn btn-success" id="add-value"><i
                                    class="fa fa-plus"></i>{{' '.__('buttons.general.crud.add') }}

                        </button>
                    </div>
                </div><!--btn-toolbar-->
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div class="bs-example">
                    @include('backend.page.includes.seo',['page'=>$page])
                </div>
            </div>
        </div> <!-- .card -->
    </div>
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">{{__('page.backend.text.crud.Widgets')}}</strong>
                @include('backend.page.includes.add-buttons')
            </div>
            <div class="card-body">
                <!-- Credit Card -->
                <div class="bs-example">
                    <div class="row" id="widgets">
                        @if(!empty($page->widgets))
                            @foreach($page->widgets as $item => $widget)
                                <div class="col-sm-6 col-md-3" data-arr="{{$item}}" data-widget_id="{{$widget->widget_id}}">
                                    <div class="thumbnail">
                                        <img src="{{\App\Models\Widget\Widget::getTypeImages($widget->type)}}">
                                        <div class="caption">
                                            <h3>{{$widget->title}}</h3>
                                            <p>
                                                <a class="btn btn-primary" data-dismiss="modal" aria-hidden="true" role="button" href="/admin/widget/{{$widget->widget_id}}/edit"><svg class="svg-inline--fa fa-edit fa-w-18 text-dark" aria-hidden="true" data-prefix="fas" data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z"></path></svg><!-- <i class="fas fa-edit text-dark"></i> --></a>
                                                <a class="btn btn-danger delete-widget"><svg class="svg-inline--fa fa-trash fa-w-14" data-toggle="tooltip" data-placement="top" title="delete" aria-labelledby="svg-inline--fa-title-1" data-prefix="fas" data-icon="trash" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><title id="svg-inline--fa-title-1">delete</title><path fill="currentColor" d="M0 84V56c0-13.3 10.7-24 24-24h112l9.4-18.7c4-8.2 12.3-13.3 21.4-13.3h114.3c9.1 0 17.4 5.1 21.5 13.3L312 32h112c13.3 0 24 10.7 24 24v28c0 6.6-5.4 12-12 12H12C5.4 96 0 90.6 0 84zm415.2 56.7L394.8 467c-1.6 25.3-22.6 45-47.9 45H101.1c-25.3 0-46.3-19.7-47.9-45L32.8 140.7c-.4-6.9 5.1-12.7 12-12.7h358.5c6.8 0 12.3 5.8 11.9 12.7z"></path></svg></a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        @endif
                    </div>
                </div>


            </div>
        </div> <!-- .card -->

    </div>
</div>
@section('category-tree-js')
    <script src="{{ asset('box/js/category-tree.js') }}"></script>
@stop



