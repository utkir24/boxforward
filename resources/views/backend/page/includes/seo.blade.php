<table id="values-table" width="100%" class="table table-striped table-responsive">
    <thead>
    <tr>
        <th width="33%">{{__('seo.backend.label.type')}} name/property</th>
        <th width="33%">{{__('seo.backend.label.key')}}</th>
        <th width="33%">{{__('seo.backend.label.value')}}</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @if(!empty($page) && !empty($page->seo))
        <?php $i = 0;?>
        @foreach($page->seo as $key => $value)
            <tr id="tr_{{ $i }}" data-index="{{ $i }}">
                <td>
                    <input name="{{"Seo[$i][type]"}}" type="text"
                           value="{{ $value['type'] }}" class="form-control"/>
                </td>
                <td>
                    <input name="{{"Seo[$i][key]"}}" type="text"
                           value="{{ $key }}" class="form-control"/>
                </td>
                <td>
                    <input name="{{"Seo[$i][value]" }}" type="text"
                           value="{{ $value['value'] }}" class="form-control"/>
                </td>
                <td>
                    <button type="button" class="btn btn-danger remove-value"
                            data-index="{{ $i }}"><i
                                class="fas fa-trash"></i>
                    </button>
                </td>
            </tr>
            <?php $i++;?>
        @endforeach
    @endif
    </tbody>
</table>

