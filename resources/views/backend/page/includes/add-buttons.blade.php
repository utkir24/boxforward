<div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
    <a href="#myModal" class="btn btn-success ml-1 button-add" data-toggle="modal" title="{{ __('buttons.general.crud.add') }}">

        <i class="fas fa-plus-circle"></i>{{' '.__('buttons.general.crud.add') }}
    </a>
</div><!--btn-toolbar-->


<!-- Modal HTML -->

<div id="myModal" class="modal fade bs-example-modal-lg">

    <div class="modal-dialog modal-lg">

        <div class="modal-content" style="height: 600px;overflow: auto;">

            <div class="modal-header">


                <h4 class="modal-title">{{__('page.backend.text.crud.Widgets')}}</h4>

            </div>

            <div class="modal-body">

                <div class="bs-example">
                  <div class="row">

                    @foreach(App\Models\Widget\Widget::langs()->get() as $type => $value)
                        <div class="col-sm-6 col-md-3" data-arr="{{$type}}" data-widget_id="{{$value->widget_id}}">
                            <div class="thumbnail">
                                <img src="{{\App\Models\Widget\Widget::getTypeImages($value->type)}}">
                                <div class="caption">
                                    <h3>{{$value->title}}</h3>
                                    <p>
                                        <a class="btn btn-success widget-add"  data-dismiss="modal" aria-hidden="true"  role="button">{{__('buttons.general.crud.add')}}</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<style>
    .panel-primary > .panel-heading {
        color: #fff;
        background-color: #29363d;
        border-color: #337ab7;
    }
</style>

