<div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
    <a href="#myModal" class="btn btn-success ml-1 button-main" data-toggle="modal" title="{{ __('page.backend.menu.create') }}">
        <i class="fas fa-plus-circle"></i>
    </a>
</div><!--btn-toolbar-->


<!-- Modal HTML -->

<div id="myModal" class="modal fade bs-example-modal-lg">

    <div class="modal-dialog modal-lg">

        <div class="modal-content">

            <div class="modal-header">


                <h4 class="modal-title">Page Template</h4>

            </div>

            <div class="modal-body">

                <div class="row">
                    @foreach(App\Models\Page\Page::getTemplates() as $type => $value)
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <a href="{{ route('admin.page.create',['template'=>$type]) }}">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">{{$value}}</h3>
                                    </div>
                                    <div class="panel-body"><img src="{{\App\Models\Page\Page::getTemplateImages($type)}}" class="img-responsive" alt="{{$value}}"></div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>

</div>

@section('js')


    <script type="text/javascript">

        $(document).ready(function(){

            $(".button-main").click(function(){

                $("#myModal").modal('show');

            });

        });

    </script>


@endsection
<style>
    .panel-primary > .panel-heading {
        color: #fff;
        background-color: #29363d;
        border-color: #337ab7;
    }
</style>

