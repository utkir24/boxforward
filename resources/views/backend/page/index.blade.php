@extends ('backend.layouts.app')

@section('breadcrumb-links')
    @include('backend.page.includes.breadcrumb-links')
@endsection
@section('content')
    @php($search = [
            'title' => '',
            'description' => '',
            'content' => '',
        ])
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-4">
                    <h4 class="card-title mb-0">
                        {{ __('page.backend.label.management') }} <small class="text-muted">{{ __('page.backend.label.active') }}</small>
                    </h4>
                </div><!--col-->
                <div class="col-lg-4">
                    <form method="get">
                        <input class="form-control" name="search[title]" placeholder="{{__('page.backend.search.find')}}"
                               value="{{ $search['title'] }}"/>
                    </form>
                </div>
                <div class="col-sm-4">
                    @include('backend.page.includes.header-buttons')
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>{{ __('page.backend.label.table.id') }}</th>
                                <th>{{ __('page.backend.label.table.title') }}</th>
                                <th>{{ __('page.backend.label.table.status') }}</th>
                                <th>{{ __('page.backend.label.table.sort') }}</th>
                                <th>{{ __('page.backend.label.table.created_at') }}</th>
                                <th>{{ __('page.backend.label.table.updated_at') }}</th>
                                <th>{{ __('page.backend.label.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($pages as $page)
                                <tr>
                                    <td>{{ $page->id }}</td>
                                    <td>{{ $page->title}}</td>
                                    <td>{!! $page->status_label !!}</td>
                                    <td>{{ $page->sort }}</td>
                                    <td>{{ $page->created_at->diffForHumans() }}</td>
                                    <td>{{ $page->updated_at->diffForHumans() }}</td>
                                    <td>{!! $page->action_buttons !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
            <div class="row">
                <div class="col-7">
                    <div class="float-left">
                        {{$pages->links()}}
                    </div>
                </div><!--col-->

                <div class="col-5">
                    <div class="float-right">
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection
