<h3>Translate</h3>
<div class="form-group row">
    <div class="col-md-12">
        <?php
        $langs = \App\Langs\Lang::langs();
        unset($langs[\App\Langs\Lang::current()]);
        ?>
            @foreach($langs as $key_lang=>$lang)
                {{$lang}}
                {{html()
                ->select('translate['.$key_lang.']',options_array(\App\Models\Menu\Menu::withoutGlobalScope(\App\Langs\LangScope::class)->lang($key_lang)->get(),'id','title'),$category->translation($key_lang)->id)
                ->class("form-control form-select2")
                ->style('height:30px')}}
            @endforeach
    </div><!--col-->
</div><!--form-group-->



<div id="tree">
    <ul id="treeData">
        {{\App\Models\Menu\Menu::renderFancy()}}
        {{html()->hidden('categories')->class('cat-fa')}}
    </ul>
</div>


@section('js')
<script>
    $(document).ready(function(){
        $("#tree").fancytree({
            checkbox: true,
            select: function(event, data) {
                var s = data.tree.getSelectedNodes();
                var r = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
                console.log(r);
                document[r] = [];
                s.map(function(node){
                    document[r].push(node.key);
                });
                $('.cat-fa').val(document[r].join(","));
            }
        })
    });
</script>
@endsection