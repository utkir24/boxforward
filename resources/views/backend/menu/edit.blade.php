@extends ('backend.menu.index')

@section ('title', __('menu.backend.label.management') . ' | ' . __('menu.backend.label.edit'))

@section('breadcrumb-links')
    @include('backend.menu.includes.breadcrumb-links')
@endsection
@section('content')
    <div class="btn-group" role="group" aria-label="category Actions">
    </div>
    {{ html()->modelForm($category, 'PATCH', route('admin.menu.update', $category->id))->class('form-horizontal')->open() }}

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('menu.backend.label.management') }}
                        <small class="text-muted">{{ __('menu.backend.label.edit') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />
            @include('backend.menu._form')
            @include('backend.menu.includes.translate')
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.menu.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->closeModelForm() }}

    @parent
@endsection
