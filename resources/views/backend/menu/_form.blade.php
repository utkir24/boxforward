<div class="row mt-12 mb-12">
    <div class="col">
        <div class="form-group row">
            <div class="col-md-12">
                {{ html()->text('title')
                    ->class('form-control')
                    ->placeholder(__('menu.backend.validation.attributes.title'))
                    ->attribute('maxlength', 191)
                    ->required()
                    ->autofocus() }}
            </div><!--col-->
        </div><!--form-group-->
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group row">
            {{ html()->label(__('menu.backend.validation.attributes.link'))->class('col-md-12 form-control-label')->for('link') }}
            <div class="col-md-12">
                {{ html()->text('link')
                    ->class('form-control')

                    ->placeholder(__('menu.backend.validation.attributes.link'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->
        <div class="form-group row">
            {{ html()->label(__('menu.backend.text.dropdown'))->class('col-md-12 form-control-label')->for('link') }}
            <div class="col-md-12">
                {{html()->select('link', options_array(\App\Models\Page\Page::withoutGlobalScope(\App\Langs\LangScope::class)->lang()->get(),'slug','title'))
                          ->attribute('id','link-dropdown')
                          ->attribute('name','select-dropdown')
                         ->class("form-control form-select2")
                          ->style('height:30px')}}
            </div><!--col-->
        </div><!--form-group-->


        <div class="form-group row">
            {{ html()->label(__('menu.backend.validation.attributes.status'))->class('col-md-12 form-control-label')->for('status') }}
            <div class="col-md-12">
                <label class="switch switch-3d switch-primary">
                    {{ html()->checkbox('status', true, '1')->class('switch-input') }}
                    <span class="switch-label"></span>
                    <span class="switch-handle"></span>
                </label>
            </div><!--col-->
        </div><!--form-group-->

        @if(((@$category instanceof \App\Models\Menu\Menu) && (intval(@$category->parent_id) == 0)) || @$category == null)
            <div class="form-group row">
                {{ html()->label(__('menu.backend.validation.attributes.type'))->class('col-md-12 form-control-label')->for('type') }}
                <div class="col-md-12">
                    {{html()->select('type',\App\Models\Menu\Menu::types())
                    ->class("form-control form-select2")
                     ->style('height:30px')}}
                </div><!--col-->
            </div><!--form-group-->
        @endif
    </div><!--col-->
</div><!--row-->
@section('js')
    <script>
        $(document).ready(function(){
            $('#link-dropdown').on('change',function () {
                $('#link').val($(this).val());
            });
        });
    </script>
@endsection
