<!DOCTYPE html>
@langrtl
<html lang="{{ app()->getLocale() }}" dir="rtl">
@else
    <html lang="{{ app()->getLocale() }}">
    @endlangrtl
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', app_name())</title>
        <meta name="description" content="@yield('meta_description', 'Laravel 5 Boilerplate')">
        <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">

    @yield('meta')

    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
    @stack('before-styles')

    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        {{ style(mix('css/backend.css')) }}

        @stack('after-styles')
        <link rel="stylesheet" href="{{ asset('css/bulma.css')  }}" />
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')  }}" />
        <link rel="stylesheet" href="{{ asset('box/css/select2.min.css')  }}" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.29.1/skin-win8/ui.fancytree.css" />


    </head>

    <body class="{{ config('backend.body_classes') }}" style="font-size: 14px;">
    @include('backend.includes.header')

    <div class="app-body">
        @include('backend.includes.sidebar')

        <main class="main">
            @include('includes.partials.logged-in-as')


            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="content-header">
                        @yield('page-header')
                    </div><!--content-header-->

                    @include('includes.partials.messages')
                    @yield('content')
                </div><!--animated-->
            </div><!--container-fluid-->
        </main><!--main-->

        @include('backend.includes.aside')
    </div><!--app-body-->

    @include('backend.includes.footer')

    <!-- Scripts -->
    @stack('before-scripts')
    {!! script(mix('js/backend.js')) !!}
    @stack('after-scripts')
    <script src="{{ asset('js/vue.js')  }}"></script>
    <script src="{{ asset('js/bootstrap.min.js')  }}"></script>
    <script src="{{ asset('box/js/select2.min.js')  }}"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script src="{{ URL::asset('vendor/stand-alone-button.js') }}"></script>

    <script>

            $('#lfm-image').filemanager('image');
            $(document).ready(function(){
                $('.form-select2').select2();
            });
           // $('div.input-group span.input-group-btn a[id^="widget-image-btn"]').filemanager('image');
    </script>

    <script src="{{ asset('js/widgets-vue.js')  }}"></script>

    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token=',
            allowedContent : true
        };
      // $('textarea').ckeditor(options);
    </script>

    <script src="/box/js/jquery.nestable.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.29.1/jquery.fancytree-all-deps.js"></script>
    @yield('category-tree-js')

    @include('backend.settings.layout.notifications')

    @yield('js')

    </body>
    </html>
