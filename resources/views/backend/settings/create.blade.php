@extends ('backend.layouts.app')

@section ('title', __('settings.backend.label.management') . ' | ' . __('settings.backend.label.create'))

@section('breadcrumb-links')
    @include('backend.settings.includes.breadcrumb-links')
@endsection

@section('content')
    {!!  \App\Langs\LangTabWidget::widget(\App\Models\Settings\Setting::class,'settings')  !!}

    <form method="POST" action="{{ url(config('settings.route')) }}" accept-charset="UTF-8"
          class="form-horizontal" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            {{ __('settings.backend.label.management') }}
                            <small class="text-muted">{{ __('settings.backend.label.create') }}</small>
                        </h4>
                    </div><!--col-->
                </div><!--row-->
                <hr/>
                <input type="hidden" name="type" value="{{ $type }}"/>

                @include('backend.settings.shared_input')
                {!!  \App\Langs\LangTabWidget::langForm() !!}
            </div><!--card-body-->
            <div class="card-footer clearfix">
                <div class="row">
                    <div class="col">
                        <a href="{{ url(config('settings.route')) }}" class="btn btn-default">
                            <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                            Cancel</a>
                    </div><!--col-->

                    <div class="col text-right">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save" aria-hidden="true"></i> {{__('buttons.general.save')}} & {{__('buttons.general.continue')}}
                        </button>
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    </form>
@endsection