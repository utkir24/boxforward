@extends ('backend.layouts.app')
@section ('title', __('settings.backend.label.management') . ' | ' . __('settings.backend.label.index'))

@section('breadcrumb-links')
    @include('backend.settings.includes.breadcrumb-links')
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <hr/>
                </div>
                <div class="col-md-6">
                    <h3><i class="fa fa-th-list" aria-hidden="true"></i> Settings</h3>
                    <span class="help-block">
                        {{__('settings.backend.text.list of all system settings')}}
        </span>
                </div>
                <div class="col-md-6">
                    <div class="text-right">
                        <br/>
                        <div class="btn-group">
                            <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <i class="fas fa-plus-circle" aria-hidden="true"></i> {{__('settings.backend.text.Add New Setting')}} <span
                                        class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                @foreach($types as $key => $type)
                                    <li>
                                        <a href="{{ url(config('settings.route').'/create?type='.$key) }}">{{ $type }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <hr/>
                </div>
            </div>
            <div class="col-md-12">
                <table class="table table-striped table-hover table-responsive" >
                    <thead>
                    <tr class="">
                        <th>{{__('settings.backend.label.table.code')}}</th>
                        <th>{{__('settings.backend.label.table.type')}}</th>
                        <th>{{__('settings.backend.label.table.label')}}</th>
                        <th>{{__('settings.backend.label.table.value')}}</th>
                        <th>{{__('settings.backend.label.table.actions')}}</th>
                    </tr>
                    <tr class="">
                        <form method="get">
                            <th>
                                <input class="form-control" name="search[code]" placeholder="Code"
                                       value="{{ $search['code'] }}"/>
                            </th>
                            <th>
                                <select class="form-control" style="height: 34px;!important;" name="search[type]">
                                    <option value="" {{ $search['type'] == '' ?'disabled selected':'' }}>
                                        Select Type
                                    </option>
                                    @foreach($types as $key => $type)
                                        <option value="{{ $key }}" {{ $search['type'] == $key?'selected':'' }}>
                                            {{ $type }}
                                        </option>
                                    @endforeach
                                </select>
                            </th>
                            <th>
                                <input class="form-control" name="search[label]" placeholder="Label"
                                       value="{{ $search['label'] }}"/>
                            </th>
                            <th>
                                <input class="form-control" name="search[value]" placeholder="Value"
                                       value="{{ $search['value'] }}"/>
                            </th>
                            <th>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                <a href="{{ url(config('settings.route')) }}"
                                   class="btn btn-warning"><i class="fa fa-eraser"></i></a>
                            </th>
                        </form>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($settings as $setting)
                        <tr id="tr_{{ $setting->id }}" class="{{ $setting->hidden?'warning':'' }}">
                            <td>{{ $setting->code }}</td>
                            <td>{{ $setting->type }}</td>
                            <td>{{ $setting->label }}</td>
                            <td>{{ str_limit($setting->getOriginal('value'),50) }}</td>
                            <td>
                                <div class="btn-group" role="group" aria-label="Settings Actions">
                                    <a href="{{route('admin.settings.edit', $setting->id)}}" data-toggle="tooltip" data-placement="top" title="{{__('buttons.general.crud.edit')}}" class="btn btn-primary"><i class="fas fa-edit text-dark"></i></a>


                                    <a href="{{ route('admin.settings.destroy',$setting->id)}}"
                                       data-method="delete"
                                       data-trans-button-cancel="{{  __('buttons.general.cancel')}}"
                                       data-trans-button-confirm="{{  __('buttons.general.crud.delete') }}"
                                       data-trans-title="{{ __('strings.backend.general.are_you_sure') }}"
                                       class="btn btn-danger">
                                        <i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="{{ __('buttons.general.crud.delete') }}">
                                        </i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">
                                <i class="fa fa-info-circle" aria-hidden="true"></i> {{__('settings.backend.text.No settings found')}}.
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="5" class="text-right">
                            {{ $settings->appends(\Request::except('page'))->links() }}
                        </td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
    <style>
        .table th{
            padding-left: 10em;
        }
        .table td{
            padding-left: 10em;
        }

    </style>
@endsection

{{--@section('js')--}}
{{--<script type="text/javascript">--}}
{{--$(document).ready(function () {--}}
{{--$('[data-toggle=confirmation]').confirmation({--}}
{{--rootSelector: '[data-toggle=confirmation]',--}}
{{--onConfirm: function (event, element) {--}}
{{--element.trigger('confirm');--}}
{{--}--}}
{{--// other options--}}
{{--});--}}

{{--$(document).on('confirm', function (e) {--}}
{{--var ele = e.target;--}}
{{--e.preventDefault();--}}

{{--$.ajax({--}}
{{--url: ele.href,--}}
{{--type: 'DELETE',--}}
{{--headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken},--}}
{{--success: function (data) {--}}
{{--if (data['success']) {--}}
{{--toastr.success(data['success'], 'Success');--}}
{{--$("#" + data['tr']).slideUp("slow");--}}
{{--} else if (data['error']) {--}}
{{--toastr.error(data['error'], 'Error');--}}
{{--} else {--}}
{{--toastr.error('Whoops Something went wrong!!', 'Error');--}}
{{--}--}}
{{--},--}}
{{--error: function (data) {--}}
{{--alert(data.responseText);--}}
{{--}--}}
{{--});--}}

{{--return false;--}}
{{--});--}}
{{--});--}}
{{--</script>--}}
{{--@endsection--}}