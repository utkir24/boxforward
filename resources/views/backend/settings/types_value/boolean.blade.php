<div class="form-group">
    <label class="control-label col-md-2" for="value">{{__('settings.backend.label.table.value')}} <sup class="text-danger">*</sup></label>
    <div class="col-md-10">
        <label class="switch switch-3d switch-primary">
            <input type="checkbox" name="value" value="1" {{ isset($setting) && $setting->hidden?'checked':'' }}
            class="switch-input"
                   data-toggle="toggle" data-onstyle="danger" data-offstyle="success"
                   data-on="Hidden" data-off="Visible"/>
            <span class="switch-label"></span>
            <span class="switch-handle"></span>
        </label>
        <span class="help-block">
            {{__('settings.backend.text.the value that assigned to this setting')}}
        </span>
    </div>
</div>