<div class="form-group {{ $errors->has('value') ? ' has-error' : '' }}">
    <label class="control-label col-md-2" for="value">{{__('settings.backend.label.table.value')}} <sup class="text-danger">*</sup></label>
    <div class="col-md-10">
        <input class="form-control" name="value" id="value" type="file" placeholder="Value"/>
        <span class="help-block">
        @if(!empty($setting->value))
            <a style="margin-bottom: 5px;" class="btn btn-info"
               href="{{ url(config('settings.route').'/download/'.$setting->id) }}"
               target="_blank">
                <i class="fa fa-download"></i> Download {{ $setting->getOriginal('value') }}
            </a>
        @endif
    </div>
</div>