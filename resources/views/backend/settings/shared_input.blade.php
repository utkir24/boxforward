<div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}">
    <label class="control-label col-md-2" for="code">Code <sup class="text-danger">*</sup></label>
    <div class="col-md-10">
        <input class="form-control" name="code" id="code" type="text" placeholder="Code"
               value="{{ old('code',isset($setting)?$setting->code:null) }}"/>
        <span class="help-block">
              {{__('settings.backend.text.code will be used for getting the setting value')}}

                        </span>
    </div>
</div>
<div class="form-group {{ $errors->has('label') ? ' has-error' : '' }}">
    <label class="control-label col-md-2" for="label">Label <sup class="text-danger">*</sup></label>
    <div class="col-md-10">
        <input class="form-control" name="label" id="label" type="text" placeholder="Label"
               value="{{ old('label',isset($setting)?$setting->label:null) }}"/>
        <span class="help-block">
             {{__('settings.backend.text.label describes the setting')}}

                        </span>
    </div>
</div>
<div class="form-group">
    <div class="col-md-10 col-md-offset-2 ">
        <label class="switch switch-3d switch-primary">
            <input type="checkbox" name="hidden" value="1" {{ isset($setting) && $setting->hidden?'checked':'' }}
                    class="switch-input"
                   data-on="Hidden" data-off="Visible"/>
            <span class="switch-label"></span>
            <span class="switch-handle"></span>
        </label>
    </div><!--col-->
</div>