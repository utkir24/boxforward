<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                {{ __('menus.backend.sidebar.general') }}
            </li>

            <li class="nav-item">
                <a class="nav-link {{ active_class(Active::checkUriPattern('admin/dashboard')) }}" href="{{ route('admin.dashboard') }}"><i class="icon-speedometer"></i> {{ __('menus.backend.sidebar.dashboard') }}</a>
            </li>

            <li class="nav-title">
                {{ __('menus.backend.sidebar.system') }}
            </li>

            @if ($logged_in_user->isAdmin())
                <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/auth*'), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/auth*')) }}" href="#">
                        <i class="icon-user"></i> {{ __('menus.backend.access.title') }}

                        @if ($pending_approval > 0)
                            <span class="badge badge-danger">{{ $pending_approval }}</span>
                        @endif
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/auth/user*'), 'open') }} ">

                            <a class="nav-link nav-dropdown-toggle  {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="#">
                                {{ __('labels.backend.access.users.management') }}
                            </a>
                            <ul class="nav-dropdown-items">
                                <li class="nav-item">
                                    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.auth.user.index') }}">
                                        {{ __('labels.backend.access.users.all') }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/user*')) }}" href="{{ route('admin.auth.user.create') }}">
                                        {{ __('labels.backend.access.users.create') }}
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/auth/role*')) }}" href="{{ route('admin.auth.role.index') }}">
                                {{ __('labels.backend.access.roles.management') }}
                            </a>
                        </li>
                    </ul>
                </li>


            @endif
            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/faq*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/faq*')) }}" href="#">
                    <i class="icon-list"></i> {{ __('faq.backend.menu.main') }}
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/faq')) }}" href="{{ route('admin.faq.index') }}">
                            {{ __('faq.backend.menu.all') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/faq/create')) }}" href="{{ route('admin.faq.create') }}">
                            {{ __('faq.backend.menu.create') }}
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/shops*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/shops*')) }}" href="#">
                    <i class="icon-list"></i> {{ __('shops.backend.menu.main') }}
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/shops')) }}" href="{{ route('admin.shops.index') }}">
                            {{ __('shops.backend.menu.all') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/shops/create')) }}" href="{{ route('admin.shops.create') }}">
                            {{ __('shops.backend.menu.create') }}
                        </a>
                    </li>
                </ul>
            </li>


            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/page*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/page*')) }}" href="#">
                    <i class="icon-list"></i> {{ __('page.backend.menu.main') }}
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/page*')) }}" href="{{ route('admin.page.index') }}">
                            {{ __('page.backend.menu.all') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/page*')) }}" href="{{ route('admin.page.create') }}">
                            {{ __('page.backend.menu.create') }}
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/post*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/post*')) }}" href="#">
                    <i class="icon-list"></i> {{ __('post.backend.menu.main') }}
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/post*')) }}" href="{{ route('admin.post.index') }}">
                            {{ __('post.backend.menu.all') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/post*')) }}" href="{{ route('admin.post.create') }}">
                            {{ __('post.backend.menu.create') }}
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/seetings*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/seetings*')) }}" href="#">
                    <i class="icon-list"></i> {{ __('settings.backend.menu.main') }}
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/settings')) }}" href="{{ route('admin.settings.index') }}">
                            {{ __('settings.backend.menu.all') }}
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/categories*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/categories*')) }}" href="#">
                    <i class="icon-list"></i> {{ __('category.backend.menu.main') }}
                </a>
                <ul class="nav-dropdown-items">
            @php(
                  $category_type_list = \App\Models\Category\Category::types()
                )
                @foreach($category_type_list as $type => $name)

                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/category/type*')) }}" href="{{ route('admin.category.type',$type) }}">
                            {{$name}}
                        </a>
                    </li>

                @endforeach
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/categories')) }}" href="{{ route('admin.category.index') }}">
                            {{ __('category.backend.menu.all') }}
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/menu*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/menu*')) }}" href="#">
                    <i class="icon-list"></i> {{ __('menu.backend.menu.main') }}
                </a>
                <ul class="nav-dropdown-items">
                    @php(
                 $menu_type_list = App\Models\Menu\Menu::types()
               )
                    @foreach($menu_type_list as $type => $name)

                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Active::checkUriPattern('admin/menu/type*')) }}" href="{{ route('admin.menu.type',$type) }}">
                                {{$name}}
                            </a>
                        </li>

                    @endforeach
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/menu')) }}" href="{{ route('admin.menu.index') }}">
                            {{ __('menu.backend.menu.all') }}
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link " href="/laravel-filemanager">
                    <i class="icon-list"></i> {{ __('labels.general.filemanager') }}
                </a>
            </li>

            <li class="nav-item nav-dropdown {{ active_class(Active::checkUriPattern('admin/widget*'), 'open') }}">
                <a class="nav-link nav-dropdown-toggle {{ active_class(Active::checkUriPattern('admin/widget*')) }}" href="#">
                    <i class="icon-list"></i> {{ __('widget.backend.menu.main') }}
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{ active_class(Active::checkUriPattern('admin/widget')) }}" href="{{ route('admin.widget.index') }}">
                            {{ __('widget.backend.menu.all') }}
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
    </nav>
</div><!--sidebar-->