<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */


return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'menu' => [
            'title'  => 'Menu menu',
            'all'    => 'All Menu',
            'create' => 'Create Menu',
            'deleted'=> 'Deleted Menu',
            'edit'   => 'Edit Menu',
            'main'   => 'Menu',
            'view'   => 'View Menu',

        ],
        'label'=>[
            'active'              => 'Active Menu',
            'create'              => 'Create Menu',
            'deleted'             => 'Deleted Menu',
            'edit'                => 'Edit Menu',
            'management'          => 'Menu Management',
            'view'                =>  'View Menu',
            'overview'            => 'Overview',
            'table' => [
                'id'          => 'ID',
                'title'       => 'Title',
                'link' => 'Link',
                'content'     => 'Content',
                'status'      => 'Status',
                'sort'        => 'Sort',
                'created_at'  => 'Created At',
                'updated_at'  => 'Updated At',
                'actions'  => 'Actions',
            ],

            'tabs' => [
                'titles' => [
                    'overview' => 'Overview',
                    'history'  => 'History',
                ],
                'content' => [
                    'overview' => [
                        'id'          => 'Id',
                        'title'       => 'Title',
                        'link' => 'Link',
                        'content'     => 'Content',
                        'status'      => 'Status',
                        'sort'        => 'Sort',
                        'created_at'  => 'Created At',
                        'deleted_at'  => 'Deleted At',
                        'updated_at'  => 'Updated At',
                    ],
                ],
            ],
        ],
        'validation' =>[
            'attributes'=>[
                'type'=>'Type',
                'active'                  => 'Active',
                'title'                   => 'Title',
                'link'             => 'Link',
                'content'                 => 'Content',
                'status'                  => 'Status',
                'sort'                    => 'Sort',
                'password'                => 'Password',
                'timezone'                => 'Timezone',
                'language'                => 'Language',
            ]
        ],
        'text'=>[
            'dropdown'=>'You can select a page for the current menu or you can register an link'
        ],
    ],
];