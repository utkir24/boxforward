<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'menu' => [
            'title'  => 'Faq menu',
            'all'    => 'All Faqs',
            'create' => 'Create Faq',
            'deleted'=> 'Deleted Faqs',
            'edit'   => 'Edit Faq',
            'main'   => 'Faqs',
            'view'   => 'View Faq',

        ],
        'label'=>[
            'active'              => 'Active Faq',
            'create'              => 'Create Faq',
            'deleted'             => 'Deleted Faq',
            'edit'                => 'Edit Faq',
            'management'          => 'Faq Management',
            'view'                =>  'View Faq',
            'overview'            => 'Overview',
            'table' => [
                'id'          => 'ID',
                'title'       => 'Title',
                'description' => 'Description',
                'content'     => 'Content',
                'status'      => 'Status',
                'sort'        => 'Sort',
                'created_at'  => 'Created At',
                'updated_at'  => 'Updated At',
                'actions'  => 'Actions',
            ],

            'tabs' => [
                'titles' => [
                    'overview' => 'Overview',
                    'history'  => 'History',
                ],
                'content' => [
                    'overview' => [
                        'id'          => 'Id',
                        'title'       => 'Title',
                        'description' => 'Description',
                        'content'     => 'Content',
                        'status'      => 'Status',
                        'sort'        => 'Sort',
                        'created_at'  => 'Created At',
                        'deleted_at'  => 'Deleted At',
                        'updated_at'  => 'Updated At',
                    ],
                ],
            ],
        ],
        'validation' =>[
            'attributes'=>[
                'active'                  => 'Active',
                'title'                   => 'Title',
                'description'             => 'Description',
                'content'                 => 'Content',
                'status'                  => 'Status',
                'sort'                    => 'Sort',
                'password'                => 'Password',
                'timezone'                => 'Timezone',
                'language'                => 'Language',
            ]
        ]
    ],
];