<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'menu' => [
            'title'  => 'Settings menu',
            'all'    => 'All Settings',
            'create' => 'Create Settings',
            'deleted'=> 'Deleted Settings',
            'edit'   => 'Edit Settings',
            'main'   => 'Settings',
            'view'   => 'View Settings',

        ],
        'label'=>[
            'active'              => 'Active Settings',
            'create'              => 'Create Settings',
            'deleted'             => 'Deleted Settings',
            'edit'                => 'Edit Settings',
            'management'          => 'Settings Management',
            'view'                =>  'View Settings',
            'overview'            => 'Overview',
            'table' => [
                'key'=>'Key',
                'id'          => 'ID',
                'code'       => 'Code',
                'value' => 'Value',
                'hidden'     => 'Status',
                'label'=>'Label',
                'type'      => 'Type',
                'created_at'  => 'Created At',
                'updated_at'  => 'Updated At',
                'actions'  => 'Actions',
            ],

            'tabs' => [
                'titles' => [
                    'overview' => 'Overview',
                    'history'  => 'History',
                ],
                'content' => [
                    'overview' => [
                        'id'          => 'ID',
                        'code'       => 'Code',
                        'value' => 'Value',
                        'hidden'     => 'Status',
                        'type'      => 'Type',
                        'label'=>'Label',
                        'created_at'  => 'Created At',
                        'updated_at'  => 'Updated At',
                        'actions'  => 'Actions',
                    ],
                ],
            ],
        ],
        'validation' =>[
            'attributes'=>[
                'id'          => 'ID',
                'code'       => 'Code',
                'key'=>'Key',
                'label'=>'Label',
                'value' => 'Value',
                'hidden'     => 'Status',
                'type'      => 'Type',
                'created_at'  => 'Created At',
                'updated_at'  => 'Updated At',
                'password'                => 'Password',
                'timezone'                => 'Timezone',
                'language'                => 'Language',
            ]
        ],
        'text'=>[
            'list of all system settings'=>'list of all system settings',
            'No settings found'=>'No settings found',
            'label describes the setting'=>'label describes the setting',
            'code will be used for getting the setting value'=>'code will be used for getting the setting value',
            'list of all system settings'=>'list of all system settings',
            'Add New Setting'=>'Add New Setting',
            'the value that assigned to this setting'=>'the value that assigned to this setting',
            'click to add new key value pair'=>'click to add new key value pair',
            'the value that assigned to this setting'=>'the value that assigned to this setting'
        ]
    ],
];