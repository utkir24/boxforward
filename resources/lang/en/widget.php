<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */


return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'menu' => [
            'title'  => 'Widget menu',
            'all'    => 'All Widgets',
            'create' => 'Create Widget',
            'deleted'=> 'Deleted Widgets',
            'edit'   => 'Edit Widget',
            'main'   => 'Widgets',
            'view'   => 'View Widget',

        ],
        'label'=>[
            'active'              => 'Active Widget',
            'create'              => 'Create Widget',
            'deleted'             => 'Deleted Widget',
            'edit'                => 'Edit Widget',
            'management'          => 'Widget Management',
            'view'                =>  'View Widget',
            'overview'            => 'Overview',
            'table' => [
                'id'          => 'ID',
                'title'       => 'Title',
                'description' => 'Description',
                'content'     => 'Content',
                'image'      => 'Image',
                'sort'        => 'Sort',
                'type'        => 'Type',
                'created_at'  => 'Created At',
                'updated_at'  => 'Updated At',
                'actions'  => 'Actions',
            ],

            'tabs' => [
                'titles' => [
                    'overview' => 'Overview',
                    'history'  => 'History',
                ],
                'content' => [
                    'overview' => [
                        'id'          => 'Id',
                        'title'       => 'Title',
                        'description' => 'Description',
                        'content'     => 'Content',
                        'image'       => 'Image',
                        'sort'        => 'Sort',
                        'type'        => 'Type',
                        'created_at'  => 'Created At',
                        'deleted_at'  => 'Deleted At',
                        'updated_at'  => 'Updated At',
                    ],
                ],
            ],
        ],
        'validation' =>[
            'attributes'=>[
                'active'                  => 'Active',
                'title'                   => 'Title',
                'description'             => 'Description',
                'content'                 => 'Content',
                'image'                  => 'Image',
                'sort'                    => 'Sort',
                'type'                    => 'Type',
                'password'                => 'Password',
                'timezone'                => 'Timezone',
                'language'                => 'Language',
            ]
        ]
    ],
    'frontend'=>[
        'widget'=>[
            'text'=>[
                'read more' => 'Read more',
                'close'=> 'Close'
                ],
            'read more' => 'Read more',
            'close' =>'Close'
        ],
        'text'=>[
            'read more' => 'Read more',],
    ]
];