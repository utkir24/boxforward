<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Exception Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in Exceptions thrown throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [

    ],

    'frontend' => [
        'text'=>[
            'You are currently logged in as'=>'You are currently logged in as',
            'Re-Login as'=>'Re-Login as',
            'Calculate the cost'=>'Calculate the cost',
            'new york'=>'New york',
            'moscow'=>'moscow',
            'sign-up'=>'Sign-up',
            'sign-in'=>'Sign-in',
            'your balance'=>'Your balance',
            'clother and shoes'=>'Clother and shoes',
            'street'=>'Street',
            'city'=>'Сity',
            'region'=>'Region',
            'code'=>'Code',
            'phone number'=>'phone number',
            'E-mail'=>'E-mail'
        ]
    ]
];
