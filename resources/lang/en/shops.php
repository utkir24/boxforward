<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'menu' => [
            'title'  => 'Shop menu',
            'all'    => 'All Shops',
            'create' => 'Create Shop',
            'deleted'=> 'Deleted Shops',
            'edit'   => 'Edit Shop',
            'main'   => 'Shops',
            'view'   => 'View Shop',

        ],
        'label'=>[
            'active'              => 'Active Shop',
            'create'              => 'Create Shop',
            'deleted'             => 'Deleted Shop',
            'edit'                => 'Edit Shop',
            'management'          => 'Shop Management',
            'view'                =>  'View Shop',
            'overview'            => 'Overview',
            'table' => [
                'id'          => 'ID',
                'title'       => 'Title',
                'description' => 'Description',
                'content'     => 'Content',
                'status'      => 'Status',
                'sort'        => 'Sort',
                'created_at'  => 'Created At',
                'updated_at'  => 'Updated At',
                'actions'  => 'Actions',
            ],

            'tabs' => [
                'titles' => [
                    'overview' => 'Overview',
                    'history'  => 'History',
                ],
                'content' => [
                    'overview' => [
                        'id'          => 'Id',
                        'title'       => 'Title',
                        'description' => 'Description',
                        'content'     => 'Content',
                        'status'      => 'Status',
                        'sort'        => 'Sort',
                        'created_at'  => 'Created At',
                        'deleted_at'  => 'Deleted At',
                        'updated_at'  => 'Updated At',
                    ],
                ],
            ],
        ],
        'validation' =>[
            'attributes'=>[
                'worth_knowing'=>'Worth knowing',
                'price' =>'Price',
                'shop_site'=>'Shop site',
                'payment_system'=>'Payment system',
                'link_site'=>'Link site',
                'active'                  => 'Active',
                'title'                   => 'Title',
                'description'             => 'Description',
                'content'                 => 'Content',
                'status'                  => 'Status',
                'sort'                    => 'Sort',
                'password'                => 'Password',
                'timezone'                => 'Timezone',
                'language'                => 'Language',
            ]
        ]
    ],
    'frontend'=>[
        'text'=>[
            'catalog'=>'All catalog',
            'shop_site'=>'Site shop:',
            'payment_system'=>'Payment methods:',
            'price'=>'Price:',
            'worth_knowing'=>'Worth knowing!'
        ]
    ]
];