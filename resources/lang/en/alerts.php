<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */
    'page'=>[
        'deleted' => 'Старница удалена.',
        'created' => 'Новая старница создана.',
        'updated' => 'Старница обновлена.',
    ],
    'backend' => [
        'roles' => [
            'created' => 'The role was successfully created.',
            'deleted' => 'The role was successfully deleted.',
            'updated' => 'The role was successfully updated.',
        ],
        'menu'=>[
            'created' => 'The menu was successfully created.',
            'deleted' => 'The menu was successfully deleted.',
            'updated' => 'The menu was successfully updated.',
        ],
        'category'=>[
            'created' => 'The category was successfully created.',
            'deleted' => 'The category was successfully deleted.',
            'updated' => 'The category was successfully updated.',
        ],
        'page'=>[
            'created' => 'The page was successfully created.',
            'deleted' => 'The page was successfully deleted.',
            'updated' => 'The page was successfully updated.',
        ],
        'faq'=>[
            'created' => 'The faq was successfully created.',
            'deleted' => 'The faq was successfully deleted.',
            'updated' => 'The faq was successfully updated.',
        ],
        'users' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email'  => 'A new confirmation e-mail has been sent to the address on file.',
            'confirmed'              => 'The user was successfully confirmed.',
            'created'             => 'The user was successfully created.',
            'deleted'             => 'The user was successfully deleted.',
            'deleted_permanently' => 'The user was deleted permanently.',
            'restored'            => 'The user was successfully restored.',
            'session_cleared'      => "The user's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated'             => 'The user was successfully updated.',
            'updated_password'    => "The user's password was successfully updated.",
        ],
    ],

    'frontend' => [
        'contact' => [
            'sent' => 'Your information was successfully sent. We will respond back to the e-mail provided as soon as we can.',
        ],
    ],
];
