<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */


return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'menu' => [
            'title'  => 'Post menu',
            'all'    => 'All posts',
            'create' => 'Create post',
            'deleted'=> 'Deleted posts',
            'edit'   => 'Edit post',
            'main'   => 'Posts',
            'view'   => 'View post',

        ],
        'label'=>[
            'active'              => 'Active post',
            'create'              => 'Create post',
            'deleted'             => 'Deleted post',
            'edit'                => 'Edit post',
            'management'          => 'Post Management',
            'view'                =>  'View post',
            'overview'            => 'Overview',
            'table' => [
                'id'          => 'ID',
                'title'       => 'Title',
                'description' => 'Description',
                'content_two' => 'Content two',
                'small_title' => 'Title small',
                'image'       => 'Image',
                'content'     => 'Content',
                'status'      => 'Status',
                'sort'        => 'Sort',
                'created_at'  => 'Created At',
                'updated_at'  => 'Updated At',
                'actions'  => 'Actions',
            ],

            'tabs' => [
                'titles' => [
                    'overview' => 'Overview',
                    'history'  => 'History',
                ],
                'content' => [
                    'overview' => [
                        'id'          => 'Id',
                        'title'       => 'Title',
                        'description' => 'Description',
                        'content'     => 'Content',
                        'content_two' => 'Content two',
                        'small_title' => 'Title small',
                        'image'       => 'Image',
                        'status'      => 'Status',
                        'sort'        => 'Sort',
                        'created_at'  => 'Created At',
                        'deleted_at'  => 'Deleted At',
                        'updated_at'  => 'Updated At',
                    ],
                ],
            ],
        ],
        'validation' =>[
            'attributes'=>[
                'active'                  => 'Active',
                'title'                   => 'Title',
                'content_two' => 'Content two',
                'small_title' => 'Title small',
                'image'       => 'Image',
                'Choose'      =>'Choose',
                'description'             => 'Description',
                'content'                 => 'Content',
                'status'                  => 'Status',
                'sort'                    => 'Sort',
                'type'                   => 'Type',
                'password'                => 'Password',
                'timezone'                => 'Timezone',
                'language'                => 'Language',
                'Category'=>'Сategory',
                'slug'=>'Slug'

            ]
        ],
        'search'=>[
            'find'=>'Search by title'
        ],
        'text' =>[
            'crud'=>[
                'Widgets'=>'Widgets',
                'Information Edit'=>'Information Edit',
                'Additional info'=>'Additional info',

            ],
        ],
    ],
    'frontend'=>[
        'text' =>[
            'error'=>[
                'description'=>'',
                'content'=>'There is no translation in the current content',
                'title'=>'Sorry, but we have not transferred this content yet'
            ]
        ],
    ]
];