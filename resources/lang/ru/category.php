<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'menu' => [
            'title'  => 'Category menu',
            'all'    => 'All Categories',
            'create' => 'Create Category',
            'deleted'=> 'Deleted Categories',
            'edit'   => 'Edit Category',
            'main'   => 'Categories',
            'view'   => 'View Category',

        ],
        'label'=>[
            'type'=>'Type',
            'active'              => 'Active Category',
            'create'              => 'Create Category',
            'deleted'             => 'Deleted Category',
            'edit'                => 'Edit Category',
            'management'          => 'Category Management',
            'view'                =>  'View Category',
            'overview'            => 'Overview',
            'table' => [
                'type'=>'Type',
                'id'          => 'ID',
                'title'       => 'Title',
                'description' => 'Description',
                'content'     => 'Content',
                'status'      => 'Status',
                'sort'        => 'Sort',
                'created_at'  => 'Created At',
                'updated_at'  => 'Updated At',
                'actions'  => 'Actions',
            ],

            'tabs' => [
                'titles' => [
                    'overview' => 'Overview',
                    'history'  => 'History',
                ],
                'content' => [
                    'overview' => [
                        'type'=>'Type',

                        'id'          => 'Id',
                        'title'       => 'Title',
                        'description' => 'Description',
                        'content'     => 'Content',
                        'status'      => 'Status',
                        'sort'        => 'Sort',
                        'created_at'  => 'Created At',
                        'deleted_at'  => 'Deleted At',
                        'updated_at'  => 'Updated At',
                    ],
                ],
            ],
        ],
        'validation' =>[
            'attributes'=>[
                'type'=>'Type',
                'slug'=>'Слаг',

                'active'                  => 'Active',
                'title'                   => 'Title',
                'description'             => 'Description',
                'content'                 => 'Content',
                'status'                  => 'Status',
                'sort'                    => 'Sort',
                'password'                => 'Password',
                'timezone'                => 'Timezone',
                'language'                => 'Language',
            ]
        ]
    ],
];