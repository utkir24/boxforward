<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'menu' => [
            'title'  => 'Settings menu',
            'all'    => 'All Settings',
            'create' => 'Create Settings',
            'deleted'=> 'Deleted Settings',
            'edit'   => 'Edit Settings',
            'main'   => 'Settings',
            'view'   => 'View Settings',

        ],
        'label'=>[
            'active'              => 'Active Settings',
            'create'              => 'Create Settings',
            'deleted'             => 'Deleted Settings',
            'edit'                => 'Edit Settings',
            'management'          => 'Settings Management',
            'view'                =>  'View Settings',
            'overview'            => 'Overview',
            'table' => [
                'id'          => 'ID',
                'code'       => 'Code',
                'value' => 'Value',
                'hidden'     => 'Status',
                'label'=>'Label',
                'type'      => 'Type',
                'created_at'  => 'Created At',
                'updated_at'  => 'Updated At',
                'actions'  => 'Actions',
            ],

            'tabs' => [
                'titles' => [
                    'overview' => 'Overview',
                    'history'  => 'History',
                ],
                'content' => [
                    'overview' => [
                        'id'          => 'ID',
                        'code'       => 'Code',
                        'value' => 'Value',
                        'hidden'     => 'Status',
                        'type'      => 'Type',
                        'label'=>'Label',
                        'created_at'  => 'Created At',
                        'updated_at'  => 'Updated At',
                        'actions'  => 'Actions',
                    ],
                ],
            ],
        ],
        'validation' =>[
            'attributes'=>[
                'id'          => 'ID',
                'code'       => 'Code',
                'label'=>'Label',
                'value' => 'Value',
                'hidden'     => 'Status',
                'type'      => 'Type',
                'created_at'  => 'Created At',
                'updated_at'  => 'Updated At',
                'password'                => 'Password',
                'timezone'                => 'Timezone',
                'language'                => 'Language',
            ]
        ]
    ],
];