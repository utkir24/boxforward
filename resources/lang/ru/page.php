<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */



return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'menu' => [
            'title'  => 'Page menu',
            'all'    => 'All Pages',
            'create' => 'Create Page',
            'deleted'=> 'Deleted Pages',
            'edit'   => 'Edit Page',
            'main'   => 'Pages',
            'view'   => 'View Page',

        ],
        'label'=>[
            'active'              => 'Active Page',
            'create'              => 'Create Page',
            'deleted'             => 'Deleted Page',
            'edit'                => 'Edit Page',
            'management'          => 'Page Management',
            'view'                =>  'View Page',
            'overview'            => 'Overview',
            'table' => [
                'id'          => 'ID',
                'title'       => 'Title',
                'content_two' => 'Content two',
                'small_title' => 'Title small',
                'image'       => 'Image',
                'Choose'      =>'Choose',
                'description' => 'Description',
                'content'     => 'Content',
                'status'      => 'Status',
                'sort'        => 'Sort',
                'created_at'  => 'Created At',
                'updated_at'  => 'Updated At',
                'actions'  => 'Actions',
            ],

            'tabs' => [
                'titles' => [
                    'overview' => 'Overview',
                    'history'  => 'History',
                ],
                'content' => [
                    'overview' => [
                        'id'          => 'Id',
                        'title'       => 'Title',
                        'content_two' => 'Content two',
                        'small_title' => 'Title small',
                        'image'       => 'Image',
                        'Choose'      =>'Choose',
                        'description' => 'Description',
                        'content'     => 'Content',
                        'status'      => 'Status',
                        'sort'        => 'Sort',
                        'created_at'  => 'Created At',
                        'deleted_at'  => 'Deleted At',
                        'updated_at'  => 'Updated At',
                    ],
                ],
            ],
        ],

        'search'=>[
            'find'=>'Search by title'
        ],
        'validation' =>[
            'attributes'=>[
                'active'                  => 'Active',
                'title'                   => 'Title',
                'description'             => 'Description',
                'content_two' => 'Content two',
                'small_title' => 'Title small',
                'image'       => 'Image',
                'Choose'      =>'Choose',
                'content'                 => 'Content',
                'status'                  => 'Status',
                'sort'                    => 'Sort',
                'type'                   => 'Type',
                'password'                => 'Password',
                'timezone'                => 'Timezone',
                'language'                => 'Language',
                'Category'=>'Категории',
                'slug'=>'Слаг'


            ]
        ],

        'text' =>[
            'crud'=>[
                'Widgets'=>'Виджеты',
                'Information Edit'=>'Настройка главных информатции',
                'Additional info'=>'Допольнителные информатции',

            ],
        ],

    ],
    'frontend'=>[
        'text' =>[
            'error'=>[
                'description'=>'',
                'content'=>'There is no translation in the current content',
                'title'=>'Sorry, but we have not transferred this content yet'
            ]
        ],
    ]

];