<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Exception Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in Exceptions thrown throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [

    ],

    'frontend' => [
        'text'=>[
            'You are currently logged in as'=>'Вы вошли в систему как',
            'Re-Login as'=>'Повторный вход в систему',
            'Calculate the cost'=>'Рассчитать стоимость',
            'new york'=>'Нью-Йорк',
            'moscow'=>'Москва',
            'sign-up'=>'Регистрация',
            'sign-in'=>'Вход',
            'your balance'=>'Ваш баланс',
            'clother and shoes'=>'одежда и обувь',
            'street'=>'Улица',
            'city'=>'Город',
            'region'=>'Штат',
            'code'=>'КОД',
            'phone number'=>'Номер телефона',
            'E-mail'=>'Е-майл'
        ]
    ]
];
