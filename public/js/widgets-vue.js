var appWidget = new Vue({
    el:'#accordions-bonuses',
    data:{
        widgets :[
            {
                title:'',description:'',content:'',
                children:[],
                image_list:[],
                images:'',
                extraFields :[
                    {
                       'name' : '',
                       'value' : ''
                    }
                ]
            }
        ],
        image_lt:[{
            id:'',
            value:''
        }],
        ClassList:[
            {
                'id':'lg',
                'title':'Large',
            },
            {
                'id':'md',
                'title':'Middle',
            },
            {
                'id':'sm',
                'title':'Small',
            },
            {
                'id':'xs',
                'title':'Extra small',
            }
        ]
    },
    mounted: function () {
        this.init();
    },
    methods: {
        init: function () {

            var id = $('#accordions-bonuses').data('widget-id');

            if (parseInt(id) > 0) {
                axios.get('/admin/widget/notifications/' + id).then(response => {
                    this.widgets = response.data
                });
            }
        },
        addWidgetChildren: function (index) {
            this.widgets[index].children.push({
                title: '', description: '', content: '',images:'',image_list:[]
            });
        },
        addExtraFiled: function (index) {
            this.widgets[index].extraFields.push({
                name: '',value:''
            });
        },
        addWidget: function () {
            this.widgets.push({
                title: '', description: '', content: '', children: [],image_list:[],extraFields:[]
            });
        },
        getParentHeadingName: function (index) {
            return 'headingcollapseParent' + index;
        },
        getParentCollapseName: function (prefix, index) {
            return prefix + 'collapseParent' + index;
        },
        getChildHeadingName: function (index, parentIndex) {
            return 'headingcollapseChild' + parentIndex + index;
        },
        getChildCollapseName: function (prefix, index, parentIndex) {
            return prefix + 'collapseChild' + parentIndex + index;
        },
        getParentInputName(index, dataName) {
            return "Widgets[" + index + "][" + dataName + "]";
        },
        getParentInputExtraName(index, extraIndex,dataName) {
            return "Widgets[" + index + "][extraField][" + extraIndex + "][" + dataName + "]";
        },
        getChildrenInputName(parentIndex, index, dataName) {
            return "Widgets[" + parentIndex + "][children][" + index + "][" + dataName + "]";
        },
        getChildrenInput: function (prentIndex, title, index, name, value, input) {
            var content = '';
            content += '<label class="label">' + title + '</label>';
            content += '<div class="control">';
            if (input) {
                content += '<input class="input is-small" ' +
                    'name="' + this.getChildrenInputName(prentIndex, index, name) + '" ' +
                    'type="text" placeholder="' + name + '" ' +
                    'value="' + value + '">';
            } else {
                content += '<textarea class="textarea" ' +
                    'name="' + this.getChildrenInputName(prentIndex, index, name) + '" ' +
                    ' placeholder="' + name + '">'+value+'</textarea>';
            }
            content += '</div>';
            return content;
        },
        getParentInput: function (title, index, name, value, input) {
            var content = '';
            content += '<label class="label">' + title + '</label>';
            content += '<div class="control">';
            if (input) {
                content += '<input class="form-control is-small" ' +
                    'name="' + this.getParentInputName(index, name) + '" ' +
                    'type="text" placeholder="' + name + '" ' +
                    'value="' + value + '">';
            } else {
                content += '<textarea class="textarea" ' +
                    'name="' + this.getParentInputName(index, name) + '" ' +
                    'type="text" placeholder="' + name + '">'+value+'</textarea>';
            }
            content += '</div>';
            return content;
        },
        getParentImageInput :function(index,name,widget){
            var content='',i=0,src='';
            content = `<div class="input-group">
                        <span class="input-group-btn">
                             <a id="widget-image-btn`+(index+1)+`"  data-input="widget-image-thumbnail`+(index+1)+`"
                                    data-preview="holder-image-widget`+(index+1)+`" 
                                    class="btn btn-primary">
                                <i class="fa fa-picture-o"></i> Image
                             </a>
                        </span>
                        <input id="widget-image-thumbnail`+(index+1)+`" 
                               class="form-control" value="`+widget.images+`"
                                type="hidden" name="`+this.getParentInputName(index,'images')+`">
                    </div>`;
            content+=`<img id="holder-image-widget`+(index+1)+`" class="img-thumbnail"  style="margin-top:15px;max-height:200px;">`;


           return content;
        },
        getParentHiddenInput: function (index, name, value) {

            var content = '';
            content += '<input class="form-control is-small" ' +
                'name="' + this.getParentInputName(index, name) + '" ' +
                'type="hidden" placeholder="' + name + '" ' +
                'value="' + value + '">';

            return content;
        },
        getChildrenHiddenInput: function (prentIndex, index, name, value) {
            var content = '';
            content += '<div class="control">';
            content += '<input class="input is-small" ' +
                'name="' + this.getChildrenInputName(prentIndex, index, name) + '" ' +
                'type="text" placeholder="' + name + '" ' +
                'value="' + value + '">';
            content += '</div>';
            return content;
        },
        deleteParent :function(index){
            this.widgets.splice(index,1);
        },
        deleteChild:function (parentIndex,index) {

          this.widgets[parentIndex].children.splice(index,1);
        },
        deleteExtrafield:function(parentIndex,index){
            this.widgets[parentIndex].extraFields.splice(index,1);
        },
        clikedNotification:function (event) {
            event.preventDefault();
            $('div.input-group span.input-group-btn a[id^="widget-image-btn"]').filemanager('image');
            var options = {
                filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
                filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token=',
                allowedContent : true
            };
            $('textarea').ckeditor(options);

            $.each(this.widgets, function (index, value) {
                var image_object = $('.card-body .form-group img#holder-image-widget'+(index+1));
                image_object.html('');
                for (image in value.image_list) {
                    image_object.append(
                        $('<img>').css('height', '5rem').attr('src', ''+value.image_list[image]+'')
                    );
                }
                image_object.trigger('change');

                $.each(value.children, function (indexChild, valueChild) {
                    var childImageObject = $('#holder-image-widget'+(indexChild+1));
                    childImageObject.html('');
                    for (imageChild in valueChild.image_list) {
                        childImageObject.append(
                            $('<img>').css('height', '5rem').attr('src', valueChild.image_list[imageChild])
                        );
                    }
                    childImageObject.trigger('change');
                });

            });
        },

        onChangeChildrenInputSelect : function (index,childIndex,event) {
            this.widgets[index].children[childIndex].description = event.target.value;
        }

    }
});

var widgetCat = new Vue({
    el:'#widget-categories',
    data:{
        widgetCat :[
            {
                category_id:0,
                count:0,
                selected:0
            }
        ],
        'list':[]

    },
    mounted: function () {
        this.init();
    },
    methods: {
        init: function () {

            var id = $('#widget-categories').data('widget-id');

            if(!id){
                id=0;
            }

            //if (parseInt(id) > 0) {
                axios.get('/admin/widget/categories/' + id).then(response => {
                    this.widgetCat = response.data.items;
                    this.list = response.data.list;
                });
         //   }
        },
        addWidgetCat: function (index) {
            this.widgetCat.push({
                category_id:0,
                count:0,
                selected:0
            });
        },
        deleteWidgetCat :function(index){
            this.widgetCat.splice(index,1);
        },

        getParentHeadingName: function (index) {
            return 'headingcollapseParent' + index;
        },

        getParentCollapseName: function (prefix, index) {
            return prefix + 'collapseParent' + index;
        },

        clickedWidgetCat:function (event) {
            event.preventDefault();
            $(document).ready(function(){
                $('.form-select2').select2();
            });
        },

        getCatInputName:function(index, dataName) {
            return "WidgetCats[" + index + "][" + dataName + "]";
        },
        isSelected :function (index,val2) {
            if(this.widgetCat[index].category_id == val2){
                item.selected = item.category_id;
                console.log(item.category_id +'=>' + val2 );
                return true;
            }
            return false;
        },

        onChangeWidgetCat:function (index,event) {
          this.widgetCat[index].category_id = event.target.value;
        }
    }
});




