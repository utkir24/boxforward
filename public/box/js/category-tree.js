$(document).ready(function(){
    $("#tree").fancytree({
        checkbox: true,
        select: function(event, data) {
            var s = data.tree.getSelectedNodes();
            var r = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
            //console.log(r);
            document[r] = [];
            s.map(function(node){
              //  console.log(node.key);
                document[r].push(node.key);
            });
            //console.log(document[r]);
            $('.cat-fa').val(document[r].join(","));
        }
    });

    $('#categories').val = '';
    $("#tree").fancytree("getTree").visit(function(node) {
       var ids = [];
        $('ul#treeData li').each(function (key,item) {
            if($(this).data('selected')==true){
                ids.push(item.id);
            }
       });
        if(ids.indexOf(node.key)!=-1){
            $('#categories').val(ids.join(","));
        }
    });
});