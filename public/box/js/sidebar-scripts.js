$(document).ready(function() {
	$('.faq-tab-item .js-faq-link').on('click', function(e){
		e.preventDefault();
		var self = $(this);
		var parent = $(this).closest('.faq-page-main_tabs_position_tabs');
		parent.find('.faq-tab-item').removeClass('active');
		parent.find('.faq-tab-item .__list li').removeClass('active');
		self.closest('.faq-tab-item').addClass('active');
		self.closest('li').addClass('active');
		$('.faq-page-main_tabs_positions_tabs').find('.faq-page-main_tabs_position_content').removeClass('active');
		$('.faq-page-main_tabs_positions_tabs').find($.attr(this, 'href')).addClass('active');
	})
	$('.faq-tab-item .js-faq-sublink').on('click', function(e){
		e.preventDefault();
		var self = $(this);
		var parent = $(this).closest('.faq-page-main_tabs_position_tabs');
		parent.find('.faq-tab-item').removeClass('active');
		parent.find('.faq-tab-item .__list li').removeClass('active');
		self.closest('.faq-tab-item').addClass('active');
		self.closest('li').addClass('active');
		if($( $.attr(this, 'href') ).length > 0){
			$('html, body').animate({
		        scrollTop: $( $.attr(this, 'href') ).offset().top
		    }, 500);
		}
	   
	})
});