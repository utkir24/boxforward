$(document).ready(function() {
    $('.faq-tab-item .js-faq-link').on('click', function(e){
        e.preventDefault();
        var self = $(this);
        var parent = $(this).closest('.faq-page-main_tabs_position_tabs');
        parent.find('.faq-tab-item').removeClass('active');
        parent.find('.faq-tab-item .__list li').removeClass('active');
        self.closest('.faq-tab-item').addClass('active');
        self.closest('li').addClass('active');
        $('.faq-page-main_tabs_positions_tabs').find('.faq-page-main_tabs_position_content').removeClass('active');
        $('.faq-page-main_tabs_positions_tabs').find($.attr(this, 'href')).addClass('active');
        $('html, body').animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 500);
    })
    $('.faq-tab-item .js-faq-sublink').on('click', function(e){
        e.preventDefault();
        var self = $(this);
        var parent = $(this).closest('.faq-page-main_tabs_position_tabs');
        parent.find('.faq-tab-item').removeClass('active');
        parent.find('.faq-tab-item .__list li').removeClass('active');
        self.closest('.faq-tab-item').addClass('active');
        self.closest('li').addClass('active');
        $('html, body').animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 500);
    });
    var _super = $.ajaxSettings.xhr;
    $.ajaxSetup({
        xhr: function() {
            var xhr = _super();
            var getAllResponseHeaders = xhr.getAllResponseHeaders;
            xhr.getAllResponseHeaders = function() {
                var allHeaders = getAllResponseHeaders.call(xhr);
                if (allHeaders) {
                    return allHeaders;
                }
                allHeaders = "";
                var concatHeader = function(i, header_name) {
                    if (xhr.getResponseHeader(header_name)) {
                        allHeaders += header_name + ": " + xhr.getResponseHeader( header_name ) + "\n";
                    }
                };
                // simple headers (fixed set)
                $(["x-runtime", "date", "etag", "server", "Last-Modified", "Pragma"]).each(concatHeader);
                // non-simple headers (add more as required)
                $(["Location"] ).each(concatHeader);
                return allHeaders;
            };
            return xhr;
        }
    });
    /*$('//#click').on('click',function(){
        $.ajax({
            url: 'https://boxfwd.com/api/v1/countries?locale=en',
            type: 'GET',
            dataType : "json",
            headers: {
                "accept": "application/json"
            },
            crossDomain: true,
            contentType: "application/json",
            complete:function(data, textStatus, jqXHR){
                console.log(data);
            }
        });
    });*/

    // var src = $('img').attr('src');
    // $('img').attr('src','http://boxforward.uz/storage/files/1/'+src);
});