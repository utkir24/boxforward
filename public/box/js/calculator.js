$(function() {

    var _Button = $('#js_calc_button');
    var _Country = $('#js_select_country');
    var _Region = $('#js_select_region');
    var _City = $('#js_select_city');
    var _Weight = $('#js_input-range');
    var _Price = $('#input_price');
    var _Count = $('#input_sum');
    var _Loader = $('#loader');
    var lang = $('html').attr('lang');
    _Loader.hide();

    var CityCount = 0;


    // var methods = [
    // {id:"box_berry_courier",name:"PickPoint courier"},
    // {id:"express",name:"USPS-Express"},
    // {id:"priority",name:"USPS-priority"},
    // {id:"box_berry_courier", name:"PickPoint courier"}
    // ];
    var methods = [];
    function enableCalcButton() {
        _Button.removeAttr("disabled");
        _Button.css('background-color', '#d90952');
    }
    function disableCalcButton() {
        _Button.attr("disabled", "disabled");
        _Button.css('background-color', '#dddddd');

        for(var i = 0; i < methods.length; i ++) {
            $("#" + methods[i]["id"]).find('.block__price').css({'height':'0px', ',min-height':'0', 'opacity':'0'});
        }
    }

    function loadCountry() {
        $.ajax({
            url: "https://account.boxfwd.com/api/v1/countries?locale="+lang,
            type: 'GET',
            success: function(json) {
                _Country.empty();
                _Country.append($('<option>').text('Выберите Страну...').attr('value', null).attr('has_regions', false));
                for(var i = 0; i < json.length; i ++) {
                    _Country.append($('<option>').text(json[i].name).attr('value', json[i].code).attr('has_regions', json[i]["has_regions?"]));
                }
            }
        });
    }
    function loadRegion() {
        $.ajax({
            url: "https://account.boxfwd.com/api/v1/countries/" +getCurrentCountryCode()+ "/regions",
            type: 'GET',
            success: function(json) {

                _Region.empty();
                _Region.append($('<option>').text('Выберите Регион...').attr('value', null).attr('has_cities', false));
                for(var i = 0; i < json.length; i ++) {
                    _Region.append($('<option>').text(json[i].name).attr('value', json[i].id).attr('has_cities', json[i]["has_cities?"]));
                }
            }
        });
    }

    function loadCity() {
        CityCount = 0;
        $.ajax({
            url: "https://account.boxfwd.com/api/v1/countries/"+getCurrentCountryCode()+"/regions/"+getCurrentRegionCode()+"/cities",
            type: 'GET',
            success: function(json) {
                _City.empty();
                _City.append($('<option>').text('Выберите Город...').attr('value', null));
                for(var i = 0; i < json.length; i ++) {
                    CityCount ++;
                    _City.append($('<option>').text(json[i].name).attr('value', json[i].id));
                }
                if(CityCount == 0) {
                    _City.hide();
                    enableCalcButton();
                } else {
                    _City.show();
                }
            }
        });
    }
    function getCurrentCountryCode() {
        var currOption = $(_Country).find("option:selected");
        var val = currOption.attr('value');
        if(val == undefined) val = "";
        return val;
    }
    function getCurrentRegionCode() {
        var currOption = $(_Region).find("option:selected");
        var val = currOption.attr('value');
        if(val == undefined || val == null) val = "";
        return val;
    }
    function getCurrentCityCode() {
        var currOption = $(_City).find("option:selected");
        var val = currOption.attr('value');
        if(val == undefined || val == null) val = "";
        return val;
    }
    function getCurrentWeight() {
        return _Weight.next().html();
    }
    function getCurrentPrice() {
        return "1";
        var res = _Price.val();
        if(res == "") res = "0";
        return res;
    }
    function getCurrentCount() {
        return "1";
        var res = _Count.val();
        if(res == "") res = "0";
        return res;
    }
    function getShippingList() {
        var s = "";
        for(var i = 0; i < methods.length; i ++) {
            if(s != "") s += ",";
            s += methods[i]["id"];
        }
        return s;
    }
    _Country.on('change', function() {
        disableCalcButton();
        _Region.hide();
        _City.hide();

        var currOption = $(_Country).find("option:selected");
        var val = currOption.attr('value');
        var has_regions = currOption.attr('has_regions');

        if(val == undefined || val == null) {
            _Region.hide();
            return ;
        }
        if(has_regions == "true") {
            loadRegion();
            _Region.show();
        } else {
            enableCalcButton();
        }
    });
    _Region.on('change', function() {
        disableCalcButton();
        _City.hide();
        var currOption = $(_Region).find("option:selected");
        var val = currOption.attr('value');
        var has_cities = currOption.attr('has_cities');
        if(val == undefined || val == null) {
            _City.hide();
            return ;
        }
        if(has_cities == "true") {
            loadCity();
        } else {
            enableCalcButton();
        }
    });
    _City.on('change', function() {
        disableCalcButton();
        var currOption = $(_City).find("option:selected");
        var val = currOption.attr('value');
        if(val == undefined || val == null) {
            return ;
        }
        enableCalcButton();
    });


    function loadShippingMethods() {

        var grid = $('.calculator-page-content_main_calculated');
        var block = $('.calculator-page-content_main_calculated_block');
        var colorclass = ["usps-express", "usps-priority", "box-pickup", "box-courier"];

        $.ajax({
            url: "https://account.boxfwd.com/api/v1/shipping_methods?locale="+lang,
            type: 'GET',
            success: function(json) {
                var newJson=[];
                for(var j=0;j<json.length;j++){
                    var title = json[j]['name'];
                    var word = title.substr(0, title.indexOf(" "));
 		    var idJson = json[j]['id'];	
                    var  notIds = ['box_berry_courier','box_berry_pickup','avia_express','avia_econom'];
                    if(notIds.indexOf(idJson)==-1){ console.log(idJson+'---------------------id---');
                        newJson.push(json[j]);
                    }
                }
                json = newJson;
                methods = json;
                for(var i = 0; i < json.length; i ++) {
                    var title = json[i]['name'];
                       console.log(json[i]['id']+'--------------------------------------');
                    var word = title.substr(0, title.indexOf(" "));

                    var obj = block.clone();
                    obj.attr('id', json[i]['id']);
                    var rest = title.substr(title.indexOf(" "));

                    if(i % 4 == 0 &&  i > 0) {
                        var grid1 = $('<div />', {
                            'class': 'calculator-page-content_main_calculated',
                        });
                        grid.css('height', 'auto');
                        grid.after(grid1);
                        grid = grid1;

                    }

                    if(word == "") {
                        word = title.substr(0, title.indexOf("-"));
                        if(word == "") {
                            word = title;
                            rest = "";
                        } else {
                            rest = title.substr(title.indexOf("-")+1)
                        }
                    }
                    obj.find('.__title').html(word).append("<span>" + rest + "</rest>");
                    obj.addClass(colorclass[i % colorclass.length]);
                    obj.appendTo(grid);
                }
                for(var i = json.length ; i < json.length + 4; i ++) {
                    if(i % 4 == 0) break;
                    var obj = block.clone();
                    obj.css('opacity', 0);
                    obj.appendTo(grid);
                }


            }
        });



        block.remove();
    }
    var left, right;
    _Button.on('click', function() {
        _Loader.show();
        setTimeout(function(){ callAjax(); }, 5);
    });

    function callAjax() {
        left = 0;
        right = methods.length;
        //
        //console.log(getCurrentCountryCode());
        for(var i = 0; i < methods.length; i ++) {
            $("#" + methods[i]["id"]).find('.__price').html('--');
        }
        var url = "https://account.boxfwd.com/api/v1/calculator?locale="+lang;
        url += "&country_code=" + getCurrentCountryCode();
        if(getCurrentRegionCode() != "") url += "&region_id=" + getCurrentRegionCode();
        if(getCurrentCityCode() != "") url += "&city_id=" + getCurrentCityCode();
        url += "&weight=" + getCurrentWeight();
        url += "&orders_price=" + getCurrentPrice();
        url += "&item_count=" + getCurrentCount();
        url += "&insurance=1";
        for(var i = 0; i < methods.length; i ++) {
            var currUrl = url;
            var method =  methods[i];
            currUrl += "&shipping_method=" + method['id'];
            //callAjax(currUrl, method['id']);
            $.ajax({
                url: currUrl,
                async:false,
                type: 'GET',
                beforeSend: function() {
                    if(left == 0) {
                        _Loader.css('display', 'block');
                    }
                    left ++;
                },
                success: function(json) {

                    if($.type(json[0]) == "object") {
                        $("#" + method["id"]).find('.__price').html(parseFloat(json[0]["total"]).toFixed(3));
                    }

                },
                complete: function() {
                    if(left == right) {
                        _Loader.hide();
                    }
                }
            });


        }
        _Loader.css('display', 'none');
    }
    loadShippingMethods();
    disableCalcButton();
    loadCountry();
    _Country.show();
    _Region.hide();
    _City.hide();

});
