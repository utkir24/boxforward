<?php

namespace App\Langs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang as Langs;

/**
 * Class Lang
 * @package App\Langs
 */
class Lang extends Model
{
    /**
     * @return array
     */
    public static function langs(){
        return [
            'en' => 'English',
            'ru' => 'Русский'
        ];
    }

    /**
     * @return mixed
     */
    public static function current(){
       // \App::setLocale("ru");
        return Langs::locale();
    }

    public static function set($lang = null){
        request()->session()->put('locale_old', Lang::current());
        if(!array_key_exists($lang,self::langs())){
            return false;
        }
        session()->put('locale', $lang);
        return \App::setLocale($lang);
    }
    public static function getDefaultLang(){
        try{
            $readerCountry = new \GeoIp2\Database\Reader(base_path('/GeoIP/GeoLite2-Country.mmdb'));
            $geo = $readerCountry->country(request()->getClientIp());
            $iso = $geo->country->isoCode;
        }catch (\Exception $exception){
            $iso = "EN";
        }
        $russian_langs_default = [
            'AZ',
            'AM',
            'BY',
            'GE',
            'KZ',
            'KG',
            'MD',
            'RU',
            'TJ',
            'TM',
            'UZ',
            'UA'
        ];
        if(in_array($iso,$russian_langs_default)){
            return "ru";
        }
        return "en";
    }

}
