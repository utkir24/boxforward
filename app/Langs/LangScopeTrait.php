<?php
/**
 * Created by PhpStorm.
 * User: jakhar
 * Date: 8/20/18
 * Time: 9:23 PM
 */

namespace App\Langs;

/**
 * Trait LangScopeTrait
 * @package App\Langs
 */
trait LangScopeTrait
{
    /**
     * @param $query
     * @param null $lang
     * @return mixed
     */
    public function scopeLang($query, $lang = null)
    {
        if($lang == null){
            $lang = Lang::current();
        }
        return $query->where('lang', $lang);
    }

    /**
     * @param $query
     * @param null $lang_hash
     * @return mixed
     */
    public function scopeLanghash($query, $lang_hash = null)
    {
        return $query->where('lang_hash', $lang_hash);
    }

    /**
     * @return mixed
     */
    public function translations(){
        return static::withoutGlobalScope(LangScope::class)->where('lang','NOT LIKE',$this->lang)->langhash($this->lang_hash)->get();
    }
    /**
     * @return mixed
     */
    public function scopeTranslation($query,$lang){
        return static::withoutGlobalScope(LangScope::class)->where(['lang' => $lang,'lang_hash' => $this->lang_hash])->first();
    }
}