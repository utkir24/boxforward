<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Langs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang as Langs;
/**
 * Class LangTabWidget
 * @package App\Langs
 */
class LangTabWidget
{
    /**
     * @param string $class_name
     * @param string $model_name
     * @param null|Model $model
     * @return string
     */
    public static function widget($class_name,$model_name,$model = null,$someParam=[])
    {
        static::initLang();
        if ($model) {
            return static::edit($model, $class_name, $model_name,$someParam);
        }
        return static::create($model_name,$class_name,$someParam);
    }

    /**
     * @param $model_name
     * @return string
     */
    private static function create($model_name,$class_name,$someParam=[])
    {
        $content = '';
        $request = request()->request;
        $locale = Langs::locale();
        $langs = Lang::langs();
        $lang  = $request->get('lang','');
        $lang_hash = $request->get('lang_hash','');
        foreach ($langs as $index => $label) {
         $class = $index == $locale ? 'btn btn-primary active' : 'btn btn-primary';
            if ($lang_hash) {
                $other_model = $class_name::where('lang_hash', $lang_hash)->where('lang', $index)->get();
                if ($other_model->count()) {
                    $content .= '<a href="' . route('admin.' . $model_name . '.edit', [$model_name=>$other_model[0],'lang'=>$other_model[0]->lang ? $other_model[0]->lang : $locale]) . '" title="' . $label . '" class="'.$class.'">' . $label . '</a>';
                } else {
                    $content .= '<a href="' . route('admin.' . $model_name . '.create', ['lang' => $index, 'lang_hash' => $lang_hash]+$someParam) . '" title="' . $label . '" class="'.$class.'">' . $label . '</a>';
                }
            } else {
                $content .= '<a href="' . route('admin.' . $model_name . '.create', ['lang' => $index]+$someParam) . '" title="' . $label . '" class="'.$class.'">' . $label . '</a>';
            }
        }
        return $content;
    }

    /**
     * @param Model $model
     * @param string $class_name
     * @param string $model_name
     * @return string
     */
    private static function edit($model, $class_name, $model_name,$someParam)
    {
        $langs = Lang::langs();
        $locale = Langs::locale();
        $content = '';
        if ($model->lang_hash) {
            foreach ($langs as $index => $label) {
                $class = $index == $locale ? 'btn btn-primary active' : 'btn btn-primary';
                if ($model->lang == $index) {
                    $content .= '<a href="' . route('admin.' . $model_name . '.edit', [$model_name=>$model,'lang'=>$model->lang ? $model->lang : $locale]) . '" title="' . $label . '" class="'.$class.'">' . $label . '</a>';
                } else {
                    $other_model = $class_name::where('lang_hash', $model->lang_hash)->where('lang', $index)->get();
                    if ($other_model->count()) {
                        $content .= '<a href="' . route('admin.' . $model_name . '.edit',[$model_name=> $other_model[0],'lang'=> $other_model[0]->lang ?  $other_model[0]->lang : $locale]) . '" title="' . $label . '" class="'.$class.'">' . $label . '</a>';
                    } else {
                        $content .= '<a href="' . route('admin.' . $model_name . '.create', ['lang' => $index, 'lang_hash' => $model->lang_hash]+$someParam) . '" title="' . $label . '" class="btn btn-primary">' . $label . '</a>';
                    }
                }
            }
        } else {
            $content = static::create($model_name,$class_name);
        }
        return $content;

    }

    public static function langForm()
    {
        $lang = request()->get('lang', null);
        $lang_hash = request()->get('lang_hash', null);
        $content = html()->hidden('lang')->value(isset($lang) ? $lang : null);
        $content .= html()->hidden('lang_hash')->value(isset($lang_hash) ? $lang_hash : null);
        return $content;
    }

    public static function initLang()
    {
        $lang = request()->get('lang', null);
        if ($lang) {
            Lang::set($lang);
        }
    }
}