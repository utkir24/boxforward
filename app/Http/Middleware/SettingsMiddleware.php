<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Backend\Settings\SettingsController;
use App\Models\Settings\Setting;
use App\User;
use Closure;
use Illuminate\Routing\Router;

class SettingsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $action_name =  $request->route()->getActionName();
        if ($action_name == SettingsController::class.'@'.'create') {
            $lang_hash = $request->get('lang_hash', null);
            if ($lang_hash) {
                $current = Setting::where('lang_hash', $lang_hash)->get();
                if ($current->count()) {
                    $request->request->add(['type'=> $current[0]->type]);
                }
            }
        }

        return $next($request);
    }
}
