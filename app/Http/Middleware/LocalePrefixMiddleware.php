<?php

namespace App\Http\Middleware;

use App\Langs\Lang;
use Closure;
use Carbon\Carbon;

/**
 * Class LocaleMiddleware.
 */
class LocalePrefixMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->segment(1);
        if($request->segments()){
            $segment = $request->segments();
            $segment[0] = $locale;
           request()->session()->put('back-url',$segment);
        }


        return $next($request);
    }
}
