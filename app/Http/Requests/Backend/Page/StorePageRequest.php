<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Http\Requests\Backend\Page;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StorePageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required|string',
            'description'     => 'string|nullable',
            'content'  => 'nullable|string',
            'status'    => 'integer|nullable',
            'sort' => 'nullable|integer',
            'template_id'=>'integer|nullable',
            'image'=>'string|nullable',
            'content_two'=>'string|nullable',
            'small_title'=>'string|nullable'
        ];
    }
}
