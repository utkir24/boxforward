<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Http\Requests\Backend\Shops;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreShopsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required|string',
            'description'     => 'nullable|string',
            'content'  => 'nullable|string',
            'status'    => 'nullable|integer',
            'sort' => 'nullable|integer',
            'worth_knowing' =>'string',
            'image' =>  'nullable|string',
            'price' =>'nullable|string',
            'payment_system'=>'nullable|string',
            'shop_site'=>'nullable|string',
            'link_site'=>'nullable|string',

        ];
    }
}
