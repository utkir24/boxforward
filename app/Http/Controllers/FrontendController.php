<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 030 30.08.18
 * Time: 20:21
 */

namespace App\Http\Controllers;


use App\Repositories\Frontend\Settings\SettingsRepository;
use Barryvdh\Debugbar\Controllers\BaseController;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

class FrontendController extends BaseController
{
    public $settings;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function __construct(SettingsRepository $settings)
    {
        $this->settings = $settings->getAll();
    }
    public function render($view,$attributes = []){
        $attributes = array_merge($attributes,['settings'=>$this->settings]);
        return view($view,$attributes);
    }
}