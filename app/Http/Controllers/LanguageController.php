<?php

namespace App\Http\Controllers;

use App\Langs\Lang;

/**
 * Class LanguageController.
 */
class LanguageController extends Controller
{
    /**
     * @param $locale
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function __invoke($locale)
    {
        Lang::set($locale);

        return redirect()->back();
    }
}
