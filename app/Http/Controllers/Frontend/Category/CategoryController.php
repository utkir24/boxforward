<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Http\Controllers\Frontend\Category;

use App\Http\Controllers\Controller;
use App\Models\Category\Category;
use App\Models\Page\Page;
use App\Repositories\Frontend\Category\CategoryRepository;
use App\Repositories\Frontend\Page\PageRepository;

class CategoryController extends Controller
{

    protected $repository;
    protected $pageRepository;


    public function __construct(CategoryRepository $repository,PageRepository $pageRepository)
    {
        $this->repository = $repository;
        $this->pageRepository = $pageRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index($slug=null)
    {
        if($slug){
            $category = Category::where('type',Category::TYPE_POST)->lang()->active()->slug($slug)->first();
            if($category){
                $clone_model = clone $category;
                $posts = $this->repository->getActivePaginateWithModel($clone_model->pages());
                return view('frontend.category.index',['posts'=>$posts,'category'=>$category,'categories'=>$this->repository->getCategoriesList()]);
            }
            abort('404');
        }
        $category = Category::where('type',Category::TYPE_POST)->lang()->active()->first();
        if($category){
            $clone_model = clone $category;
            $posts = $this->repository->getActivePaginateWithModel($clone_model->pages());
            return view('frontend.category.index',['posts'=>$posts,'category'=>$category,'categories'=>$this->repository->getCategoriesList()]);
        }
        abort('404');
    }


    public function post($slug)
    {

        $post = $this->pageRepository->getOneBySlug($slug);
        if (!$post) abort('404');

        return view('frontend.category.post', ['post' => $post, 'categories' => $this->repository->getCategoriesList()]);
    }

}