<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Http\Controllers\Frontend\Faq;

use App\Http\Controllers\Controller;
use App\Models\Category\Category;
use App\Repositories\Frontend\Page\PageRepository;

class FaqController extends Controller
{

    protected $faqRepository;


    public function __construct(PageRepository $faqRepository)
    {
        $this->faqRepository = $faqRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $categories = Category::langWithTypeAndSort(Category::TYPE_FAQ)->nested()->get();

        return view('frontend.faq.test',['faqs'=>$this->faqRepository->getActivePaginated(),'categories'=>$categories]);
    }

}