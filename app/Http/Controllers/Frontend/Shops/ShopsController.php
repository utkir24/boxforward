<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 028 28.09.18
 * Time: 22:29
 */

namespace App\Http\Controllers\Frontend\Shops;
use App\Http\Controllers\Controller;
use App\Models\Category\Category;
use App\Repositories\Frontend\Shops\ShopsRepository;

class ShopsController extends Controller
{

    protected $shopsRepository;


    public function __construct(ShopsRepository $shopsRepository)
    {
        $this->shopsRepository = $shopsRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $categories = Category::langWithTypeAndSort(Category::TYPE_SHOPS)->nested()->get();

        return view('frontend.shops.index',['shops'=>$this->shopsRepository->getActivePaginated(),'categories'=>$categories]);
    }

}