<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */



namespace App\Http\Controllers\Frontend\Page;

use App\Http\Controllers\Controller;
use App\Langs\Lang;
use App\Models\Category\Category;
use App\Models\Page\Page;
use App\Repositories\Frontend\Page\PageRepository;
use Illuminate\Http\Request;


/**
 * Class PageController
 * @package App\Http\Controllers\Frontend\Page
 */
class PageController extends Controller
{

    /**
     * @var
     */
    protected $pageRepository;


    /**
     * PageController constructor.
     * @param PageRepository $pageRepository
     */
    public function __construct(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }




    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view()
    {
        $slug = request()->route()->parameter('slug');

        if ($page = Page::slug($slug)->active()->first()):

            $lang_page = Page::langhash($page->lang_hash)->active()->lang()->first();

            if ($lang_page) {
                if ($lang_page->type == Page::TYPE_PAGE) {
                    return $this->getTemplate($lang_page);
                }

                return view('frontend.page.post_image',['page'=>$lang_page]);
            }
        endif;
        return view('frontend.page.error');
    }

//    public function getLink($lang_page)
//    {
//        if($lang_page->type === Page::TYPE_POST){
//            return view('frontend.page.page-images',['page'=>$lang_page]);
//        }
//        if($lang_page->type === Page::TYPE_PAGE){
//            return view('frontend.page.page-default',['page'=>$lang_page]);
//        }
//        if($lang_page->type === Page::TYPE_PAGE){
//            return view('frontend.page.page-main',['page'=>$lang_page]);
//        }
//        return view('frontend.page.error');
//    }

    public  function getTemplate($lang_page){

        if($lang_page->template_id === Page::TEMPLATE_MAIN  ){
            return view('frontend.page.page_main',['page'=>$lang_page]);
        }
        if($lang_page->template_id === Page::TEMPLATE_PAGE_IMAGES ){
            return view('frontend.page.page_image',['page'=>$lang_page]);
        }
        if($lang_page->template_id === Page::TEMPLATE_PAGE_DEFAULT){
            return view('frontend.page.post',['page'=>$lang_page]);
        }
        return view('frontend.page.error');
    }


}