<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 022 22.08.18
 * Time: 20:51
 */

namespace App\Http\Controllers\Frontend\Site;


use App\Http\Controllers\Controller;
use App\Http\Controllers\FrontendController;
use App\Models\Page\Page;
use App\Repositories\Backend\Page\PageRepository;
use App\Repositories\Frontend\Settings\SettingsRepository;

class SiteController extends FrontendController
{
    public $page;

    public function __construct(SettingsRepository $settings,PageRepository $page )
    {
        parent::__construct($settings);

        $this->page = $page;
    }
    public function pages($slug)
    {
        $page = $this->page->getOne($slug);
        return $this->render('frontend.site.index', ['page' => $page]);
    }
    public function index(){
        $helper = new \App\Helpers\Settings\SettingsHelper();
        if($helper->has('homeurl_'.app()->getLocale())) {
            $url = $helper->get('homeurl_' . app()->getLocale());
            $page = Page::where('slug', $url)->active()->first();
            if ($page) {
                $lang_page = Page::where('lang_hash',$page->lang_hash)->active()->lang()->first();

                if(!empty($lang_page)){
                    return $this->getLink($lang_page);
                }else{
                    return view('frontend.page.error');
                }
            }

        }
            abort(404);
    }
    public function getLink($lang_page)
    {
        if($lang_page->template_id === Page::TEMPLATE_MAIN  ){
            return view('frontend.page.page_main',['page'=>$lang_page]);
        }
        if($lang_page->template_id === Page::TEMPLATE_PAGE_IMAGES ){
            return view('frontend.page.page_image',['page'=>$lang_page]);
        }
        if($lang_page->template_id === Page::TEMPLATE_PAGE_DEFAULT){
            return view('frontend.page.post',['page'=>$lang_page]);
        }
        if($lang_page->type === Page::TYPE_FAQ){
            return redirect('frontend.faq.test');
        }
    }
}