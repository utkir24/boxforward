<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Http\Controllers\Backend\Faq;

use App\Http\Requests\Backend\Faq\ManageFaqRequest;
use App\Http\Requests\Backend\Faq\StoreFaqRequest;
use App\Http\Requests\Backend\Faq\UpdateFaqRequest;
use App\Models\Faq\Faq;
use App\Models\Page\Page;
use App\Models\Page\PageCategories;
use App\Repositories\Backend\Faq\FaqRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class FaqController
 * @package App\Http\Controllers\Backend\Faq
 */
class FaqController extends Controller
{

    /**
     * @var FaqRepository
     */
    protected $faqRepository;


    /**
     * FaqController constructor.
     * @param FaqRepository $faqRepository
     */
    public function __construct(FaqRepository $faqRepository)
    {
        $this->faqRepository = $faqRepository;

        $this->middleware('permission:read');

        $this->middleware('permission:create', ['only' => ['create','store']]);

        $this->middleware('permission:update', ['only' => ['edit','update']]);

        $this->middleware('permission:delete', ['only' => ['destroy']]);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $search_query = $request->search;

        if(!empty($search_query)){
            $pages = $this->faqRepository->getSearch(25,'created_at','desc',$search_query);
        }
        else{
            $pages = $this->faqRepository->getActivePaginated();
        }
        return view('backend.faq.index',['faqs'=>$pages]);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('backend.faq.create');
    }


    /**
     * @param StoreFaqRequest $request
     * @return mixed
     */
    public function store(StoreFaqRequest $request)
    {
        $this->faqRepository->create($request->only(
            'title',
            'description',
            'content',
            'status',
            'sort',
            'lang',
            'lang_hash'
        ));

        return redirect()->route('admin.faq.index')->withFlashSuccess(__('Created'));
    }


    /**
     * @param ManageFaqRequest $request
     * @param Faq $faq
     * @return mixed
     */
    public function show(ManageFaqRequest $request , Faq $faq )
    {
        return view('backend.faq.show')->withFaq($faq);
    }

    /**
     * @param ManageFaqRequest $request
     * @param Faq $faq
     * @return mixed
     */
    public function edit(ManageFaqRequest $request, Faq $faq)
    {
        return view('backend.faq.edit')->withFaq($faq);
    }

    /**
     * @param UpdateFaqRequest $request
     * @param Faq $faq
     * @return mixed
     */
    public function update(UpdateFaqRequest $request, Faq $faq)
    {

        $this->faqRepository->update($faq, $request->only(
            'title',
            'description',
            'content',
            'status',
            'sort'
        ));

        return redirect()->route('admin.faq.index')->withFlashSuccess(__('alerts.faq.updated'));
    }

    /**
     * @param ManageFaqRequest $request
     * @param Faq $faq
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageFaqRequest $request, Faq $faq)
    {
        $this->faqRepository->deleteById($faq->id);
        return redirect()->route('admin.faq.index')->withFlashSuccess(__('alerts.backend.users.deleted'));
    }


}
