<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */




namespace App\Http\Controllers\Backend\Post;

use App\Http\Requests\Backend\Page\ManagePageRequest;
use App\Http\Requests\Backend\Page\StorePageRequest;
use App\Http\Requests\Backend\Page\UpdatePageRequest;
use App\Langs\Lang;
use App\Models\Page\Page;
use App\Repositories\Backend\Page\PageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class pageController
 * @package App\Http\Controllers\Backend\page
 */
class PostController extends Controller
{

    /**
     * @var PageRepository
     */
    protected $pageRepository;


    /**
     * PageController constructor.
     * @param PageRepository $pageRepository
     */
    public function __construct(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;

        $this->middleware('permission:read');

        $this->middleware('permission:create', ['only' => ['create','store']]);

        $this->middleware('permission:update', ['only' => ['edit','update']]);

        $this->middleware('permission:delete', ['only' => ['destroy']]);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $search_query = $request->search;
        if(!empty($search_query)){
            $pages = $this->pageRepository->getSearch(Page::TYPE_POST,25,'created_at','desc',$search_query);
        }
        else{
            $pages = $this->pageRepository->getActivePaginated(Page::TYPE_POST);
        }


        return view('backend.post.index',['pages'=>$pages]);
    }

    public function type($type = Page::TYPE_POST){

        return view('backend.post.index',['pages'=>$this->pageRepository->getActivePaginated($type)]);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('backend.post.create');
    }


    /**
     * @param StorePageRequest $request
     * @return mixed
     */
    public function store(StorePageRequest $request)
    {
        $this->pageRepository->create($request->only(
            'title',
            'description',
            'content',
            'status',
            'image',
            'template_id',
            'content_two',
            'small_title',
            'sort',
            'lang',
            'lang_hash',
            'type'
        ));

        return redirect()->route('admin.post.index')->withFlashSuccess(__('Created'));
    }


    /**
     * @param ManagePageRequest $request
     * @param Page $page
     * @return mixed
     */
    public function show(ManagePageRequest $request , Page $post )
    {
        $page = $post;
        return view('backend.post.show')->withpage($page);
    }

    /**
     * @param ManagePageRequest $request
     * @param Page $page
     * @return mixed
     */
    public function edit(ManagePageRequest $request, Page $post)
    {
        $page = $post;
        return view('backend.post.edit')->withpage($page);
    }

    /**
     * @param UpdatePageRequest $request
     * @param Page $page
     * @return mixed
     */
    public function update(UpdatePageRequest $request, Page $post)
    {
        $page  = $post;

        $this->pageRepository->update($page, $request->only(
            'id',
            'title',
            'description',
            'content',
            'status',
            'sort',
            'type',
            'image',
            'slug',
            'template_id',
            'content_two',
            'small_title'
        ));



        return redirect()->route('admin.post.index')->withFlashSuccess(__('alerts.page.updated'));
    }

    /**
     * @param ManagePageRequest $request
     * @param Page $page
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManagePageRequest $request, Page $post)
    {
        $page = $post;
        $this->pageRepository->deleteById($page->id);
        return redirect()->route('admin.post.index')->withFlashSuccess(__('alerts.backend.users.deleted'));
    }


}
