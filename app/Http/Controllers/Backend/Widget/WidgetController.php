<?php

namespace App\Http\Controllers\Backend\Widget;

use App\Http\Requests\Backend\Widget\ManageWidgetRequest;
use App\Http\Requests\Backend\Widget\StoreWidgetRequest;
use App\Http\Requests\Backend\Widget\UpdateWidgetRequest;
use App\Models\Notification\Notification;
use App\Models\Notification\NotificationField;
use App\Models\Widget\Widget;
use App\Repositories\Backend\Widget\WidgetRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WidgetController extends Controller
{
    /**
     * @var WidgetRepository
     */
    protected $repository;

    /**
     * WidgetController constructor.
     * @param WidgetRepository $repository
     */
    public function __construct(WidgetRepository $repository)
    {
        $this->repository = $repository;

        $this->middleware('permission:read');

        $this->middleware('permission:create', ['only' => ['create','store']]);

        $this->middleware('permission:update', ['only' => ['edit','update']]);

        $this->middleware('permission:delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search_query = $request->search;
        if(!empty($search_query)){
            $widget = $this->repository->getSearch(0,25,'created_at','desc',$search_query);
        }
        else{
            $widget = $this->repository->getActivePaginated();
        }
        return view('backend.widget.index',['widgets'=>$widget]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type = request()->get('type',0);

        return view('backend.widget.create',['type'=>$type]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @param StoreWidgetRequest $request
     * @return mixed
     */
    public function store(StoreWidgetRequest $request)
    {


        $this->repository->create($request->only(
            'title',
            'description',
            'image',
            'lang',
            'lang_hash',
            'type'
        ));

        return redirect()->route('admin.widget.index')->withFlashSuccess(__('Created'));
    }

    /**
     * @param ManageWidgetRequest $request
     * @param Widget $widget
     * @return mixed
     */
    public function show(ManageWidgetRequest $request , Widget $widget )
    {
        return view('backend.widget.show',['widget'=>$widget]);
    }

    /**
     * @param ManageWidgetRequest $request
     * @param Widget $widget
     * @return mixed
     */
    public function edit(ManageWidgetRequest $request, Widget $widget)
    {

            return view('backend.widget.edit',['widget'=>$widget]);
    }

    /**
     * @param UpdateWidgetRequest $request
     * @param Widget $widget
     * @return mixed
     */
    public function update(UpdateWidgetRequest $request, Widget $widget)
    {
     //   dd($request->get('WidgetCats'));

        $this->repository->update($widget, $request->only(
            'title',
            'description',
            'content',
            'image',
            'type'
        ));

        return redirect()->route('admin.widget.index')->withFlashSuccess(__('alerts.widget.updated'));
    }

    /**
     * @param ManageWidgetRequest $request
     * @param Widget $widget
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageWidgetRequest $request, Widget $widget)
    {
        $this->repository->deleteById($widget->widget_id);
        return redirect()->route('admin.widget.index')->withFlashSuccess(__('alerts.backend.users.deleted'));
    }

    public function notifications(Request $request){

        $widget = Widget::where('widget_id',$request->input('id'))->first();

        if($widget) {

            $notifications = $widget->notifications->map(function (Notification $item, $key) {

                $children = $item->children->map(function (Notification $child, $childKey) {
                    return [
                        'id'=>$child->notification_id,
                        'title' => $child->title,
                        'description' => $child->description,
                        'content' => $child->content
                    ];
                })->all();

                return [
                    'id'=>$item->notification_id,
                    'title' => $item->title,
                    'description' => $item->description,
                    'content' => $item->content,
                    'children' => $children ? $children : []
                ];

            })->all();

            $response = [
                'data' => $notifications
            ];

            return response()->json($response, 200);
        }

        return response()->json(['data'=>[]], 200);

    }
}
