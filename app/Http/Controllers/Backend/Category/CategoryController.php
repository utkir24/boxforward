<?php

namespace App\Http\Controllers\Backend\Category;

use App\Http\Requests\Backend\Category\ManageCategoryRequest;
use App\Http\Requests\Backend\Category\UpdateCategoryRequest;
use App\Http\Requests\Backend\Page\StoreCategoryRequest;
use App\Langs\Lang;
use App\Models\Category\Category;
use App\Repositories\Backend\Category\CategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepository
     */
    protected $repository;

    /**
     * CategoryController constructor.
     * @param CategoryRepository $repository
     */
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;

        $this->middleware('permission:read');

        $this->middleware('permission:create', ['only' => ['create','store']]);

        $this->middleware('permission:update', ['only' => ['edit','update']]);

        $this->middleware('permission:delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nested = Category::nested()->get();
        return view('backend.category.index',['nested'=> $nested,'type'=>0]);
    }

    public function type($type = Category::TYPE_PAGE){

        $nested = Category::nested()->where('type',$type)->get();
        return view('backend.category.index',['nested'=> $nested,'type'=>$type]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @param StorePageRequest $request
     * @return mixed
     */
    public function store(StoreCategoryRequest $request)
    {

        $this->repository->create($request->only(
            'title',
            'description',
            'status',
            'type',
            'slug'
        ));

        return redirect()->route('admin.category.index')->withFlashSuccess(__('Created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ManageCategoryRequest $request, Category $category)
    {

        return view('backend.category.edit')->with(['category'=>$category,'type'=>$category->type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $this->repository->update($category, $request->all());
        $this->repository->setTranslation($category,$request->only(['translate']));
        return redirect()->route('admin.category.index')->withFlashSuccess(__('alerts.backend.category.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->deleteById($id);
        return redirect()->route('admin.category.index')->withFlashSuccess(__('alerts.backend.category.deleted'));
    }

    public function ajax(Request $request){
        $list = $request->get('list');
        dump($list);
        if(count($list) == 0){return;}
        $list = $this->filterCategoryAjax($list);
        dump($list);
        foreach ($list as $index=>$item){
            $category = Category::find($item['id']);
            $category->parent_id = null;
            $category->sort = $index + 1;
            $category->save();
            if(!array_key_exists('children',$item)){continue;}
            dump("has children");
            $this->updateajaxchild($item);
        }
    }

    private function filterCategoryAjax($list)
    {
        $arr = [];
        foreach ($list as $l) {
            if ($l['id'] == 0 && array_key_exists('children', $l)) {
                $arr += $l['children'];
            } else {
                $arr += $l;
            }
        }

        return $arr;
    }
    private function updateajaxchild($item = []){
        if(!array_key_exists('children',$item)){return false;}
        $childs = $item['children'];
        dump($item);
        foreach ($childs as $index=>$child){
            $category_item = Category::find($item['id']);
            if($category_item){
                $child_category = Category::find($child['id']);
                if($child_category){
                    $child_category->parent_id = $category_item->id;
                    $child_category->sort = $index + 1;
                    $child_category->save();
                }

            }
            $this->updateajaxchild($child);
        }
        return true;
    }

    public function forbiddenResponse()
    {
        return response()->view('backend.includes.errors.errors.403');
    }
}
