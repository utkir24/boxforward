<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 028 28.09.18
 * Time: 21:09
 */

namespace App\Http\Controllers\Backend\Shops;
use App\Http\Requests\Backend\Shops\ManageShopsRequest;
use App\Http\Requests\Backend\Shops\StoreShopsRequest;
use App\Http\Requests\Backend\Shops\UpdateShopsRequest;
use App\Models\Shops\Shops;
use App\Repositories\Backend\Shops\ShopsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShopsController extends Controller
{

    /**
     * @var ShopsRepository
     */
    protected $shopRepository;


    /**
     * ShopController constructor.
     * @param ShopsRepository $shopRepository
     */
    public function __construct(ShopsRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;

        $this->middleware('permission:read');

        $this->middleware('permission:create', ['only' => ['create', 'store']]);

        $this->middleware('permission:update', ['only' => ['edit', 'update']]);

        $this->middleware('permission:delete', ['only' => ['destroy']]);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $search_query = $request->search;

        if (!empty($search_query)) {
            $pages = $this->shopRepository->getSearch(25, 'created_at', 'desc', $search_query);
        } else {
            $pages = $this->shopRepository->getActivePaginated();
        }
        return view('backend.shops.index', ['shops' => $pages]);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('backend.shops.create');
    }


    /**
     * @param StoreShopsRequest $request
     * @return mixed
     */
    public function store(StoreShopsRequest $request)
    {
        $this->shopRepository->create($request->only(
            'title',
            'description',
            'content',
            'status',
            'price',
            'payment_system',
            'shop_site',
            'worth_knowing',
            'link_site',
            'image',
            'sort',
            'lang',
            'lang_hash'
        ));

        return redirect()->route('admin.shops.index')->withFlashSuccess(__('Created'));
    }


    /**
     * @param ManageShopsRequest $request
     * @param Shops $shops
     * @return mixed
     */
    public function show(ManageShopsRequest $request, Shops $shop)
    {
        return view('backend.shops.show')->withShops($shop);
    }

    /**
     * @param ManageShopsRequest $request
     * @param Shops $shops
     * @return mixed
     */
    public function edit(ManageShopsRequest $request, Shops $shop)
    {
        return view('backend.shops.edit')->withShops($shop);
    }

    /**
     * @param UpdateShopsRequest $request
     * @param Shops $shops
     * @return mixed
     */
    public function update(UpdateShopsRequest $request, Shops $shop)
    {
        $this->shopRepository->update($shop, $request->only(
            'title',
            'description',
            'content',
            'status',
            'price',
            'payment_system',
            'shop_site',
            'worth_knowing',
            'image',
            'sort',
            'link_site'
        ));

        return redirect()->route('admin.shops.index')->withFlashSuccess(__('alerts.shops.updated'));
    }

    /**
     * @param ManageShopsRequest $request
     * @param Shops $shops
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageShopsRequest $request, Shops $shop)
    {
        $this->shopRepository->deleteById($shop->id);
        return redirect()->route('admin.shops.index')->withFlashSuccess(__('alerts.backend.users.deleted'));
    }

}