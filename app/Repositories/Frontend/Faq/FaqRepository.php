<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */



namespace App\Repositories\Frontend\Faq;


use App\Models\Faq\Faq;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

class FaqRepository extends BaseRepository
{
    /**
     * @return mixed|string|Faq
     */
    public function model()
    {
        return Faq::class;
    }

    /**
     * @param array $catIds
     * @param int $paged
     * @param string $orderBy
     * @param string $sort
     * @return LengthAwarePaginator
     */
    public function getActivePaginated($catIds=[],$paged = 20, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->active()
            ->lang()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    public function getAll()
    {
        return $this->model->active()->get();
    }

}