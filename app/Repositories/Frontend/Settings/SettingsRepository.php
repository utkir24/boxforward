<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 030 30.08.18
 * Time: 10:18
 */

namespace App\Repositories\Frontend\Settings;


use App\Models\Settings\Setting;
use App\Repositories\BaseRepository;

class SettingsRepository extends BaseRepository
{
    public function model()
    {
        return Setting::class;
    }

    public function getAll()
    {
        return $this->model->active()->lang()->get();
    }

}