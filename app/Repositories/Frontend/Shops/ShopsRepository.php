<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 028 28.09.18
 * Time: 22:30
 */

namespace App\Repositories\Frontend\Shops;

use App\Models\Shops\Shops;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

class ShopsRepository extends BaseRepository
{
    /**
     * @return mixed|string|Shops
     */
    public function model()
    {
        return Shops::class;
    }

    /**
     * @param array $catIds
     * @param int $paged
     * @param string $orderBy
     * @param string $sort
     * @return LengthAwarePaginator
     */
    public function getActivePaginated($catIds = [], $paged = 20, $orderBy = 'created_at', $sort = 'desc'): LengthAwarePaginator
    {
        return $this->model
            ->active()
            ->lang()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    public function getAll()
    {
        return $this->model->active()->get();
    }
}


