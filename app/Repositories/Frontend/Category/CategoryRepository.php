<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */


namespace App\Repositories\Frontend\Category;


use App\Models\Category\Category;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

class CategoryRepository extends BaseRepository
{
    /**
     * @return mixed|string|Category
     */
    public function model()
    {
        return Category::class;
    }


    /**
     * @param $type
     * @param int $paged
     * @param string $orderBy
     * @param string $sort
     * @return LengthAwarePaginator
     */
    public function getActivePaginated($type,$paged = 20, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->active()
            ->lang()
            ->type(Category::TYPE_POST)
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    public function getAll()
    {
        return $this->model->active()->get();
    }

    public function getActivePaginateWithModel($model,$paged = 15, $orderBy = 'created_at', $sort = 'desc'){

        return $model->active()
                     ->lang()
                     ->orderBy($orderBy, $sort)
                     ->paginate($paged);
    }

    public function getCategoriesList(){

        return Category::where('type',Category::TYPE_POST)->active()
                           ->lang()
                           ->get();
    }

    public function getOneShopCategory($slug){
      return Category::where('type',Category::TYPE_SHOPS)->lang()->active()->slug($slug)->first();
    }

}