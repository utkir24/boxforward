<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Repositories\Backend\Faq;

use App\Exceptions\GeneralException;
use App\Models\Faq\Faq;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

/**
 * Class FaqRepository
 * @package App\Repositories\Backend\Faq
 */
class FaqRepository extends BaseRepository
{
    /**
     * @return mixed|string
     */
    public function model()
    {
        return Faq::class;
    }

    /**
     * @param int $paged
     * @param string $orderBy
     * @param string $sort
     * @return LengthAwarePaginator
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->langs()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function getSearch($paged = 25, $orderBy = 'created_at', $sort = 'desc',$search_query = ''){
        $search = [
            'title' => '',
            'description' => '',
            'content' => '',
        ];
        if (!empty($search_query)) {
            foreach ($search_query as $key => $value) {
                if (!empty($value)) {
                    $search[$key] = $value;
                    $models =$this->model->where($key, 'like', '%' . strip_tags(trim($value)) . '%')->active()->langs();
                }
            }
        }
        $models = $models->orderBy($orderBy, $sort)->paginate($paged);
        return $models;
    }

    /**
     * @param Faq $faq
     * @param array $data
     * @return Faq
     * @throws \Throwable
     */
    public function update(Faq $faq, array $data) : Faq
    {

        return DB::transaction(function () use ($faq, $data) {
            if ($faq->update([
                'title' => $data['title'],
                'description' => $data['description'],
                'content' => $data['content'],
                'status' => isset($data['status']) ? $data['status'] : 0,
                'sort' => $data['sort']
            ])) {
                return $faq;
            }

            throw new GeneralException(__('exceptions.faq.update_error'));
        });
    }


    /**
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function deleteById($id) : bool
    {
        $this->unsetClauses();

        return $this->getById($id)->delete();
    }
}