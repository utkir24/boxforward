<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Repositories\Backend\Page;

use App\Exceptions\GeneralException;
use App\Models\Page\Page;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

/**
 * Class PageRepository
 * @package App\Repositories\Backend\Page
 */
class PageRepository extends BaseRepository
{
    /**
     * @return mixed|string
     */
    public function model()
    {
        return Page::class;
    }

    /**
     * @param int $paged
     * @param string $orderBy
     * @param string $sort
     * @return LengthAwarePaginator
     */
    public function getActivePaginated($type=0,$paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        $models = $this->model
            ->langs();
          if ($type > 0) {
              $models = $models->type($type);
          }
           $models = $models->orderBy($orderBy, $sort)->paginate($paged);
          return $models;
    }
    public function getSearch($type=0,$paged = 25, $orderBy = 'created_at', $sort = 'desc',$search_query = ''){
        $search = [
            'title' => '',
            'description' => '',
            'content' => '',
        ];
        if (!empty($search_query)) {
            foreach ($search_query as $key => $value) {
                if (!empty($value)) {
                    $search[$key] = $value;
                    $models =$this->model->where($key, 'like', '%' . strip_tags(trim($value)) . '%')->active()->langs();
                    if ($type > 0) {
                        $models = $models->type($type);
                    }
                }
            }
        }
        $models = $models->orderBy($orderBy, $sort)->paginate($paged);

        return $models;
    }

    /**
     * @param Page $page
     * @param array $data
     * @return Page
     * @throws \Throwable
     */
    public function update(Page $page, array $data) : Page
    {

        return DB::transaction(function () use ($page, $data) {
            if ($page->update([
                'title' => $data['title'],
                'description' => $data['description'],
                'content' => $data['content'],
                'status' => isset($data['status']) ? $data['status'] : 0 ,
                'sort' => $data['sort'],
                'type' => $data['type'],
                'slug'=> isset($data['slug']) ? $data['slug'] : $page->slug,
                'small_title'=> isset($data['small_title'])?$data['small_title']: '',
                'content_two'=> isset($data['content_two'])?$data['content_two']: '',
                'image'=> isset($data['image'])?$data['image']: '',
            ])) {
                return $page;
            }

            throw new GeneralException(__('exceptions.page.update_error'));
        });
    }


    /**
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function deleteById($id) : bool
    {
        $this->unsetClauses();

        return $this->getById($id)->delete();
    }

    public function getOne($link){
       $model = $this->model->where('slug',$link)->langs()->active()->first();
       if(!empty($model)){
           return $model;
       }else
           return abort(404);
    }
}