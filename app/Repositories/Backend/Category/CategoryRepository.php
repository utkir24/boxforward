<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Repositories\Backend\Category;

use App\Exceptions\GeneralException;
use App\Langs\LangScopeTrait;
use App\Models\Category\Category;
use App\Models\Page\Page;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class CategoryRepository extends BaseRepository
{
    use LangScopeTrait;

    public function model()
    {
        return Category::class;
    }

    /**
     * @param int $paged
     * @param string $orderBy
     * @param string $sort
     * @return LengthAwarePaginator
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->active()
            ->lang()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param Page $page
     * @param array $data
     * @return Page
     * @throws \Throwable
     */
    public function update(Category $category, array $data) : Category
    {

        return DB::transaction(function () use ($category, $data) {
            if ($category->update($data)) {
                if(array_key_exists('type',$data)){
                    self::setType($category,$data['type']);
                }
                return $category;
            }

            throw new GeneralException(__('exceptions.category.update_error'));
        });
    }
    //setTranslation

    public function setTranslation(Category $category, array $data) : Category
    {

        return DB::transaction(function () use ($category, $data) {
            if(!array_key_exists('translate',$data)){return false;}
            $translates = $data['translate'];
            if(count($translates) == 0){return false;}
            foreach ($translates as $t_lang=>$translate){
                $old_translation = $category->translation($t_lang);
                if(!(($category->id - $old_translation->id) == 0)){
                    Category::where('id', $old_translation->id)->update(['lang_hash' => str_random(40)."_null"]);
                }
                Category::where('id', $translate)->update(['lang_hash' => $category->lang_hash]);
            }
            return $category;
            throw new GeneralException(__('exceptions.category.update_error'));
        });
    }

    public static function setType(Category $category, $type = 1){
        $categories = Category::where(['parent_id' => $category->id])->get();
        Category::where('id',$category->id)->update(['type' => $type]);
        if(!$categories){return true;}
        foreach ($categories as $cat){
            Category::where('id',$cat->id)->update(['type' => $category->type]);
            self::setType($cat,$type);
        }
        return true;
    }

    /**
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function deleteById($id) : bool
    {
        $this->unsetClauses();

        return $this->getById($id)->delete();
    }
}