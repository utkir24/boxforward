<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 028 28.09.18
 * Time: 21:16
 */

namespace App\Repositories\Backend\Shops;


use App\Models\Shops\Shops;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;


class ShopsRepository  extends BaseRepository
{

    /**
     * @return mixed|string
     */
    public function model()
    {
        return Shops::class;
    }

    /**
     * @param int $paged
     * @param string $orderBy
     * @param string $sort
     * @return LengthAwarePaginator
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->langs()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }
    public function getSearch($paged = 25, $orderBy = 'created_at', $sort = 'desc',$search_query = ''){
        $search = [
            'title' => '',
            'description' => '',
            'content' => '',
        ];
        if (!empty($search_query)) {
            foreach ($search_query as $key => $value) {
                if (!empty($value)) {
                    $search[$key] = $value;
                    $models =$this->model->where($key, 'like', '%' . strip_tags(trim($value)) . '%')->active()->langs();
                }
            }
        }
        $models = $models->orderBy($orderBy, $sort)->paginate($paged);
        return $models;
    }

    /**
     * @param Faq $faq
     * @param array $data
     * @return Faq
     * @throws \Throwable
     */
    public function update(Shops $faq, array $data) : Shops
    {

        return DB::transaction(function () use ($faq, $data) {
            if ($faq->update([
                'title' => $data['title'],
                'description' => $data['description'],
                'content' => $data['content'],
                'status' =>  isset($data['status']) ? $data['status'] : 0 ,
                'sort' => $data['sort'],
                'worth_knowing'=>$data['worth_knowing'],
                'image'=>$data['image'],
                'price'=>$data['price'],
                'payment_system'=>$data['payment_system'],
                'shop_site'=>$data['shop_site'],
                'link_site'=>$data['link_site'],
            ])) {
                return $faq;
            }

            throw new GeneralException(__('exceptions.shops.update_error'));
        });
    }


    /**
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function deleteById($id) : bool
    {
        $this->unsetClauses();

        return $this->getById($id)->delete();
    }

}