<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Repositories\Backend\Menu;

use App\Exceptions\GeneralException;
use App\Langs\LangScopeTrait;
use App\Models\Menu\Menu;
use App\Models\Page\Page;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class MenuRepository extends BaseRepository
{
    use LangScopeTrait;

    public function model()
    {
        return Menu::class;
    }

    /**
     * @param int $paged
     * @param string $orderBy
     * @param string $sort
     * @return LengthAwarePaginator
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->active()
            ->lang()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param Page $page
     * @param array $data
     * @return Page
     * @throws \Throwable
     */
    public function update(Menu $menu, array $data) : Menu
    {
        return DB::transaction(function () use ($menu, $data) {
            if ($menu->update($data)) {
                if(array_key_exists('type',$data)){
                    self::setType($menu,$data['type']);
                }
                return $menu;
            }

            throw new GeneralException(__('exceptions.menu.update_error'));
        });
    }
    //setTranslation

    public function setTranslation(Menu $category, array $data) : Menu
    {

        return DB::transaction(function () use ($category, $data) {
            $translates = $data['translate'];
            if(count($translates) == 0){return;}
            foreach ($translates as $t_lang=>$translate){
                $old_translation = $category->translation($t_lang);
                if(!(($category->id - $old_translation->id) == 0)){
                    Menu::where('id', $old_translation->id)->update(['lang_hash' => str_random(40)."_null"]);
                }
                Menu::where('id', $translate)->update(['lang_hash' => $category->lang_hash]);
            }
            return $category;
            throw new GeneralException(__('exceptions.category.update_error'));
        });
    }

    public static function setType(Menu $category, $type = 1){
        $categories = Menu::where(['parent_id' => $category->id])->get();
        Menu::where('id',$category->id)->update(['type' => $type]);
        if(!$categories){return true;}
        foreach ($categories as $cat){
            Menu::where('id',$cat->id)->update(['type' => $category->type]);
            self::setType($cat,$type);
        }
        return true;
    }

    /**
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function deleteById($id) : bool
    {
        $this->unsetClauses();

        return $this->getById($id)->delete();
    }
}