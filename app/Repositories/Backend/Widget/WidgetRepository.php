<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */



namespace App\Repositories\Backend\Widget;


use App\Exceptions\GeneralException;
use App\Models\Widget\Widget;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class WidgetRepository extends BaseRepository
{
    /**
     * @return mixed|string|Widget
     */
    public function model()
    {
        return Widget::class;
    }

    /**
     * @param int $paged
     * @param string $orderBy
     * @param string $sort
     * @return LengthAwarePaginator
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        $models = $this->model
            ->lang();
        $models = $models->orderBy($orderBy, $sort)->paginate($paged);
        return $models;
    }
    public function getSearch($type=0,$paged = 25, $orderBy = 'created_at', $sort = 'desc',$search_query = ''){
        $search = [
            'title' => '',
            'description' => '',
            'content' => '',
        ];
        if (!empty($search_query)) {
            foreach ($search_query as $key => $value) {
                if (!empty($value)) {
                    $search[$key] = $value;
                    $models =$this->model->where($key, 'like', '%' . strip_tags(trim($value)) . '%')->langs();
                    if ($type > 0) {
                        $models = $models->type($type);
                    }
                }
            }
        }
        $models = $models->orderBy($orderBy, $sort)->paginate($paged);

        return $models;
    }

    /**
     * @param Widget $widget
     * @param array $data
     * @return Widget
     * @throws \Throwable
     */
    public function update(Widget $widget, array $data) : Widget
    {

        return DB::transaction(function () use ($widget, $data) {
            if ($widget->update([
                'title' => $data['title'],
                'description' => $data['description'],
                'image' => $data['image'],
                'type' => $data['type'],
            ])) {
                return $widget;
            }

            throw new GeneralException(__('exceptions.page.update_error'));
        });
    }


    /**
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function deleteById($id) : bool
    {
        $this->unsetClauses();

        return $this->getById($id)->delete();
    }

}