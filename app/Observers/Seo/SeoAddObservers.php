<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 020 20.09.18
 * Time: 7:27
 */

namespace App\Observers\Seo;
use Illuminate\Database\Eloquent\Model;


class SeoAddObservers
{
    public $fieldName = 'Seo';
    public $seo;
    public $model;

    /**
     * @param Model $model
     */

    public function saving(Model $model){
        $this->model = $model;
//        dd(request()->get($this->fieldName));
        $value = [];

        $items = $this->requestSeo();
        if (!empty($items) && is_array($items)) {
            foreach ($items as $item) {
                if (empty($item['key']) || empty($item['value']) ||  empty($item['type'])) {
                    continue;
                }
                $value[$item['key']] = [
                    'value'=>$item['value'],
                    'type'=>$item['type']
                ];
            }
        }
        if (empty($value)) {
            $this->model->seo = '';
        } else {
            $this->model->seo = $value;
        }
    }



    public function validate(){
        $this->requestSeo();
        if($this->isEmpty()){return true;}
        return false;
    }

    public function clearSeo(){
        $this->model->{$this->relation_name}()->detach();
    }


    public function requestSeo(){
        return request()->get($this->fieldName,null);
    }



    public function isEmpty(){
        if(empty($this->widgets )){return true;}
        return false;
    }
}