<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */


namespace App\Observers\Slug;


use Illuminate\Database\Eloquent\Model;

class SlugObserver
{
    /**
     * Listen to the Model created event.
     *
     * @param  Model  $model
     * @return void
     */
    public function creating(Model $model) : void
    {
        $lang = request()->session()->get('locale_old', null);
        $lang_hash = request()->get('lang_hash',null);
        if($lang && $lang_hash) {
            $class_name = get_class($model);
            $old_model = $class_name::where('lang', $lang)->where('lang_hash', $lang_hash)->get();
            if ($old_model->count()) {
                $model->slug = $old_model[0]->slug;
            }
        }
    }

    /**
     * Listen to the Model created event.
     *
     * @param  Model  $model
     * @return void
     */
   /* public function updated(Model $model) : void
    {

        $lang_hash = $model->lang_hash;
        $lang = request()->session()->get('locale_old', null);

        if($lang_hash && $lang){
            $class_name = get_class($model);
            $next_model = $class_name::where('lang', $lang)->where('lang_hash', $lang_hash)->get();
            if($next_model->count()){
                $next_model->slug = $model->slug;
                $next_model->save();

            }
        }
    }*/


}