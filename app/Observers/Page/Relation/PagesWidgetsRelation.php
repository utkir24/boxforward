<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 020 20.09.18
 * Time: 7:27
 */

namespace App\Observers\Page\Relation;
use App\Models\Widget\PagesWidget;
use App\Models\Widget\Widget;
use Illuminate\Database\Eloquent\Model;


class PagesWidgetsRelation
{
    public $relation_name = 'widgets';
    public $fieldName = 'widgets';
    public $relation_class_name = Widget::class;
    public $widgets;
    public $model;

    /**
     * @param Model $model
     */
    public function saved(Model $model)
    {
        $this->model = $model;
        $this->clearTableRelation();
        if($this->validate()){return false;}
        $this->beforeSaved();
        $this->manyToManyAdd();
    }

    /**
     * @return bool
     */
    public function validate(){
        $this->requestWidget();
        if($this->isEmpty()){return true;}
        return false;
    }

    /**
     *
     */
    public function beforeSaved(){
        $this->widgetExplode();
        $this->widgetUnique();
    }

    /**
     *
     */
    public function clearTableRelation(){
        $this->model->{$this->relation_name}()->detach();
    }

    /**
     * @return mixed
     */
    public function requestWidget(){
        $this->widgets =  request()->get($this->fieldName,null);

    }

    /**
     * @return bool
     */
    public function isEmpty(){
        if(empty($this->widgets )){return true;}
        return false;
    }



    /**
     * @return array
     */
    public function widgetUnique(){
        $this->widgets =  array_unique($this->widgets);
    }

    /**
     * @param string $delimer
     * @return array
     */
    public function widgetExplode($delimer = ','){
        $this->widgets =  explode($delimer,$this->widgets);

    }

    /**
     *
     */
    public function manyToManyAdd(){
        foreach($this->widgets as $widget){
            $this->model->{$this->relation_name}()->attach($widget,
                ['sort' => PagesWidget::where('page_id',$this->model->id)->max('sort') + 1]);
        }
    }

}