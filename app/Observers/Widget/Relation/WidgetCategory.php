<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Observers\Widget\Relation;


use App\Models\Category\Category;
use Illuminate\Database\Eloquent\Model;
use \App\Models\Widget\WidgetCategory as WidgetCategoryModel;
class WidgetCategory
{
    public $relation_name = 'categories';
    public $fieldName = 'WidgetCats';
    public $fieldName2 = 'categories';
    public $relation_class_name = Category::class;
    public function saved(Model $model)
    {
        $model->{$this->relation_name}()->detach();
        $categories = $this->getCategories();
        if (is_array($categories) && count($categories)) {
            foreach ($categories as $catForm) {
                if($this->validate($catForm)) {
                    $category = Category::where('id', $catForm['category_id'])->first();
                    if ($category) {
                        $error = $model->{$this->relation_name}()->attach($category->id,
                            [
                                'sort' => WidgetCategoryModel::where('widget_id', $model->widget_id)->max('sort') + 1,
                                'count' => $catForm['count']
                            ]);

                    }
                }
            }
        }


        $catIds = $this->getCategoryForFancyTree();
        if (is_array($catIds) && count($catIds)) {
            $class_name = $this->relation_class_name;
            $categoreis = $class_name::find($catIds);
            if ($categoreis) {
                $model->{$this->relation_name}()->attach($categoreis);
            }
        }


    }

    protected  function getCategories()
    {
        $request = request();
        $categories = $request->get($this->fieldName, null);
        if (!empty($categories)&& is_array($categories)) {
            $collection = collect($categories);
            $filtered = $collection->filter(function ($value, $key) {
                return (!empty($value['category_id']) || !empty($value['count'])) && $value['count'] > 0 && $value['category_id'] > 0 ;
            });
            return $filtered->all();
        }
        return [];
    }

    protected function validate($catForm)
    {
        $one = array_key_exists('category_id', $catForm) && array_key_exists('count', $catForm);
        if ($one) {
           return $catForm['category_id'] > 0 && $catForm['count'] > 0;
        }
        return false;
    }

    protected function getCategoryForFancyTree(){
        $request = request();
        $categories = $request->get($this->fieldName2, null);
        if ($categories) {
            return explode(',', $categories);
        }
        return [];
    }
}