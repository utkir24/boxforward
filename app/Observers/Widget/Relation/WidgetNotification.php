<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

/**
 * Created by PhpStorm.
 * User: utkir
 * Date: 18.09.2018
 * Time: 5:25
 */

namespace App\Observers\Widget\Relation;


use App\Models\Notification\Notification;
use App\Models\Notification\NotificationChildren;
use App\Models\Notification\NotificationField;
use App\Models\Notification\NotificationImages;
use Illuminate\Database\Eloquent\Model;
use App\Models\Widget\WidgetNotification as WidgetNotificationRelation;

class WidgetNotification
{
    public $relation_name = 'notifications';
    public $fieldName = 'Widgets';
    public $relation_class_name = Notification::class;


    public function saved(Model $model)
    {
        $model->{$this->relation_name}()->detach();
        $notifications = $this->getNotifications();
        if (is_array($notifications) && count($notifications)) {
            $class_name = $this->relation_class_name;
            foreach ($notifications as $notification){
                if(array_key_exists('id',$notification) && $notification['id'] > 0) {
                    /**
                     * @var $note Notification
                     */
                    $note = Notification::where('notification_id', $notification['id'])->first();
                    if ($note) {
                        $note = $this->setNotification($note,$notification);
                        if ($note->save()) {
                            $note->children()->detach();
                            $this->saveNotificationChildren($notification,$note);
                            $this->saveNotificationImages($note,$notification);
                            $this->saveNotificationExtraFiled($notification,$note);
                            $model->{$this->relation_name}()->attach($note->notification_id,
                                ['sort' => WidgetNotificationRelation::where('widget_id', $model->widget_id)->max('sort') + 1]);
                        }
                    }
                }else {
                    $note = $this->saveNotification($notification);
                    if ($note->save()) {
                        $this->saveNotificationChildren($notification,$note);
                        $this->saveNotificationImages($note,$notification);
                        $this->saveNotificationExtraFiled($notification,$note);
                        $model->{$this->relation_name}()->attach($note->notification_id,
                            ['sort' => WidgetNotificationRelation::where('widget_id', $model->widget_id)->max('sort') + 1]);
                    }
                }
            }
        }
    }

    protected  function getNotifications()
    {
        $request = request();
        $notifications= $request->get($this->fieldName, null);
        if (!empty($notifications && is_array($notifications))) {
            $collection = collect($notifications);
            $filtered = $collection->filter(function ($value, $key) {
                return !empty($value['title']) || !empty($value['description'])|| !empty($value['content']);
            });
            return $filtered->all();
        }
        return [];
    }


    protected function saveNotificationChildren($notification,$note)
    {
        if(array_key_exists('children',$notification) && !empty($notification['children'])) {
            foreach ($notification['children'] as $child) {
                if(array_key_exists('id',$child) && $child['id'] > 0){
                    $note_child = Notification::where('notification_id',$child['id'])->first();
                    if($note_child){
                        $note_child = $this->setNotification($note_child,$child);
                    }
                }else{
                    $note_child = $this->saveNotification($child);
                }


                if ($note_child && $note_child->save()) {
                    $note_child->parents()->attach($note->notification_id,
                        ['sort' => NotificationChildren::where('parent_id', $note->notification_id)->max('sort') + 1]);
                }
            }
        }
    }

    protected function saveNotification($notification)
    {
        $model = new Notification();
        $model = $this->setNotification($model,$notification);

        return $model;
    }

    protected function setNotification($model,$notification)
    {
        $model->title = $notification['title'] ?? '' ;
        $model->description = $notification['description']?? '';
        $model->content = $notification['content']?? '';
        return $model;
    }

    protected function saveNotificationImages($model,$notification){
        if(array_key_exists('images',$notification) && !empty($notification['images'])){
            $model->images()->delete();
            $images = explode(',',$notification['images']);
            foreach ($images as $img){
                $noteImg = new NotificationImages();
                $noteImg->notification_id = $model->notification_id;
                $noteImg->image = $img;
                $noteImg->sort  = NotificationImages::where('notification_id',$model->notification_id)->max('sort')+1;
                $noteImg->save();
            }
        }
    }

    /**
     * @param $notification
     * @param $note Notification
     */
    protected function saveNotificationExtraFiled($notification,$note)
    {
        $note->fieldsExtra()->delete();
        if(array_key_exists('extraField',$notification) && !empty($notification['extraField'])) {
            $data=[];
            foreach ($notification['extraField'] as $child) {
                if(array_key_exists('name',$child) && array_key_exists('value',$child) && !empty($child['name']) && !empty($child['value'])){
                 $data+=[$child['name']=>$child['value']];
                }
            }

            if (!empty($data)) {
                $extra = new NotificationField();
                $extra->notification_id = $note->notification_id;
                $extra->data = $data;
                $extra->save();
            }
        }
    }





}