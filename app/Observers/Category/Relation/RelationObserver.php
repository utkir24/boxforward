<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */


namespace App\Observers\Category\Relation;


use App\Models\Category\Category;
use Illuminate\Database\Eloquent\Model;

class RelationObserver
{
    public $relation_name = 'categories';
    public $fieldName = 'categories';
    public $relation_class_name = Category::class;


    public function saved(Model $model)
    {
        $model->{$this->relation_name}()->detach();
        $catIds = $this->getCategories();
        if (is_array($catIds) && count($catIds)) {
            $class_name = $this->relation_class_name;
            $categoreis = $class_name::whereIn('id',$catIds)->get();
            if ($categoreis) {
                $model->{$this->relation_name}()->attach($categoreis);
            }
        }
    }

    protected  function getCategories()
    {
        $request = request();
        $categories = $request->get($this->fieldName, null);
        if ($categories) {
            return explode(',', $categories);
        }
        return [];
    }

}