<?php

namespace App\Providers;

use App\Langs\Lang;
use App\Models\Category\Category;
use App\Models\Faq\Faq;
use App\Models\Menu\Menu;
use App\Models\Page\Page;
use App\Models\Settings\Setting;
use App\Models\Shops\Shops;
use App\Models\Widget\Widget;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider.
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*
         * Application locale defaults for various components
         *
         * These will be overridden by LocaleMiddleware if the session local is set
         */

        /*
         * setLocale for php. Enables ->formatLocalized() with localized values for dates
         */
        setlocale(LC_TIME, config('app.locale_php'));

        /*
         * setLocale to use Carbon source locales. Enables diffForHumans() localized
         */
        Carbon::setLocale(config('app.locale'));

        /*
         * Set the session variable for whether or not the app is using RTL support
         * For use in the blade directive in BladeServiceProvider
         */
        if (! app()->runningInConsole()) {
            if (config('locale.languages')[config('app.locale')][2]) {
                session(['lang-rtl' => true]);
            } else {
                session()->forget('lang-rtl');
            }
        }

        // Force SSL in production
        if ($this->app->environment() == 'production') {
            //URL::forceScheme('https');
        }

        // Set the default string length for Laravel5.4
        // https://laravel-news.com/laravel-5-4-key-too-long-error
        Schema::defaultStringLength(191);

        // Set the default template for Pagination to use the included Bootstrap 4 template
        \Illuminate\Pagination\AbstractPaginator::defaultView('pagination::bootstrap-4');
        \Illuminate\Pagination\AbstractPaginator::defaultSimpleView('pagination::simple-bootstrap-4');


        Faq::creating(function ($faq) {
            if(strlen($faq->lang) == null){
                $faq->lang = Lang::current();
            }
            if(strlen($faq->lang_hash) == null){
                $faq->lang_hash = str_random(40);
            }
        });
        Shops::creating(function ($shops) {
            if(strlen($shops->lang) == null){
                $shops->lang = Lang::current();
            }
            if(strlen($shops->lang_hash) == null){
                $shops->lang_hash = str_random(40);
            }
        });

        Page::creating(function ($page) {
            if(strlen($page->lang) == 0){
                $page->lang = Lang::current();
            }
            if(strlen($page->lang_hash) == 0){
                $page->lang_hash = str_random(40);
            }
        });

        Setting::creating(function ($setting) {
            if(strlen($setting->lang) == 0){
                $setting->lang = Lang::current();
            }
            if(strlen($setting->lang_hash) == 0){
                $setting->lang_hash = str_random(40);
            }
        });

        Category::creating(function ($setting) {
            if(strlen($setting->lang) == 0){
                $setting->lang = Lang::current();
            }
            if(strlen($setting->lang_hash) == 0){
                $setting->lang_hash = str_random(40);
            }
        });
        Menu::creating(function ($setting) {
            if(strlen($setting->lang) == 0){
                $setting->lang = Lang::current();
            }
            if(strlen($setting->lang_hash) == 0){
                $setting->lang_hash = str_random(40);
            }
        });

        Widget::creating(function ($widget) {
            if(strlen($widget->lang) == 0){
                $widget->lang = Lang::current();
            }
            if(strlen($widget->lang_hash) == 0){
                $widget->lang_hash = str_random(40);
            }
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        /*
         * Sets third party service providers that are only needed on local/testing environments
         */
        if ($this->app->environment() != 'production') {
            /**
             * Loader for registering facades.
             */
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();

            /*
             * Load third party local aliases
             */
            $loader->alias('Debugbar', \Barryvdh\Debugbar\Facade::class);
        }
    }
}
