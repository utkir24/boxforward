<?php

namespace App\Providers;

use App\Models\Auth\User;
use App\Models\Category\Category;
use App\Models\Faq\Faq;
use App\Models\Page\Page;
use App\Models\Shops\Shops;
use App\Models\Widget\Widget;
use App\Observers\Category\Relation\RelationObserver;
use App\Observers\Page\Relation\PagesWidgetsRelation;
use App\Observers\Seo\SeoAddObservers;
use App\Observers\Slug\SlugObserver;
use App\Observers\User\UserObserver;
use App\Observers\Widget\Relation\WidgetCategory;
use App\Observers\Widget\Relation\WidgetNotification;
use Illuminate\Support\ServiceProvider;

/**
 * Class ObserverServiceProvider.
 */
class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Faq::observe([SlugObserver::class,RelationObserver::class]);
        Page::observe([RelationObserver::class]);
        Shops::observe([RelationObserver::class]);
        Widget::observe([WidgetNotification::class,WidgetCategory::class]);
        Page::observe([PagesWidgetsRelation::class,SeoAddObservers::class]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
