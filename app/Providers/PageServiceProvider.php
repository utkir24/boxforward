<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Providers;

use App\Models\Faq\Faq;
use App\Models\Page\Page;
use Illuminate\Support\ServiceProvider;

class PageServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Faq::creating(function ($faq) {
            if($faq->type == null){
                $faq->type = Page::TYPE_FAQ;
            }
        });
        Page::creating(function ($page) {
            if($page->type == null){
                $page->type = Page::TYPE_PAGE;
            }
        });

    }


}