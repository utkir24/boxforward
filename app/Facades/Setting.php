<?php

namespace App\Facades;

use App\Helpers\Settings\SettingsHelper;
use Illuminate\Support\Facades\Facade;

class Setting extends Facade
{
    /**
     * @return mixed
     */
    protected static function getFacadeAccessor()
    {
        return SettingsHelper::class;
    }
}