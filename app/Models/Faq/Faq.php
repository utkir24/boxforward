<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Models\Faq;

use App\Langs\LangScope;
use App\Langs\LangScopeTrait;
use App\Models\Category\Category;
use App\Models\Faq\Traits\Attribute\FaqAttribute;
use App\Models\Faq\Traits\Method\FaqMethod;
use App\Models\Faq\Traits\Scope\FaqGlobalScope;
use App\Models\Faq\Traits\Scope\FaqScope;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Class Faq
 * @package App\Models\Faq
 * @method active
 */
class Faq extends Model
{
    const STATUS_ACTIVE = 1;
    const STATUC_DEACTIVE = 0;

    use LangScopeTrait,
        HasSlug,
        FaqAttribute,
        FaqMethod,
        FaqScope;

    /**
     * @var string
     */
    protected $table = 'pages';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    public $fillable=[
      'title',
      'description',
      'content',
      'status',
      'sort',
      'lang',
      'lang_hash',
       'slug'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new FaqGlobalScope());
    }

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->usingLanguage('en')
            ->doNotGenerateSlugsOnUpdate();
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class,'faq_category','faq_id','category_id');
    }


}
