<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Models\Faq\Traits\Scope;

use App\Models\Faq\Faq;
use Illuminate\Support\Facades\Lang as Langs;

trait FaqScope
{
    /**
     * @param $query
     * @param bool $status
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', Faq::STATUS_ACTIVE);
    }

    public function scopeLangs($query)
    {
        $lang = Langs::locale();
        return $query->where('lang', 'LIKE', $lang);
    }

}