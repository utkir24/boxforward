<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Models\Faq\Traits\Attribute;


/**
 * Trait UserAttribute.
 */
trait FaqAttribute
{

    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<span class='badge badge-success'>".__('labels.general.active').'</span>';
        }

        return "<span class='badge badge-danger'>".__('labels.general.inactive').'</span>';
    }

    public function getCategoryIdAttribute()
    {
        $category = $this->categories()->get();
        if($category){
            return $category[0]->id;
        }
       return 0;
    }

    public function getCategoryParentIdAttribute()
    {
        $category = $this->categories()->get();
        if($category){
            return $category[0]->parent_id;
        }
        return 0;
    }




    /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
        return '<a href="'.route('frontend.faq.index').'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'" class="btn btn-info"><i class="fas fa-eye"></i></a>';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="'.route('admin.faq.edit', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit text-dark"></i></a>';
    }


    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {

        return '<a href="' . route('admin.faq.destroy', $this) . '"
                      data-method="delete"
                      data-trans-button-cancel="' . __('buttons.general.cancel') . '"
                      data-trans-button-confirm="' . __('buttons.general.crud.delete') . '"
                      data-trans-title="' . __('strings.backend.general.are_you_sure') . '"
                      class="btn btn-danger">
                      <i class="fas fa-trash" data-toggle="tooltip" data-placement="top" title="' . __('buttons.general.crud.delete') . '">
                      </i>
			      </a> ';

    }



    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '
    	<div class="btn-group" role="group" aria-label="Faq Actions">
		  '.$this->show_button.'
		  '.$this->edit_button.'
		  '.$this->delete_button.'
		</div>';
    }
}
