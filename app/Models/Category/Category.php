<?php

namespace App\Models\Category;

use App\Langs\Lang;
use App\Langs\LangScope;
use App\Langs\LangScopeTrait;
use App\Models\Category\Traits\Attribute\CategoryAttribute;
use App\Models\Category\Traits\Scope\CategoryScope;
use App\Models\Faq\Faq;
use App\Models\Page\Page;
use App\Models\Shops\Shops;
use App\Models\Widget\Widget;
use Illuminate\Database\Eloquent\Model;
use Nestable\NestableTrait;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Category extends Model
{


    const CREATED_AT = null;
    const UPDATED_AT = null;

    const STATUS_ACTIVE = 1;
    const STATUC_DEACTIVE = 0;

    const TYPE_POST = 1000;
    const TYPE_FAQ = 2000;
    const TYPE_PAGE = 3000;
    const TYPE_SHOPS = 4000;

    protected $parent = 'parent_id';

    use LangScopeTrait;
    use CategoryScope;
    use NestableTrait;
    use CategoryAttribute;
    use HasSlug;

    /**
     * @var array
     */
    public $fillable=[
        'title',
        'description',
        'status',
        'type',
        'slug'
    ];

    protected static function boot()
    {
        parent::boot();
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->doNotGenerateSlugsOnUpdate();
    }

    public static function types(){

        return [
            static::TYPE_POST => 'Post Category',
            static::TYPE_FAQ => 'FAQ Category',
            static::TYPE_PAGE => 'Page Category',
            static::TYPE_SHOPS => 'Shop Category',

        ];
    }

    public static function renderNestedHtml($type = 0,$all=true){

        $nested = static::getWithRootCategory($type);
        if(count($nested) == 0){return;}
        echo '<div class="dd"><ol class="dd-list">';
            foreach ($nested as $category){
                $link_remove = route("admin.category.destroy",['id' => $category['id']]);
                $link = route("admin.category.edit",['id' => $category['id']]);
                echo '<li class="dd-item" data-id="'.$category['id'].'">
                        <div class="dd-handle">'.$category['title'].'</div>
                        <a href="'.$link.'"><span class="fa fa-edit edit-button"></span></a>
                        <a href="'.$link_remove.'"><span class="fa fa-trash-alt remove-button"></span></a>';
                                if (count($category['children']) > 0) {
                                    self::renderChild($category['children']);
                                }
                echo '</li>';
            }
        echo '</ol></div>';
    }
    public static function renderChild($categories = []){
        echo '<ol class="dd-list">';
            foreach ($categories as $category):
                $link_remove = route("admin.category.destroy",['id' => $category['id']]);
                $link = route("admin.category.edit",['id' => $category['id']]);
                $link_view = '';

                if($category['type']==Category::TYPE_POST){
                    $link_view = "<a href=".route('frontend.category.index', $category['slug'])."><span class='fas fa-eye view-button'></span></a>";
                }

                echo '<li class="dd-item" data-id="'.$category['id'].'">
                            <div class="dd-handle">'.$category['title'].'</div>
                            <a href="'.$link.'"><span class="fa fa-edit edit-button"></span></a>'.$link_view.'
                            <a href="'.$link_remove.'"><span class="fa fa-trash-alt remove-button"></span></a>';
                            if(count($category['children']) > 0){
                                self::renderChild($category['children']);
                            }
                  echo '</li>';
            endforeach;
        echo '</ol>';
    }

    public static function renderFancy($type = 0,$selected = []){

        $nested = static::getWithRootCategory($type);
        if(!$nested){return;}
        static::renderRoot($nested,$selected);
    }

    public static function renderFancyPage($type = 0,$selected = [],$in_types=[])
    {
        $nested = static::getWithRootCategory($type,$in_types);
        if (!$nested) {
            return;
        }
        static::renderRoot($nested, $selected);
    }


    public static function renderRoot($nested,$selected=[]){

        foreach ($nested as $category):
            $selected_attr = "";
            if(in_array($category['id'],$selected)){
                $selected_attr = 'data-selected="true"';
            }else{
                $selected_attr = '';
            }
            echo '<li id="'.$category['id'].'" class="folder expanded" '.$selected_attr.'>';
            echo $category['title'];
            if(array_key_exists('children',$category) > 0){
                self::renderChildFancy($category['children'],$selected);
            }
            echo "</li>";
        endforeach;
    }



    public static function renderChildFancy($items = [],$selected = []){
        echo "<ul>";
                foreach ($items as $category):
                    if(in_array($category['id'],$selected)){
                        $selected_attr = 'data-selected="true"';
                    }else{
                        $selected_attr = '';
                    }
                    echo '<li id="'.$category['id'].'" class="folder expanded" '.$selected_attr.'>';
                    echo $category['title'];
                    if(array_key_exists('children',$category) > 0){
                        self::renderChildFancy($category['children'],$selected);
                        }
                    echo "</li>";
                endforeach;
        echo "</ul>";
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function pages()
    {
        return $this->belongsToMany(Page::class,'page_category');
    }

    public function faqs()
    {
        return $this->belongsToMany(Faq::class,'faq_category');
    }
    public function shops(){
        return $this->belongsToMany(Shops::class,'shops_categories','category_id','shop_id');
    }

    public function widgets(){
        return $this->belongsToMany(Widget::class,'widget_categories','category_id','widget_id');
    }

    public static function getWithRootCategory($type,$in_types=[]){

        $all=[];

        if($type>0) {
            $nested = self::langWithTypeAndSort($type)->nested()->get();
            if(count($nested)) {
                $all[] = self::rootCategory($type, $nested);
            }
        }else{
            $types = self::types();
            foreach ($types as $index => $value) {
                if(!empty($in_types)){
                    if(in_array($index,$in_types)){
                        $nested = self::langWithTypeAndSort($index)->nested()->get();
                        if(count($nested)) {
                            $all[] = self::rootCategory($index, $nested);
                        }
                    }
                }else{
                    $nested = self::langWithTypeAndSort($index)->nested()->get();
                    if(count($nested)) {
                        $all[] = self::rootCategory($index, $nested);
                    }
                }

            }
        }
        return $all;

    }

    public static function  rootCategory($type,$nested)
    {
        $root = [];
        switch ($type) {
            case Category::TYPE_PAGE :
                $root = [
                    'id' => 0,
                    'title' => 'Page Category',
                    'children' => $nested,
                    'parent_id' => null,
                ];

                break;

            case Category::TYPE_POST :
                $root = [
                    'id' => 0,
                    'title' => 'Post Category',
                    'children' => $nested,
                    'parent_id' => null
                ];

                break;
            case Category::TYPE_FAQ :
                $root = [
                    'id' => 0,
                    'title' => 'Faq Category',
                    'children' => $nested,
                    'parent_id' => null
                ];
                break;

            case Category::TYPE_SHOPS:
                $root = [
                    'id' => 0,
                    'title' => 'Shop Categories',
                    'children' => $nested,
                    'parent_id' => null
                ];
                break;

            default :
                $root = [
                    'id' => 0,
                    'title' => 'Category',
                    'children' => $nested,
                    'parent_id' => null

                ];
                break;
        }


        return $root;
    }
    public static function getDropdownList(){

       return  Category::where('type',static::TYPE_POST)->lang()->get()
                ->map(function ($cat) {
                     return ['title'=>$cat->title,'id'=>$cat->id];
                })->all();
    }

    public static function getSlugById($id){

        $model = Category::where('id',$id)->lang()->active()->first();
        if($model){
            return $model->slug;
        }
        return false;
    }

}
