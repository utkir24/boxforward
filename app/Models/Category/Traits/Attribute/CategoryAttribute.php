<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */



namespace App\Models\Category\Traits\Attribute;


/**
 * Trait UserAttribute.
 */
trait CategoryAttribute
{

    /**
     * @return string
     */
    public function getSingleLinkAttribute()
    {
       return route('frontend.category.index',$this->slug);
    }
}
