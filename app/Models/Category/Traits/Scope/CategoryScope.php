<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */


namespace App\Models\Category\Traits\Scope;


use App\Langs\Lang;
use App\Models\Category\Category;
use Illuminate\Support\Facades\Lang as Langs;

trait CategoryScope
{
    /**
     * @param $query
     * @param bool $status
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', Category::STATUS_ACTIVE);
    }

    public function scopeType($query, $type = Category::TYPE_POST)
    {
        return $query->where('type', $type);
    }
    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }


    /**
     * @param $query
     * @param null $lang
     * @return mixed
     */
    public function scopelangWithSort($query, $lang = null,$sort = 'sort',$orderby = 'asc')
    {
        if($lang == null){
            $lang = Lang::current();
        }
        return $query->where('lang', $lang)->orderBy($sort,$orderby);
    }


    /**
     * @param $query
     * @param null $lang
     * @return mixed
     */
    public function scopelangWithTypeAndSort($query, $type = 1000,$lang = null,$sort = 'sort',$orderby = 'asc')
    {
        if($lang == null){
            $lang = Lang::current();
        }
        return $query->where(['lang' => $lang,'type' => $type])->orderBy($sort,$orderby);
    }


    public function scopeTypeWithFaqs($query)
    {
        return $query->where(['lang' => Lang::current(), 'type' => Category::TYPE_FAQ]);
    }

    public function scopelangWithAllTypeForPage($query,$lang = null,$sort = 'sort',$orderby = 'asc')
    {
        if($lang == null){
            $lang = Lang::current();
        }
        return $query->where(['lang' => $lang])->where('status', '<>', Category::TYPE_FAQ)->orderBy($sort,$orderby);
    }


}