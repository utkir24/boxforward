<?php

namespace App\Models\Widget;

use App\Models\Notification\Notification;
use Illuminate\Database\Eloquent\Model;

class WidgetNotification extends Model
{
    protected $table = 'widget_notifications';

    public static function boot() {
        parent::boot();

        static::deleting(function(WidgetNotification $model) {
            $model->notification()->delete();
        });
    }

    public function widget()
    {
        return $this->belongsTo(Widget::class,'widget_id','widget_id');
    }

    public function notification()
    {
        return $this->belongsTo(Notification::class,'notification_id','notification_id');
    }
}
