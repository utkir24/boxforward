<?php

namespace App\Models\Widget;

use Illuminate\Database\Eloquent\Model;

class PagesWidget extends Model
{
    protected $table = 'pages_widgets';
}
