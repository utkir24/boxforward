<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */



namespace App\Models\Widget\Traits\Scope;


use App\Langs\Lang;
use App\Models\Category\Category;
use Illuminate\Support\Facades\Lang as Langs;

trait WidgetScope
{
    public function scopeLangs($query)
    {
        $lang = Langs::locale();
        return $query->where('lang', 'LIKE', $lang);
    }


}