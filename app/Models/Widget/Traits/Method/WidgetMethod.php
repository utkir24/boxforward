<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Models\Widget\Traits\Method;

use App\Models\Widget\Widget;

/**
 * Trait UserMethod.
 */
trait WidgetMethod
{
    /**
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->hasRole(config('access.users.admin_role'));
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status;
    }

    public static function getTypes()
    {
        return [
            Widget::TYPE_BONUSES => 'Бонусные программи',
            Widget::TYPE_HOW_BUY => 'Этап покупки',
            Widget::TYPE_PAYMENTS_METHOD => 'Методы оплаты',
            Widget::TYPE_PRICES_MAIN => 'Цена',
            Widget::TYPE_REVIEWS_GRID => 'Отзыви GridView',
            Widget::TYPE_HOME_SHORT_INFO => 'Информатции для шаблона Главная',
            Widget::TYPE_REASON => 'Reason',
            Widget::TYPE_SERVICES=>'Сервисы',
            Widget::TYPE_SERVICES_INFO=>'Сервисные информатции',
            Widget::TYPE_ABOUT_US => 'О нас',
            Widget::TYPE_REVIEWS => 'Отзыви ListView',
            Widget::TYPE_ARTICLES => 'Новости',
            Widget::TYPE_DELIVERY_PAGE => 'Способы отправки',
            Widget::TYPE_START_SHOPPING => 'Купи быстрее',
            Widget::TYPE_SHOPS => 'Магазины'
        ];

    }

    public static function getTypeImages($type)
    {
        if (array_key_exists($type, WidgetMethod::getTypes())) {
            return asset('box/img/widget-images/widget-' . $type . '.jpg');
        }
        return '';
    }

    public static function getWidgetDefaultTitle($type){

        $collection = collect(self::getTypes());

        return  $collection->get($type,'');
    }

    public static function TypesDropDownList(){
        return Widget::getTypes();
    }

}
