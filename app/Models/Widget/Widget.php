<?php

namespace App\Models\Widget;

use App\Langs\LangScopeTrait;
use App\Models\Category\Category;
use App\Models\Notification\Notification;
use App\Models\Page\Page;
use App\Models\Widget\Traits\Attribute\WidgetAttribute;
use App\Models\Widget\Traits\Method\WidgetMethod;
use App\Models\Widget\Traits\Scope\WidgetScope;
use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{

    const TYPE_BONUSES = 1;
    const TYPE_HOW_BUY = 2;
    const TYPE_PAYMENTS_METHOD = 3;
    const TYPE_PRICES_MAIN = 4;
    const TYPE_REVIEWS_GRID = 5;
    const TYPE_HOME_SHORT_INFO =6;
    const TYPE_REASON = 7;
    const TYPE_SERVICES = 8;
    const TYPE_SERVICES_INFO = 9;
    const TYPE_ABOUT_US = 10;
    const TYPE_REVIEWS = 11;
    const TYPE_ARTICLES = 12;
    const TYPE_DELIVERY_PAGE = 13;
    const TYPE_START_SHOPPING = 14;
    const TYPE_SHOPS = 15;

    use LangScopeTrait,
        WidgetAttribute,
        WidgetScope,
        WidgetMethod;


    protected $table = 'widgets';
    protected $primaryKey = 'widget_id';

    public $fillable = [
        'title',
        'description',
        'image',
        'type',
        'lang',
        'lang_hash',
    ];

    public $timestamps = true;

    public function widgetNotifications()
    {
        return $this->hasMany(WidgetNotification::class,'widget_id','widget_id');
    }

    public function notifications()
    {
        return $this->belongsToMany(Notification::class,'widget_notifications','widget_id','notification_id');
    }
    public function pages()
    {
        return $this->belongsToMany(Page::class,'pages_widgets');
    }


    public function widgetCategories()
    {
        return $this->hasMany(WidgetCategory::class,'widget_id','widget_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class,'widget_categories','widget_id','category_id');
    }


}
