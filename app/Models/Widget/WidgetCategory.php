<?php

namespace App\Models\Widget;

use App\Models\Category\Category;
use Illuminate\Database\Eloquent\Model;

/**
 * Class WidgetCategory
 * @package App\Models\Widget
 * @property integer $id
 * @property integer $widget_id
 * @property integer $category_id
 * @property integer $sort
 * @property integer $count
 */
class WidgetCategory extends Model
{
    protected $table = 'widget_categories';
    public $timestamps = false;


    public function widget()
    {
        return $this->belongsTo(Widget::class,'widget_id','widget_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class,'id','category_id');
    }
}
