<?php

namespace App\Models\Notification;

use App\Models\Widget\Widget;
use App\Models\Widget\WidgetNotification;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{

    protected $table='notifications';

    public $fillable = [
        'title',
        'description',
        'content',
    ];

    public $timestamps = true;
    protected $primaryKey = 'notification_id';

    public function widgetNotifications()
    {
        return $this->hasMany(WidgetNotification::class,'notification_id','notification_id');
    }

    public function widgets()
    {
        return $this->belongsToMany(Widget::class,'widget_notifications','notification_id','widget_id');
    }

    public function children()
    {
        return $this->belongsToMany(Notification::class, 'notification_children', 'parent_id','notification_id');
    }

    public function parents()
    {
        return $this->belongsToMany(Notification::class, 'notification_children', 'notification_id','parent_id');
    }

    public function images()
    {
        return $this->hasMany(NotificationImages::class,'notification_id','notification_id');
    }

    public function fieldsExtra()
    {
        return $this->hasMany(NotificationField::class , 'notification_id','notification_id');
    }


}

