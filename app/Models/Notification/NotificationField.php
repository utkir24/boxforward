<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Model;

class NotificationField extends Model
{
    protected $casts = [
        'data' => 'array'
    ];

    protected $table='notification_fields';

    public $fillable = [
        'notification_id',
        'data'
    ];

    public $timestamps=false;

    public function notification()
    {
        return $this->hasOne(Notification::class, 'notification_id', 'notification_id');
    }
}
