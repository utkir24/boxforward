<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Model;

class NotificationImages extends Model
{
    protected $table='notification_images';

    public $fillable = [
        'notification_id',
        'image',
        'sort',
    ];

    public $timestamps=false;

}
