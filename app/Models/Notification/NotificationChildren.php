<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Model;

class NotificationChildren extends Model
{
    protected $table = 'notification_children';

    public $fillable = [
        'parent_id',
        'notification_id',
        'sort',
    ];
}
