<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */


namespace App\Models\Menu\Traits\Scope;


use App\Langs\Lang;
use App\Models\Menu\Menu;
use Illuminate\Support\Facades\Lang as Langs;

trait MenuScope
{
    /**
     * @param $query
     * @param bool $status
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', Menu::STATUS_ACTIVE);
    }
    /**
     * @param $query
     * @param null $lang
     * @return mixed
     */
    public function scopelangWithSort($query, $lang = null,$sort = 'sort',$orderby = 'asc')
    {
        if($lang == null){
            $lang = Lang::current();
        }
        return $query->where('lang', $lang)->orderBy($sort,$orderby);
    }



    public function scopelangWithTypeAndSort($query, $type = 1000,$lang = null,$sort = 'sort',$orderby = 'asc')
    {
        if($lang == null){
            $lang = Lang::current();
        }
        return $query->where(['lang' => $lang,'type' => $type])->orderBy($sort,$orderby);
    }

    public function typeHeader($query){
        return $query->where(['type'=>Menu::TYPE_HEADER]);
    }



    public function scopeTypeWithFaqs($query)
    {
        return $query->where(['lang' => Lang::current(), 'type' => Category::TYPE_FAQ]);
    }

    public function scopelangWithAllTypeForPage($query,$lang = null,$sort = 'sort',$orderby = 'asc')
    {
        if($lang == null){
            $lang = Lang::current();
        }
        return $query->where(['lang' => $lang])->where('status', '<>', Category::TYPE_FAQ)->orderBy($sort,$orderby);
    }



}