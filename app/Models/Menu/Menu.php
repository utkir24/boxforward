<?php

namespace App\Models\Menu;


use App\Langs\LangScopeTrait;
use App\Models\Menu\Traits\Scope\MenuScope;
use App\Models\Page\Page;
use Illuminate\Database\Eloquent\Model;
use Nestable\NestableTrait;

class Menu extends Model
{
    use LangScopeTrait;
    use MenuScope;
    use NestableTrait;

    protected $parent = 'parent_id';

    const CREATED_AT = null;
    const UPDATED_AT = null;

    const STATUS_ACTIVE = 1;
    const STATUC_DEACTIVE = 0;

    const TYPE_FOOTER = 1000;
    const TYPE_SIDEBAR = 2000;
    const TYPE_HEADER = 3000;
    /**
     * @var array
     */
    public $fillable=[
        'title',
        'link',
        'status',
        'type',
        'slug'
    ];

    protected static function boot()
    {
        parent::boot();
    }
    public static function types(){
        return [
            static::TYPE_FOOTER=> 'Footer',
            static::TYPE_HEADER=> 'Header',
            static::TYPE_SIDEBAR=> 'Sidebar',
        ];
    }


    public static function renderNestedHtml($type = 0,$all=true){
        $nested = static::getWithRootCategory($type);
        if(count($nested) == 0){return;}
        echo '<div class="dd"><ol class="dd-list">';
        foreach ($nested as $category){
            $link_remove = route("admin.menu.destroy",['id' => $category['id']]);
            $link = route("admin.menu.edit",['id' => $category['id']]);
            echo '<li class="dd-item" data-id="'.$category['id'].'">
                        <div class="dd-handle">'.$category['title'].'</div><a href="'.$link.'"><span class="fa fa-edit edit-button"></span></a><a href="'.$link_remove.'"><span class="fa fa-trash-alt remove-button"></span></a>';
            if (count($category['children']) > 0) {
                self::renderChild($category['children']);
            }
            echo '</li>';
        }
        echo '</ol></div>';
    }
    public static function renderChild($categories = []){
        echo '<ol class="dd-list">';
            foreach ($categories as $category):
                $link_remove = route("admin.menu.destroy",['id' => $category['id']]);
                $link = route("admin.menu.edit",['id' => $category['id']]);
                echo '<li class="dd-item" data-id="'.$category['id'].'">
                            <div class="dd-handle">'.$category['title'].'</div><a href="'.$link.'"><span class="fa fa-edit edit-button"></span></a><a href="'.$link_remove.'"><span class="fa fa-trash-alt remove-button"></span></a>';
                            if(count($category['children']) > 0){
                                self::renderChild($category['children']);
                            }
                  echo '</li>';
            endforeach;
        echo '</ol>';
    }

    public static function getWithRootCategory($type,$in_types=[]){

        $all=[];

        if($type>0) {
            $nested = Menu::langWithTypeAndSort($type)->nested()->get();
            if(count($nested)) {
                $all[] = self::rootMenu($type, $nested);
            }
        }else{
            $types = self::types();
            foreach ($types as $index => $value) {
                if(!empty($in_types)){
                    if(in_array($index,$in_types)){
                        $nested = self::langWithTypeAndSort($index)->nested()->get();
                        if(count($nested)) {
                            $all[] = self::rootMenu($index, $nested);
                        }
                    }
                }else{
                    $nested = self::langWithTypeAndSort($index)->nested()->get();
                    if(count($nested)) {
                        $all[] = self::rootMenu($index, $nested);
                    }
                }

            }
        }
        return $all;

    }
    public static function  rootMenu($type,$nested)
    {

        $root = [];
        switch ($type) {
            case Menu::TYPE_HEADER:
                $root = [
                    'id' => 0,
                    'title' => 'Header Menu',
                    'children' => $nested,
                    'parent_id' => null
                ];

                break;

            case Menu::TYPE_SIDEBAR:
                $root = [
                    'id' => 0,
                    'title' => 'Sidebar Menu',
                    'children' => $nested,
                    'parent_id' => null
                ];

                break;
            case Menu::TYPE_FOOTER:
                $root = [
                    'id' => 0,
                    'title' => 'Footer Menu',
                    'children' => $nested,
                    'parent_id' => null
                ];
                break;

            default :
                $root = [
                    'id' => 0,
                    'title' => 'Menu',
                    'children' => $nested,
                    'parent_id' => null

                ];
                break;
        }


        return $root;
    }

    public static function renderFancy($type=1000){
        $nested = self::langWithTypeAndSort(null,$type)->nested()->get();
            if(!$nested){return;}
        foreach ($nested as $category):
                echo '<li id="'.$category['id'].'" class="folder expanded">';
                echo $category['title'];
                    if(array_key_exists('children',$category) > 0){
                        self::renderChildFancy($category['children']);
                    }
                echo "</li>";
            endforeach;
    }
    public static function renderChildFancy($items = []){
        echo "<ul>";
                foreach ($items as $category):
                    echo '<li id="'.$category['id'].'" class="folder expanded">';
                    echo $category['title'];
                    if(array_key_exists('children',$category) > 0){
                        self::renderChildFancy($category['children']);
                        }
                    echo "</li>";
                endforeach;
        echo "</ul>";
    }
    public static function getMenu($type = self::TYPE_HEADER){
        return Menu::where('type',$type)->active()->orderBy('sort',SORT_ASC)->lang()->get();
    }

    public function pages()
    {
        return $this->belongsToMany(Page::class,'page_menu');
    }


}
