<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */



namespace App\Models\Page;


use Illuminate\Database\Eloquent\Model;

class PageCategories extends Model
{
    public $fillable =['page_id','category_id'];

    public $table = 'page_category';

}