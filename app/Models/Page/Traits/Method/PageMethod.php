<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Models\Page\Traits\Method;

use App\Models\Page\Page;
/**
 * Trait UserMethod.
 */
trait PageMethod
{
    /**
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->hasRole(config('access.users.admin_role'));
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status;
    }
    public static function dropdownListTemplates(){
        return [
            Page::TEMPLATE_MAIN =>'Главная страница',
            Page::TEMPLATE_PAGE_DEFAULT =>'Без картинки',
            Page::TEMPLATE_PAGE_IMAGES =>'С картинкой'
        ];
    }


    public static function getTemplates()
    {
        return [
            Page::TEMPLATE_PAGE_DEFAULT => 'Шаблон по умолчанию',
            Page::TEMPLATE_PAGE_IMAGES => 'Шаблон с картиной',
            Page::TEMPLATE_MAIN => 'Шаблон Главная'
        ];

    }

    public static function getTemplateImages($template)
    {
        if (array_key_exists($template, PageMethod::getTemplates())) {
            return asset('box/img/page-images/page-' . $template . '.jpg');
        }
        return '';
    }

    public function singleLinkForCategory()
    {
        return route('frontend.category.post', $this->slug);
    }

}
