<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */


namespace App\Models\Page\Traits\Scope;


use App\Models\Page\Page;
use Illuminate\Support\Facades\Lang as Langs;

trait PageScope
{
    /**
     * @param $query
     * @param bool $status
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('status', Page::STATUS_ACTIVE);
    }

    public function scopeLangs($query)
    {
        $lang = Langs::locale();
        return $query->where('lang', 'LIKE', $lang);
    }

    public function scopeType($query, $type = Page::TYPE_PAGE)
    {
        return $query->where('type', $type);
    }

    public function scopeSlug($query,$slug)
    {
        return $query->where('slug', $slug);
    }

    public function scopeSortupdated($query)
    {
        return $query->orderBy('updated_at', 'DESC');
    }


}