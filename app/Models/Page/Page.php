<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */


namespace App\Models\Page;

use App\Langs\LangScope;
use App\Langs\LangScopeTrait;
use App\Models\Category\Category;
use App\Models\Menu\Menu;
use App\Models\Page\Traits\Attribute\PageAttribute;
use App\Models\Page\Traits\Method\PageMethod;
use App\Models\Page\Traits\Scope\PageGlobalScope;
use App\Models\Page\Traits\Scope\PageScope;
use App\Models\Widget\Widget;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Class Faq
 * @package App\Models\Faq
 * @method active
 */
class Page extends Model
{

    const STATUS_ACTIVE = 1;
    const STATUC_DEACTIVE = 0;

    const TYPE_PAGE = 1; //page
    const TYPE_FAQ = 2; //qa_faqs
    const TYPE_POST = 3;

    /*const TYPE_MENU = 3;//nav_menu_item
    const TYPE_ACF = 4; //acf
    const TYPE_ATTACHMENT=5; //attachment
    const TYPE_CONTENT_BLOCK = 6; //content_block
    const TYPE_INSTRUCTION = 7; //instruction
    const TYPE_REVIEW = 8; //review
    const TYPE_REVISION = 9; //revision
    const TYPE_SHOP = 10; // shop
    const TYPE_TILE = 11; //tile*/
    const TEMPLATE_MAIN = 2;
    const TEMPLATE_PAGE_DEFAULT = 1;
    const TEMPLATE_PAGE_IMAGES = 3;

    use LangScopeTrait,
        PageScope,
        PageMethod,
        PageAttribute,
        HasSlug;

    /**
     * @var string
     */
    protected $casts = [
      'seo'=>'array'
    ];
    protected $table = 'pages';

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    public $fillable=[
      'title',
      'description',
      'content',
      'status',
      'content_two',
      'template_id',
      'small_title',
      'image',
      'type',
      'sort',
      'slug',
      'lang',
      'lang_hash',
    ];

    protected static function boot()
    {
        parent::boot();
       static::addGlobalScope(new PageGlobalScope());
    }


    public static function types(){

        return [
            static::TYPE_POST =>'Post',
            static::TYPE_PAGE =>'Page'
        ];

    }


    public function getImage(){
        return $this->image;
    }
    public function getContentTwo(){
        return $this->content_two;
    }


    public static function routesTypes(){
        return [

        ];
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->usingLanguage('en')
            ->doNotGenerateSlugsOnUpdate();

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class,'page_category','page_id','category_id');
    }
    public function menus()
    {
        return $this->belongsToMany(Menu::class,'page_menu','page_id','menu_id');
    }
    public function widgets(){
        return $this->belongsToMany(Widget::class,'pages_widgets','page_id','widget_id');
    }


}
