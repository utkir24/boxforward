<?php

namespace App\Models\Settings;

use App\Langs\LangScopeTrait;
use App\Models\Settings\Traits\Attribute\SettingAttribute;
use App\Models\Settings\Traits\Method\SettingMethod;
use App\Models\Settings\Traits\Scope\SettingScope;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use LangScopeTrait;
    use SettingScope;
    use SettingMethod;
    use SettingAttribute;
    const STATUS_ACTIVE = 1;

    public function __construct(array $attributes = [])
    {
        $this->table = config('settings.table', 'settings');
        parent::__construct($attributes);
    }

    protected $guarded = ['id'];

    public function setCodeAttribute($value)
    {
        $this->attributes['code'] = str_slug($value, '_');
    }

    public function getTypeAttribute()
    {
        return $this->attributes['type'] = strtoupper($this->attributes['type']);
    }

    public function getValueAttribute()
    {
        $value = $this->attributes['value'];

        switch ($this->attributes['type']) {
            case 'FILE':
                if (!empty($value)) {
                    return config('settings.upload_path') . '/' . $value;
                }
                break;
            case 'SELECT':
                $values = json_decode($value, true);
                if ($values) {
                    return $values;
                } else {
                    return [];
                }
                break;
            case 'BOOLEAN':
                if ($value == 'true') {
                    return true;
                } else {
                    return false;
                }
                break;
            case 'NUMBER':
                return floatval($value);
                break;
        }

        return $value;
    }

}
