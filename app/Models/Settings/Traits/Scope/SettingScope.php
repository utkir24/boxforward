<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */

namespace App\Models\Settings\Traits\Scope;

use App\Models\Settings\Setting;
use Illuminate\Support\Facades\Lang as Langs;

trait SettingScope
{
    /**
     * @param $query
     * @param bool $status
     * @return mixed
     */
    public function scopeActive($query, $status = true)
    {
        return $query->where('hidden', Setting::STATUS_ACTIVE);
    }

    public function scopeLangs($query)
    {
        $lang = Langs::locale();
        return $query->where('lang', 'LIKE', $lang);
    }

}