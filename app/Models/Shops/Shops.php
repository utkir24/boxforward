<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 028 28.09.18
 * Time: 20:52
 */

namespace App\Models\Shops;


use App\Langs\LangScopeTrait;
use App\Models\Category\Category;
use App\Models\Shops\Traits\Attribute\ShopsAttribute;
use App\Models\Shops\Traits\Method\ShopsMethod;
use App\Models\Shops\Traits\Scope\ShopsGlobalScope;
use App\Models\Shops\Traits\Scope\ShopsScope;
use Illuminate\Database\Eloquent\Model;

class Shops extends Model
{

    const STATUS_ACTIVE = 1;
    const STATUC_DEACTIVE = 0;

    use LangScopeTrait,
        ShopsScope,
        ShopsMethod,
        ShopsAttribute;

    protected $table = 'shops';

    /**
     * @var bool
     */
    public $timestamps = true;


    /**
     * @var array
     */
    public $fillable=[
        'title',
        'description',
        'content',
        'status',
        'price',
        'payment_system',
        'shop_site',
        'worth_knowing',
        'link_site',
        'image',
        'sort',
        'lang',
        'lang_hash',
    ];





    public static function routesTypes(){
        return [

        ];
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class,'shops_categories','shop_id','category_id');
    }


}