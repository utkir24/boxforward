<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 028 28.09.18
 * Time: 21:07
 */

namespace App\Models\Shops;


use Illuminate\Database\Eloquent\Model;

class ShopsCategories extends Model
{
    public $fillable =['shop_id','category_id'];

    public $table = 'shops_categories';

}