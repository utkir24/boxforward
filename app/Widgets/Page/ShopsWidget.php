<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 030 30.09.18
 * Time: 11:58
 */

namespace App\Widgets\Page;


use App\Models\Category\Category;
use App\Models\Page\Page;
use App\Models\Shops\Shops;
use App\Repositories\Backend\Page\PageRepository;
use App\Repositories\Frontend\Category\CategoryRepository;
use App\Widgets\InterfaceWidget;

class ShopsWidget implements InterfaceWidget
{
    public $widget;
    public $repository;
    public $slug;
    public function __construct($widget){
        if (!empty($widget)){
            $this->widget = $widget;
        }else {return false;}
        $this->repository = new CategoryRepository();
        $this->slug = self::getRequest();
    }

    /**
     * @return bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * главный функционал работает после конструктура
     */
    public function execute()
    {
        if(empty($this->widget)){return false;}
        if(empty($this->widget->categories)){return false;}
        if(($dataProvider = $this->getData()) === null){return false;}
        return view('Widgets::shops-items', [
            'categories' => $this->widget->categories,
            'dataProvider'=>$dataProvider,
            'slug'=>$this->slug
        ]);
    }

    /**
     * @return mixed возврашает данные по слагу например слаг обув то возрашает категории обув и магазини по категории обув
     */
    protected function getCategoryFindSlugData(){
        $category = $this->repository->getOneShopCategory($this->slug['category']);
        if(empty($category)){return null;}
        $clone_model = clone $category;
        $shops = $this->repository->getActivePaginateWithModel($clone_model->shops(),12);
        return $shops;
    }

    /**
     * данные по умолчанию
     */
    protected function getDefaultData(){
       $shops =   Shops::active()->lang()->orderBy('created_at', SORT_DESC)->paginate(12);
       if(empty($shops)){return null;}
       return $shops;
    }

    /**
     * Возврашает данные магазинов по категории
     */
    protected function getData(){
        if($this->slug !==false)
        {
            return $this->getCategoryFindSlugData();
        }
        else
        {
            return $this->getDefaultData();
        }
    }


    /**
     * @return array|bool|string
     * Берём данные от Url например ?category="slug" берём сдес параметр slug и возврашаем
     */
    protected static function  getRequest()
    {
      $request = \Request::query();
      if(isset($request['category'])&&!empty($request['category'])) {
          return $request;
      }else{
          return false;
      }
    }




}