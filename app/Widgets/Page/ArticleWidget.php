<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 017 17.09.18
 * Time: 11:46
 */

namespace App\Widgets\Page;


use App\Models\Page\Page;
use App\Repositories\Backend\Page\PageRepository;
use App\Widgets\InterfaceWidget;

class ArticleWidget implements InterfaceWidget
{
    public $widget;

    public function __construct($widget){
        if (!empty($widget)){
            $this->widget = $widget;
        }else {return false;}
    }

    public function execute()
    {
        if(empty($this->widget)){return false;}
        if(empty($this->widget->categories)){return false;}
        return view('Widgets::articles-items', [
            'categories' => $this->widget->categories,
        ]);
    }

}
