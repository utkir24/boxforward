<?php
namespace App\Widgets\Page;

use App\Models\Widget\Widget;
use App\Widgets\InterfaceWidget;


class PageWidgets implements InterfaceWidget
{
    public $widget;

    public function __construct($widget){
        if (!empty($widget)){
            $this->widget = $widget;
        }else {return false;}
    }
    public function execute(){
        if(($template = $this->getTemplate()) === false){ return false;}
        return view('Widgets::'.$template, [
            'widget' => $this->widget
        ]);
    }

    public function getTemplate(){
        switch($this->widget->type){
            case Widget::TYPE_ARTICLES: return 'articles';break;
            case Widget::TYPE_BONUSES:
                if(empty($this->widget->image)){return 'bonuses_no_image';break;}
                else return 'bonuses';break;
            case Widget::TYPE_DELIVERY_PAGE: return 'delivery_page';break;
            case Widget::TYPE_HOW_BUY: return 'how_buy';break;
            case Widget::TYPE_PAYMENTS_METHOD: return 'payments_method';break;
            case Widget::TYPE_PRICES_MAIN: return 'prices_main';break;
            case Widget::TYPE_REASON: return 'reasons';break;
            case Widget::TYPE_REVIEWS: return 'reviews';break;
            case Widget::TYPE_REVIEWS_GRID: return 'reviews_grid';break;
            case Widget::TYPE_SERVICES_INFO: return 'service_info';break;
            case Widget::TYPE_SERVICES: return 'services';break;
            case Widget::TYPE_START_SHOPPING: return 'start_shopping';break;
            case Widget::TYPE_ABOUT_US:return 'about_us';break;
            case Widget::TYPE_SHOPS:return 'shops';break;

            default: return false;
        }
    }

    public static function getClass($item){
        if($item === 1){return 'second';}
        else{return 'first';}
    }
}