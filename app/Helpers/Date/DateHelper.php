<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 012 12.09.18
 * Time: 17:02
 */

namespace App\Helpers\Date;


use Carbon\Carbon;

class DateHelper
{
    public static function getCreatedNewYork()
    {
        $timeZone = 'America/New_York';
        return Carbon::now($timeZone)->format('Y.m.d H:i');
    }

    public static function getCreatedMoskow()
    {
        $timeZone = 'Europe/Moscow';
        return Carbon::now($timeZone)->format('Y.m.d H:i');
    }

}