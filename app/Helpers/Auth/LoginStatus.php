<?php
/**
 * @author Jakhar <https://github.com/jakharbek>
 * @author Nazrullo <https://github.com/nazrullo>
 * @author O`tkir    <https://github.com/utkir24>
 * @team Adigitalteam <https://github.com/adigitalteam>
 * @package Boxforward
 */


namespace App\Helpers\Auth;


use GuzzleHttp\Client;

class LoginStatus
{
    public $client;
    public $response;
    public $address;
    public $url;
    public $user_name;
    public $balance;
    public $token;
    public $photo;
    public $thumb_photo;

    public function __construct()
    {
        $this->client = new Client();
        $this->response = json_decode($this->client->get('https://boxfwd.com/profile/info')->getBody(),true);
       //$this->response = json_decode($this->getDefaultJsonData(),true);
        $this->response = self::isExist($this->response,'data') ? $this->response['data']:[];
        $this->setAttributeApi();

    }

    public  function isLoginApi()
    {
        $response = $this->response;
        if ( self::isExist($response, 'status')) {
            if ($response['status'] == 'OK') {
                return true;
            }
        }
        return false;
    }

    protected static function isExist($array,$key)
    {
        return array_key_exists($key,$array);
    }

    public function getFilterData()
    {
        return  $this->response;
    }

    protected function setAttributeApi()
    {
        $this->address = self::isExist($this->response,'address') ? $this->response['address'] : [];
        $this->url = self::isExist($this->response,'url') ? $this->response['url'] : '';
        $this->user_name = self::isExist($this->response,'user_name') ? $this->response['user_name'] : '';
        $this->balance = self::isExist($this->response,'balance') ? $this->response['balance'] : '0.00';
        $this->token = self::isExist($this->response,'token') ? $this->response['token'] : '';
        $this->photo = self::isExist($this->response,'photo') ? $this->response['photo'] : asset('box/img/user.svg');
        $this->thumb_photo = self::isExist($this->response,'thumb_photo') ? $this->response['thumb_photo'] : asset('box/img/user.svg');

    }

    protected function getDefaultJsonData(){
        return '{
              "data": {
                "status": "OK",
                "address": {
                  "Одежда и обувь": {
                    "title": "Адреса безналоговых складов",
                    "street": "310 Braen Ave, suite 0001071",
                    "state": "New Jersey",
                    "name": "demo user",
                    "city": "Wyckoff",
                    "zip": "07481",
                    "phone": "732-893-7447"
                  },
                  "Техника, игрушки и аксессуары": {
                    "title": "Адреса безналоговых складов",
                    "street": "818-A South Heald Street, suite 0001071",
                    "name": "demo user",
                    "city": "Wilmington",
                    "state": "Delaware",
                    "zip": "19801",
                    "phone": "732-893-7447"
                  }
                },
                "url": "/ru/addresses",
                "user_name": "demo user",
                "balance": "892.85",
                "token": "9TGLxnmr7Xdakkkk7RpM",
                "photo": "https://d3ekw62vlhmnyb.cloudfront.net/uploads/user/avatar/071/100/71/joanna-kosinska-469281-unsplash.jpg",
                "thumb_photo": "https://d3ekw62vlhmnyb.cloudfront.net/uploads/user/avatar/071/100/71/small_thumb_joanna-kosinska-469281-unsplash.jpg"
              }
            }';
    }

}