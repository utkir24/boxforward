<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotificationWidget extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widget_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('widget_id');
            $table->unsignedInteger('notification_id');
            $table->integer('sort')->nullable();

            $table->foreign('widget_id')->references('widget_id')->on('widgets')->onDelete('cascade');
            $table->foreign('notification_id')->references('notification_id')->on('notifications')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('widget_notifications');
    }
}
