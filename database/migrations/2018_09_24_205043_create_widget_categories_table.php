<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWidgetCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('widget_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('widget_id');
            $table->unsignedInteger('category_id');
            $table->integer('count')->default(0);
            $table->integer('sort')->nullable();

            $table->foreign('widget_id')->references('widget_id')->on('widgets')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('widget_categories');
    }
}
