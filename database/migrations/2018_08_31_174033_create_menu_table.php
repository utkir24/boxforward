<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',255);
            $table->string('link',255)->defoult(null);
            $table->smallInteger('status')->default(0);
            $table->integer('sort')->default(null);
            $table->integer('type');
            $table->timestamps();
            $table->string('lang',15);
            $table->string('lang_hash',255);
            $table->unsignedInteger('parent_id')->default(null);
            $table->string('slug',255)->default(null);
            $table->text('description')->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
