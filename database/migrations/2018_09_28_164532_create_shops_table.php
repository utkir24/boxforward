<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',255)->defoult(null);
            $table->text('description')->defoult(null);
            $table->text('content')->defoult(null);
            $table->text('payment_system')->defoult(null);
            $table->text('price')->defoult(null);
            $table->string('shop_site',255)->defoult(null);
            $table->string('link_site',255)->defoult(null);
            $table->string('image',255)->defoult(null);
            $table->integer('sort')->defoult(0);
            $table->integer('status')->defoult(0);
            $table->string('worth_knowing')->defoult(null);
            $table->string('lang',15);
            $table->string('lang_hash',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
