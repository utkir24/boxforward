<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('template_id')->nullable();
            $table->string('small_title',255)->nullable();
            $table->string('title',255)->nullable();
            $table->text('description')->nullable();
            $table->text('content')->nullable();
            $table->text('content_two')->nullable();
            $table->smallInteger('status');
            $table->integer('sort')->nullable();
            $table->integer('type');
            $table->string('image',255)->nullable();
            $table->string('slug',255);
            $table->string('lang',15);
            $table->string('lang_hash',255);
            $table->json('seo')->nullable();
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
