<?php

use App\Models\Menu\Menu;
use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Add the master administrator, user id of 1
        Menu::create([
            'title'        => 'Forum',
            'link'         => '/forum',
            'status'             => 1,
            'sort'          => 0,
            'type' => 3000,
            'lang'         => 'en',
            'lang_hash'=>'7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmF',
            'parent_id'=>0
        ]);

        Menu::create([
            'title'        => 'Форум',
            'link'         => '/forum',
            'status'             => 1,
            'sort'          => 0,
            'type' => 3000,
            'lang'         => 'ru',
            'lang_hash'=>'7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmF',
            'parent_id'=>0
        ]);
        Menu::create([
            'title'        => 'КАК ПОКУПАТЬ',
            'link'         => '/how-buy-page',
            'status'             => 1,
            'sort'          => 0,
            'type' => 3000,
            'lang'         => 'ru',
            'lang_hash'=>'7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmq',
            'parent_id'=>0
        ]);
        Menu::create([
            'title'        => 'HOW BUY',
            'link'         => '/how-buy-page',
            'status'             => 1,
            'sort'          => 0,
            'type' => 3000,
            'lang'         => 'en',
            'lang_hash'=>'7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmq',
            'parent_id'=>0
        ]);
        Menu::create([
            'title'        => 'СТОИМОСТЬ',
            'link'         => '/prices',
            'status'             => 1,
            'sort'          => 0,
            'type' => 3000,
            'lang'         => 'ru',
            'lang_hash'=>'7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmD',
            'parent_id'=>0
        ]);

        Menu::create([
            'title'        => 'PRICES',
            'link'         => '/prices',
            'status'             => 1,
            'sort'          => 0,
            'type' => 3000,
            'lang'         => 'en',
            'lang_hash'=>'7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmD',
            'parent_id'=>0
        ]);


        Menu::create([
            'title'        => 'ДОСТАВКА',
            'link'         => '/delivery-page',
            'status'             => 1,
            'sort'          => 0,
            'type' => 3000,
            'lang'         => 'ru',
            'lang_hash'=>'7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmM',
            'parent_id'=>0
        ]);
        Menu::create([
            'title'        => 'DELIVERY',
            'link'         => '/delivery-page',
            'status'             => 1,
            'sort'          => 0,
            'type' => 3000,
            'lang'         => 'en',
            'lang_hash'=>'7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmM',
            'parent_id'=>0
        ]);

        Menu::create([
            'title'        => 'FAQ',
            'link'         => '/faq',
            'status'             => 1,
            'sort'          => 0,
            'type' => 3000,
            'lang'         => 'en',
            'lang_hash'=>'7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmL',
            'parent_id'=>0
        ]);

        Menu::create([
            'title'        => 'ВОПРОСЫ ОТВЕТИ',
            'link'         => '/faq',
            'status'             => 1,
            'sort'          => 0,
            'type' => 3000,
            'lang'         => 'ru',
            'lang_hash'=>'7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmL',
            'parent_id'=>0
        ]);


        $this->enableForeignKeys();
    }
}
