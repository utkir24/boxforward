<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

/**
 * Class PermissionRoleTableSeeder.
 */
class PermissionRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Create Roles
        $admin = Role::create(['name' => config('access.users.admin_role')]);
        $journalist = Role::create(['name' => 'journalist']);
        $moder = Role::create(['name' => 'moderator']);
        $user = Role::create(['name' => config('access.users.default_role')]);

        // Create Permissions
        Permission::create(['name' => 'view backend']);
        Permission::create(['name' => 'create']);
        Permission::create(['name' => 'update']);
        Permission::create(['name' => 'read']);
        Permission::create(['name' => 'delete']);
        Permission::create(['name' => 'publish']);

        // ALWAYS GIVE ADMIN ROLE ALL PERMISSIONS
        $admin->syncPermissions('view backend','create','update','read','delete','publish');
        $moder->syncPermissions('view backend','create','update','delete','publish','read');
        $journalist->syncPermissions('view backend','create','read');


        $this->enableForeignKeys();
    }
}
