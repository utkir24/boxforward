-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Сен 27 2018 г., 11:34
-- Версия сервера: 5.5.60
-- Версия PHP: 7.1.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `admin_boxforward`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cache`
--

CREATE TABLE `cache` (
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `lang` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `title`, `description`, `status`, `sort`, `type`, `created_at`, `updated_at`, `lang`, `lang_hash`, `parent_id`, `slug`) VALUES
                                                                                                                                                               (11, 'Первые шаги', '', 1, 1, 2000, NULL, NULL, 'ru', 'Gu54BKYX1ePQsp6m2MjGQdEY855iLZ7cXdYMS9bs', 0, 'first_steps'),
                                                                                                                                                               (12, '1. Как зарегистрироваться на сайте?', 'Как зарегистрироваться на сайте', 1, 1, 2000, NULL, NULL, 'ru', '7At7xVSvxN87Z159SFqVD3UxvmmRvBLC64xmAmqp', 11, 'cat_1_1'),
                                                                                                                                                               (13, '2. Зачем нужны документы, подтверждающие личность', '', 1, 2, 2000, NULL, NULL, 'ru', 'tcaMK5KsZssAo7ebPuEdUzOjVa3NGvJHelavf6dR', 11, 'cat_1_2'),
                                                                                                                                                               (14, '3. Как узнать мой Персональный Адрес?', '', 1, 3, 2000, NULL, NULL, 'ru', '0lJAKgSCVWSE4RKHvapqudELhFaVPQDl9LZlrmIX', 11, 'cat_1_3'),
                                                                                                                                                               (15, '4. Как отслеживать действия по Заказам и Посылкам?', '', 1, 4, 2000, NULL, NULL, 'ru', 'RYhf3vuBMR083hyBjuYfNsNU5Jb4WoAFjjExtQGb', 11, 'cat_1_4'),
                                                                                                                                                               (16, '5. К кому обращаться с вопросам?', '', 1, 5, 2000, NULL, NULL, 'ru', 'SUnANZE89KYtg8jLIwe9DKHMUWRmDSAdcIGigoiX', 11, 'cat_1_5'),
                                                                                                                                                               (17, 'Заказы', '', 1, 2, 2000, NULL, NULL, 'ru', 'pc803ByMqamsNo5hj7OT2lwX1boqUaQyQ4hVhWxM', 0, 'orders'),
                                                                                                                                                               (18, '1. На какой адрес следует оформлять свои покупки в интернет-магазинах?', '', 1, 1, 2000, NULL, NULL, 'ru', 'qtswHS3WjWSyWY1ymlo8p6AE1tQDSeoZbSXHtsoZ', 17, 'cat_2_1'),
                                                                                                                                                               (19, '2. Как правильно оформить новый Заказ в системе?', '', 1, 2, 2000, NULL, NULL, 'ru', 'LOzTbBcnJTDUFjf8XeZ7xtFKrOSvueCUnSAEJGiQ', 17, 'cat_2_2'),
                                                                                                                                                               (20, '3. Для чего нужна кнопка «Копировать»?', '', 1, 3, 2000, NULL, NULL, 'ru', 'C8PLnWoXJTE6PycIECeUX7VM0P4O7Nu7VzVcZrMp', 17, 'cat_2_3'),
                                                                                                                                                               (21, '4. Каковы ограничения при оформлении Заказов ?', '', 1, 4, 2000, NULL, NULL, 'ru', 'VlEPv0yzie8bwmV23hH2GLEiH5zdsxpqvZT9p6Ul', 17, 'cat_2_4'),
                                                                                                                                                               (22, '5. Могу ли я прислать на свой Персональный Адрес крупногабаритный заказ?', '', 1, 5, 2000, NULL, NULL, 'ru', 'AzzjQWfz8FRt6VxF6MHy68uPgaRAGqKWgQVthbRU', 17, 'cat_2_5'),
                                                                                                                                                               (23, '6. Что означают различные статусы Заказов?', '', 1, 6, 2000, NULL, NULL, 'ru', '5cZ8l8oXV3Um6Cd9MeRSeAtR1X8KKj9iNS4b2LtV', 17, 'cat_2_6'),
                                                                                                                                                               (24, '7. Как скоро после уведомления почтовой службы о доставке Заказа на склад я смогу увидеть его в системе?', '', 1, 7, 2000, NULL, NULL, 'ru', 'JZdC4TVVpUSc06ttJs7a5gXuGjINmokj53vuksPY', 17, 'cat_2_7'),
                                                                                                                                                               (25, '8. На каком этапе можно заказать фотографию Заказа или другие услуги, связанные с оформлением Заказа на склад?', '', 1, 8, 2000, NULL, NULL, 'ru', 'MTjJHCOknIo7k6gn8TDKEuaodynI2iOCZ0mvOCRG', 17, 'cat_2_8'),
                                                                                                                                                               (26, '9. Можно ли заказать отдельно услуги по проверке соответствия товаров в Заказе?', '', 1, 9, 2000, NULL, NULL, 'ru', 'ufi2Kc0yVAREZDzuql8M6q4gDLugsgW74W6JWtzF', 17, 'cat_2_9'),
                                                                                                                                                               (27, '10. Как долго можно хранить Заказ на складе?', '', 1, 10, 2000, NULL, NULL, 'ru', 'lKtRs1ilBMlOy62xbsJc5Pvl3tgaQwb4evnwhRRN', 17, 'cat_2_10'),
                                                                                                                                                               (30, 'Посреднические услуги', 'Вопросы по услугам посредника', 1, 3, 2000, NULL, NULL, 'ru', 'cHhejvbe6aIxuuMTOPO0GDoZsGXTPTnnpj76ecMT', 0, 'posrednik'),
                                                                                                                                                               (31, '1.Оказываете ли вы помощь в покупке товаров, если магазин не принимает мою карту?', '', 1, 1, 2000, NULL, NULL, 'ru', 'IrWbHopkZ6KvoSSmNKI98i1gCCD8n4n1SwkmO034', 30, 'cat3_1'),
                                                                                                                                                               (32, '2.Могу ли я собрать в одну Посылку Заказы оформленные самостоятельно и Заказы оформленные через посредника?', '', 1, 2, 2000, NULL, NULL, 'ru', 'oxkCTUiLKap9UbFenqqeypYzfUdy6Lhx344fxWRR', 30, 'cat3_2'),
                                                                                                                                                               (33, 'Посылки и консолидация', 'Услуги склада по консолидации и отправке', 1, 3, 2000, NULL, NULL, 'ru', 'C2G4eqy8fF9nt2x5Hh4ccjuEUzMbteM4H6NoSh4L', 30, 'conso'),
                                                                                                                                                               (34, '1.Как собрать Посылку из отдельных Заказов?', '', 1, 4, 2000, NULL, NULL, 'ru', 'sH0wXDuxqmtxgh10x8bkkV9jZEWSytDVOWdPKoKK', 30, 'cat4_1'),
                                                                                                                                                               (35, '2.Что означают различные статусы Посылок?', '', 1, 5, 2000, NULL, NULL, 'ru', 'qttWu9DECHNZiBAQiYDnJb9j2GD8s3jhKNMdX4sp', 30, 'cat4_2'),
                                                                                                                                                               (36, '3.На каком этапе можно начать консолидацию Заказов в Посылку?', '', 1, 6, 2000, NULL, NULL, 'ru', 'tpLove740eCMXdevUXDc6uaPvsheHowJdtGYkXfo', 30, 'cat4_3'),
                                                                                                                                                               (37, '4.До какого момента можно редактировать Посылку?', '', 1, 7, 2000, NULL, NULL, 'ru', 'T7DkXilut1rrnpWzHzixuz5bdyYnwAPbImUFpgvG', 30, 'cat4_4'),
                                                                                                                                                               (38, '5.Каковы стандартные размеры коробок, в которые упаковывается Посылка?', '', 1, 8, 2000, NULL, NULL, 'ru', 'lzDry9kiWKMIFr8LYG2LHk94njrvzJDGNfDyjKUF', 30, 'cat4_5'),
                                                                                                                                                               (39, '6.Существуют ли ограничения по размеру и весу Посылок?', '', 1, 9, 2000, NULL, NULL, 'ru', '08RQSDeZov37ub6n42dxr5NOTLUbMGr1MPeS1nt0', 30, 'cat4_6'),
                                                                                                                                                               (40, '7.Что происходит с Заказом не поместившимся в Посылку?', '', 1, 10, 2000, NULL, NULL, 'ru', 'PTMEgyrWTODligWhpNfUQdwWRu9ztH76bVZjOooD', 30, 'cat4_7'),
                                                                                                                                                               (41, '8.Можно ли «расконсолидировать» собранную Посылку?', '', 1, 11, 2000, NULL, NULL, 'ru', 'xsBZ3SP6iP829Mnrm5sKUd4rcRvrp63S3jzezoCS', 30, 'cat4_8'),
                                                                                                                                                               (42, '9.На какой основе рассчитывается оплата услуг и почтового отправления?', '', 1, 12, 2000, NULL, NULL, 'ru', 'IlJ0UalTKzmmEar4BLpyRnn4EJ4bAHCcqL4ZGYEY', 30, 'cat4_9'),
                                                                                                                                                               (43, '10.Что происходит после того, как Посылка сконсолидирована?', '', 1, 13, 2000, NULL, NULL, 'ru', 'YwvHL07aC8Nvlz0ycn1qOiYkczGwYmg2ql2lRQS5', 30, 'cat4_10'),
                                                                                                                                                               (44, '11. Как долго можно хранить Посылку на складе?', '', 1, 14, 2000, NULL, NULL, 'ru', 'q2ocxyxTNLe48t22TRg0j6Qg5s88lLbCXz6iUOS4', 30, 'cat4_11'),
                                                                                                                                                               (45, '12. Как быстро после оплаты услуг и почтового отправления Посылка покинет склад?', '', 1, 15, 2000, NULL, NULL, 'ru', '8eJQRmZYA2q2QDCrMEQSFdYhCeZhuO0W9hhY5Ms1', 30, 'cat4_12'),
                                                                                                                                                               (46, '13. Как отслеживать передвижение Посылки?', '', 1, 16, 2000, NULL, NULL, 'ru', '0JmHeuFH1FyrBKdOYkjOai82ZH9fufwnUCOsPiW9', 30, 'cat4_13'),
                                                                                                                                                               (47, 'Почта и Таможня', 'Почта и таможня ', 1, 4, 2000, NULL, NULL, 'ru', 'v8pWgZKPlrsVRMwrB3lInFZXXbXHzwnc1J2rRAni', 0, 'postal'),
                                                                                                                                                               (48, '1. Как оформить таможенную декларацию?', '', 1, 1, 2000, NULL, NULL, 'ru', '8LNp6tEaSJEseP4qj5V8zHhTlt9w9dLDZZgsx00I', 47, 'cat5_1'),
                                                                                                                                                               (49, '2. Какую цену указывать при оформлении таможенной декларации?', '', 1, 2, 2000, NULL, NULL, 'ru', 'pLOkzYUbC6Hb5ZCFa6riLWpKUKMETJwytkFdqcjS', 47, 'cat5_2'),
                                                                                                                                                               (50, '3. Каковы таможенные лимиты для моей страны?', '', 1, 3, 2000, NULL, NULL, 'ru', '6cm3OLbA3LqipcIBEY6wjOMeyWO4ih2myRqOnAa3', 47, 'cat5_3'),
                                                                                                                                                               (51, '4. Какие товары запрещены к пересылке?', '', 1, 4, 2000, NULL, NULL, 'ru', 'WGvGRdfQe5XPOy1G79nSAop8k266SKVXIBMPN5Oy', 47, 'cat5_4'),
                                                                                                                                                               (52, '5. Как приблизительно оценить стоимость почтовой доставки моей Посылки?', '', 1, 5, 2000, NULL, NULL, 'ru', 'GQuss5T0hAzumPcJYiF2WRbS272zma1RPTfop9Pe', 47, 'cat5_5'),
                                                                                                                                                               (53, '6. Чем отличается Экспресс доставка от Приоритетной?', '', 1, 6, 2000, NULL, NULL, 'ru', '3m55ySZCZal2t9CeyJmzfsNelIBHsCv4MXLAytfR', 47, 'cat5_6'),
                                                                                                                                                               (54, 'Страховка и претензии', 'Страхование посылок', 1, 5, 2000, NULL, NULL, 'ru', 'vWwEFUfJpFFvbDCcapUNzRZRlewkJXuEHcCX4km6', 0, 'insurance'),
                                                                                                                                                               (55, '1. На какую сумму я могу застраховать свою Посылку?', '', 1, 1, 2000, NULL, NULL, 'ru', 'XjzxMflE1Pt6Dox53lJjgldNM55vFu2XKUsJjVfL', 54, 'cat6_1'),
                                                                                                                                                               (56, '3. Что считается Страховым Случаем?', '', 1, 2, 2000, NULL, NULL, 'ru', 'PO3BMF3P7KSLZgcbDDcrUVtNj98r1IqCoviFL2tI', 54, 'cat6_3'),
                                                                                                                                                               (57, '4. Что не считается Страховым Случаем?', '', 1, 3, 2000, NULL, NULL, 'ru', 'Tue3kve1RbUGj0R8JBsTsSK6zArmBeswPqV2GcUL', 54, 'cat6_4'),
                                                                                                                                                               (58, '5. Как выплачивается страховка по Страховому Случаю', '', 1, 4, 2000, NULL, NULL, 'ru', 'jaFJyaiulVTbS1l19G2760hE0A0otf5eEXyFzhAD', 54, 'cat6_5'),
                                                                                                                                                               (59, 'Тарифы и оплата', 'тарифы на услуги', 1, 6, 2000, NULL, NULL, 'ru', 'SCfEd4Swt2CQfr9ikQJnnl1Src87clncnbFK63Fa', 0, 'prices'),
                                                                                                                                                               (60, '1. Цены на услуги', '', 1, 1, 2000, NULL, NULL, 'ru', 'P1BL284a5HpRgkKO0vfk7YHhMEKLoVCD7YZXItra', 59, 'cat7_1'),
                                                                                                                                                               (61, '2. Тарифы Почтовой Службы США', '', 1, 2, 2000, NULL, NULL, 'ru', 'Vr8hbmfT9eqVgvsYjD7bCW89LkRkDtfshJrFOCyk', 59, 'cat7_2'),
                                                                                                                                                               (62, '3. Способы оплаты', '', 1, 3, 2000, NULL, NULL, 'ru', 'q9RfANwBVQBNu55eVAEb1MODO6pJyTnZoO8nMbfy', 59, 'cat7_3'),
                                                                                                                                                               (63, 'Разное', 'Разное', 1, 7, 2000, NULL, NULL, 'ru', 'Y5wEboWD5cx9q596jWQx9YqYvKnhTQYaLmeVQEbi', 0, 'misc'),
                                                                                                                                                               (64, '1. График работы', '', 1, 1, 2000, NULL, NULL, 'ru', 'rNLpgEUxQYJa4EkYhVxiGbYWc1MMXPEhF9vKRq4T', 63, 'cat8_1'),
                                                                                                                                                               (65, '2. Реквизиты компании', '', 1, 2, 2000, NULL, NULL, 'ru', 'uW3BvWYOQ8xKZMvx467wr1zEpDkM7tGBzQy1kOzA', 63, 'cat8_2'),
                                                                                                                                                               (66, '2. Как рассчитывается сумма страховой премии?', '', 1, 3, 2000, NULL, NULL, 'ru', 'EcCwmTSCFfhs8FuZU7W5p8BAJvvmHbRyBIs2LsPm', 63, 'cat6_2'),
                                                                                                                                                               (112, '7.Чем отличается Боксберри от других видов отправлений?', '', 1, 4, 2000, NULL, NULL, 'ru', 'YNrEx6r5UtnPBClCKMupCdiuCx8D4KP4003UgyvN', 63, 'cat5_7'),
                                                                                                                                                               (113, '8. Какие документы необходимо готовить для доставки Боксберри?', '', 1, 5, 2000, NULL, NULL, 'ru', 'DmhzqQLjGQ8XFjf0KAqpNcMhuLqw4RsNKXk96yby', 63, 'cat5_8'),
                                                                                                                                                               (115, '3. Политика конфиденциальности', '', 1, 6, 2000, NULL, NULL, 'ru', 'LExjRQPN9otFh0n2WP485y8MJHoKaozNuG6lkEzl', 63, 'cat8_3'),
                                                                                                                                                               (398, '14. Услуга “Заполнение декларации Boxberry”', '', 1, 7, 2000, NULL, NULL, 'ru', 'LkDeNbXne7NaczcoAmWhrwVfENlvsO0qDmU5lO1V', 63, '14-%d1%83%d1%81%d0%bb%d1%83%d0%b3%d0%b0-%d0%b7%d0%b0%d0%bf%d0%be%d0%bb%d0%bd%d0%b5%d0%bd%d0%b8%d0%b5-%d0%b4%d0%b5%d0%ba%d0%bb%d0%b0%d1%80%d0%b0%d1%86%d0%b8%d0%b8-boxberry'),
                                                                                                                                                               (621, 'How do I order from this site', 'How do I ordered ?', 1, 0, 2000, NULL, NULL, 'en', '7At7xVSvxN87Z159SFqVD3UxvmmRvBLC64xmAmqp', 0, 'how-do-i-order-from-this-site'),
                                                                                                                                                               (622, 'Post category one', 'description', 1, 0, 1000, NULL, NULL, 'en', 'eKwingEi8J0PA3dkv1rCmVO67pzvEM3uYeED4HvF', NULL, 'post-category-one');

-- --------------------------------------------------------

--
-- Структура таблицы `faq_category`
--

CREATE TABLE `faq_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `faq_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `faq_category`
--

INSERT INTO `faq_category` (`id`, `faq_id`, `category_id`) VALUES
                                                                  (53, 138, 12),
                                                                  (54, 140, 13),
                                                                  (55, 152, 14),
                                                                  (56, 155, 15),
                                                                  (57, 159, 16),
                                                                  (58, 177, 18),
                                                                  (59, 179, 19),
                                                                  (60, 185, 20),
                                                                  (61, 187, 21),
                                                                  (62, 189, 22),
                                                                  (63, 198, 23),
                                                                  (64, 201, 24),
                                                                  (65, 204, 25),
                                                                  (66, 206, 26),
                                                                  (67, 209, 27),
                                                                  (68, 332, 31),
                                                                  (69, 338, 32),
                                                                  (70, 8251, 33),
                                                                  (71, 347, 34),
                                                                  (72, 351, 35),
                                                                  (73, 355, 36),
                                                                  (74, 359, 37),
                                                                  (75, 361, 38),
                                                                  (76, 363, 39),
                                                                  (77, 368, 40),
                                                                  (78, 377, 41),
                                                                  (79, 371, 42),
                                                                  (80, 375, 43),
                                                                  (81, 381, 44),
                                                                  (82, 383, 45),
                                                                  (83, 385, 46),
                                                                  (84, 3251, 47),
                                                                  (85, 387, 48),
                                                                  (86, 391, 49),
                                                                  (87, 393, 50),
                                                                  (88, 398, 51),
                                                                  (89, 405, 52),
                                                                  (90, 411, 53),
                                                                  (91, 3853, 54),
                                                                  (92, 3854, 54),
                                                                  (93, 3856, 54),
                                                                  (94, 3858, 54),
                                                                  (95, 418, 55),
                                                                  (96, 420, 56),
                                                                  (97, 422, 57),
                                                                  (98, 424, 58),
                                                                  (99, 4126, 59),
                                                                  (100, 426, 60),
                                                                  (101, 430, 61),
                                                                  (102, 440, 62),
                                                                  (103, 444, 64),
                                                                  (104, 447, 65),
                                                                  (105, 809, 66),
                                                                  (106, 3250, 112),
                                                                  (107, 3492, 115);

-- --------------------------------------------------------

--
-- Структура таблицы `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `sort` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `page_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `lang` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `title`, `link`, `status`, `sort`, `type`, `page_id`, `created_at`, `updated_at`, `lang`, `lang_hash`, `parent_id`, `slug`, `description`) VALUES
                                                                                                                                                                             (1, 'Forum', '/page/forum', 1, 0, 3000, 0, NULL, NULL, 'en', '7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmF', 0, NULL, NULL),
                                                                                                                                                                             (2, 'Форум', '/page/forum', 1, 0, 3000, 0, NULL, NULL, 'ru', 'uV0GllhMKQjm8mmDgFwAflKaBiw37fEe3m5xjMqZ_null', 0, NULL, NULL),
                                                                                                                                                                             (3, 'КАК ПОКУПАТЬ', '/page/kak-pokupat', 1, 0, 3000, 0, NULL, NULL, 'ru', '7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmq', 0, NULL, NULL),
                                                                                                                                                                             (4, 'HOW BUY', '/page/how-buy-page', 1, 0, 3000, 0, NULL, NULL, 'en', '7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmq', 0, NULL, NULL),
                                                                                                                                                                             (5, 'СТОИМОСТЬ', '/page/stoimost', 1, 0, 3000, 0, NULL, NULL, 'ru', '7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmD', 0, NULL, NULL),
                                                                                                                                                                             (6, 'PRICES', '/page/prices', 1, 0, 3000, 0, NULL, NULL, 'en', '7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmD', 0, NULL, NULL),
                                                                                                                                                                             (7, 'ДОСТАВКА', '/page/dostavka', 1, 0, 3000, 0, NULL, NULL, 'ru', '7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmM', 0, NULL, NULL),
                                                                                                                                                                             (8, 'DELIVERY', '/page/delivery-1', 1, 0, 3000, 0, NULL, NULL, 'en', '7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmM', 0, NULL, NULL),
                                                                                                                                                                             (9, 'FAQ', '/faq', 1, 0, 3000, 0, NULL, NULL, 'en', '7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmL', 0, NULL, NULL),
                                                                                                                                                                             (10, 'ВОПРОСЫ ОТВЕТЫ', '/faq', 1, 0, 3000, 0, NULL, NULL, 'ru', '7C445h7JaunGHdqs4tro4U3EArapZkl3VRWzSwmL', 0, NULL, NULL),
                                                                                                                                                                             (11, 'Home', '/page/home', 1, 0, 3000, 0, NULL, NULL, 'en', 'FXo4FBC4MLooz88MlXNONLWZTpBVzjz0xrWMA7Np', 0, NULL, NULL),
                                                                                                                                                                             (144, 'Главная', '/page/glavnaya', 1, 0, 3000, 0, NULL, NULL, 'ru', 'FXo4FBC4MLooz88MlXNONLWZTpBVzjz0xrWMA7Np', 0, NULL, NULL),
                                                                                                                                                                             (146, 'Как покупать', '/page/kak-pokupat', 1, NULL, 2000, 0, NULL, NULL, 'ru', 'kMmmBGTLbP3vjLowdBbwElIXcAeIXVX3dLaWkYh7', NULL, NULL, NULL),
                                                                                                                                                                             (147, 'Стоимость', '/page/stoimost', 1, NULL, 2000, 0, NULL, NULL, 'ru', 'Y8WsW9aNP27y6ktNPY0gdCzPyAQKYjXyX6TOsEqj', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_page`
--

CREATE TABLE `menu_page` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
                                                             (1, '0000_00_00_000000_create_settings_table', 1),
                                                             (2, '2014_10_12_000000_create_users_table', 1),
                                                             (3, '2014_10_12_100000_create_password_resets_table', 1),
                                                             (4, '2017_09_03_144628_create_permission_tables', 1),
                                                             (5, '2017_09_11_174816_create_social_accounts_table', 1),
                                                             (6, '2017_09_26_140332_create_cache_table', 1),
                                                             (7, '2017_09_26_140528_create_sessions_table', 1),
                                                             (8, '2017_09_26_140609_create_jobs_table', 1),
                                                             (9, '2018_04_08_033256_create_password_histories_table', 1),
                                                             (10, '2018_08_26_165523_create_categories_table', 1),
                                                             (11, '2018_08_31_154834_create_table_page', 1),
                                                             (12, '2018_08_31_174033_create_menu_table', 1),
                                                             (13, '2018_08_31_191616_create_page_category_table', 1),
                                                             (14, '2018_08_31_231857_create_faq_category_table', 1),
                                                             (15, '2018_09_02_181509_create_table_menu_page', 1),
                                                             (22, '2018_09_15_155651_create_table_widget', 2),
                                                             (23, '2018_09_15_160058_create_table_notification', 2),
                                                             (24, '2018_09_15_160253_create_table_pages_widget', 2),
                                                             (25, '2018_09_15_160409_create_table_notification_widget', 2),
                                                             (26, '2018_09_15_160453_create_table_notification_images', 2),
                                                             (27, '2018_09_15_160552_create_table_notification_children', 2),
                                                             (28, '2018_09_24_205043_create_widget_categories_table', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
                                                                           (1, 'App\\Models\\Auth\\User', 1),
                                                                           (2, 'App\\Models\\Auth\\User', 2),
                                                                           (3, 'App\\Models\\Auth\\User', 3),
                                                                           (4, 'App\\Models\\Auth\\User', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `notifications`
--

CREATE TABLE `notifications` (
  `notification_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `notifications`
--

INSERT INTO `notifications` (`notification_id`, `title`, `description`, `content`, `created_at`, `updated_at`) VALUES
                                                                                                                      (1, '', '', '<p>PayPal - золотой стандарт электронных транзакций, предлагающий непревзойденную простоту и безопасность, используя только ваш адрес электронной почты и пароль</p>', '2018-09-23 08:30:35', '2018-09-24 10:10:00'),
                                                                                                                      (2, 'asd', 'asd', '<p><img alt=\"\" src=\"http://devbox.vrstka.site//storage/photos/1/3adfd3f8b64801f4f6de1d3f5ea3152d--logo-design--computer-security.jpg\" style=\"width: 736px; height: 736px;\" />asdad</p>\r\n\r\n<p>sfsdfsdfsd</p>', '2018-09-23 12:12:35', '2018-09-23 12:18:39'),
                                                                                                                      (3, 'fdsf', 'dfsdfs', '<p><img alt=\"\" src=\"http://devbox.vrstka.site//storage/photos/shares/3303705-programming-wallpapers.png\" style=\"width: 1920px; height: 1200px;\" /></p>', '2018-09-23 12:24:55', '2018-09-23 12:25:53'),
                                                                                                                      (4, 'sdf', 'dsf', '<p>sdf</p>', '2018-09-23 12:27:38', '2018-09-23 12:27:38'),
                                                                                                                      (5, 'vvvvvvvv', 'vvvvvvvvvvvv', '<p>sdfsdf</p>', '2018-09-23 12:27:38', '2018-09-23 12:27:38'),
                                                                                                                      (6, '', '', '<p>Qiwi - электронный кошелек, который предлагает мгновенные платежи и удобные возможности пополнения счета через физические терминалы и телефонные приложения</p>', '2018-09-24 10:10:00', '2018-09-24 10:10:00'),
                                                                                                                      (7, '', '', '<p>PayPal - золотой стандарт электронных транзакций, предлагающий непревзойденную простоту и безопасность, используя только ваш адрес электронной почты и пароль</p>', '2018-09-24 10:10:00', '2018-09-24 10:10:00'),
                                                                                                                      (8, 'ОБРАБОТКА ПОДАЧИ ПАКЕТОВ', '', '<p>$ 0 для приема и обработки ваших пакетов</p>', '2018-09-24 10:15:05', '2018-09-24 10:15:05'),
                                                                                                                      (9, 'ФОТО ВСЕ ВКЛЮЧЕННЫХ ПУНКТОВ', '', '<p>$ 0 за фотографию ваших полученных товаров, счетов-фактур и ярлыков доставки</p>', '2018-09-24 10:15:05', '2018-09-24 10:15:05'),
                                                                                                                      (10, 'БЕЗОПАСНОЕ ХРАНЕНИЕ', '', '<p>Свободное хранение до 90 дней</p>', '2018-09-24 10:15:05', '2018-09-24 10:15:05'),
                                                                                                                      (11, 'КОНСОЛИДАЦИЯ PARCEL', '', '<p>Никакие сборы за объединение сохраненных предметов в одну партию</p>', '2018-09-24 10:15:05', '2018-09-24 10:15:05'),
                                                                                                                      (12, 'ДОПОЛНИТЕЛЬНЫЕ УСЛУГИ', '', '<p>$ 10 за запрос</p>', '2018-09-24 10:15:05', '2018-09-24 10:15:05'),
                                                                                                                      (13, 'ДОПОЛНИТЕЛЬНОЕ СТРАХОВАНИЕ', '', '<p>1,5% от заявленной стоимости посылки</p>', '2018-09-24 10:15:05', '2018-09-24 10:15:05'),
                                                                                                                      (14, 'ПОМОЩЬ ПО ПОКУПКЕ', '', '<pre data-fulltext=\"\" data-placeholder=\"Перевод\" dir=\"ltr\" id=\"tw-target-text\">\r\n5% от покупки</pre>', '2018-09-24 10:15:05', '2018-09-24 10:15:05'),
                                                                                                                      (15, 'ПОДПИСКА ОБСЛУЖИВАНИЯ', '', '<p>От $ 50 в месяц. Доступны 30,60 и 90 дней подписки</p>', '2018-09-24 10:15:05', '2018-09-24 10:15:05'),
                                                                                                                      (16, 'СПЕЦИАЛЬНОЕ ЦЕНЫ', '', '<p>Для доставки более 100 кг / месяц</p>', '2018-09-24 10:15:05', '2018-09-24 10:15:05'),
                                                                                                                      (17, 'Нет налога с продаж', '', '<p>Склады в штатах Делавэр и Нью-Джерси BoxForward находятся в штатах штата Делавэр и Нью-Джерси &laquo;без налога с продаж&raquo;, что позволяет нашим клиентам экономить до 10% на своих покупках, оптимизируя общую стоимость покупок и доставки из США</p>', '2018-09-24 12:09:03', '2018-09-24 12:09:03'),
                                                                                                                      (18, 'Быстрая и надежная доставка в вашу дверь', '', '<p>Основываясь на вашей родной стране, мы предлагаем различные варианты доставки, некоторые из которых являются эксклюзивными для клиентов BoxForward. - Почтовая служба США для глобального покрытия, доставляющая либо на ваш домашний адрес (экспресс), либо почтовый отдел (Priority) - Эксклюзивные услуги курьерской доставки Boxberry для наших клиентов в России - Другие варианты доставки через морской или наземный транспорт из Европы</p>', '2018-09-24 12:09:03', '2018-09-24 12:09:03'),
                                                                                                                      (19, 'Бесплатное и безопасное хранение', '', '<p>Мы будем получать ваши товары, фотографировать и размещать их на вашем счете и хранить ваши товары - абсолютно бесплатно на срок до 90 дней. Просто сообщите нам о вашей покупке, и мы уведомим вас, когда она будет получена и сохранена на нашем складе.</p>', '2018-09-24 12:09:03', '2018-09-24 12:09:03'),
                                                                                                                      (20, 'Отзывчивое обслуживание клиентов', '', '<p>Наши высококвалифицированные представители службы поддержки клиентов готовы ответить на любые вопросы и выполнить ваши запросы на обслуживание в течение максимум 24 часов с момента их получения в нашем почтовом ящике обслуживания клиентов: service@boxfwd.com</p>', '2018-09-24 12:09:03', '2018-09-24 12:09:03'),
                                                                                                                      (21, 'Дополнительные услуги', '', '<p>Иногда даже очень обширный список наших бесплатных услуг может не отвечать конкретной потребности, которую может иметь наш клиент. Таким образом, мы рады предложить дополнительные платные услуги, которые включают взаимодействие с онлайн-магазинами в отношении вашей покупки, возврата, дополнительных фотографий и тестирования товаров для покупок и т. Д.</p>', '2018-09-24 12:09:03', '2018-09-24 12:09:03'),
                                                                                                                      (22, 'Помощь при покупках', '', '<p>Наша партнерская компания Zone of Shopping может помочь вам найти нужные товары по самым выгодным ценам и поможет с покупкой в ​​выбранных онлайновых магазинах, которые не принимают иностранные кредитные карты. Вместе мы предлагаем полный цикл обслуживания, поддерживающий вас с момента первого контакта с нами, путем успешной доставки ваших товаров в ваш дом.</p>', '2018-09-24 12:09:03', '2018-09-24 12:09:03'),
                                                                                                                      (23, 'Нарзик Н.', 'Посылка №3232 Доставлено в Москву', '<p>Спасибо что предлагаете клиентам наиболее выгодные условия отправки (такие как BoxBerry). С учетом текущей экономической ситуации - дорогого стоит такое отношение. С удовольствием продолжу сотрудничать с вашим сервисом и обязательно воспользуюсь отправкой Boxberry и в дальнейшем.</p>', '2018-09-24 12:13:40', '2018-09-24 12:13:40'),
                                                                                                                      (24, 'Наталья Р.', 'Посылка №3264 Доставлено в Москву', '<p>Спасибо что предлагаете клиентам наиболее выгодные условия отправки (такие как BoxBerry). С учетом текущей экономической ситуации - дорогого стоит такое отношение. С удовольствием продолжу сотрудничать с вашим сервисом и обязательно воспользуюсь отправкой Boxberry и в дальнейшем.</p>', '2018-09-24 12:13:40', '2018-09-24 12:13:40'),
                                                                                                                      (25, 'Алекс Ц.', 'Посылка №4562 Доставлено в Москву', '<p>Спасибо что предлагаете клиентам наиболее выгодные условия отправки (такие как BoxBerry). С учетом текущей экономической ситуации - дорогого стоит такое отношение. С удовольствием продолжу сотрудничать с вашим сервисом и обязательно воспользуюсь отправкой Boxberry и в дальнейшем.</p>', '2018-09-24 12:13:40', '2018-09-24 12:13:40'),
                                                                                                                      (26, 'Обслуживание клиентов', '', '<p>Пожалуйста, не стесняйтесь обращаться к нам с любыми техническими или сервисными вопросами 24/7 по адресу: service@boxfwd.com</p>', '2018-09-24 12:16:12', '2018-09-24 12:16:12'),
                                                                                                                      (27, 'Вспомогательные покупки', '', '<p>Если вы хотите использовать наш сервисный сервис, пожалуйста, отправьте запрос, включая конкретные магазины и предметы, которые вы хотите приобрести, по адресу: orders@boxfwd.com</p>', '2018-09-24 12:16:12', '2018-09-24 12:16:12'),
                                                                                                                      (28, 'Информация о компании:', '', '<p>ООО &laquo;Торговое и судовое&raquo;<br />\r\n310 Braen Ave., Wyckoff, NJ 07481<br />\r\nТел: +1 (734) 846-1630</p>', '2018-09-24 12:16:12', '2018-09-24 12:16:12'),
                                                                                                                      (29, '', '', '<p>Веб-сайт BoxForward управляется Shop and Ship LLC, американским провайдером личных покупок, хранения и доставки услуг для международных клиентов.</p>\r\n\r\n<p>Shop and Ship LLC была основана в 2010 году и с тех пор постоянно растет, предлагая своим клиентам непревзойденный уровень персонализированных услуг и логистических решений.</p>\r\n\r\n<p>Мы всегда открыты для вас вопросами и отзывами, поэтому, пожалуйста, не стесняйтесь обращаться к нам с вашими вопросами и предложениями, заполнив нашу контактную форму на веб-сайте.</p>', '2018-09-24 12:17:03', '2018-09-24 12:17:03'),
                                                                                                                      (30, 'Прекрасное качество и дешевые цены!', 'Мария, 34 года, мама 2-х детей, Москва', '<p>&quot;Соотношение цены, качества и внешнего вида вещей в наших магазинах оставляет желает лучшего. Особенно это касается детских товаров. Либо покупаешь качественные и красивые вещи за космические суммы, либо что-то недолговечное, и очень часто не особо стильное и красивое, за тоже, в общем-то не малые деньги. По совету знакомой попробовала купить вещи в магазине Carter`s в США. Получив вещи, просто была в шоке &ndash; качественные, стильные вещи и стоимость очень дешевая. Теперь я регулярно делаю покупки в США. Children`s Place, Gymboree, Amazon, а потом еще и сайты закрытых распродаж Zulily, Ideeli - стали моими любимыми магазинами. И самое важное &ndash; в США постоянно какие-нибудь распродажи!&rdquo;</p>', '2018-09-24 12:33:29', '2018-09-24 12:33:29'),
                                                                                                                      (31, 'Регистрация', '', '<p>Для покупок в США вам понадобится местный адрес, на который американские магазины будут высылать товар.</p>\r\n\r\n<p>Желательно, что бы этот адрес был в штате, в котором не взымается налог с продаж. Компания BXFWD предоставляет вам такой адрес СОВЕРШЕННО БЕСПЛАТНО, в двух штатах - Делавер (полное отсутствие налогов) и Нью-джерси (отсутствие налогов на одежду и обувь).</p>\r\n\r\n<p>После регистрации мы предоставляем Вам почтовый адрес в США и доступ к личному кабинету на нашем сайте, в котором вы сможете отслеживать Ваши заказы, консолидировать посылки и оплачивать услуги.</p>', '2018-09-24 12:38:14', '2018-09-24 12:38:14'),
                                                                                                                      (32, 'Покупка товаров', '', '<p>Приступаем к покупкам! Для начала выберите интересующий Вас товар или магазин. В этом Вам поможет наш каталог товаров и список магазинов.</p>\r\n\r\n<p>Для наиболее популярных магазинов, мы составили подробные инструкции, которые шаг за шагом помогут Вам совершать покупку - от регистрации на сайте магазина до отправки товара на наш склад.</p>\r\n\r\n<p>Для покупок Вам также необходима банковская карта (кредитная или дебетовая) или счет в электронном кошельке PayPal. Неоторые магазины США не принимают к оплате карты, не выпущенные в США. В подобном случае, Вы всегда можете обратиться за помощью к нашему партнеру-посреднику, который поможет выкупить понравившийся Вам товар.</p>', '2018-09-24 12:38:14', '2018-09-24 12:38:14'),
                                                                                                                      (33, 'Хранение и консолидация', '', '<p>После оплаты заказа, магазин высылает товар на указанный Вами адрес склада. Мы совершенно бесплатно примем товар на хранение, сфотографируем его и с удовольствием подождем остальных Ваших заказов из других магазинов. Ведь консолидируя товары из разных магазинов в одну поссылку, Вы сможете существенно сэкономить на стоимости пересылки товара. Проверьте это на нашем калькуляторе доставки.</p>\r\n\r\n<p>Как только Вы будете готовы отправить закаы, Вы сможете отправить посылку на консолидацию, используя личный кабинет для выбора одного из множества возможных способов доставки и указания Ваших пожеланий по упаковке.</p>\r\n\r\n<p>Все сопроводительные документы Вы тоже сможете оформить в личном кабинете. Мы гарнтируем быстрое выполнение Вашей просьбы, качественную упаковку и подготовку посылки к дальнему путешествию. В случае необходимости, Вы всегда можете обратится в службу поддержки с вопросом о правильности оформления сопроводительных документов или за помощью в выборе лучшего способа отправки для Вашей посылки</p>', '2018-09-24 12:38:14', '2018-09-24 12:38:14'),
                                                                                                                      (34, 'Отправка и доставка', '', '<p>Приступаем к покупкам! Для начала выберите интересующий Вас товар или магазин. В этом Вам поможет наш каталог товаров и список магазинов.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Для наиболее популярных магазинов, мы составили подробные инструкции, которые шаг за шагом помогут Вам совершить покупку - от регистрации на сайте магазина до отправки товара на наш склад.</p>\r\n\r\n<p>Для покупок Вам также необходима банковская карта (кредитная или дебетовая) или счет в электронном кошельке PayPal. Неоторые магазины США не принимают к оплате карты, не выпущенные в США. В подобном случае, Вы всегда можете обратиться за помощью к нашему партнеру-посреднику, который поможет выкупить понравившийся Вам товар.</p>', '2018-09-24 12:38:14', '2018-09-24 12:38:14'),
                                                                                                                      (35, 'fdf', 'df', '<p>sdf</p>', '2018-09-25 18:51:43', '2018-09-25 18:51:43'),
                                                                                                                      (36, 'sd', 'sdf', '<p>dsf</p>', '2018-09-25 18:51:43', '2018-09-25 18:51:43'),
                                                                                                                      (37, 'fdg', 'fg', '<p>fg</p>', '2018-09-25 18:51:59', '2018-09-25 18:51:59'),
                                                                                                                      (38, 'content', 'При проверке текста на уникальность система онлайн-антиплагиата найдет в Интернете страницы, содержащие его полные или частичные копии. Содержимое этих страниц будет сравниваться с указанным текстом для выявления совпадений.', '<p>При&nbsp;<strong>проверке текста на уникальность</strong>&nbsp;система онлайн-антиплагиата найдет в Интернете страницы, содержащие его полные или частичные копии. Содержимое этих страниц будет сравниваться с указанным текстом для выявления совпадений.</p>\r\n\r\n<p>На основе найденных совпадений будет подсчитана общая уникальность текста в процентах, а также уникальность относительно каждой найденной страницы с совпадениями. Вы сможете посмотреть, какие части текста были найдены на каждой из проанализированных страниц.</p>\r\n\r\n<p>Если Вы знаете, что указанный текст размещён на каком-то сайте и не хотите учитывать его при подсчёте общей уникальности, введите адрес в поле &quot;Игнорировать сайт&quot;. Достаточно ввести домен.</p>', '2018-09-26 03:37:29', '2018-09-26 04:46:14'),
                                                                                                                      (39, 'register', 'При проверке текста на уникальность система онлайн-антиплагиата найдет в Интернете страницы, содержащие его полные или частичные копии. Содержимое этих страниц будет сравниваться с указанным текстом для выявления совпадений.', '<p>При&nbsp;<strong>проверке текста на уникальность</strong>&nbsp;система онлайн-антиплагиата найдет в Интернете страницы, содержащие его полные или частичные копии. Содержимое этих страниц будет сравниваться с указанным текстом для выявления совпадений.</p>\r\n\r\n<p>На основе найденных совпадений будет подсчитана общая уникальность текста в процентах, а также уникальность относительно каждой найденной страницы с совпадениями. Вы сможете посмотреть, какие части текста были найдены на каждой из проанализированных страниц.</p>\r\n\r\n<p>Если Вы знаете, что указанный текст размещён на каком-то сайте и не хотите учитывать его при подсчёте общей уникальности, введите адрес в поле &quot;Игнорировать сайт&quot;. Достаточно ввести домен.</p>', '2018-09-26 04:46:14', '2018-09-26 04:46:14'),
                                                                                                                      (40, 'При проверке', 'При проверке текста на уникальность система онлайн-антиплагиата найдет в Интернете страницы, содержащие его полные или частичные копии. Содержимое этих страниц будет сравниваться с указанным текстом для выявления совпадений.', '<p>При&nbsp;<strong>проверке текста на уникальность</strong>&nbsp;система онлайн-антиплагиата найдет в Интернете страницы, содержащие его полные или частичные копии. Содержимое этих страниц будет сравниваться с указанным текстом для выявления совпадений.</p>', '2018-09-26 04:46:14', '2018-09-26 04:46:14'),
                                                                                                                      (41, 'При проверке', 'При проверке', '<p>При&nbsp;<strong>проверке текста на уникальность</strong>&nbsp;система онлайн-антиплагиата найдет в Интернете страницы, содержащие его полные или частичные копии. Содержимое этих страниц будет сравниваться с указанным текстом для выявления совпадений.</p>', '2018-09-26 04:46:14', '2018-09-26 04:46:14');

-- --------------------------------------------------------

--
-- Структура таблицы `notification_children`
--

CREATE TABLE `notification_children` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL,
  `notification_id` int(10) UNSIGNED NOT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `notification_images`
--

CREATE TABLE `notification_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `notification_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `notification_images`
--

INSERT INTO `notification_images` (`id`, `notification_id`, `image`, `sort`) VALUES
                                                                                    (32, 4, 'http://devbox.vrstka.site//storage/photos/1/3068920-lamborghini-gallardo-wallpapers.jpg', 1),
                                                                                    (33, 5, 'http://devbox.vrstka.site//storage/photos/1/2017_novitec_mclaren_570gt_4k-1920x1080.jpg', 1),
                                                                                    (34, 5, 'http://devbox.vrstka.site//storage/photos/1/2018_novitec_mclaren_570s_spider_4k-1920x1080.jpg', 2),
                                                                                    (35, 5, 'http://devbox.vrstka.site//storage/photos/1/2017_bentley_exp_12_speed_6e_concept_4k-1920x1080.jpg', 3),
                                                                                    (36, 1, 'http://devbox.vrstka.site//storage/photos/1/mc.png', 1),
                                                                                    (37, 1, 'http://devbox.vrstka.site//storage/photos/1/visa.png', 2),
                                                                                    (38, 6, 'http://devbox.vrstka.site//storage/photos/shares/qiwi.png', 1),
                                                                                    (39, 7, 'http://devbox.vrstka.site//storage/photos/1/pay-pal.png', 1),
                                                                                    (40, 23, 'http://devbox.vrstka.site//storage/photos/1/icon.png', 1),
                                                                                    (41, 24, 'http://devbox.vrstka.site//storage/photos/1/icon.png', 1),
                                                                                    (42, 25, 'http://devbox.vrstka.site//storage/photos/1/icon.png', 1),
                                                                                    (43, 30, 'http://devbox.vrstka.site//storage/photos/1/img.jpg', 1),
                                                                                    (44, 38, 'http://devbox.vrstka.site//storage/photos/1/icon.png', 1),
                                                                                    (45, 39, 'undefined', 1),
                                                                                    (46, 40, 'undefined', 1),
                                                                                    (47, 41, 'undefined', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `template_id` int(11) DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `small_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_two` text COLLATE utf8mb4_unicode_ci,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `sort` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang_hash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `template_id`, `title`, `small_title`, `description`, `content`, `content_two`, `status`, `sort`, `type`, `image`, `slug`, `lang`, `lang_hash`, `created_at`, `updated_at`) VALUES
                                                                                                                                                                                                              (138, 1, '1. Как зарегистрироваться на сайте?', NULL, '', 'Для регистрации на сайте нажмите, пожалуйста, кнопку «Получить адрес в США», ознакомьтесь с условиями предоставления услуг и заполните необходимую информацию в форме регистрации. Обратите, пожалуйста, внимание – поля формы следует заполнять латинскими буквами.\r\n\r\nПосле заполнения необходимых данных и их проверки, на указанный адрес электронной почты Вы получите письмо с просьбой подтвердить регистрацию и пароль. Обращаем Ваше внимание, что в<span style=\"color: #282828;\">се документы проходят проверку подлинности, после которой администратор сайта принимает решение о подтверждении регистрации или отказе.</span>\r\n\r\nЕсли же, по какой-либо причине, потребуется изменить или дополнить данные указанные при регистрации, мы напишем Вам письмо с подробным указанием необходимых действий на указанный почтовый ящик.\r\n\r\nПредлагаем Вам также ознакомится с <a href=\"https://boxfwd.com/pages/blog/instruction/%D0%BA%D0%B0%D0%BA-%D0%BF%D0%BE%D0%BB%D1%83%D1%87%D0%B8%D1%82%D1%8C-%D0%BF%D0%B5%D1%80%D1%81%D0%BE%D0%BD%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9-%D0%B0%D0%B4%D1%80%D0%B5%D1%81/\">инструкцией</a> как пройти регистрацию на сайте.\r\n\r\n&nbsp;', '', 1, 1, 2, NULL, '1', 'ru', 'JMa4yVpGjO8Bv4degq9POf51zwOX1ubj2EjI3nT5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (140, 1, '2. Зачем нужны документы, подтверждающие личность?', NULL, '', 'Подобные документы позволяют нам понизить вероятность мошеннических операций с использованием адреса в США, тем самым повышая уровень доверия к сервису BOXFORWARD со стороны государственных и банковских служб. Высокий уровень доверия в свою очередь положительно отражается на объеме и типе предоставляемых нами услуг.\r\n\r\n&nbsp;', '', 1, 2, 2, NULL, '2', 'ru', 'RSxHSEQL3zZSHO6WR5QQL9i2LTjQom6bBNkCaVnK', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (152, 1, '3. Как узнать мой Персональный Адрес?', NULL, '', 'После успешной регистрации Персональный Адрес (включая присвоенный уникальный номер сьюта) Вы сможете увидеть в личном кабинете на нашем сайте на вкладке «Адреса».', '', 1, 3, 2, NULL, '3', 'ru', 'ToPRMIkdaKWaVv3d4Aaz2EveYglVgvnFHw0pnDjH', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (155, 1, '4. Как отслеживать действия по Заказам и Посылкам?', NULL, '', 'Действия по Заказам и Посылкам можно отслеживать несколькими способами:\r\n<ol>\r\n	<li>На вкладке «Последние события» в Вашем личном кабинете – на эту страницу выводятся основные данные и сообщения связанные с перемещением Заказов и Посылок. При помощи кнопок «Заказы на складе», «Ожидает упаковки», «Ожидают оплаты», «Ожидают отправки»  можно быстро увидеть список Заказов или Посылок в определенном статусе (например «Ожидает оплаты») и совершить необходимые действия. Также на этой странице показаны сообщения об изменении статусов Заказов и Посылок, которые вы можете удалить после прочтения.</li>\r\n	<li>Вкладки «Заказы» и «Посылки» – внутри этих закладок находится вся необходимая информация по Заказам и Посылкам. Вы можете сортировать и фильтровать списки по различным параметрам, а также найти интересующий вас Заказ или Посылку указав данные в окне поиска. Каждая строка отображает основную информацию по Заказу или Посылке и включает доступные функции, например заполнение декларации. Нажатие на иконку «Подробно» раскроет дополнительную информацию и возможные опции обработки, а также покажет основные события, связанные с данным Заказом или Посылкой.</li>\r\n	<li>Электронная почта – все уведомления об изменении статусов Заказов и Посылок по умолчанию приходят на указанный адрес электронной почты. Эту функциональность можно отключить во вкладке «Рассылка».</li>\r\n</ol>', '', 1, 4, 2, NULL, '4', 'ru', 'G48kuuqUUXTBTRasFq0TlYy5OE7VRcTOnc97RjEw', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (159, 1, '5. К кому обращаться с вопросам? ', NULL, '', 'По вопросам связанным с клиентским обслуживанием пожалуйста обращайтесь на <a href=\"mailto:service@boxfwd.com\">service@boxfwd.com</a>.\r\n\r\nТехнические вопросы и пожелания по функционалу сайта направляйте на <a href=\"mailto:techsupport@boxfwd.com%20\">techsupport@boxfwd.com</a>.', '', 1, 5, 2, NULL, '5', 'ru', 'xiSzAfloulj0Cy2NWZ9eTwUoHF5AmXeEXXOrYvRj', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (177, 1, '1. На какой адрес следует оформлять свои покупки в интернет-магазинах? ', NULL, '', 'Пожалуйста оформляйте покупки на адрес указанный во вкладке «Адресная книга», не забывая при этом указать уникальный номер сьюта. Телефон укажите свой, или же используйте наш автоответчик 732-893-7447.\r\n\r\n<i>Например:</i>\r\n<ul>\r\n	<li>First Name: Ivan</li>\r\n	<li>Last Name: Ivanov</li>\r\n	<li>Street Address (line 1): 310 Braen Ave,</li>\r\n	<li>Street Address (line 2): Suite 000XXXX</li>\r\n	<li>City: Wyckoff</li>\r\n	<li>State: New Jersey</li>\r\n	<li>Zip: 07481</li>\r\n	<li>Phone: 732-893-7447</li>\r\n</ul>', '', 1, 6, 2, NULL, '6', 'ru', 'VzScCkiVzzEAaPbz3T8WtlqAndUKmZY3l1z5mHC8', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (179, 1, '2. Как правильно оформить новый Заказ в системе?', NULL, '', 'Для того чтобы сообщить складу о новом заказе, перейдите в закладку «Заказы» и нажмите на кнопку «Добавить заказ». Всю необходимая информация для внесения заказа на сайт Вы можете взять из е-мейла с order confirmation (подтверждения заказа) и/или с shipping confirmation (подтверждения отправки заказа), которое Вам присылает магазин.\r\n\r\nВ открывшейся форме обязательно укажите название магазина и номер заказа, также Вы можете выбрать необходимые опции обработки заказа и добавить номер отслеживания, если он уже известен. В поле заметки внесите любую информацию, которая может оказаться полезной или которую вы заинтересованы сохранить по данному заказу. Нажмите кнопку «Сохранить».\r\n\r\nЗаказ всегда можно редактировать и добавлять новую информацию. Для этого нажмите на кнопку «Редактировать» в строке заказа и введите информацию в соответствующем поле. Не забудьте нажать «Сохранить», иначе изменения будут утеряны.\r\n\r\nДля начала заполнения таможенной декларации нажмите на иконку «+» или цифру обозначающую кол-во уже занесенных предметов, находящуюся в строке под заголовком «Содержимое».\r\n\r\nРекомендуем Вам ознакомится <a href=\"https://boxfwd.com/pages/blog/instruction/%D0%BA%D0%B0%D0%BA-%D0%B2%D0%BD%D0%B5%D1%81%D1%82%D0%B8-%D0%B7%D0%B0%D0%BA%D0%B0%D0%B7-%D0%BD%D0%B0-%D1%81%D0%B0%D0%B9%D1%82/\">с инструкцией по внесению нового заказа на сайт</a>.', '', 1, 7, 2, NULL, '7', 'ru', 'ZPTrTrwNayNyZEt42TwTLTrvRNgznPVfqaSufLwQ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (185, 1, '3. Для чего нужна кнопка «Копировать»?', NULL, '', 'Часто интернет-магазины высылают товары несколькими отдельными отправлениями. При этом номер заказа не изменяется, но отправлениям присваиваются разные номера отслеживания. Используя кнопку «Копировать», вы можете создать дубликат существующего заказа, но указать при этом другой номер отслеживания.\r\n\r\nПравильные номера отслеживания позволяют нам легко определить принадлежность товаров поступающих на наш склад и ускорить процесс их оформления.', '', 1, 8, 2, NULL, '8', 'ru', 'pjyM3UOVP6xOHxv4u9fDXbugiBOnZ2h6CI68rDWl', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (187, 1, '4. Каковы ограничения при оформлении Заказов ?', NULL, '', 'В соответствии с опубликованными условиями, мы не принимаем к хранению на склад товары требующие специальной лицензии на хранение, контрафактные товары, товары приобретенные мошенническим путем, товары, запрещенные к вывозу из США, а также товары, запрещенные к пересылке или ввозу в страну-адресат (см. раздел Почта и Таможня).', '', 1, 9, 2, NULL, '18', 'ru', 'dgt1CgPx6C238nKQYfSN7r5YdNtakR4i6wP6nhXj', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (189, 1, '5. Могу ли я прислать на свой Персональный Адрес крупногабаритный заказ?', NULL, '', 'Мы принимаем крупногабаритные заказы по предварительному согласованию со службой поддержки <a href=\"mailto:service@boxfwd.com\">service@boxfwd.com</a>. Рекомендуем Вам первоначально просчитать стоимость доставки выбранного крупногабаритного заказа, обратившись в нашу службу поддержки, а затем уже заказывать товар.\r\n\r\nКрупногабаритные заказы, присланные без предварительного согласования, на склад приняты не будут.\r\n\r\n<span style=\"color: #000000;\">Для расчетов нужно использовать только </span><b style=\"color: #000000;\">транспортные вес и размеры</b><span style=\"color: #000000;\">! Если они не указаны в описании товара, необходимо будет отправить соответствующий запрос продавцу. </span>', '', 1, 10, 2, NULL, '19', 'ru', 'zkhwYzl7ppsEwcgXh22MuvL1ICm8SJwSuRg1Pphq', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (198, 1, '6. Что означают различные статусы Заказов?', NULL, '', '<ul>\r\n	<li>В пути – Заказ был оформлен в магазине и занесен в систему</li>\r\n	<li>На складе – Заказ прибыл на склад и оформлен к хранению</li>\r\n	<li>Ожидается консолидация – было передано указание на консолидацию Заказа в посылку</li>\r\n	<li>Ожидает отправки – Заказ упакован в Посылку и ожидает отправки. Для более подробной информации обратите внимание на статус Посылки, в которую был упакован Заказ</li>\r\n	<li>Отправлено – Посылка с Заказом покинула склад</li>\r\n	<li>Архив - заказ перенесен вместе с посылкой, в которую он входит в архив и не отображается в эккаунте. При необходимости, посылку можно вернуть из архива и заказ снова будет виден ан вкладе \"Заказы\".</li>\r\n</ul>', '', 1, 11, 2, NULL, '20', 'ru', '1W7GeS0BtCgB0xbmg3HxcX4BB7w5J20HiHPD3bS7', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (201, 1, '7. Как скоро после уведомления почтовой службы о доставке Заказа на склад я смогу увидеть его в системе?', NULL, '', 'Заказы, поступившие на основной склад в Нью Джерси, Вы сможете увидеть в своем эккаунте максимум в течение 24 часов с момента поступления на склад, за исключением выходных и праздничных дней.\r\n\r\nЗаказы, поступившие на склад в Делавере, <span style=\"color: #222222;\">перевозятся на основной склад в Нью Джерси в настоящее время 1-2 раза в неделю. После чего они обрабатываются по желанию клиентов и отображаются в аккаунте.</span>\r\nЕсли Заказ не изменил свой статус в течение 24 часов с момента уведомления о доставке, напишите нам на <a href=\"mailto:service@boxfwd.com\">service@boxfwd.com</a> с пометкой 24 hours и мы свяжемся для уточнения деталей.', '', 1, 12, 2, NULL, '21', 'ru', '8OYOa9qSvItRjW1CKLmc1jkp5NF3lT406mcPloTj', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (204, 1, '8. На каком этапе можно заказать фотографию Заказа или другие услуги, связанные с оформлением Заказа на склад? ', NULL, '', 'Данные услуги бесплатно можно заказать и редактировать до поступления Заказа на склад. После изменения статуса Заказа на «На складе» или «Ожидает консолидации», бесплатные услуги связанные с обработкой Заказов недоступны.', '', 1, 13, 2, NULL, '22', 'ru', 'V776KaYphq8ebZMWC9JpLKwWJtbVkuFf0noIoMJO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (206, 1, '9. Можно ли заказать отдельно услуги по проверке соответствия товаров в Заказе?', NULL, '', 'Вы можете заказать услугу по проверке  заказа во вкладке «Платные услуги». Стоимость услуги составляет $10, эта сумма должна быть на вашем счету на момент заказа.', '', 1, 14, 2, NULL, '23', 'ru', 'ZSWGSTdCIqzkfF4ktnYosd5Frf81EYL5aSIdZ80B', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (209, 1, '10. Как долго можно хранить Заказ на складе?', NULL, '', 'Заказы, оформленные на склад хранятся бесплатно сроком до 90 дней. После истечения данного срока, мы оставляем за собой право взимать дополнительную оплату за хранение Заказа или передать Заказ в благотворительную организацию.', '', 1, 15, 2, NULL, '24', 'ru', 'lIRrg8sWMDT2Xb5wj7QTcRrH1AVg3vMhb3ER9SGR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (332, 1, '1. Оказываете ли вы помощь в покупке товаров, если магазин не принимает мою карту?', NULL, '', 'Партнером сервиса BOXFORWARD является компания <a href=\"http://zoneofshopping.com/\">Zone Of Shopping</a>, уже много лет работающая на рынке посреднических услуг.\r\n\r\nЕсли у Вас возникла потребность в подобных услугах, пожалуйста, напишите нам на <a href=\"mailto:orders@nycmsk.com\">orders@</a><a href=\"mailto:service@boxfwd.com\">boxfwd.com</a> и с Вами свяжутся представители нашего партнера.', '', 1, 16, 2, NULL, '25', 'ru', 'UpiyHLuA4a1EzsJClSTvlkygTW1N7xs2Lk2Rxwgx', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (338, 1, '2. Могу ли я собрать в одну Посылку Заказы оформленные самостоятельно и Заказы оформленные через посредника?', NULL, '', 'Да, конечно. При этом зона ответственности BOXFORWARD ограничена услугами по принятию на склад, консолидации и почтового отправления. Дополнительные услуги и просьбы обговариваются напрямую с посредником.', '', 1, 17, 2, NULL, '10', 'ru', 'bm1siUejgrhrBCKUaq7VALUbgoWgRIbh0OVgCGyO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (347, 1, '1. Как собрать Посылку из отдельных Заказов?', NULL, '', 'Для того, чтобы собрать Посылку из отдельных Заказов, нажмите кнопку «Создать посылку» в закладке «Заказы» либо в закладке «Посылки». В открывшейся форме внесите название новой посылки, сумму на которую вы хотите застраховать вложение и предпочтительный способ доставки.  Затем выберите необходимые опции упаковки и отметьте доступные к консолидации Заказы, которые вы бы хотели сложить в эту Посылку.\r\n\r\nЗаполните адрес, на который Посылка должна быть доставлена, или выберете его из списка частых адресов занесенных во вкладке «Адресная книга». Нажмите «Сохранить» для сохранения данных.\r\n\r\nВы можете редактировать  (добавлять/убирать) Заказы, находящиеся в Посылке, а также изменять адрес и другие параметры нажав на иконку «Подробно» в строке Посылки.\r\n\r\nДля начала заполнения таможенной декларации нажмите на иконку «+» или цифру обозначающую количество уже занесенных предметов, находящуюся в строке под заголовком «Декларация».', '', 1, 18, 2, NULL, '9', 'ru', 'O1DwhYlx9g5AySeCNCOTJsqIrWIffZyxKPyxqVaS', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (351, 1, '2. Что означают различные статусы Посылок?', NULL, '', '<ul>\r\n	<li>Черновик - Вы имеете возможность создать черновик посылки, который можете в последствии редактировать - и заказы, которые в нее входят и информацию о получателе. После отправку на консолидацию редактирование информации в посылке невозможно.</li>\r\n	<li>Ожидает заказов – некоторые/все  Заказы, указанные в Посылке еще не поступили на склад.</li>\r\n	<li>Ожидает упаковки – все Заказы, указанные в Посылке, находятся на складе и задание на упаковку передано оператору.</li>\r\n	<li>Ожидает проверки - статус только для посылок Боксберри. Означает, что служба поддержки проверяет соответствие внесенных данных с приложенными документами, а именно указанное ФИО получателя - с паспортом; цены, вещи и их количество вещей, указанные в декларации - с приложенными инвойсами.</li>\r\n	<li>Собрана, готова к оплате – упаковка Посылки завершена и выставлен счет на оплату услуг склада и почтового отправления.</li>\r\n	<li>Проверена, готова к оплате - документы к Вашей посылке Боксберри проверены и ошибок не найдено, выставлен счет на оплату услуг склада и почтового отправления.</li>\r\n	<li>Частично упакована – посылка упакована, но не все заказы поместились в посылку по габаритам или по весу. Не поместившиеся заказы вернулись на склад. Требуется отредактировать декларацию.</li>\r\n	<li>Оплачена, ожидает отправки – услуги и почтовое отправление оплачены, Посылку готовят к отправке.</li>\r\n	<li>Ожидает транспортировки - Посылка подготовлена к отправке и ожидает передачи курьеру.</li>\r\n	<li>Передано для отправки - Посылка передана курьеру выбранной службы отправления.</li>\r\n	<li>Отправлено – Посылка передана почтовой/курьерской компании для отправки адресату</li>\r\n	<li>Архив - посылка и все входящие в нее заказы переносят в архив для очистки места на вкладках Вашего эккаунта - посылки и заказы не будут отображаться на вкладках. При необходимости посылку можно вынуть из архива и она снова отобразится на вкладке.</li>\r\n</ul>', '', 1, 19, 2, NULL, '11', 'ru', 'LhSjFckYvLhtCOCOCVReCTasNxqUQ3Y6ZJNeu2hG', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (355, 1, '3. На каком этапе можно начать консолидацию Заказов в Посылку? ', NULL, '', 'На любом. Как только Заказ оформлен в системе, он становится доступным для включения в Посылку.', '', 1, 20, 2, NULL, '12', 'ru', 'ElbBAco4oPLNSJRj7xN5DDWBAMwgqyiEpV53pHva', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (359, 1, '4. До какого момента можно редактировать Посылку?', NULL, '', 'Добавление Заказов и редактирование информации о получатели возможно до отправки Посылки на консолидацию (пока Посылка имеет статус \"Черновик\").\r\n\r\nУдаление Заказов, изменение адреса получателя, суммы страховки, опций упаковки и способа доставки возможны при обращение в службу поддержки при условии, что посылку еще не начали упаковывать.', '', 1, 21, 2, NULL, '13', 'ru', 'FWYUYlfRjpkeWyRoXA6DYr9tU0dXFgWrycmWFYtO', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (361, 1, '5. Каковы стандартные размеры и вес коробок, в которые упаковывается Посылка?', NULL, '', 'Размеры и вес коробок, которые используются для упаковки посылок:\r\n\r\n<span style=\"color: #000000;\">1. Коробка для Приорити весит:</span><br style=\"color: #000000;\" /><span style=\"color: #000000;\">- стандартная 18х18х12\'\' весит чуть меньше 1.8 lbs</span><br style=\"color: #000000;\" /><br style=\"color: #000000;\" /><span style=\"color: #000000;\">2. Максимальная коробка для ЕМС весит:</span><br style=\"color: #000000;\" /><span style=\"color: #000000;\">- стандартная 24х18Х24\'\' весит чуть меньше 2 lbs - это коробки-трансформеры, их можно уменьшить по швам, чтобы не оставалось пустого места</span><br style=\"color: #000000;\" /><span style=\"color: #000000;\">- из уплотненного картона 24х18Х24\'\' весит около 4 lbs - эти коробки не трасформируются</span>\r\n\r\nПри этом, если Посылку можно упаковать в меньшую по размеру коробку  без ущерба для качества упаковки, мы постараемся это сделать используя имеющиеся в наличии материалы.', '', 1, 22, 2, NULL, '14', 'ru', '7E9rmpaiD5V3gUkZPoz66VdSsizNocVHfdMqbrPd', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (363, 1, '6. Существуют ли ограничения по размеру и весу Посылок?', NULL, '', 'Почтовая Служба США установила следующие ограничения на размер и вес отправляемых посылок :\r\n<table border=\"1\" cellspacing=\"1\" cellpadding=\"1\">\r\n<tbody>\r\n<tr>\r\n<td valign=\"top\" width=\"195\"></td>\r\n<td valign=\"top\" width=\"198\">Экспресс</td>\r\n<td valign=\"top\" width=\"198\">Приоритет</td>\r\n</tr>\r\n<tr>\r\n<td valign=\"top\" width=\"195\">Длина</td>\r\n<td valign=\"top\" width=\"198\">не более 60\'\'/152 см</td>\r\n<td valign=\"top\" width=\"198\">не более 42\'\'/107 см</td>\r\n</tr>\r\n<tr>\r\n<td valign=\"top\" width=\"195\">Длина и обхват</td>\r\n<td valign=\"top\" width=\"198\">не более 108\'\'/ 274 см</td>\r\n<td valign=\"top\" width=\"198\">не более 79\'\' / 200 см</td>\r\n</tr>\r\n<tr>\r\n<td valign=\"top\" width=\"195\">Вес</td>\r\n<td valign=\"top\" width=\"198\">70 фунтов/32 кг</td>\r\n<td valign=\"top\" width=\"198\">44 фунта/20 кг</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', 1, 23, 2, NULL, '15', 'ru', 'CZusYFkcU0OjFHeqt5dZou3NRDF2IweYL4Ju4PGA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (368, 1, '7. Что происходит с Заказом не поместившимся в Посылку?', NULL, '', 'Заказ не поместившийся в Посылку при упаковке будет возвращен на склад и его статус изменится на « На складе». На данном этапе мы не предоставляем возможность автоматически приоритизировать Заказы в Посылке, однако мы постараемся учесть просьбы направленные в службу клиентской поддержки <a href=\"mailto:service@boxfwd.com\">service@boxfwd.com</a>.\r\n\r\nОбращаем Ваше внимание, что для посылок <b style=\"color: #000000;\">USPS - Priority п</b><span style=\"color: #000000;\">ри создании посылки Вы можете отметить нужную функцию, которую склад должен выполнить, если заказы не поместятся в коробку для Priority (переупаковать и отправить Express / Авиа-эконом или вернуть на склад):</span>\r\n\r\n<a href=\"https://boxfwd.com/pages/wp-content/uploads/2012/02/upakovka.jpg\" target=\"_blank\"><img class=\"aligncenter wp-image-3669 size-full\" src=\"https://boxfwd.com/pages/wp-content/uploads/2012/02/upakovka.jpg\" alt=\"upakovka\" width=\"1059\" height=\"567\" /></a><br style=\"color: #000000;\" />', '', 1, 24, 2, NULL, '16', 'ru', 'cyOcREHDUUOTxazPs69J01mtdHkc08Dcztgtsaxa', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (371, 1, '9. На какой основе рассчитывается оплата услуг и почтового отправления?', NULL, '', 'Оплата услуг рассчитывается на основе тарифов указанных в разделе «Услуги и оплата».\r\n\r\nОплата отправлений рассчитывается на основе розничных цен Почтовой Службы США и курьерских служб, исходя из веса Посылки и выбранного способа отправки.', '', 1, 25, 2, NULL, '17', 'ru', 'vnlhqWW9Nez6qBfgHBfs1dnNTJdpsaSgw9WubL5t', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (375, 1, '10. Что происходит после того, как Посылка сконсолидирована?', NULL, '', 'После того, как Посылка собрана, Вам <span style=\"color: #000000;\">придет уведомление о том, что посылка собрана, и в личном кабинете на вкладке \"Посылки\" в графе собранной посылки отобразится сумма к оплате</span>, а готовая Посылка возвращена на склад. После оплаты по счету, Посылка будет отправлена получателю.', '', 1, 26, 2, NULL, '26', 'ru', 'Av28GwRj6qgID1wDRU4aeG95Y71ReQ59bSi73Mqm', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (377, 1, '8. Можно ли «расконсолидировать» собранную Посылку?', NULL, '', 'Вы можете запросить данную услугу, обратившись в нашу службу поддержки. Стоимость данной услуги составляет $20. В результате выполнения услуги, заказы из посылки будут приняты на склад, как один или несколько заказов, без разделения по первоначальным заказам. К каждому новому заказу будут загружены фотографии содержимого.', '', 1, 27, 2, NULL, '27', 'ru', 'fSg8PfnwCo1oi9jTn1cADsfuFM1mzfkIWcMRuF0B', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (381, 1, '11. Как долго можно хранить Посылку на складе?', NULL, '', 'Консолидированная Посылка хранится бесплатно сроком до 15 дней. После истечения данного срока, мы оставляем за собой право взимать дополнительную оплату за хранение Посылки или передать Посылку в благотворительную организацию.', '', 1, 28, 2, NULL, '28', 'ru', 'h0DmVRvjlF0MnJDxX0HZdSJdxjghlhsAYYp11jyM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (383, 1, '12. Как быстро после оплаты услуг и почтового отправления Посылка покинет склад?', NULL, '', 'Посылки, отправляемые службой USPS, передаются курьеру почтовой службы утром каждый рабочий день (с понедельника по пятницу).\r\n\r\nПосылки, отправляемые службой СПСР, передаются курьерской службе утром каждый четверг.\r\n\r\nПосылки, отправляеемые службой Боксберри, передаются для отправки каждый вторник и четверг.', '', 1, 29, 2, NULL, '29', 'ru', '9wSMODrz1bZNn34CiyWrQEJ4amEcsHMJVwHeEtxy', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (385, 1, '13. Как отслеживать передвижение Посылки?', NULL, '', 'Мы предоставляем номер отслеживания для каждой отправленной Посылки. Этот номер виден в строке Посылки и перейдя по нему и воспользовавшись сервисами отслеживания вы сможете определить местонахождение Вашей Посылки.\r\n\r\nПосылки службы USPS можно отследить на сайте - https://www.usps.com/.\r\n\r\nПосылки Боксберри можно отследить на сайте - http://boxberry.ru/tracking/.', '', 1, 30, 2, NULL, '30', 'ru', '9Xe5u0ZVCWqiDrRdetnCppZwQXHJ9tQm1cR2zXgp', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (387, 1, '1. Как оформить таможенную декларацию?', NULL, '', 'Оформить таможенную декларацию можно двумя способами:\r\n\r\nОформление во вкладке «Заказы» – нажмите на иконку «+» в строке Заказа и внесите товары, которые принадлежат данному Заказу, указывая название вещи, количество и стоимость. Данную декларацию можно редактировать, нажав на цифру находящуюся под заголовком «Декларация» в строке Заказа.\r\nПри консолидации Заказов в Посылку, декларации Заказов будут объединены в декларацию Посылки, доступную для дальнейшего редактирования. Е<span style=\"color: #000000;\">сли при консолидации заказ не поместится в посылку и вернется на склад, то его декларация \"вернется\" вместе с ним.</span>\r\n\r\nОформление во вкладке «Посылки» – нажмите на иконку «+» в строке Посылки и внесите товары, которые будут отправлены в данной Посылке, указывая принадлежность к Заказу (опционально), название вещи, количество и стоимость. Данную декларацию можно редактировать, нажав на цифру находящуюся под заголовком «Декларация» в строке Посылки.\r\n\r\nОбратите, пожалуйста, внимание, что наименования вещей должны заноситься латинскими буквами.', '', 1, 31, 2, NULL, '31', 'ru', '3qqi6XdQUmVwYckayxsLRuOI1PGMDXAeJYOtCv0s', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (391, 1, '2. Какую цену указывать при оформлении таможенной декларации?', NULL, '', 'В таможенной декларации указывается общая стоимость вложения согласно инвойсов или выписки из банка, с учетом скидки и без учета почтовых и таможенных расходов.\r\n\r\nДля отправления авиа-экспресс таможенная декларация должна включать налог и стоимость доставки (их можно добавить к одной из позиций; если есть общая скидка на заказ, ее можно отнять от стоимости одной из позиций).', '', 1, 32, 2, NULL, '32', 'ru', '1jkpMaDi9DDnsodkSBxWV17Tes70gV2YrsV2a7mv', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (393, 1, '3. Каковы таможенные лимиты для моей страны?', NULL, '', 'Лимиты беспошлинного ввоза <b>для России — 1000 евро </b>в месяц на адрес одного получателя; <b>для Украины — 150 евро</b> на одного получателя в сутки.\r\n\r\nРазмер таможенного лимиты для своей страны можно посмотреть на сайте таможенного органа своей страны.', '', 1, 33, 2, NULL, '33', 'ru', 'NH7vFYJSTJSdXvK6fPoFgH2m65CjvlQ74spAewcD', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (398, 1, '4. Какие товары запрещены к  пересылке?', NULL, '', 'Список запрещенных товаров включает, но не ограничен следующими товарами:\r\n<ul>\r\n	<li>Легковоспламеняющиеся и взрывоопасные предметы</li>\r\n	<li>Химикаты и ртуть</li>\r\n	<li>Аэрозоли</li>\r\n	<li>Батареи и аккумуляторы</li>\r\n	<li>Валюта</li>\r\n	<li>Огнестрельное оружие и боеприпасы, а также их имитация</li>\r\n	<li>Наркотические вещества</li>\r\n	<li>Радиоактивные материалы</li>\r\n	<li>Удобрения, растения и семена растений</li>\r\n	<li>Животные и продукты</li>\r\n	<li>Лекарственные препараты</li>\r\n	<li>Алкоголь</li>\r\n	<li>Видеопродукция и печатные издания порнографического содержания</li>\r\n	<li>Драгоценные камни и материалы</li>\r\n	<li>Культурные ценности</li>\r\n	<li>Радиопередатчики и радиоприемники определенных частот</li>\r\n</ul>\r\nБолее подробная информация по ограничениям для ввоза в РФ находится <a href=\"http://www.russianpost.ru/rp/servise/ru/home/postuslug/goodslist\">здесь.</a>\r\n\r\nЕсли Вы отправляете в другие страны, пожалуйста, ознакомьтесь с соответствующими таможенными правилами до заказа товаров из магазина.', '', 1, 34, 2, NULL, '34', 'ru', 'tyF5Ab2FXM5CYozP63UFFrdT6HMzgGXISOc5mqXR', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (405, 1, '5. Как приблизительно оценить стоимость доставки моей Посылки?', NULL, '', 'Для оценки приблизительной стоимости отправки, воспользуйтесь нашим <span style=\"color: #810c2c;\"><a title=\"Калькулятор\" href=\"https://boxfwd.com/pages/#calculator\"><span style=\"color: #810c2c;\">Калькулятором.</span></a></span>', '', 1, 35, 2, NULL, '35', 'ru', 'ejQ96ryKYOOObuG5EykY0zSb729y4f8odnT9A46O', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (411, 1, '6. Чем отличается Экспресс доставка от Приоритетной?', NULL, '', 'Сроками доставки, максимально разрешенным весом и габаритaми\r\n<table border=\"1\" cellspacing=\"1\" cellpadding=\"1\">\r\n<tbody>\r\n<tr>\r\n<td valign=\"top\" width=\"195\"></td>\r\n<td valign=\"top\" width=\"198\">Экспресс</td>\r\n<td valign=\"top\" width=\"198\">Приоритет</td>\r\n</tr>\r\n<tr>\r\n<td valign=\"top\" width=\"195\">Сроки доставки</td>\r\n<td valign=\"top\" width=\"198\"> 1-2 недели</td>\r\n<td valign=\"top\" width=\"198\"> 2 – 4 недели</td>\r\n</tr>\r\n<tr>\r\n<td valign=\"top\" width=\"195\">Длина</td>\r\n<td valign=\"top\" width=\"198\">не более 60\'\'/152 см</td>\r\n<td valign=\"top\" width=\"198\">не более 42\'\'/107 см</td>\r\n</tr>\r\n<tr>\r\n<td valign=\"top\" width=\"195\">Длина и обхват</td>\r\n<td valign=\"top\" width=\"198\">не более 108\'\'/ 274 см</td>\r\n<td valign=\"top\" width=\"198\">не более 79\'\'/ 200 см</td>\r\n</tr>\r\n<tr>\r\n<td valign=\"top\" width=\"195\">Вес</td>\r\n<td valign=\"top\" width=\"198\">70 фунтов/32 кг</td>\r\n<td valign=\"top\" width=\"198\">44 фунта/20 кг</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n&nbsp;', '', 1, 36, 2, NULL, '36', 'ru', 'X9aG28BBaW1aUFesDRN7sQ5wQRD8EnkM4GB9XAI9', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (418, 1, '1. На какую сумму я могу застраховать свою Посылку?', NULL, '', 'На сумму не превышающую сумму указанную в таможенной декларации, до максимальной суммы в<span style=\"color: #810C2C;\"> $1300.</span>', '', 1, 37, 2, NULL, '37', 'ru', 'Yn0SKZfQfHLdnTIvLyMykAKP9SsT0HRJa2dcvMDm', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (420, 1, '3. Что считается Страховым Случаем у USPS?', NULL, '', 'Страховым Случаем считается факт потери Посылки предназначенной Заказчику, официально задокументированный Почтовой Службой США.\r\n\r\nМинимальный период для открытия расследования по недоставленной Посылке составляет 30 дней месяца с момента регистрации Посылки в системе Почтовой Службы США.\r\n\r\nКомпания оставляет за собой право запросить документы (квитанции магазина и пр.), подтверждающие действительную стоимость вложения, указанного в таможенной декларации и отказать в выплате страховки если такие документы отсутствуют или не соответствуют указанной стоимости.', '', 1, 38, 2, NULL, '38', 'ru', 'MsJXEFMBRCEN4gvM6RtLu3chxO9Ti4Jhy92nnOm0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (422, 1, '4. Что не считается Страховым Случаем у USPS? ', NULL, '', 'Конфискация Посылки таможенными службами или невозможность доставить Посылку по указанному Заказчиком адресу не признаются Страховыми Случаями.', '', 1, 39, 2, NULL, '39', 'ru', 'YuCgAavlzuzHmpmpGE1MfkNP3XFi5edcPHbWZx1C', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (424, 1, '5. Как выплачивается страховка по Страховому Случаю?', NULL, '', 'После официального подтверждения утери Посылки Почтовой Службой США, мы обязуемся выплатить полную сумму страховки в течение 15 дней.', '', 1, 40, 2, NULL, '40', 'ru', 'UBd8EbMwk6laxm2vF2HkBYb1ibJ8I7CoHp5bz40r', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (426, 1, '1. Цены на услуги', NULL, '', '<ul>\r\n	<li><span style=\"color: #000000;\">Хранение заказов сроком до 90 дней – бесплатно ($2/день после истечения срока)</span></li>\r\n	<li><span style=\"color: #000000;\">Хранение консолидированных посылок сроком до 15 дней – бесплатно ($5/день после истечения срока)</span></li>\r\n	<li><span style=\"color: #000000;\">Фотография почтовой этикетки, вложения и квитанции – бесплатно</span></li>\r\n	<li><span style=\"color: #000000;\">Первый заказ всегда консолидируется бесплатно – т.е. если в посылке всего один заказ, то стоимость услуг склада равна $0!</span></li>\r\n	<li><span style=\"color: #000000;\">Консолидация от 2 до 5 заказов в посылку, дополнительная упаковка в пластиковый пакет и прочную коробку, почтовое отправление - $5 за консолидированную посылку + розничный тариф Почтовой Службы</span></li>\r\n	<li><span style=\"color: #000000;\">Консолидация от 6 до 10 заказов в посылку, дополнительная упаковка в пластиковый пакет и прочную коробку, почтовое отправление - $10 за консолидированную посылку + розничный тариф Почтовой Службы</span></li>\r\n	<li><span style=\"color: #000000;\">Консолидация более 10 заказов в посылку, дополнительная упаковка в пластиковый пакет и прочную коробку, почтовое отправление - $20 за консолидированную посылку + розничный тариф Почтовой Службы</span></li>\r\n	<li><span style=\"color: #000000;\">Посреднические услуги при выкупе и отправке заказов – от 5% до 15%, в зависимости</span> от типа заказа. Подробное описание можно найти на сайте нашего <a title=\"Расценки на услуги\" href=\"http://zoneofshopping.com/index/tarify/0-14\">партнера.</a></li>\r\n</ul>', '', 1, 41, 2, NULL, '41', 'ru', 'FTMCHEBmiI4A6S4HyhKZdkSAwNUxaPZEAsbHyvU5', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (430, 1, '2. Тарифы Почтовой Службы США и Альтернативных каналов', NULL, '', 'Самые актуальные тарифы на услуги Почтовых и Курьерских служб предоставлены в нашем <span style=\"color: #810c2c;\"><a class=\"calculator-url\" style=\"color: #810c2c;\" href=\"#\"><span style=\"color: #810c2c;\">Калькуляторе.</span></a></span\r\n\r\n', '', 1, 42, 2, NULL, '42', 'ru', 'K4xJeEnFifsNHY6ZkjYSHAC6r3P1pzpihnDOZmRV', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (440, 1, '3. Способы оплаты ', NULL, '', 'Нашим клиентам доступны следующие способы оплаты без дополнительным комиссий:\r\n<ul>\r\n	<li>Кошелек PayPal</li>\r\n	<li>Банковские карты:  Visa и Mastercard</li>\r\n	<li>Системa электроных платежей и терминалов QIWI</li>\r\n	<li>Оплата с баланса в личном кабинете</li>\r\n</ul>\r\nОплата осуществляется в долларах США.\r\n\r\n<b style=\"color: #222222;\">Автоматическая оплата посылки</b>\r\n\r\n<span style=\"color: #222222;\">В случае, если:</span><br style=\"color: #222222;\" /><span style=\"color: #222222;\">1. Ваша посылка USPS-Express, USPS-Priority, авиа-эконом или море-эконом собрана и готова к оплате, и в посылке заполнена хотя бы одна строка декларации ИЛИ</span><br style=\"color: #222222;\" /><span style=\"color: #222222;\">2. Ваша посылка Boxberry собрана, оформлена и проверена (т.е. статус посылки изменился на \"Проверена, ожидает оплаты\") </span><br style=\"color: #222222;\" /><span style=\"color: #222222;\">3. На балансе Вашего эккаунта находится достаточное количество денег для оплаты посылки, </span><span style=\"color: #222222;\">то списание Ваших средств с баланса в счет уплаты посылки происходит </span><b style=\"color: #222222;\">автоматически </b>в течении нескольких минут после получения статуса \"собрана, готова к оплате\" или \"проверена, ожидает оплаты\"<span style=\"color: #222222;\">. </span>', '', 1, 43, 2, NULL, '43', 'ru', 'IwYp5QDQcONgkif9GeCX5vvEabNbj1t3DP56EsFY', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (444, 1, '1. График работы', NULL, '', 'Служба клиентской поддержки:\r\n<span style=\"color: #282828;\">Понедельник – суббота, с 8:00 утра до 2:00 ночи (Московское время)</span><br style=\"color: #282828;\" /><span style=\"color: #282828;\">Воскресенье – выходной.</span>\r\n\r\nСлужба клиентской поддержки обрабатывает запросы клиентов в течении 24 часов.\r\n\r\nСклад:\r\nПонедельник – Пятница, с 9:00 до 17:00 (Североамериканское Восточное Время)', '', 1, 44, 2, NULL, '44', 'ru', 'gwjX2rTLOuHJTPpFiVslV9aYJbbXXyGsxZK6IitX', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (447, 1, '2. Реквизиты компании', NULL, '', 'Strategic Marketing & Promotions L.P.\r\nSL017351\r\n44 Main Street Douglas, South Lanarkshire\r\nML11 0QW, Scotland', '', 1, 45, 2, NULL, '45', 'ru', 'hC4eOht4CM2FwNJwXzVl1EMf9rDlgWlHY8Qjvlz2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (809, 1, '2. Как рассчитывается сумма страховой премии?', NULL, '', 'Сумма страховой премии рассчитывается как $1.6 за каждые $100 вложения. Страхуя вложение на сумму в $1000, Вы заплатите $16 страховой премии.', '', 1, 46, 2, NULL, '46', 'ru', 'Ow1L8dyBborq5bGLUSe6h1ZTg8eOZuR4by1uXc7I', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (3250, 1, '7. Чем отличается Боксберри от других видов отправления?', NULL, '', 'Боксберри – это самый быстрый и очень выгодный способ доставки в Россию. Доставка осуществляется прямым авиарейсом из Нью-Йорка в Москву и затем, после таможенной очистки, посылки передаются нашему партнеру, компании Boxberry, т.е. посылки с нашего склада отправляются сразу в аэропорт без малейших задержек и пересылаются в Москву.\r\n\r\nВы можете выбрать курьерскую доставкудо двери или же забрать посылку из одного из пунктов самовывоза.\r\n\r\nСрок доставки от 1 до 8 дней с момента таможенной очистки в Москве, в зависимости от региона.', '', 1, 47, 2, NULL, '7-%d1%87%d0%b5%d0%bc-%d0%be%d1%82%d0%bb%d0%b8%d1%87%d0%b0%d0%b5%d1%82%d1%81%d1%8f-%d0%b1%d0%be%d0%ba%d1%81%d0%b1%d0%b5%d1%80%d1%80%d0%b8-%d0%be%d1%82-%d0%b4%d1%80%d1%83%d0%b3%d0%b8%d1%85-%d0%b2%d0%b8', 'ru', 'KLbzxtGCQbr8P5gE7j28BdICaDhyUlUCwQvSqv4D', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (3251, 1, '8. Какие документы необходимо готовить для доставки Боксберри?', NULL, '', 'Для оформления посылки Вам необходимы следующие документы: копия паспорта получателя (разворот с фотографией и разворот с пропиской), инвойсы из магазинов и декларация.\r\nДекларацию к посылке Вам необходимо заполнить в личном кабинете на сайте. Для каждого товара необходимо предоставить название вещи, артикул, название магазина, кол-во, цену ед. товара, ссылку на магазин, бренд товара, описание товара на русском и английском языке.\r\nДля заполнения декларации найдите необходимую посылку в таблице посылок и нажмите на \"+\" в колонке \"Декларация\". Вы также можете заполнить декларацию нажав на \"Редактировать\" и выбрав закладку \"Декларация\" в открывшейся форме.', '', 1, 48, 2, NULL, '8-%d0%ba%d0%b0%d0%ba%d0%b8%d0%b5-%d0%b4%d0%be%d0%ba%d1%83%d0%bc%d0%b5%d0%bd%d1%82%d1%8b-%d0%bd%d0%b5%d0%be%d0%b1%d1%85%d0%be%d0%b4%d0%b8%d0%bc%d0%be-%d0%b3%d0%be%d1%82%d0%be%d0%b2%d0%b8%d1%82%d1%8c', 'ru', 'WCS0QonTruB9BEYcMOVXz31TNHMkHE7WwNVe1DTV', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (3492, 1, '3. Политика конфиденциальности', NULL, '', 'Политика конфиденциальности ( privacy policy) нашей компании описана на <a href=\" https://boxfwd.com/pages/privacy-policy/\" title=\"Privacy Policy\" target=\"_blank\">этой странице</a>\r\n\r\nПо любым вопросам, связанным с этой политикой, пожалуйста обращайтесь в нашу службу поддержки.', '', 1, 49, 2, NULL, '%d0%bf%d0%be%d0%bb%d0%b8%d1%82%d0%b8%d0%ba%d0%b0-%d0%ba%d0%be%d0%bd%d1%84%d0%b8%d0%b4%d0%b5%d0%bd%d1%86%d0%b8%d0%b0%d0%bb%d1%8c%d0%bd%d0%be%d1%81%d1%82%d0%b8', 'ru', 'Fd1641IyIFRA80jT9kAr0MtCrI0wlLlZKUGeuFEm', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (3853, 1, '8. Правила принятия претензий по комплектации посылок', NULL, '', '<span style=\"color: #222222;\">Просим вас учитывать, что претензии по комплектности посылки принимаются только при соблюдении следующих условий:</span><br style=\"color: #666666;\" /><span style=\"color: #222222;\">- обращение не позже чем в течение 2 недель после получения посылки</span><br style=\"color: #666666;\" /><span style=\"color: #222222;\">- при наличии фото коробки посылки и всех лейблов и сопроводительных бумаг\r\n- при наличии видеосъемки процесса распаковки и разбора посылки</span><br style=\"color: #666666;\" /><span style=\"color: #222222;\">- при наличии фото заказа на сайте.</span>', '', 1, 50, 2, NULL, '6-%d0%bf%d1%80%d0%b5%d1%82%d0%b5%d0%bd%d0%b7%d0%b8%d0%b8', 'ru', 'HC6JTjBbPa9unFYbzAvT7IUH4kscxet1yWMLUVEn', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (3854, 1, '6. Есть ли страховка для посылок, отправляемых курьерскими службами?', NULL, '', 'Оформление страховки возможно также и для посылок СПСР и Боксберри.\r\n\r\n<span style=\"color: #050607;\">Вложение автоматически бесплатно страхуется от утери на сумму в $20 за каждый кг. веса. Предоставляется также возможность приобрести дополнительную страховку BOXFORWARD из расчета 1.5% от  страхуемой стоимости вложения. </span>Страхуя вложение на сумму в $1000, Вы заплатите $15 страховой премии (максимально возможная оценка вложения одной посылки - $1300).\r\n\r\nКомпания СПСР считает страховым случаем только полную утерю посылки. В случаи наступления страхового случая, стоимость посылки компенсируется согласно оплаченной страховке, если она была оформлена.\r\n\r\nКомпания Боксберри несет ответственность за посылки, переданные им, как при полной утери посылки, так и при ее повреждении, если с ней что-то случилось по их вине. Исчезновение вещей во время таможенного досмотра или потеря посылки на таможни не является страховым случаем. Размер компенсации:\r\n\r\n1)     Если страховка не оформлена, то Боксберри возмещает стоимость максимум до 5 тыс. руб. (если посылка стоит меньше, то возмещают стоимость посылки; если больше - то только до 5 т.р)\r\n\r\n2)     Если страховка оформлена, то Боксберри возмещает полную стоимость.\r\n\r\nПретензии принимаются Боксберри только во время получения посылки. Т.е. необходимо при получении составить претензию (в отделении или с курьером), подписать ее у перевозчика - и только потом расписываться за посылку. Если при получение не было жалобы, то претензии клиента рассматриваться не будут и компенсация не будет начислена.', '', 1, 51, 2, NULL, '%d0%b5%d1%81%d1%82%d1%8c-%d0%bb%d0%b8-%d1%81%d1%82%d1%80%d0%b0%d1%85%d0%be%d0%b2%d0%ba%d0%b0-%d0%b4%d0%bb%d1%8f-%d0%bf%d0%be%d1%81%d1%8b%d0%bb%d0%be%d0%ba-%d0%be%d1%82%d0%bf%d1%80%d0%b0%d0%b2%d0%bb', 'ru', 'PO8ygz3hGv7OXkQRgA4faAIMdRkIfwXF2TIPQx5y', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (3856, 1, '7. Что делать, если у меня есть претензии по комплектности полученной посылки?', NULL, '', '<span style=\"color: #000000;\">Обратите внимание на процедуру подачи претензий в отношении посылок, отправленных почтовым отправлением или Boxberry:</span>\r\n\r\n<br style=\"color: #000000;\" /><span style=\"color: #000000;\">1. Порядок действий по проверке посылки и подаче претензий, если Ваша посылка оказалась с явно нарушенной упаковкой или с недостачей / испорченными вещами:</span>\r\n\r\n– <a href=\"https://boxfwd.com/pages/blog/instruction/инструкция-по-приёму-почтовых-посыло/\">для почтовых отправлений;</a>\r\n\r\n– <a href=\"https://boxfwd.com/pages/blog/instruction/инструкция-по-приёму-посылок-boxberry/\">для Boxberry</a>.\r\n\r\n<br style=\"color: #000000;\" />2. Если Ваша посылка пришла позже заявленных сроков / не была доставлена:\r\n\r\n- претензии по нарушению сроков доставки рассматривает Boxberry. Вы можете либо самостоятельно обратиться на горячую линию Boxberry и оставить свою претензию к сроку доставки, либо написать в службу поддержки BOXFORWARD.\r\n\r\n- претензия подается в свободной форме с указанием трек-номера посылки.\r\n\r\n- срок подачи претензии – в течение 5 дней с момента получения посылки.\r\n\r\n- срок рассмотрения заявки 15 рабочих дней.\r\n\r\nПри этом необходимо учитывать, что в сроки доставки не включается время, проведенное посылкой на таможне. Не принимаются претензии в том случае, если задержка доставки произошла по причине некорректного оформления клиентом документов.\r\n\r\n&nbsp;\r\n\r\n3. Все остальные случаи\r\n\r\n- все претензии в первую очередь рассматриваются службой поддержки <span style=\"color: #000000;\">BOXFORWARD</span>. В зависимости от результатов мы проинформируем клиента о дальнейших действиях.\r\n\r\n<b>Также обращаем Ваше внимание, что не принимаются никакие претензии в случае, если причиной проблем с посылкой являются:</b>\r\n<ul>\r\n	<li>стихийные бедствия: землетрясения, циклоны, ураганы, наводнения, пожары, эпидемии, туманы, снегопады или морозы;</li>\r\n	<li>форс-мажорные обстоятельства, включая, помимо прочего: войны, катастрофы, действия враждебно настроенных лиц, забастовки, эмбарго, реализации рисков, характерных для воздушной перевозки, местных конфликтов и акций гражданского неповиновения;</li>\r\n	<li>перебои в работе сетей местного или национального воздушного и наземного сообщения, технические неисправности на транспорте и в оборудовании;</li>\r\n	<li>скрытые недостатки или врожденные дефекты содержимого;</li>\r\n	<li>действие или бездействие таможенных органов, служащих авиалиний и аэропортов, или государственных чиновников.</li>\r\n	<li>того, что содержимое посылки является запрещенным предметом, в том числе, если он был принят к перевозке по ошибке.</li>\r\n</ul>\r\n&nbsp;', '', 1, 52, 2, NULL, '7-%d1%87%d1%82%d0%be-%d0%b4%d0%b5%d0%bb%d0%b0%d1%82%d1%8c-%d0%b5%d1%81%d0%bb%d0%b8-%d1%83-%d0%bc%d0%b5%d0%bd%d1%8f-%d0%b5%d1%81%d1%82%d1%8c-%d0%bf%d1%80%d0%b5%d1%82%d0%b5%d0%bd%d0%b7%d0%b8%d0%b8', 'ru', 'Hr3Pz0Axjg23g6lfpWJiPGJd8FAMOEyN7OjMbwUN', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (3858, 1, '9. Что делать, если у меня потерялся заказ?', NULL, '', '<b>Вариант 1.</b>\r\n\r\nЗаказ поступил на Ваш сьют, а потом потерялся (т.е. Вы его не получили в посылке). – Вы можете подать в службу поддержки претензию по комплектации посылки.\r\n\r\n&nbsp;\r\n\r\n<b>Вариант 2.</b>\r\n\r\nВы видите, что по треку заказ якобы поступил на наш склад, но в эккаунте не появился.\r\n\r\nТогда действуем следующим образом:\r\n<ul>\r\n	<li>ждем несколько дней и внимательно проверяем все заказы, поступившие на склад за этот период в статусе \"принят складом\", даже если указано, что такой заказ - из другого магазина. Рекомендую в таких случаях просить склад сделать фото всех таких заказов - зачастую \"потеряшки\" находят на этом этапе.</li>\r\n	<li>если в своем эккаунте не нашли, отправляем в службу поддержки BOXFORWARD копию инвойса магазина, номер трека, и просим проверить на складе, не поступал ли такой заказ, и не находится ли он в неопознанных (обычно в корзине неопознанных лежит 2-3 заказа).</li>\r\n	<li>одновременно связываемся с магазином и говорим о том, что получили пакет заказов от USPS / UPS / FedEx, расписались за пакет, но в итоге заказа от данного магазина у себя не обнаружили. Просим магазин подать claim транспортировщику о недоставке этого конкретного заказа. Также уточняем, может быть у заказа в пути менялся трек, и Вы неправильно его отслеживаете.</li>\r\n	<li>если трек менялся, сообщаем службе поддержки BOXFORWARD новый трек и просим еще раз проверить его поступление на склад.</li>\r\n</ul>\r\nЕсли в итоге данных поисков выясниться, что Ваш заказ не поступал на склад, то дальнейшие разбирательства нужно будет вести только с магазином и транспортировщиком, поскольку за сохранность не полученных заказов мы ответственности не несем.\r\n\r\n&nbsp;', '', 1, 53, 2, NULL, '9-%d1%87%d1%82%d0%be-%d0%b4%d0%b5%d0%bb%d0%b0%d1%82%d1%8c-%d0%b5%d1%81%d0%bb%d0%b8-%d1%83-%d0%bc%d0%b5%d0%bd%d1%8f-%d0%bf%d0%be%d1%82%d0%b5%d1%80%d1%8f%d0%bb%d1%81%d1%8f-%d0%b7%d0%b0%d0%ba%d0%b0', 'ru', 'bcXqvt3LnATnrAct7pFVzgCDCiTeujQfPClmtx9p', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (4126, 1, '4. Абонемент', NULL, '', '<p style=\"color: #222222;\">Это новая услуга, которая доступна нашим клиентам. Если Вы часто отправляете посылки (например, у Вас много мелких заказов с е-бей), но общий вес посылок не достигает объемов СП в месяц, приобретение абонемента поможет Вам сэкономить на услугах склада.</p>\r\n<p style=\"color: #222222;\">Абонемент не является обязательным платежом, это опциональная услуга - Вы можете, как и раньше, оплачивать консолидацию каждой собранной посылки, а можете просто купить абонемент на срок от 1 до 6 месяцев. Стоимость абонементов (на 30, 90 и 180 дней) - <a style=\"color: #0088cc;\" href=\"https://boxfwd.com/pages/servises_test/\">https://boxfwd.com/pages/servises_test/<span class=\"badge badge-notification clicks\" style=\"color: #919191;\" title=\"1 клик\">1</span></a></p>\r\n<p style=\"color: #222222;\">Абонемент начинает действовать с дня оплаты, срок действия рассчитывается в календарных днях. Обслуживание абонементов автоматизировано.</p>\r\n\r\n<h4 style=\"color: #222222;\"><strong>Что дает абонемент?</strong></h4>\r\n<p style=\"color: #222222;\"><em>В течении срока действия абонемента:</em>\r\n- все Ваши посылки будут консолидироваться бесплатно. Вы можете отправлять посылки даже небольшого веса, но с множеством заказов, хоть каждый день - и все равно сэкономить!\r\n- мы по умолчанию будем делать бесплатные фото всех поступающих на Ваш сьют заказов, независимо от того, был ли внесен трек, в каком статусе поступил заказ и успели ли Вы отметить запрос на фото\r\n- Вы продолжаете участвовать в <a style=\"color: #0088cc;\" href=\"https://boxfwd.com/pages/%D0%B1%D0%BE%D0%BD%D1%83%D1%81%D0%BD%D1%8B%D0%B5-%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D1%8B/\">бонусной программе \"100 рублей\"</a> - и будете получать бонусы за отправленные посылки\r\n- Вы можете участвовать в акциях, а также применять промо-коды на скидку (если таковые появятся)</p>', '', 1, 54, 2, NULL, '%d0%b0%d0%b1%d0%be%d0%bd%d0%b5%d0%bc%d0%b5%d0%bd%d1%82', 'ru', 'BUiD7dJcIpFtRo6HXrVOT7O8LmHQHjlakZBbbxKp', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
                                                                                                                                                                                                              (8251, 1, '14. Услуга \"Заполнение декларации Boxberry\"', NULL, '', '<p style=\"color: #282828;\">1.Чтобы заказать услугу, вам необходимо обратиться в нашу службу поддержки (service@boxfwd.com), указать номер посылки и предоставить все инвойсы с ссылками на товары</p>\r\n<p style=\"color: #282828;\">2.Инвойсы и ссылки должны быть корректными и в той форме, в которой загружаются в раздел посылки “Документы”, а также должна быть возможность зайти по ссылке при нажатии на нее мышкой.</p>\r\n<p style=\"color: #282828;\">3.Услуга предоставляется в течении 12 часов в рабочее время службы поддержки, после того, как посылка будет упакована.</p>\r\n<p style=\"color: #282828;\">Стоимость: до 20 строк – $10 , от 20 до 50 строк– $20, свыше 50 строк – $30.</p>\r\n<p style=\"color: #282828;\">Обратите внимание, чтобы успеть в ближайшую отправку Boxberry, нужно подать заявку не позже 17:00 дня перед отправкой (по Московскому времени).\r\n—————————————————————————–\r\n*услуга доступна и для других видов отправления (предоставлять ссылки в таком случае не нужно)</p>', '', 1, 55, 2, NULL, '14-%d1%83%d1%81%d0%bb%d1%83%d0%b3%d0%b0-%d0%b7%d0%b0%d0%bf%d0%be%d0%bb%d0%bd%d0%b5%d0%bd%d0%b8%d0%b5-%d0%b4%d0%b5%d0%ba%d0%bb%d0%b0%d1%80%d0%b0%d1%86%d0%b8%d0%b8-boxberry', 'ru', 'IwYq8u2HQjsnUZBSOWaIQ04Gz6i3x1vsjfjoyfZQ', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `pages` (`id`, `template_id`, `title`, `small_title`, `description`, `content`, `content_two`, `status`, `sort`, `type`, `image`, `slug`, `lang`, `lang_hash`, `created_at`, `updated_at`) VALUES
                                                                                                                                                                                                              (9936, 3, 'fwe', NULL, 'wer', '<p>werwerwer</p>', '<p>werwe</p>', 1, 1, 1, 'http://devbox.vrstka.site//storage/photos/1/2018_novitec_mclaren_570s_spider_4k-1920x1080.jpg', 'fwe', 'en', 'ST90XM2tufd9iMCuzAGhVL2UibpHeAZa74WXrePr', '2018-09-26 01:58:46', '2018-09-26 01:58:46'),
                                                                                                                                                                                                              (9940, NULL, 'dfghj', NULL, 'sdfsdfsd', '<p>dfghj</p>', NULL, 1, 1, 3, 'http://devbox.vrstka.site//storage/photos/1/2017_novitec_mclaren_570gt_4k-1920x1080.jpg', 'dfghj', 'en', 'hsyMPqyRP4iY2fkVe65VxO1Yx4ZMo29150RjbYp4', '2018-09-26 12:23:33', '2018-09-26 12:23:33');

-- --------------------------------------------------------

--
-- Структура таблицы `pages_widgets`
--

CREATE TABLE `pages_widgets` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `widget_id` int(10) UNSIGNED NOT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `pages_widgets`
--

INSERT INTO `pages_widgets` (`id`, `page_id`, `widget_id`, `sort`) VALUES
                                                                          (31, 9936, 18, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `page_category`
--

CREATE TABLE `page_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `page_category`
--

INSERT INTO `page_category` (`id`, `page_id`, `category_id`) VALUES
                                                                    (1330, 3847, 74),
                                                                    (1331, 3837, 74),
                                                                    (1332, 3780, 74),
                                                                    (1333, 3876, 74),
                                                                    (1334, 3880, 74),
                                                                    (1335, 3815, 74),
                                                                    (1336, 3899, 74),
                                                                    (1337, 3821, 74),
                                                                    (1338, 3686, 74),
                                                                    (1339, 3706, 74),
                                                                    (1340, 3726, 74),
                                                                    (1341, 3758, 74),
                                                                    (1342, 3764, 74),
                                                                    (1343, 3769, 74),
                                                                    (1344, 3773, 74),
                                                                    (1345, 3911, 74),
                                                                    (1346, 3916, 74),
                                                                    (1347, 3922, 74),
                                                                    (1348, 3976, 74),
                                                                    (1349, 4002, 74),
                                                                    (1350, 4053, 74),
                                                                    (1351, 4076, 74),
                                                                    (1352, 4098, 74),
                                                                    (1353, 4104, 74),
                                                                    (1354, 4108, 74),
                                                                    (1355, 4133, 74),
                                                                    (1356, 4152, 74),
                                                                    (1357, 4164, 74),
                                                                    (1358, 4169, 74),
                                                                    (1359, 4173, 74),
                                                                    (1360, 4177, 74),
                                                                    (1361, 4188, 74),
                                                                    (1362, 4233, 74),
                                                                    (1363, 4248, 74),
                                                                    (1364, 4258, 74),
                                                                    (1365, 4265, 74),
                                                                    (1366, 4271, 74),
                                                                    (1367, 4276, 74),
                                                                    (1368, 4282, 74),
                                                                    (1369, 4290, 74),
                                                                    (1370, 4303, 74),
                                                                    (1371, 4308, 74),
                                                                    (1372, 4313, 74),
                                                                    (1373, 4326, 74),
                                                                    (1374, 4332, 74),
                                                                    (1375, 4337, 74),
                                                                    (1376, 4343, 74),
                                                                    (1377, 4355, 74),
                                                                    (1378, 4385, 74),
                                                                    (1379, 4392, 74),
                                                                    (1380, 4397, 74),
                                                                    (1381, 4402, 74),
                                                                    (1382, 4407, 74),
                                                                    (1383, 4411, 74),
                                                                    (1384, 4416, 74),
                                                                    (1385, 4422, 74),
                                                                    (1386, 4431, 74),
                                                                    (1387, 4443, 74),
                                                                    (1388, 4448, 74),
                                                                    (1389, 4459, 74),
                                                                    (1390, 4465, 74),
                                                                    (1391, 4495, 74),
                                                                    (1392, 4501, 74),
                                                                    (1393, 4506, 74),
                                                                    (1394, 4512, 74),
                                                                    (1395, 4517, 74),
                                                                    (1396, 4527, 74),
                                                                    (1397, 4536, 74),
                                                                    (1398, 4550, 74),
                                                                    (1399, 4559, 74),
                                                                    (1400, 4571, 74),
                                                                    (1401, 4577, 74),
                                                                    (1402, 4596, 74),
                                                                    (1403, 4610, 74),
                                                                    (1404, 4629, 74),
                                                                    (1405, 4634, 74),
                                                                    (1406, 4653, 74),
                                                                    (1407, 4659, 74),
                                                                    (1408, 4675, 74),
                                                                    (1409, 4682, 74),
                                                                    (1410, 4708, 74),
                                                                    (1411, 4715, 74),
                                                                    (1412, 4720, 74),
                                                                    (1413, 4728, 74),
                                                                    (1414, 4752, 74),
                                                                    (1415, 4757, 74),
                                                                    (1416, 4763, 74),
                                                                    (1417, 4791, 74),
                                                                    (1418, 4796, 74),
                                                                    (1419, 4801, 74),
                                                                    (1420, 4806, 74),
                                                                    (1421, 4811, 74),
                                                                    (1422, 4816, 74),
                                                                    (1423, 4824, 74),
                                                                    (1424, 4846, 74),
                                                                    (1425, 4859, 74),
                                                                    (1426, 4865, 74),
                                                                    (1427, 4872, 74),
                                                                    (1428, 4877, 74),
                                                                    (1429, 4884, 74),
                                                                    (1430, 4889, 74),
                                                                    (1431, 4894, 74),
                                                                    (1432, 4899, 74),
                                                                    (1433, 4909, 74),
                                                                    (1434, 4914, 74),
                                                                    (1435, 4919, 74),
                                                                    (1436, 4924, 74),
                                                                    (1437, 4930, 74),
                                                                    (1438, 4937, 74),
                                                                    (1439, 4940, 74),
                                                                    (1440, 4945, 74),
                                                                    (1441, 4950, 74),
                                                                    (1442, 4958, 74),
                                                                    (1443, 4963, 74),
                                                                    (1444, 4984, 74),
                                                                    (1445, 5006, 74),
                                                                    (1446, 5011, 74),
                                                                    (1447, 5016, 74),
                                                                    (1448, 5021, 74),
                                                                    (1449, 5026, 74),
                                                                    (1450, 5032, 74),
                                                                    (1451, 5037, 74),
                                                                    (1452, 5087, 74),
                                                                    (1453, 5092, 74),
                                                                    (1454, 5097, 74),
                                                                    (1455, 5102, 74),
                                                                    (1456, 5128, 74),
                                                                    (1457, 5137, 74),
                                                                    (1458, 5148, 74),
                                                                    (1459, 5154, 74),
                                                                    (1460, 5160, 74),
                                                                    (1461, 5165, 74),
                                                                    (1462, 5202, 74),
                                                                    (1463, 5207, 74),
                                                                    (1464, 5212, 74),
                                                                    (1465, 5236, 74),
                                                                    (1466, 5241, 74),
                                                                    (1467, 5255, 74),
                                                                    (1468, 5263, 74),
                                                                    (1469, 5275, 74),
                                                                    (1470, 5282, 74),
                                                                    (1471, 5286, 74),
                                                                    (1472, 5295, 74),
                                                                    (1473, 5325, 74),
                                                                    (1474, 5330, 74),
                                                                    (1475, 5334, 74),
                                                                    (1476, 5338, 74),
                                                                    (1477, 5341, 74),
                                                                    (1478, 5367, 74),
                                                                    (1479, 5371, 74),
                                                                    (1480, 5376, 74),
                                                                    (1481, 5380, 74),
                                                                    (1482, 5388, 74),
                                                                    (1483, 5393, 74),
                                                                    (1484, 5412, 74),
                                                                    (1485, 5417, 74),
                                                                    (1486, 5421, 74),
                                                                    (1487, 5426, 74),
                                                                    (1488, 5430, 74),
                                                                    (1489, 5434, 74),
                                                                    (1490, 5461, 74),
                                                                    (1491, 5466, 74),
                                                                    (1492, 5471, 74),
                                                                    (1493, 5475, 74),
                                                                    (1494, 5484, 74),
                                                                    (1495, 5488, 74),
                                                                    (1496, 5494, 74),
                                                                    (1497, 5498, 74),
                                                                    (1498, 5507, 74),
                                                                    (1499, 5527, 74),
                                                                    (1500, 5531, 74),
                                                                    (1501, 5535, 74),
                                                                    (1502, 5539, 74),
                                                                    (1503, 5543, 74),
                                                                    (1504, 5547, 74),
                                                                    (1505, 5551, 74),
                                                                    (1506, 5564, 74),
                                                                    (1507, 5569, 74),
                                                                    (1508, 5573, 74),
                                                                    (1509, 5588, 74),
                                                                    (1510, 5594, 74),
                                                                    (1511, 5616, 74),
                                                                    (1512, 5620, 74),
                                                                    (1513, 5635, 74),
                                                                    (1514, 5639, 74),
                                                                    (1515, 5643, 74),
                                                                    (1516, 5664, 74),
                                                                    (1517, 5668, 74),
                                                                    (1518, 5673, 74),
                                                                    (1519, 5675, 74),
                                                                    (1520, 5685, 74),
                                                                    (1521, 5689, 74),
                                                                    (1522, 5693, 74),
                                                                    (1523, 5697, 74),
                                                                    (1524, 5702, 74),
                                                                    (1525, 5736, 74),
                                                                    (1526, 5740, 74),
                                                                    (1527, 5765, 74),
                                                                    (1528, 5769, 74),
                                                                    (1529, 5774, 74),
                                                                    (1530, 5778, 74),
                                                                    (1531, 5789, 74),
                                                                    (1532, 5792, 74),
                                                                    (1533, 5796, 74),
                                                                    (1534, 5802, 74),
                                                                    (1535, 5815, 74),
                                                                    (1536, 5820, 74),
                                                                    (1537, 5824, 74),
                                                                    (1538, 5828, 74),
                                                                    (1539, 5832, 74),
                                                                    (1540, 5836, 74),
                                                                    (1541, 5889, 74),
                                                                    (1542, 5895, 74),
                                                                    (1543, 5932, 74),
                                                                    (1544, 5937, 74),
                                                                    (1545, 5942, 74),
                                                                    (1546, 5957, 74),
                                                                    (1547, 5964, 74),
                                                                    (1548, 5969, 74),
                                                                    (1549, 5999, 74),
                                                                    (1550, 6007, 74),
                                                                    (1551, 6011, 74),
                                                                    (1552, 6016, 74),
                                                                    (1553, 6040, 74),
                                                                    (1554, 6045, 74),
                                                                    (1555, 6049, 74),
                                                                    (1556, 6053, 74),
                                                                    (1557, 6074, 74),
                                                                    (1558, 6077, 74),
                                                                    (1559, 6081, 74),
                                                                    (1560, 6085, 74),
                                                                    (1561, 6089, 74),
                                                                    (1562, 6095, 74),
                                                                    (1563, 6112, 74),
                                                                    (1564, 6116, 74),
                                                                    (1565, 6121, 74),
                                                                    (1566, 6130, 74),
                                                                    (1567, 6146, 74),
                                                                    (1568, 6152, 74),
                                                                    (1569, 6156, 74),
                                                                    (1570, 6193, 74),
                                                                    (1571, 6198, 74),
                                                                    (1572, 6203, 74),
                                                                    (1573, 6215, 74),
                                                                    (1574, 6219, 74),
                                                                    (1575, 6225, 74),
                                                                    (1576, 6228, 74),
                                                                    (1577, 6252, 74),
                                                                    (1578, 6257, 74),
                                                                    (1579, 6261, 74),
                                                                    (1580, 6265, 74),
                                                                    (1581, 6275, 74),
                                                                    (1582, 6291, 74),
                                                                    (1583, 6296, 74),
                                                                    (1584, 6300, 74),
                                                                    (1585, 6314, 74),
                                                                    (1586, 6324, 74),
                                                                    (1587, 6345, 74),
                                                                    (1588, 6349, 74),
                                                                    (1589, 6353, 74),
                                                                    (1590, 6356, 74),
                                                                    (1591, 6360, 74),
                                                                    (1592, 6384, 74),
                                                                    (1593, 6387, 74),
                                                                    (1594, 6391, 74),
                                                                    (1595, 6395, 74),
                                                                    (1596, 6399, 74),
                                                                    (1597, 6406, 74),
                                                                    (1598, 6426, 74),
                                                                    (1599, 6439, 74),
                                                                    (1600, 6443, 74),
                                                                    (1601, 6447, 74),
                                                                    (1602, 6472, 74),
                                                                    (1603, 6481, 74),
                                                                    (1604, 6485, 74),
                                                                    (1605, 6490, 74),
                                                                    (1606, 6512, 74),
                                                                    (1607, 6525, 74),
                                                                    (1608, 6530, 74),
                                                                    (1609, 6535, 74),
                                                                    (1610, 6551, 74),
                                                                    (1611, 6566, 74),
                                                                    (1612, 6569, 74),
                                                                    (1613, 6579, 74),
                                                                    (1614, 6584, 74),
                                                                    (1615, 6607, 74),
                                                                    (1616, 6611, 74),
                                                                    (1617, 6615, 74),
                                                                    (1618, 6620, 74),
                                                                    (1619, 6627, 74),
                                                                    (1620, 6634, 74),
                                                                    (1621, 6637, 74),
                                                                    (1622, 6658, 74),
                                                                    (1623, 6661, 74),
                                                                    (1624, 6664, 74),
                                                                    (1625, 6668, 74),
                                                                    (1626, 6680, 74),
                                                                    (1627, 6704, 74),
                                                                    (1628, 6710, 74),
                                                                    (1629, 6714, 74),
                                                                    (1630, 6719, 74),
                                                                    (1631, 6739, 74),
                                                                    (1632, 6743, 74),
                                                                    (1633, 6771, 74),
                                                                    (1634, 6775, 74),
                                                                    (1635, 6782, 74),
                                                                    (1636, 6810, 74),
                                                                    (1637, 6815, 74),
                                                                    (1638, 6820, 74),
                                                                    (1639, 6824, 74),
                                                                    (1640, 6830, 74),
                                                                    (1641, 6858, 74),
                                                                    (1642, 6862, 74),
                                                                    (1643, 6865, 74),
                                                                    (1644, 6869, 74),
                                                                    (1645, 6875, 74),
                                                                    (1646, 6883, 74),
                                                                    (1647, 6906, 74),
                                                                    (1648, 6909, 74),
                                                                    (1649, 6914, 74),
                                                                    (1650, 6918, 74),
                                                                    (1651, 6926, 74),
                                                                    (1652, 6939, 74),
                                                                    (1653, 6947, 74),
                                                                    (1654, 6957, 74),
                                                                    (1655, 6964, 74),
                                                                    (1656, 6970, 74),
                                                                    (1657, 6986, 74),
                                                                    (1658, 7007, 74),
                                                                    (1659, 7016, 74),
                                                                    (1660, 7023, 74),
                                                                    (1661, 7033, 74),
                                                                    (1662, 7039, 74),
                                                                    (1663, 7045, 74),
                                                                    (1664, 7052, 74),
                                                                    (1665, 7063, 74),
                                                                    (1666, 7081, 74),
                                                                    (1667, 7089, 74),
                                                                    (1668, 7094, 74),
                                                                    (1669, 7104, 74),
                                                                    (1670, 7118, 74),
                                                                    (1671, 7126, 74),
                                                                    (1672, 7141, 74),
                                                                    (1673, 7148, 74),
                                                                    (1674, 7154, 74),
                                                                    (1675, 7164, 74),
                                                                    (1676, 7170, 74),
                                                                    (1677, 7179, 74),
                                                                    (1678, 7212, 74),
                                                                    (1679, 7218, 74),
                                                                    (1680, 7224, 74),
                                                                    (1681, 7237, 74),
                                                                    (1682, 7243, 74),
                                                                    (1683, 7285, 74),
                                                                    (1684, 7292, 74),
                                                                    (1685, 7298, 74),
                                                                    (1686, 7304, 74),
                                                                    (1687, 7312, 74),
                                                                    (1688, 7319, 74),
                                                                    (1689, 7365, 74),
                                                                    (1690, 7374, 74),
                                                                    (1691, 7372, 74),
                                                                    (1692, 7402, 74),
                                                                    (1693, 7410, 74),
                                                                    (1694, 7417, 74),
                                                                    (1695, 7452, 74),
                                                                    (1696, 7459, 74),
                                                                    (1697, 7465, 74),
                                                                    (1698, 7471, 74),
                                                                    (1699, 7478, 74),
                                                                    (1700, 7495, 74),
                                                                    (1701, 7499, 74),
                                                                    (1702, 7510, 74),
                                                                    (1703, 7517, 74),
                                                                    (1704, 7523, 74),
                                                                    (1705, 7533, 74),
                                                                    (1706, 7541, 74),
                                                                    (1707, 7547, 74),
                                                                    (1708, 7553, 74),
                                                                    (1709, 7562, 74),
                                                                    (1710, 7571, 74),
                                                                    (1711, 7580, 74),
                                                                    (1712, 7587, 74),
                                                                    (1713, 7597, 74),
                                                                    (1714, 7603, 74),
                                                                    (1715, 7614, 74),
                                                                    (1716, 7619, 74),
                                                                    (1717, 7660, 74),
                                                                    (1718, 7734, 74),
                                                                    (1719, 7739, 74),
                                                                    (1720, 7744, 74),
                                                                    (1721, 7757, 74),
                                                                    (1722, 7766, 74),
                                                                    (1723, 7782, 74),
                                                                    (1724, 7787, 74),
                                                                    (1725, 7792, 74),
                                                                    (1726, 8040, 74),
                                                                    (1727, 8055, 74),
                                                                    (1728, 8066, 74),
                                                                    (1729, 8072, 74),
                                                                    (1730, 8077, 74),
                                                                    (1731, 8082, 74),
                                                                    (1732, 8094, 74),
                                                                    (1733, 8100, 74),
                                                                    (1734, 8107, 74),
                                                                    (1735, 8114, 74),
                                                                    (1736, 8119, 74),
                                                                    (1737, 8124, 74),
                                                                    (1738, 8142, 74),
                                                                    (1739, 8147, 74),
                                                                    (1740, 8156, 74),
                                                                    (1741, 8162, 74),
                                                                    (1742, 8168, 74),
                                                                    (1743, 8178, 74),
                                                                    (1744, 8183, 74),
                                                                    (1745, 8190, 74),
                                                                    (1746, 8200, 74),
                                                                    (1747, 8205, 74),
                                                                    (1748, 8216, 74),
                                                                    (1749, 8225, 74),
                                                                    (1750, 8231, 74),
                                                                    (1751, 8237, 74),
                                                                    (1752, 8244, 74),
                                                                    (1753, 8252, 74),
                                                                    (1754, 8261, 74),
                                                                    (1755, 8266, 74),
                                                                    (1756, 8274, 74),
                                                                    (1757, 8281, 74),
                                                                    (1758, 8288, 74),
                                                                    (1759, 8294, 74),
                                                                    (1760, 8299, 74),
                                                                    (1761, 8311, 74),
                                                                    (1762, 8317, 74),
                                                                    (1763, 8332, 74),
                                                                    (1764, 8337, 74),
                                                                    (1765, 8342, 74),
                                                                    (1766, 8349, 74),
                                                                    (1767, 8354, 74),
                                                                    (1768, 8360, 74),
                                                                    (1769, 8432, 74),
                                                                    (1770, 8442, 74),
                                                                    (1771, 8449, 74),
                                                                    (1772, 8454, 74),
                                                                    (1773, 8460, 74),
                                                                    (1774, 8466, 74),
                                                                    (1775, 8482, 74),
                                                                    (1776, 8489, 74),
                                                                    (1777, 8499, 74),
                                                                    (1778, 8505, 74),
                                                                    (1779, 8511, 74),
                                                                    (1780, 8517, 74),
                                                                    (1781, 8529, 74),
                                                                    (1782, 8536, 74),
                                                                    (1783, 8542, 74),
                                                                    (1784, 8548, 74),
                                                                    (1785, 8597, 74),
                                                                    (1786, 8620, 74),
                                                                    (1787, 8625, 74),
                                                                    (1788, 8631, 74),
                                                                    (1789, 8637, 74),
                                                                    (1790, 8652, 74),
                                                                    (1791, 8646, 74),
                                                                    (1792, 8670, 74),
                                                                    (1793, 8676, 74),
                                                                    (1794, 8679, 74),
                                                                    (1795, 8684, 74),
                                                                    (1796, 8690, 74),
                                                                    (1797, 8698, 74),
                                                                    (1798, 8702, 74),
                                                                    (1799, 8716, 74),
                                                                    (1800, 8721, 74),
                                                                    (1801, 8727, 74),
                                                                    (1802, 8735, 74),
                                                                    (1803, 8742, 74),
                                                                    (1804, 8746, 74),
                                                                    (1805, 8751, 74),
                                                                    (1806, 8756, 74),
                                                                    (1807, 8761, 74),
                                                                    (1808, 8771, 74),
                                                                    (1809, 8777, 74),
                                                                    (1810, 8782, 74),
                                                                    (1811, 8787, 74),
                                                                    (1812, 8792, 74),
                                                                    (1813, 8800, 74),
                                                                    (1814, 8806, 74),
                                                                    (1815, 8815, 74),
                                                                    (1816, 8819, 74),
                                                                    (1817, 8824, 74),
                                                                    (1818, 8829, 74),
                                                                    (1819, 8833, 74),
                                                                    (1820, 8838, 74),
                                                                    (1821, 8846, 74),
                                                                    (1822, 8851, 74),
                                                                    (1823, 8857, 74),
                                                                    (1824, 8869, 74),
                                                                    (1825, 8875, 74),
                                                                    (1826, 8881, 74),
                                                                    (1827, 8887, 74),
                                                                    (1828, 8892, 74),
                                                                    (1829, 8904, 74),
                                                                    (1830, 8909, 74),
                                                                    (1831, 8915, 74),
                                                                    (1832, 8920, 74),
                                                                    (1833, 8926, 74),
                                                                    (1834, 8931, 74),
                                                                    (1835, 8936, 74),
                                                                    (1836, 8945, 74),
                                                                    (1837, 8951, 74),
                                                                    (1838, 8958, 74),
                                                                    (1839, 8963, 74),
                                                                    (1840, 8967, 74),
                                                                    (1841, 8974, 74),
                                                                    (1842, 8979, 74),
                                                                    (1843, 8984, 74),
                                                                    (1844, 8989, 74),
                                                                    (1845, 9018, 74),
                                                                    (1846, 9025, 74),
                                                                    (1847, 9030, 74),
                                                                    (1848, 9038, 74),
                                                                    (1849, 9043, 74),
                                                                    (1850, 9059, 74),
                                                                    (1851, 9064, 74),
                                                                    (1852, 9070, 74),
                                                                    (1853, 9087, 74),
                                                                    (1854, 9091, 74),
                                                                    (1855, 9095, 74),
                                                                    (1856, 9099, 74),
                                                                    (1857, 9102, 74),
                                                                    (1858, 9109, 74),
                                                                    (1859, 9114, 74),
                                                                    (1860, 9134, 74),
                                                                    (1861, 9138, 74),
                                                                    (1862, 9142, 74),
                                                                    (1863, 9146, 74),
                                                                    (1864, 9150, 74),
                                                                    (1865, 9170, 74),
                                                                    (1866, 9175, 74),
                                                                    (1867, 9180, 74),
                                                                    (1868, 9184, 74),
                                                                    (1869, 9188, 74),
                                                                    (1870, 9209, 74),
                                                                    (1871, 9218, 74),
                                                                    (1872, 9224, 74),
                                                                    (1873, 9228, 74),
                                                                    (1874, 9236, 74),
                                                                    (1875, 9241, 74),
                                                                    (1876, 9245, 74),
                                                                    (1877, 9251, 74),
                                                                    (1878, 9256, 74),
                                                                    (1879, 9274, 74),
                                                                    (1880, 9278, 74),
                                                                    (1881, 9283, 74),
                                                                    (1882, 9287, 74),
                                                                    (1883, 9291, 74),
                                                                    (1884, 9309, 74),
                                                                    (1885, 9319, 74),
                                                                    (1886, 9326, 74),
                                                                    (1887, 9333, 74),
                                                                    (1888, 9352, 74),
                                                                    (1889, 9361, 74),
                                                                    (1890, 9366, 74),
                                                                    (1891, 9381, 74),
                                                                    (1892, 9387, 74),
                                                                    (1893, 9391, 74),
                                                                    (1894, 9398, 74),
                                                                    (1895, 9403, 74),
                                                                    (1896, 9407, 74),
                                                                    (1897, 9410, 74),
                                                                    (1898, 9414, 74),
                                                                    (1899, 9419, 74),
                                                                    (1900, 9444, 74),
                                                                    (1901, 9450, 74),
                                                                    (1902, 9454, 74),
                                                                    (1903, 9462, 74),
                                                                    (1904, 9471, 74),
                                                                    (1905, 9475, 74),
                                                                    (1906, 9479, 74),
                                                                    (1907, 9487, 74),
                                                                    (1908, 9512, 74),
                                                                    (1909, 9584, 74),
                                                                    (1910, 9589, 74),
                                                                    (1911, 9600, 74),
                                                                    (1912, 9604, 74),
                                                                    (1913, 9610, 74),
                                                                    (1914, 9614, 74),
                                                                    (1915, 9618, 74),
                                                                    (1916, 9621, 74),
                                                                    (1917, 9625, 74),
                                                                    (1918, 9629, 74),
                                                                    (1919, 9634, 74),
                                                                    (1920, 9638, 74),
                                                                    (1921, 9660, 74),
                                                                    (1922, 9664, 74),
                                                                    (1923, 9668, 74),
                                                                    (1924, 9675, 74),
                                                                    (1925, 9685, 74),
                                                                    (1926, 9690, 74),
                                                                    (1927, 9700, 74),
                                                                    (1928, 9704, 74),
                                                                    (1929, 9708, 74),
                                                                    (1930, 9724, 74),
                                                                    (1931, 9756, 74),
                                                                    (1932, 9793, 74),
                                                                    (1933, 9808, 74),
                                                                    (1934, 9811, 74),
                                                                    (1935, 9822, 74),
                                                                    (1936, 9829, 74),
                                                                    (1937, 9833, 74),
                                                                    (1938, 9837, 74),
                                                                    (1939, 9842, 74),
                                                                    (1940, 9852, 74),
                                                                    (1941, 9854, 74),
                                                                    (1942, 9858, 74),
                                                                    (1943, 9865, 74),
                                                                    (1944, 9871, 74),
                                                                    (1945, 9879, 74),
                                                                    (1946, 9883, 74),
                                                                    (1947, 9893, 74),
                                                                    (1948, 9895, 74),
                                                                    (1949, 9904, 74),
                                                                    (1950, 9899, 74),
                                                                    (1951, 3825, 79),
                                                                    (1952, 3634, 79),
                                                                    (1953, 3646, 79),
                                                                    (1954, 3747, 79),
                                                                    (1955, 3929, 79),
                                                                    (1956, 4115, 79),
                                                                    (1957, 4181, 79),
                                                                    (1958, 4210, 79),
                                                                    (1959, 4254, 79),
                                                                    (1960, 4295, 79),
                                                                    (1961, 4407, 79),
                                                                    (1962, 4470, 79),
                                                                    (1963, 4541, 79),
                                                                    (1964, 4664, 79),
                                                                    (1965, 4768, 79),
                                                                    (1966, 4780, 79),
                                                                    (1967, 4829, 79),
                                                                    (1968, 4969, 79),
                                                                    (1969, 5107, 79),
                                                                    (1970, 5300, 79),
                                                                    (1971, 5442, 79),
                                                                    (1972, 5511, 79),
                                                                    (1973, 5598, 79),
                                                                    (1974, 5647, 79),
                                                                    (1975, 5745, 79),
                                                                    (1976, 5840, 79),
                                                                    (1977, 5856, 79),
                                                                    (1978, 5906, 79),
                                                                    (1979, 5973, 79),
                                                                    (1980, 6023, 79),
                                                                    (1981, 6057, 79),
                                                                    (1982, 6099, 79),
                                                                    (1983, 6134, 79),
                                                                    (1984, 6171, 79),
                                                                    (1985, 6234, 79),
                                                                    (1986, 6270, 79),
                                                                    (1987, 6279, 79),
                                                                    (1988, 6328, 79),
                                                                    (1989, 6367, 79),
                                                                    (1990, 6410, 79),
                                                                    (1991, 6451, 79),
                                                                    (1992, 6494, 79),
                                                                    (1993, 6554, 79),
                                                                    (1994, 6588, 79),
                                                                    (1995, 6641, 79),
                                                                    (1996, 6673, 79),
                                                                    (1997, 6726, 79),
                                                                    (1998, 6786, 79),
                                                                    (1999, 6838, 79),
                                                                    (2000, 6893, 79),
                                                                    (2001, 6933, 79),
                                                                    (2002, 6997, 79),
                                                                    (2003, 7070, 79),
                                                                    (2004, 7133, 79),
                                                                    (2005, 7185, 79),
                                                                    (2006, 7232, 79),
                                                                    (2007, 7249, 79),
                                                                    (2008, 7325, 79),
                                                                    (2009, 7372, 79),
                                                                    (2010, 7424, 79),
                                                                    (2011, 8046, 79),
                                                                    (2012, 8088, 79),
                                                                    (2013, 8133, 79),
                                                                    (2014, 8174, 79),
                                                                    (2015, 8210, 79),
                                                                    (2016, 8257, 79),
                                                                    (2017, 8305, 79),
                                                                    (2018, 8323, 79),
                                                                    (2019, 8383, 79),
                                                                    (2020, 8471, 79),
                                                                    (2021, 8523, 79),
                                                                    (2022, 8657, 79),
                                                                    (2023, 8707, 79),
                                                                    (2024, 8766, 79),
                                                                    (2025, 8797, 79),
                                                                    (2026, 8815, 79),
                                                                    (2027, 8862, 79),
                                                                    (2028, 8866, 79),
                                                                    (2029, 8942, 79),
                                                                    (2030, 9003, 79),
                                                                    (2031, 9079, 79),
                                                                    (2032, 9121, 79),
                                                                    (2033, 9155, 79),
                                                                    (2034, 9192, 79),
                                                                    (2035, 9260, 79),
                                                                    (2036, 9296, 79),
                                                                    (2037, 9337, 79),
                                                                    (2038, 9370, 79),
                                                                    (2039, 9427, 79),
                                                                    (2040, 9634, 79),
                                                                    (2041, 9642, 79),
                                                                    (2042, 9668, 79),
                                                                    (2043, 9868, 80),
                                                                    (2044, 3599, 109),
                                                                    (2045, 3286, 109),
                                                                    (2046, 3337, 109),
                                                                    (2047, 3349, 109),
                                                                    (2048, 3691, 109),
                                                                    (2049, 3721, 109),
                                                                    (2050, 3738, 109),
                                                                    (2051, 4687, 109),
                                                                    (2052, 9348, 109),
                                                                    (2053, 9594, 109),
                                                                    (2054, 9596, 109),
                                                                    (2055, 2686, 110),
                                                                    (2056, 3572, 110),
                                                                    (2057, 4019, 110),
                                                                    (2058, 4181, 82),
                                                                    (2059, 9079, 82),
                                                                    (2060, 3634, 116),
                                                                    (15921, 2536, 87),
                                                                    (15922, 2376, 87),
                                                                    (15923, 2403, 87),
                                                                    (15924, 2553, 87),
                                                                    (15925, 2555, 87),
                                                                    (15926, 2575, 87),
                                                                    (15927, 2585, 87),
                                                                    (15928, 2664, 87),
                                                                    (15929, 2631, 87),
                                                                    (15930, 2637, 87),
                                                                    (15931, 2707, 87),
                                                                    (15932, 3044, 87),
                                                                    (15933, 2410, 87),
                                                                    (15934, 3052, 87),
                                                                    (15935, 3307, 87),
                                                                    (15936, 3319, 87),
                                                                    (15937, 3371, 87),
                                                                    (15938, 2536, 88),
                                                                    (15939, 2376, 88),
                                                                    (15940, 2548, 88),
                                                                    (15941, 2553, 88),
                                                                    (15942, 2555, 88),
                                                                    (15943, 2585, 88),
                                                                    (15944, 2623, 88),
                                                                    (15945, 2571, 88),
                                                                    (15946, 2579, 88),
                                                                    (15947, 2631, 88),
                                                                    (15948, 2637, 88),
                                                                    (15949, 2660, 88),
                                                                    (15950, 2664, 88),
                                                                    (15951, 2980, 88),
                                                                    (15952, 3042, 88),
                                                                    (15953, 3044, 88),
                                                                    (15954, 3046, 88),
                                                                    (15955, 2410, 88),
                                                                    (15956, 3052, 88),
                                                                    (15957, 3228, 88),
                                                                    (15958, 3230, 88),
                                                                    (15959, 3271, 88),
                                                                    (15960, 3280, 88),
                                                                    (15961, 3283, 88),
                                                                    (15962, 3317, 88),
                                                                    (15963, 3319, 88),
                                                                    (15964, 3369, 88),
                                                                    (15965, 3371, 88),
                                                                    (15966, 2530, 89),
                                                                    (15967, 2376, 89),
                                                                    (15968, 2553, 89),
                                                                    (15969, 2555, 89),
                                                                    (15970, 2664, 89),
                                                                    (15971, 3044, 89),
                                                                    (15972, 3042, 89),
                                                                    (15973, 3228, 89),
                                                                    (15974, 3317, 89),
                                                                    (15975, 3280, 89),
                                                                    (15976, 2376, 90),
                                                                    (15977, 2642, 90),
                                                                    (15978, 2592, 90),
                                                                    (15979, 2664, 90),
                                                                    (15980, 3044, 90),
                                                                    (15981, 3280, 90),
                                                                    (15982, 2376, 98),
                                                                    (15983, 2553, 98),
                                                                    (15984, 2571, 98),
                                                                    (15985, 2664, 98),
                                                                    (15986, 3044, 98),
                                                                    (15987, 3369, 98),
                                                                    (15988, 3280, 98),
                                                                    (15989, 2530, 99),
                                                                    (15990, 2539, 99),
                                                                    (15991, 3226, 99),
                                                                    (15992, 2542, 99),
                                                                    (15993, 2536, 99),
                                                                    (15994, 2533, 99),
                                                                    (15995, 2370, 99),
                                                                    (15996, 2378, 99),
                                                                    (15997, 2383, 99),
                                                                    (15998, 2391, 99),
                                                                    (15999, 2393, 99),
                                                                    (16000, 2397, 99),
                                                                    (16001, 2401, 99),
                                                                    (16002, 2403, 99),
                                                                    (16003, 2376, 99),
                                                                    (16004, 2408, 99),
                                                                    (16005, 2415, 99),
                                                                    (16006, 2463, 99),
                                                                    (16007, 2465, 99),
                                                                    (16008, 2467, 99),
                                                                    (16009, 2546, 99),
                                                                    (16010, 2548, 99),
                                                                    (16011, 2551, 99),
                                                                    (16012, 2553, 99),
                                                                    (16013, 2555, 99),
                                                                    (16014, 2559, 99),
                                                                    (16015, 2561, 99),
                                                                    (16016, 2563, 99),
                                                                    (16017, 2566, 99),
                                                                    (16018, 2568, 99),
                                                                    (16019, 2571, 99),
                                                                    (16020, 2575, 99),
                                                                    (16021, 2577, 99),
                                                                    (16022, 2581, 99),
                                                                    (16023, 2583, 99),
                                                                    (16024, 2585, 99),
                                                                    (16025, 2588, 99),
                                                                    (16026, 2590, 99),
                                                                    (16027, 2592, 99),
                                                                    (16028, 2594, 99),
                                                                    (16029, 2596, 99),
                                                                    (16030, 2599, 99),
                                                                    (16031, 2601, 99),
                                                                    (16032, 2605, 99),
                                                                    (16033, 2607, 99),
                                                                    (16034, 2612, 99),
                                                                    (16035, 2615, 99),
                                                                    (16036, 2617, 99),
                                                                    (16037, 2619, 99),
                                                                    (16038, 2621, 99),
                                                                    (16039, 2623, 99),
                                                                    (16040, 2625, 99),
                                                                    (16041, 2627, 99),
                                                                    (16042, 2631, 99),
                                                                    (16043, 3263, 99),
                                                                    (16044, 2637, 99),
                                                                    (16045, 2639, 99),
                                                                    (16046, 2642, 99),
                                                                    (16047, 2644, 99),
                                                                    (16048, 2647, 99),
                                                                    (16049, 2649, 99),
                                                                    (16050, 2651, 99),
                                                                    (16051, 2653, 99),
                                                                    (16052, 2660, 99),
                                                                    (16053, 2662, 99),
                                                                    (16054, 2664, 99),
                                                                    (16055, 2666, 99),
                                                                    (16056, 2671, 99),
                                                                    (16057, 2674, 99),
                                                                    (16058, 2676, 99),
                                                                    (16059, 2679, 99),
                                                                    (16060, 2681, 99),
                                                                    (16061, 2683, 99),
                                                                    (16062, 2709, 99),
                                                                    (16063, 2980, 99),
                                                                    (16064, 2982, 99),
                                                                    (16065, 2986, 99),
                                                                    (16066, 3005, 99),
                                                                    (16067, 3007, 99),
                                                                    (16068, 3038, 99),
                                                                    (16069, 3040, 99),
                                                                    (16070, 3042, 99),
                                                                    (16071, 3044, 99),
                                                                    (16072, 3048, 99),
                                                                    (16073, 2410, 99),
                                                                    (16074, 3050, 99),
                                                                    (16075, 3052, 99),
                                                                    (16076, 3054, 99),
                                                                    (16077, 3056, 99),
                                                                    (16078, 3228, 99),
                                                                    (16079, 3230, 99),
                                                                    (16080, 3232, 99),
                                                                    (16081, 3256, 99),
                                                                    (16082, 3259, 99),
                                                                    (16083, 3261, 99),
                                                                    (16084, 3269, 99),
                                                                    (16085, 3271, 99),
                                                                    (16086, 3277, 99),
                                                                    (16087, 3280, 99),
                                                                    (16088, 3307, 99),
                                                                    (16089, 3309, 99),
                                                                    (16090, 3311, 99),
                                                                    (16091, 3313, 99),
                                                                    (16092, 3319, 99),
                                                                    (16093, 3359, 99),
                                                                    (16094, 3361, 99),
                                                                    (16095, 3590, 99),
                                                                    (16096, 3369, 99),
                                                                    (16097, 3371, 99),
                                                                    (16098, 3373, 99),
                                                                    (16099, 2536, 100),
                                                                    (16100, 2530, 100),
                                                                    (16101, 2533, 100),
                                                                    (16102, 2370, 100),
                                                                    (16103, 2403, 100),
                                                                    (16104, 2376, 100),
                                                                    (16105, 2413, 100),
                                                                    (16106, 2544, 100),
                                                                    (16107, 2548, 100),
                                                                    (16108, 2553, 100),
                                                                    (16109, 2555, 100),
                                                                    (16110, 2561, 100),
                                                                    (16111, 2571, 100),
                                                                    (16112, 2585, 100),
                                                                    (16113, 2605, 100),
                                                                    (16114, 2615, 100),
                                                                    (16115, 2623, 100),
                                                                    (16116, 2627, 100),
                                                                    (16117, 2637, 100),
                                                                    (16118, 2642, 100),
                                                                    (16119, 2660, 100),
                                                                    (16120, 2662, 100),
                                                                    (16121, 2664, 100),
                                                                    (16122, 2666, 100),
                                                                    (16123, 2683, 100),
                                                                    (16124, 2707, 100),
                                                                    (16125, 2982, 100),
                                                                    (16126, 2631, 100),
                                                                    (16127, 2986, 100),
                                                                    (16128, 3042, 100),
                                                                    (16129, 3044, 100),
                                                                    (16130, 3048, 100),
                                                                    (16131, 2410, 100),
                                                                    (16132, 3052, 100),
                                                                    (16133, 3056, 100),
                                                                    (16134, 3228, 100),
                                                                    (16135, 3230, 100),
                                                                    (16136, 3269, 100),
                                                                    (16137, 3271, 100),
                                                                    (16138, 3361, 100),
                                                                    (16139, 3307, 100),
                                                                    (16140, 3280, 100),
                                                                    (16141, 3309, 100),
                                                                    (16142, 3317, 100),
                                                                    (16143, 3319, 100),
                                                                    (16144, 3369, 100),
                                                                    (16145, 3371, 100),
                                                                    (16146, 2571, 101),
                                                                    (16147, 2533, 101),
                                                                    (16148, 3359, 101),
                                                                    (16149, 3226, 101),
                                                                    (16150, 2542, 101),
                                                                    (16151, 2980, 101),
                                                                    (16152, 2539, 101),
                                                                    (16153, 3590, 101),
                                                                    (16154, 2530, 101),
                                                                    (16155, 2536, 101),
                                                                    (16156, 2370, 101),
                                                                    (16157, 2373, 101),
                                                                    (16158, 2378, 101),
                                                                    (16159, 2383, 101),
                                                                    (16160, 2391, 101),
                                                                    (16161, 2393, 101),
                                                                    (16162, 2397, 101),
                                                                    (16163, 2399, 101),
                                                                    (16164, 2401, 101),
                                                                    (16165, 2403, 101),
                                                                    (16166, 2376, 101),
                                                                    (16167, 2408, 101),
                                                                    (16168, 2415, 101),
                                                                    (16169, 2463, 101),
                                                                    (16170, 2465, 101),
                                                                    (16171, 2467, 101),
                                                                    (16172, 2546, 101),
                                                                    (16173, 2548, 101),
                                                                    (16174, 2551, 101),
                                                                    (16175, 2553, 101),
                                                                    (16176, 2555, 101),
                                                                    (16177, 2559, 101),
                                                                    (16178, 2561, 101),
                                                                    (16179, 2563, 101),
                                                                    (16180, 2566, 101),
                                                                    (16181, 2568, 101),
                                                                    (16182, 2575, 101),
                                                                    (16183, 2581, 101),
                                                                    (16184, 2585, 101),
                                                                    (16185, 2588, 101),
                                                                    (16186, 2590, 101),
                                                                    (16187, 2594, 101),
                                                                    (16188, 2596, 101),
                                                                    (16189, 2599, 101),
                                                                    (16190, 2601, 101),
                                                                    (16191, 2603, 101),
                                                                    (16192, 2605, 101),
                                                                    (16193, 2607, 101),
                                                                    (16194, 2609, 101),
                                                                    (16195, 2612, 101),
                                                                    (16196, 2615, 101),
                                                                    (16197, 2623, 101),
                                                                    (16198, 2625, 101),
                                                                    (16199, 2627, 101),
                                                                    (16200, 2631, 101),
                                                                    (16201, 3269, 101),
                                                                    (16202, 2635, 101),
                                                                    (16203, 2637, 101),
                                                                    (16204, 2639, 101),
                                                                    (16205, 2644, 101),
                                                                    (16206, 2647, 101),
                                                                    (16207, 2649, 101),
                                                                    (16208, 2651, 101),
                                                                    (16209, 2653, 101),
                                                                    (16210, 2660, 101),
                                                                    (16211, 2662, 101),
                                                                    (16212, 2664, 101),
                                                                    (16213, 2666, 101),
                                                                    (16214, 2668, 101),
                                                                    (16215, 2671, 101),
                                                                    (16216, 2674, 101),
                                                                    (16217, 2676, 101),
                                                                    (16218, 2679, 101),
                                                                    (16219, 2681, 101),
                                                                    (16220, 2709, 101),
                                                                    (16221, 2982, 101),
                                                                    (16222, 2986, 101),
                                                                    (16223, 3005, 101),
                                                                    (16224, 3038, 101),
                                                                    (16225, 3040, 101),
                                                                    (16226, 3042, 101),
                                                                    (16227, 3044, 101),
                                                                    (16228, 3048, 101),
                                                                    (16229, 2410, 101),
                                                                    (16230, 3052, 101),
                                                                    (16231, 3054, 101),
                                                                    (16232, 3056, 101),
                                                                    (16233, 3228, 101),
                                                                    (16234, 3230, 101),
                                                                    (16235, 3232, 101),
                                                                    (16236, 3256, 101),
                                                                    (16237, 3259, 101),
                                                                    (16238, 3261, 101),
                                                                    (16239, 3263, 101),
                                                                    (16240, 3271, 101),
                                                                    (16241, 3277, 101),
                                                                    (16242, 3280, 101),
                                                                    (16243, 3307, 101),
                                                                    (16244, 3309, 101),
                                                                    (16245, 3311, 101),
                                                                    (16246, 3313, 101),
                                                                    (16247, 3319, 101),
                                                                    (16248, 3361, 101),
                                                                    (16249, 3369, 101),
                                                                    (16250, 3371, 101),
                                                                    (16251, 3373, 101),
                                                                    (16252, 3226, 102),
                                                                    (16253, 2542, 102),
                                                                    (16254, 2980, 102),
                                                                    (16255, 2536, 102),
                                                                    (16256, 2539, 102),
                                                                    (16257, 2530, 102),
                                                                    (16258, 2533, 102),
                                                                    (16259, 2370, 102),
                                                                    (16260, 2373, 102),
                                                                    (16261, 2378, 102),
                                                                    (16262, 2383, 102),
                                                                    (16263, 2391, 102),
                                                                    (16264, 2393, 102),
                                                                    (16265, 2397, 102),
                                                                    (16266, 2401, 102),
                                                                    (16267, 2403, 102),
                                                                    (16268, 2376, 102),
                                                                    (16269, 2408, 102),
                                                                    (16270, 2415, 102),
                                                                    (16271, 2463, 102),
                                                                    (16272, 2465, 102),
                                                                    (16273, 2467, 102),
                                                                    (16274, 2546, 102),
                                                                    (16275, 2548, 102),
                                                                    (16276, 2551, 102),
                                                                    (16277, 2553, 102),
                                                                    (16278, 2555, 102),
                                                                    (16279, 2571, 102),
                                                                    (16280, 2559, 102),
                                                                    (16281, 2561, 102),
                                                                    (16282, 2563, 102),
                                                                    (16283, 2566, 102),
                                                                    (16284, 2568, 102),
                                                                    (16285, 2575, 102),
                                                                    (16286, 2577, 102),
                                                                    (16287, 2579, 102),
                                                                    (16288, 2581, 102),
                                                                    (16289, 2583, 102),
                                                                    (16290, 2585, 102),
                                                                    (16291, 2588, 102),
                                                                    (16292, 2590, 102),
                                                                    (16293, 2592, 102),
                                                                    (16294, 2594, 102),
                                                                    (16295, 2596, 102),
                                                                    (16296, 2599, 102),
                                                                    (16297, 2601, 102),
                                                                    (16298, 2603, 102),
                                                                    (16299, 2605, 102),
                                                                    (16300, 2607, 102),
                                                                    (16301, 2609, 102),
                                                                    (16302, 2612, 102),
                                                                    (16303, 2615, 102),
                                                                    (16304, 2617, 102),
                                                                    (16305, 2619, 102),
                                                                    (16306, 2623, 102),
                                                                    (16307, 2625, 102),
                                                                    (16308, 2627, 102),
                                                                    (16309, 2631, 102),
                                                                    (16310, 3267, 102),
                                                                    (16311, 3269, 102),
                                                                    (16312, 2635, 102),
                                                                    (16313, 2637, 102),
                                                                    (16314, 2639, 102),
                                                                    (16315, 2642, 102),
                                                                    (16316, 2644, 102),
                                                                    (16317, 2647, 102),
                                                                    (16318, 2649, 102),
                                                                    (16319, 2651, 102),
                                                                    (16320, 2653, 102),
                                                                    (16321, 2660, 102),
                                                                    (16322, 2662, 102),
                                                                    (16323, 2664, 102),
                                                                    (16324, 2666, 102),
                                                                    (16325, 2668, 102),
                                                                    (16326, 2671, 102),
                                                                    (16327, 2674, 102),
                                                                    (16328, 2676, 102),
                                                                    (16329, 2679, 102),
                                                                    (16330, 2681, 102),
                                                                    (16331, 2683, 102),
                                                                    (16332, 2709, 102),
                                                                    (16333, 2982, 102),
                                                                    (16334, 2986, 102),
                                                                    (16335, 3005, 102),
                                                                    (16336, 3038, 102),
                                                                    (16337, 3040, 102),
                                                                    (16338, 3042, 102),
                                                                    (16339, 3044, 102),
                                                                    (16340, 3046, 102),
                                                                    (16341, 3048, 102),
                                                                    (16342, 2410, 102),
                                                                    (16343, 3050, 102),
                                                                    (16344, 3052, 102),
                                                                    (16345, 3054, 102),
                                                                    (16346, 3056, 102),
                                                                    (16347, 3228, 102),
                                                                    (16348, 3230, 102),
                                                                    (16349, 3232, 102),
                                                                    (16350, 3256, 102),
                                                                    (16351, 3259, 102),
                                                                    (16352, 3261, 102),
                                                                    (16353, 3263, 102),
                                                                    (16354, 3271, 102),
                                                                    (16355, 3277, 102),
                                                                    (16356, 3280, 102),
                                                                    (16357, 3283, 102),
                                                                    (16358, 3307, 102),
                                                                    (16359, 3309, 102),
                                                                    (16360, 3311, 102),
                                                                    (16361, 3313, 102),
                                                                    (16362, 3319, 102),
                                                                    (16363, 3359, 102),
                                                                    (16364, 3361, 102),
                                                                    (16365, 3590, 102),
                                                                    (16366, 3369, 102),
                                                                    (16367, 3371, 102),
                                                                    (16368, 3373, 102),
                                                                    (16369, 2536, 103),
                                                                    (16370, 2386, 103),
                                                                    (16371, 2376, 103),
                                                                    (16372, 2546, 103),
                                                                    (16373, 2553, 103),
                                                                    (16374, 2555, 103),
                                                                    (16375, 2559, 103),
                                                                    (16376, 2561, 103),
                                                                    (16377, 2563, 103),
                                                                    (16378, 2568, 103),
                                                                    (16379, 2571, 103),
                                                                    (16380, 2575, 103),
                                                                    (16381, 2585, 103),
                                                                    (16382, 2596, 103),
                                                                    (16383, 2607, 103),
                                                                    (16384, 2612, 103),
                                                                    (16385, 2615, 103),
                                                                    (16386, 2619, 103),
                                                                    (16387, 2625, 103),
                                                                    (16388, 2631, 103),
                                                                    (16389, 2637, 103),
                                                                    (16390, 2639, 103),
                                                                    (16391, 2644, 103),
                                                                    (16392, 2647, 103),
                                                                    (16393, 2649, 103),
                                                                    (16394, 2651, 103),
                                                                    (16395, 2660, 103),
                                                                    (16396, 2662, 103),
                                                                    (16397, 2664, 103),
                                                                    (16398, 2666, 103),
                                                                    (16399, 2668, 103),
                                                                    (16400, 2679, 103),
                                                                    (16401, 2681, 103),
                                                                    (16402, 2980, 103),
                                                                    (16403, 2982, 103),
                                                                    (16404, 2986, 103),
                                                                    (16405, 3005, 103),
                                                                    (16406, 3038, 103),
                                                                    (16407, 3040, 103),
                                                                    (16408, 3042, 103),
                                                                    (16409, 3044, 103),
                                                                    (16410, 3046, 103),
                                                                    (16411, 3048, 103),
                                                                    (16412, 2533, 103),
                                                                    (16413, 2410, 103),
                                                                    (16414, 3052, 103),
                                                                    (16415, 3054, 103),
                                                                    (16416, 3056, 103),
                                                                    (16417, 3228, 103),
                                                                    (16418, 3256, 103),
                                                                    (16419, 3259, 103),
                                                                    (16420, 3269, 103),
                                                                    (16421, 3271, 103),
                                                                    (16422, 3277, 103),
                                                                    (16423, 3280, 103),
                                                                    (16424, 3307, 103),
                                                                    (16425, 3309, 103),
                                                                    (16426, 3311, 103),
                                                                    (16427, 3313, 103),
                                                                    (16428, 3319, 103),
                                                                    (16429, 3359, 103),
                                                                    (16430, 3590, 103),
                                                                    (16431, 3263, 103),
                                                                    (16432, 3369, 103),
                                                                    (16433, 3371, 103),
                                                                    (16434, 2536, 104),
                                                                    (16435, 2403, 104),
                                                                    (16436, 2376, 104),
                                                                    (16437, 2546, 104),
                                                                    (16438, 2553, 104),
                                                                    (16439, 2555, 104),
                                                                    (16440, 2575, 104),
                                                                    (16441, 2581, 104),
                                                                    (16442, 2585, 104),
                                                                    (16443, 2588, 104),
                                                                    (16444, 2601, 104),
                                                                    (16445, 2615, 104),
                                                                    (16446, 2619, 104),
                                                                    (16447, 2623, 104),
                                                                    (16448, 2559, 104),
                                                                    (16449, 2627, 104),
                                                                    (16450, 2631, 104),
                                                                    (16451, 3267, 104),
                                                                    (16452, 2637, 104),
                                                                    (16453, 2639, 104),
                                                                    (16454, 2660, 104),
                                                                    (16455, 2599, 104),
                                                                    (16456, 2664, 104),
                                                                    (16457, 2666, 104),
                                                                    (16458, 2671, 104),
                                                                    (16459, 2674, 104),
                                                                    (16460, 2683, 104),
                                                                    (16461, 2709, 104),
                                                                    (16462, 2982, 104),
                                                                    (16463, 2986, 104),
                                                                    (16464, 3038, 104),
                                                                    (16465, 3044, 104),
                                                                    (16466, 2410, 104),
                                                                    (16467, 3052, 104),
                                                                    (16468, 3054, 104),
                                                                    (16469, 3056, 104),
                                                                    (16470, 3228, 104),
                                                                    (16471, 3230, 104),
                                                                    (16472, 3269, 104),
                                                                    (16473, 3271, 104),
                                                                    (16474, 3307, 104),
                                                                    (16475, 3309, 104),
                                                                    (16476, 3369, 104),
                                                                    (16477, 3371, 104),
                                                                    (16478, 2539, 105),
                                                                    (16479, 3226, 105),
                                                                    (16480, 2536, 105),
                                                                    (16481, 2530, 105),
                                                                    (16482, 2403, 105),
                                                                    (16483, 2376, 105),
                                                                    (16484, 2465, 105),
                                                                    (16485, 2546, 105),
                                                                    (16486, 2553, 105),
                                                                    (16487, 2555, 105),
                                                                    (16488, 2561, 105),
                                                                    (16489, 2566, 105),
                                                                    (16490, 2571, 105),
                                                                    (16491, 2575, 105),
                                                                    (16492, 2583, 105),
                                                                    (16493, 2585, 105),
                                                                    (16494, 2603, 105),
                                                                    (16495, 2607, 105),
                                                                    (16496, 2615, 105),
                                                                    (16497, 2619, 105),
                                                                    (16498, 2559, 105),
                                                                    (16499, 2631, 105),
                                                                    (16500, 3263, 105),
                                                                    (16501, 2637, 105),
                                                                    (16502, 2644, 105),
                                                                    (16503, 2647, 105),
                                                                    (16504, 2662, 105),
                                                                    (16505, 2664, 105),
                                                                    (16506, 2668, 105),
                                                                    (16507, 2681, 105),
                                                                    (16508, 2707, 105),
                                                                    (16509, 2980, 105),
                                                                    (16510, 2982, 105),
                                                                    (16511, 2986, 105),
                                                                    (16512, 3005, 105),
                                                                    (16513, 3040, 105),
                                                                    (16514, 3042, 105),
                                                                    (16515, 3044, 105),
                                                                    (16516, 3048, 105),
                                                                    (16517, 2410, 105),
                                                                    (16518, 3052, 105),
                                                                    (16519, 3056, 105),
                                                                    (16520, 3228, 105),
                                                                    (16521, 3230, 105),
                                                                    (16522, 3232, 105),
                                                                    (16523, 3256, 105),
                                                                    (16524, 3259, 105),
                                                                    (16525, 3261, 105),
                                                                    (16526, 3269, 105),
                                                                    (16527, 3271, 105),
                                                                    (16528, 3277, 105),
                                                                    (16529, 3280, 105),
                                                                    (16530, 3307, 105),
                                                                    (16531, 3309, 105),
                                                                    (16532, 3311, 105),
                                                                    (16533, 3313, 105),
                                                                    (16534, 3319, 105),
                                                                    (16535, 3359, 105),
                                                                    (16536, 3361, 105),
                                                                    (16537, 3590, 105),
                                                                    (16538, 3369, 105),
                                                                    (16539, 3371, 105),
                                                                    (16540, 3373, 105),
                                                                    (16541, 2536, 106),
                                                                    (16542, 2539, 106),
                                                                    (16543, 2530, 106),
                                                                    (16544, 2393, 106),
                                                                    (16545, 2376, 106),
                                                                    (16546, 2415, 106),
                                                                    (16547, 2553, 106),
                                                                    (16548, 2555, 106),
                                                                    (16549, 2561, 106),
                                                                    (16550, 2571, 106),
                                                                    (16551, 2575, 106),
                                                                    (16552, 2577, 106),
                                                                    (16553, 2585, 106),
                                                                    (16554, 2599, 106),
                                                                    (16555, 2605, 106),
                                                                    (16556, 2615, 106),
                                                                    (16557, 2619, 106),
                                                                    (16558, 2621, 106),
                                                                    (16559, 2623, 106),
                                                                    (16560, 2581, 106),
                                                                    (16561, 2563, 106),
                                                                    (16562, 2631, 106),
                                                                    (16563, 2637, 106),
                                                                    (16564, 2639, 106),
                                                                    (16565, 2642, 106),
                                                                    (16566, 2660, 106),
                                                                    (16567, 2662, 106),
                                                                    (16568, 2592, 106),
                                                                    (16569, 2617, 106),
                                                                    (16570, 2664, 106),
                                                                    (16571, 2668, 106),
                                                                    (16572, 2671, 106),
                                                                    (16573, 2674, 106),
                                                                    (16574, 2707, 106),
                                                                    (16575, 2709, 106),
                                                                    (16576, 2982, 106),
                                                                    (16577, 2986, 106),
                                                                    (16578, 3042, 106),
                                                                    (16579, 3044, 106),
                                                                    (16580, 3048, 106),
                                                                    (16581, 2410, 106),
                                                                    (16582, 3052, 106),
                                                                    (16583, 3056, 106),
                                                                    (16584, 3228, 106),
                                                                    (16585, 3230, 106),
                                                                    (16586, 3269, 106),
                                                                    (16587, 3307, 106),
                                                                    (16588, 3309, 106),
                                                                    (16589, 3311, 106),
                                                                    (16590, 3317, 106),
                                                                    (16591, 3369, 106),
                                                                    (16592, 2544, 107),
                                                                    (16593, 2615, 107),
                                                                    (16594, 2664, 107),
                                                                    (16595, 2986, 107),
                                                                    (16596, 2410, 107),
                                                                    (16597, 3228, 107),
                                                                    (16598, 3319, 107),
                                                                    (16599, 3359, 107),
                                                                    (16600, 3307, 107),
                                                                    (16601, 3371, 107),
                                                                    (16602, 2376, 108),
                                                                    (16603, 2553, 108),
                                                                    (16604, 2592, 108),
                                                                    (16605, 2664, 108),
                                                                    (16606, 2642, 108),
                                                                    (16607, 3007, 108),
                                                                    (16608, 3048, 108),
                                                                    (16609, 3044, 108),
                                                                    (16610, 3042, 108),
                                                                    (16611, 3230, 108),
                                                                    (16612, 3228, 108),
                                                                    (16613, 3280, 108),
                                                                    (16614, 3317, 108),
                                                                    (16616, 7547, 321),
                                                                    (16617, 8178, 321),
                                                                    (16618, 15326, 8442),
                                                                    (16619, 9940, 622);

-- --------------------------------------------------------

--
-- Структура таблицы `password_histories`
--

CREATE TABLE `password_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `password_histories`
--

INSERT INTO `password_histories` (`id`, `user_id`, `password`, `created_at`, `updated_at`) VALUES
                                                                                                  (1, 1, '$2y$10$pQ20QHcda1jkFO/F2EUGRuZBosyIgwjJWvr/06gL9lf2mTCw505Ju', '2018-09-12 13:03:29', '2018-09-12 13:03:29'),
                                                                                                  (2, 2, '$2y$10$eiGYjaVs.oUammjQ6s7w7eXAF4YEmPfRF7YIVokWIXmM1Rtno6BRK', '2018-09-12 13:03:29', '2018-09-12 13:03:29'),
                                                                                                  (3, 3, '$2y$10$GJ4.ZuTzuF.pUtpbP22gxOEUrrq6JVLnxHV/AyRkzfu5OJxZg9oJK', '2018-09-12 13:03:29', '2018-09-12 13:03:29'),
                                                                                                  (4, 4, '$2y$10$cKsMOjnhwdR8cKwrQ49ZgOFcQpb/oftjaZgE8qBb2QXHMSBVmBQJK', '2018-09-12 13:03:29', '2018-09-12 13:03:29');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
                                                                                          (1, 'view backend', 'web', '2018-09-12 13:03:29', '2018-09-12 13:03:29'),
                                                                                          (2, 'create', 'web', '2018-09-12 13:03:29', '2018-09-12 13:03:29'),
                                                                                          (3, 'update', 'web', '2018-09-12 13:03:29', '2018-09-12 13:03:29'),
                                                                                          (4, 'read', 'web', '2018-09-12 13:03:29', '2018-09-12 13:03:29'),
                                                                                          (5, 'delete', 'web', '2018-09-12 13:03:29', '2018-09-12 13:03:29'),
                                                                                          (6, 'publish', 'web', '2018-09-12 13:03:29', '2018-09-12 13:03:29');

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
                                                                                    (1, 'administrator', 'web', '2018-09-12 13:03:29', '2018-09-12 13:03:29'),
                                                                                    (2, 'journalist', 'web', '2018-09-12 13:03:29', '2018-09-12 13:03:29'),
                                                                                    (3, 'moderator', 'web', '2018-09-12 13:03:29', '2018-09-12 13:03:29'),
                                                                                    (4, 'guest', 'web', '2018-09-12 13:03:29', '2018-09-12 13:03:29');

-- --------------------------------------------------------

--
-- Структура таблицы `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
                                                                       (1, 1),
                                                                       (2, 1),
                                                                       (3, 1),
                                                                       (4, 1),
                                                                       (5, 1),
                                                                       (6, 1),
                                                                       (1, 2),
                                                                       (2, 2),
                                                                       (4, 2),
                                                                       (1, 3),
                                                                       (2, 3),
                                                                       (3, 3),
                                                                       (4, 3),
                                                                       (5, 3),
                                                                       (6, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('BOOLEAN','NUMBER','DATE','TEXT','SELECT','FILE','TEXTAREA') COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `hidden` tinyint(1) NOT NULL,
  `lang` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `code`, `type`, `label`, `value`, `hidden`, `lang`, `lang_hash`, `created_at`, `updated_at`) VALUES
                                                                                                                                  (15, 'homeurl_en', 'TEXT', 'homeUrl', 'home', 1, 'en', 'QlDkUJVAtSgrhcimRvysMrmYyw9thRKhMKjS5W8p', '2018-09-05 15:44:47', '2018-09-09 11:53:02'),
                                                                                                                                  (16, 'homeurl_ru', 'TEXT', 'homeUrl-ru', 'bystraya-i-nadezhnaya-dostavka-iz-internet-magazinov-ssha-do-vashego-doma', 1, 'ru', 'QlDkUJVAtSgrhcimRvysMrmYyw9thRKhMKjS5W8p', '2018-09-05 15:45:37', '2018-09-24 12:21:04'),
                                                                                                                                  (18, 'contactus_en', 'TEXTAREA', 'ContactUs', '<div class=\"footer_content_position\">\r\n                        <h4 class=\"__title\">\r\n                            Contact Us\r\n                        </h4>\r\n                        <p class=\"__tel-title\">\r\n                            Tel. in USA:\r\n                        </p>\r\n                        <p class=\"__tel\">\r\n                            <a href=\"tel:+1 (732) 893-7447\" class=\"__link\">\r\n                                +1 (732) 893-7447\r\n                            </a>\r\n                        </p>\r\n                        <p class=\"__address-title\">\r\n                            Warehouse address\r\n                        </p>\r\n                        <address class=\"__address\">\r\n                            310 Vraen Ave, Wyckoff, NJ 07481\r\n                        </address>\r\n                    </div>', 1, 'en', 'TuKSda8dlSwB6ZcGbqWLzwmXBm5EF2pOXg1YMUNB', '2018-09-06 05:35:33', '2018-09-06 05:46:27'),
                                                                                                                                  (19, 'contactus_ru', 'TEXTAREA', 'ContactUs', '<div class=\"footer_content_position\">\r\n<h4 class=\"__title\">Свяжитесь с нами</h4>\r\n\r\n<p class=\"__tel-title\">Номер телефона:</p>\r\n\r\n<p class=\"__tel\"><a class=\"__link\" href=\"tel:+1 (732) 893-7447\">+1 (732) 893-7447 </a></p>\r\n\r\n<p class=\"__address-title\">Адрес склада</p>\r\n\r\n<address class=\"__address\">310 Vraen Ave, Wyckoff, NJ 07481</address>\r\n</div>', 1, 'ru', 'TuKSda8dlSwB6ZcGbqWLzwmXBm5EF2pOXg1YMUNB', '2018-09-06 05:46:48', '2018-09-26 05:12:13');

-- --------------------------------------------------------

--
-- Структура таблицы `social_accounts`
--

CREATE TABLE `social_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'gravatar',
  `avatar_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_changed_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '1',
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'UTC',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `uuid`, `first_name`, `last_name`, `email`, `avatar_type`, `avatar_location`, `password`, `password_changed_at`, `active`, `confirmation_code`, `confirmed`, `timezone`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
                                                                                                                                                                                                                                                                       (1, '4ae63e5e-841c-4d49-8150-eeb52f4aaafa', 'Admin', 'Istrator', 'admin@admin.com', 'gravatar', NULL, '$2y$10$pQ20QHcda1jkFO/F2EUGRuZBosyIgwjJWvr/06gL9lf2mTCw505Ju', NULL, 1, 'bea2e8b263c97d888230e45207b3d590', 1, 'UTC', 'qgwld1AVicfwEKuERs5PfRIQuCTMesItycaioMA9E4p8TzBgdkRqAFMLWLUg', '2018-09-12 13:03:29', '2018-09-12 13:03:29', NULL),
                                                                                                                                                                                                                                                                       (2, '8549907f-2549-4445-90f5-96c0cb3f8876', 'Clark', 'Kent', 'journalist@journalist.com', 'gravatar', NULL, '$2y$10$eiGYjaVs.oUammjQ6s7w7eXAF4YEmPfRF7YIVokWIXmM1Rtno6BRK', NULL, 1, '0bb40e4f3f4518e3c31c48267eba6783', 1, 'UTC', 'kpLTZZWnPRORE4qyVqnxsbSP5f9UPkvRncbr1Yue9FYmZEvzLR3XLuU2K7wM', '2018-09-12 13:03:29', '2018-09-12 13:03:29', NULL),
                                                                                                                                                                                                                                                                       (3, '06772948-a490-4d01-81e4-774622f86fc4', 'Moderator', 'moder', 'moder@moder.com', 'gravatar', NULL, '$2y$10$GJ4.ZuTzuF.pUtpbP22gxOEUrrq6JVLnxHV/AyRkzfu5OJxZg9oJK', NULL, 1, 'c1adc1010a5c73c2ef56f967ff158a4c', 1, 'UTC', '44EyyWbjuzp4UPM1QuekhGQ6phtmUEeMQ1ZnuRYYwyUbSWDE1FOUSnSP2bub', '2018-09-12 13:03:29', '2018-09-12 13:03:29', NULL),
                                                                                                                                                                                                                                                                       (4, 'a4d582a3-8a5d-422f-abe3-202ef9ae77e2', 'Guest', 'guest', 'guest@guest.com', 'gravatar', NULL, '$2y$10$cKsMOjnhwdR8cKwrQ49ZgOFcQpb/oftjaZgE8qBb2QXHMSBVmBQJK', NULL, 1, 'dd3fa12bfb3b77b8be11cee3d39dbb1e', 1, 'UTC', NULL, '2018-09-12 13:03:29', '2018-09-12 13:03:29', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `widgets`
--

CREATE TABLE `widgets` (
  `widget_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `type` int(11) DEFAULT NULL,
  `lang` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_hash` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `widgets`
--

INSERT INTO `widgets` (`widget_id`, `title`, `description`, `image`, `type`, `lang`, `lang_hash`, `created_at`, `updated_at`) VALUES
                                                                                                                                     (1, 'Методы оплаты', 'BoxForward обеспечивает максимальную гибкость, безопасность и удобство, когда дело доходит до вариантов оплаты', NULL, 3, 'ru', '6U0tLwdxVsdM5Q1KcOdHq9eOdnOj4c04bNtILUB3', '2018-09-23 08:30:34', '2018-09-24 10:10:00'),
                                                                                                                                     (5, 'Мы предлагаем лучшие цены и внимательное обслуживание клиентов!', 'Рассчитать стоимость обслуживания и доставки', NULL, 8, 'ru', 'osZxQHoT7qbJGoUMjDHY1R6Et0bAMYQUKOPo8Ogq', '2018-09-24 10:15:05', '2018-09-24 10:15:05'),
                                                                                                                                     (6, '6 причин, почему это ЛЕГКИЕ, КОНВЕННЫЕ и FASTto магазин 6 причин, почему это ЛЕГКО, CONVINENT и FASTto магазин с BOXFORWARD:', 'With our service you save at every step of the delivery process - from on-line purchase to delivery on your doorstep', NULL, 7, 'ru', 'jMs1tBpnp0X0S7YGpVzGFgZSsI40JNHIhnwEtJZa', '2018-09-24 12:09:03', '2018-09-24 12:09:03'),
                                                                                                                                     (7, 'Отзыви ListView', NULL, NULL, 11, 'ru', 'hcPZULO5EUNfy72prHZ6r4odQ3ys3MHndUyZgY0O', '2018-09-24 12:13:40', '2018-09-24 12:13:40'),
                                                                                                                                     (8, 'Новости', 'Новости', NULL, 12, 'ru', '6lMhxjuV3w3SoeQudlXnakzL3kyeRfke5t9gtj8j', '2018-09-24 12:14:29', '2018-09-24 12:14:29'),
                                                                                                                                     (9, 'Сервисные информатции', NULL, NULL, 9, 'ru', 'Aa2VAw5ByhE1XnWoDmt0roU9dPp0GzsJqHz6gmky', '2018-09-24 12:16:12', '2018-09-24 12:16:12'),
                                                                                                                                     (10, 'О нас', 'Веб-сайт BoxForward управляется Shop and Ship LLC, американским провайдером личных покупок, хранения и доставки услуг для международных клиентов.  Shop and Ship LLC была основана в 2010 году и с тех пор постоянно растет, предлагая своим клиентам непревзойденный уровень персонализированных услуг и логистических решений.  Мы всегда открыты для вас вопросами и отзывами, поэтому, пожалуйста, не стесняйтесь обращаться к нам с вашими вопросами и предложениями, заполнив нашу контактную форму на веб-сайте.', NULL, 10, 'ru', 'BSLXHmuJQoXUl0upOIeKkAA1RLRFpTG10IBkVC4u', '2018-09-24 12:17:03', '2018-09-24 12:17:03'),
                                                                                                                                     (11, 'Отзыви GridView', NULL, NULL, 5, 'ru', 'slKZLxnuj6hiaWg5GlMpUbn5atz7fQQcTW2N2nMb', '2018-09-24 12:33:29', '2018-09-24 12:33:29'),
                                                                                                                                     (12, 'Этап покупки', NULL, NULL, 2, 'ru', 'LXsmXb9YiMUdIgeHTYV68nAZrjqAbyKQMZcJXGFq', '2018-09-24 12:38:14', '2018-09-24 12:38:14'),
                                                                                                                                     (17, 'Новости', NULL, NULL, 12, 'en', '1ROxEl38w5nrhlxH7DUyMy4BaQD8BfleLtGMxdj6', '2018-09-24 18:01:40', '2018-09-24 18:01:40'),
                                                                                                                                     (18, 'Этап покупки', NULL, NULL, 2, 'en', 'HYiCZXRikX1iIBkIRmQWvpIXUsRuauhAjTtMseKg', '2018-09-25 18:51:43', '2018-09-25 18:51:43'),
                                                                                                                                     (19, 'Новости', NULL, NULL, 12, 'en', '81akmA1YrzN2uYyTBClkL5NGFfE5lZ9Pgu7ON31f', '2018-09-25 18:57:48', '2018-09-25 18:57:48'),
                                                                                                                                     (20, 'Новости', NULL, NULL, 12, 'ru', 'z0oRxxae1U7TSD9UE080G17aE4faOf9ki4GANOtx', '2018-09-25 19:00:15', '2018-09-25 19:00:15'),
                                                                                                                                     (21, 'Информатции для шаблона Главная', NULL, NULL, 6, 'ru', 'FTvCBmM1LHItvryEwW3l7IqqF9ApKW96d41Roiei', '2018-09-26 03:37:29', '2018-09-26 03:37:29');

-- --------------------------------------------------------

--
-- Структура таблицы `widget_categories`
--

CREATE TABLE `widget_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `widget_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `widget_categories`
--

INSERT INTO `widget_categories` (`id`, `widget_id`, `category_id`, `count`, `sort`) VALUES
                                                                                           (11, 17, 621, 32, 1),
                                                                                           (15, 19, 621, 222, 1),
                                                                                           (16, 19, 621, 13, 2),
                                                                                           (17, 19, 621, 12, 3),
                                                                                           (18, 19, 621, 32, 4),
                                                                                           (40, 20, 13, 35, 1),
                                                                                           (41, 20, 14, 435, 2),
                                                                                           (42, 20, 36, 45, 3),
                                                                                           (43, 20, 38, 234, 4),
                                                                                           (44, 20, 15, 42, 5),
                                                                                           (45, 20, 25, 23, 6);

-- --------------------------------------------------------

--
-- Структура таблицы `widget_notifications`
--

CREATE TABLE `widget_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `widget_id` int(10) UNSIGNED NOT NULL,
  `notification_id` int(10) UNSIGNED NOT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `widget_notifications`
--

INSERT INTO `widget_notifications` (`id`, `widget_id`, `notification_id`, `sort`) VALUES
                                                                                         (31, 1, 1, 1),
                                                                                         (32, 1, 6, 2),
                                                                                         (33, 1, 7, 3),
                                                                                         (34, 5, 8, 1),
                                                                                         (35, 5, 9, 2),
                                                                                         (36, 5, 10, 3),
                                                                                         (37, 5, 11, 4),
                                                                                         (38, 5, 12, 5),
                                                                                         (39, 5, 13, 6),
                                                                                         (40, 5, 14, 7),
                                                                                         (41, 5, 15, 8),
                                                                                         (42, 5, 16, 9),
                                                                                         (43, 6, 17, 1),
                                                                                         (44, 6, 18, 2),
                                                                                         (45, 6, 19, 3),
                                                                                         (46, 6, 20, 4),
                                                                                         (47, 6, 21, 5),
                                                                                         (48, 6, 22, 6),
                                                                                         (49, 7, 23, 1),
                                                                                         (50, 7, 24, 2),
                                                                                         (51, 7, 25, 3),
                                                                                         (52, 9, 26, 1),
                                                                                         (53, 9, 27, 2),
                                                                                         (54, 9, 28, 3),
                                                                                         (55, 10, 29, 1),
                                                                                         (56, 11, 30, 1),
                                                                                         (57, 12, 31, 1),
                                                                                         (58, 12, 32, 2),
                                                                                         (59, 12, 33, 3),
                                                                                         (60, 12, 34, 4),
                                                                                         (63, 18, 35, 1),
                                                                                         (64, 18, 36, 2),
                                                                                         (65, 18, 37, 3),
                                                                                         (67, 21, 38, 1),
                                                                                         (68, 21, 39, 2),
                                                                                         (69, 21, 40, 3),
                                                                                         (70, 21, 41, 4);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `faq_category`
--
ALTER TABLE `faq_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `faq_category_faq_id_foreign` (`faq_id`),
  ADD KEY `faq_category_category_id_foreign` (`category_id`);

--
-- Индексы таблицы `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menu_page`
--
ALTER TABLE `menu_page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_page_page_id_foreign` (`page_id`),
  ADD KEY `menu_page_menu_id_foreign` (`menu_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Индексы таблицы `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Индексы таблицы `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notification_id`);

--
-- Индексы таблицы `notification_children`
--
ALTER TABLE `notification_children`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_children_parent_id_foreign` (`parent_id`),
  ADD KEY `notification_children_notification_id_foreign` (`notification_id`);

--
-- Индексы таблицы `notification_images`
--
ALTER TABLE `notification_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_images_notification_id_foreign` (`notification_id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages_widgets`
--
ALTER TABLE `pages_widgets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pages_widgets_page_id_foreign` (`page_id`),
  ADD KEY `pages_widgets_widget_id_foreign` (`widget_id`);

--
-- Индексы таблицы `page_category`
--
ALTER TABLE `page_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_category_page_id_foreign` (`page_id`),
  ADD KEY `page_category_category_id_foreign` (`category_id`);

--
-- Индексы таблицы `password_histories`
--
ALTER TABLE `password_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_histories_user_id_foreign` (`user_id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_code_unique` (`code`);

--
-- Индексы таблицы `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_accounts_user_id_foreign` (`user_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Индексы таблицы `widgets`
--
ALTER TABLE `widgets`
  ADD PRIMARY KEY (`widget_id`);

--
-- Индексы таблицы `widget_categories`
--
ALTER TABLE `widget_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `widget_categories_widget_id_foreign` (`widget_id`),
  ADD KEY `widget_categories_category_id_foreign` (`category_id`);

--
-- Индексы таблицы `widget_notifications`
--
ALTER TABLE `widget_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `widget_notifications_widget_id_foreign` (`widget_id`),
  ADD KEY `widget_notifications_notification_id_foreign` (`notification_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=623;

--
-- AUTO_INCREMENT для таблицы `faq_category`
--
ALTER TABLE `faq_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT для таблицы `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;

--
-- AUTO_INCREMENT для таблицы `menu_page`
--
ALTER TABLE `menu_page`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `notifications`
--
ALTER TABLE `notifications`
  MODIFY `notification_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT для таблицы `notification_children`
--
ALTER TABLE `notification_children`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `notification_images`
--
ALTER TABLE `notification_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9941;

--
-- AUTO_INCREMENT для таблицы `pages_widgets`
--
ALTER TABLE `pages_widgets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT для таблицы `page_category`
--
ALTER TABLE `page_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16620;

--
-- AUTO_INCREMENT для таблицы `password_histories`
--
ALTER TABLE `password_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `social_accounts`
--
ALTER TABLE `social_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `widgets`
--
ALTER TABLE `widgets`
  MODIFY `widget_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT для таблицы `widget_categories`
--
ALTER TABLE `widget_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT для таблицы `widget_notifications`
--
ALTER TABLE `widget_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `faq_category`
--
ALTER TABLE `faq_category`
  ADD CONSTRAINT `faq_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `faq_category_faq_id_foreign` FOREIGN KEY (`faq_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `menu_page`
--
ALTER TABLE `menu_page`
  ADD CONSTRAINT `menu_page_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `menu_page_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `notification_children`
--
ALTER TABLE `notification_children`
  ADD CONSTRAINT `notification_children_notification_id_foreign` FOREIGN KEY (`notification_id`) REFERENCES `notifications` (`notification_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notification_children_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `notifications` (`notification_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `notification_images`
--
ALTER TABLE `notification_images`
  ADD CONSTRAINT `notification_images_notification_id_foreign` FOREIGN KEY (`notification_id`) REFERENCES `notifications` (`notification_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `pages_widgets`
--
ALTER TABLE `pages_widgets`
  ADD CONSTRAINT `pages_widgets_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `pages_widgets_widget_id_foreign` FOREIGN KEY (`widget_id`) REFERENCES `widgets` (`widget_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `password_histories`
--
ALTER TABLE `password_histories`
  ADD CONSTRAINT `password_histories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD CONSTRAINT `social_accounts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `widget_categories`
--
ALTER TABLE `widget_categories`
  ADD CONSTRAINT `widget_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `widget_categories_widget_id_foreign` FOREIGN KEY (`widget_id`) REFERENCES `widgets` (`widget_id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `widget_notifications`
--
ALTER TABLE `widget_notifications`
  ADD CONSTRAINT `widget_notifications_notification_id_foreign` FOREIGN KEY (`notification_id`) REFERENCES `notifications` (`notification_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `widget_notifications_widget_id_foreign` FOREIGN KEY (`widget_id`) REFERENCES `widgets` (`widget_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
