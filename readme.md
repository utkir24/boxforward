## Installation

Clone project from a remote CVS service;

Copy:

`docker-compose.example.yml` to `docker-compose.example.yml`
`.enc.example` to `.enc` 

Adjust all the settings you need (database credentials, etc.)

Then just type in terminal (standing on a project root):

`docker-compose up -d`

`docker-compose exec php-fpm composer install`

`docker-compose exec php-fpm php key:generate`
